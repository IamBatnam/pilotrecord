#ifndef PROCESS_APP_H
#define PROCESS_APP_H

#ifdef _MSC_VER
#pragma once
#include <Windows.h>
#endif



#include <string>

class Process
{
public:
	Process(std::string strAppName);
	~Process();

	bool FindApplicationOpen();
	void KillTheClient();
private:
	DWORD dPID;

	std::string mStrAppName;
};




#endif // !PROCESS_APP_H


