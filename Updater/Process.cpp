#include "Process.h"

#include <Tlhelp32.h>
#include <qstring.h>


Process::Process(std::string strAppName)
	: mStrAppName(strAppName)
{
	
}


Process::~Process()
{
}

bool Process::FindApplicationOpen()
{
	bool bResult = false;

	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);

	BOOL hRes;
	if(hRes = Process32First(hSnapShot, &pEntry))
	do
	{
		if (QString::fromWCharArray(pEntry.szExeFile).compare(mStrAppName.c_str(), Qt::CaseInsensitive) == 0)
		{
			bResult = true;
			break;
		}
	} while (hRes = Process32Next(hSnapShot, &pEntry));

	CloseHandle(hSnapShot);

	return bResult;
}

void Process::KillTheClient()
{
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, NULL);
	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);
	BOOL hRes = Process32First(hSnapShot, &pEntry);
	while (hRes)
	{
		
		if(QString::fromWCharArray(pEntry.szExeFile).compare(mStrAppName.c_str(), Qt::CaseInsensitive) == 0)
		{
			HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0,
				(DWORD)pEntry.th32ProcessID);
			if (hProcess != NULL)
			{
				TerminateProcess(hProcess, 9);
				CloseHandle(hProcess);
			}
		}
		hRes = Process32Next(hSnapShot, &pEntry);
	}
	CloseHandle(hSnapShot);
}
