/********************************************************************************
** Form generated from reading UI file 'Updater.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UPDATER_H
#define UI_UPDATER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UpdaterClass
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *statusLabel;
    QProgressBar *progressBar;
    QHBoxLayout *horizontalLayout;
    QLabel *progLabel;

    void setupUi(QMainWindow *UpdaterClass)
    {
        if (UpdaterClass->objectName().isEmpty())
            UpdaterClass->setObjectName(QStringLiteral("UpdaterClass"));
        UpdaterClass->resize(367, 103);
        UpdaterClass->setMinimumSize(QSize(367, 103));
        UpdaterClass->setMaximumSize(QSize(367, 103));
        centralWidget = new QWidget(UpdaterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        statusLabel->setMinimumSize(QSize(0, 0));
        statusLabel->setWordWrap(true);

        horizontalLayout_2->addWidget(statusLabel);


        verticalLayout->addLayout(horizontalLayout_2);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setMaximum(0);
        progressBar->setValue(-1);
        progressBar->setTextVisible(false);

        verticalLayout->addWidget(progressBar);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        progLabel = new QLabel(centralWidget);
        progLabel->setObjectName(QStringLiteral("progLabel"));
        progLabel->setMinimumSize(QSize(347, 18));
        progLabel->setMaximumSize(QSize(347, 18));

        horizontalLayout->addWidget(progLabel);


        verticalLayout->addLayout(horizontalLayout);

        UpdaterClass->setCentralWidget(centralWidget);

        retranslateUi(UpdaterClass);

        QMetaObject::connectSlotsByName(UpdaterClass);
    } // setupUi

    void retranslateUi(QMainWindow *UpdaterClass)
    {
        UpdaterClass->setWindowTitle(QApplication::translate("UpdaterClass", "Updater", Q_NULLPTR));
        statusLabel->setText(QApplication::translate("UpdaterClass", "Initializing...", Q_NULLPTR));
        progLabel->setText(QApplication::translate("UpdaterClass", "Download", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class UpdaterClass: public Ui_UpdaterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UPDATER_H
