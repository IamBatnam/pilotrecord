#include "Updater.h"

#include <QMessageBox>
#include <QtConcurrent>
#include <miniz.h>
#include <QDir>
#include <QFile>
#include <fstream>
#include "Process.h"

QStringList bytesFormat = { "B", "KB", "MB", "GB", "TB" };

Updater::Updater(QWidget *parent)
	: QMainWindow(parent)
	, updateTimer(nullptr)
	, pCurl(nullptr)
	, stage(Updater::GETINFO_STAGE)
	, pZipFile(nullptr)
	, downloadSize(0)
{
	ui.setupUi(this);
	setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);

	updateTimer = new QTimer(this);
	connect(updateTimer, SIGNAL(timeout()), this, SLOT(UpdateLoop()));
	updateTimer->start(100);

	curl_global_init(CURL_GLOBAL_DEFAULT);
	pCurl = curl_easy_init();

}

Updater::~Updater()
{
	if (updateTimer)
	{
		updateTimer->stop();
		delete updateTimer;
	}

	curl_easy_cleanup(pCurl);
}

int Updater::WriteFunction(char * pData, size_t size, size_t nmemb, void * pointer)
{
	if (pointer == nullptr)
		return 0;

	auto objPtr = (Updater*)pointer;
	if (objPtr->GetStage() == Updater::GETINFO_STAGE)
		return objPtr->GetInfoStage(pData, size, nmemb);
	else if (objPtr->GetStage() == Updater::DOWNLOAD_STAGE)
		return objPtr->DownloadStage(pData, size, nmemb);

	return 0;
}

int Updater::GetInfoStage(char * pData, size_t size, size_t nmemb)
{
	strVersionURL.append(pData, size*nmemb);
	return size * nmemb;
}

int Updater::DownloadStage(char * pData, size_t size, size_t nmemb)
{
	auto wrote = fwrite(pData, size, nmemb, pZipFile);
	return wrote;
}

int Updater::XferInfo(void * pClient, size_t dltotal, size_t dlnow, size_t ultotal, size_t ulnow)
{
	if (!pClient)
		assert(0);

	auto pUpdate = (Updater*)pClient;
	pUpdate->SetProgressInfo(dltotal, dlnow);

	return 0;
}

void Updater::SetProgressInfo(size_t dltotal, size_t dlnow)
{
	if (ui.progressBar->maximum() != dltotal)
		QMetaObject::invokeMethod(ui.progressBar, "setRange", Qt::QueuedConnection, Q_ARG(int, 0), Q_ARG(int, dltotal));
	
	//ui.progLabel->setText(QString().sprintf("Download: %d/%d", dlnow, dltotal));
	QMetaObject::invokeMethod(ui.progLabel, "setText", Qt::QueuedConnection, Q_ARG(QString, QString("Download: %1/%2").arg(BytesFormat(dlnow)).arg(BytesFormat(dltotal))));
	QMetaObject::invokeMethod(ui.progressBar, "setValue", Qt::QueuedConnection, Q_ARG(int, dlnow));
}

void Updater::showEvent(QShowEvent * event)
{
	QWidget::showEvent(event);
	auto bBool = QMetaObject::invokeMethod(this, "Download", Qt::QueuedConnection);
}

void Updater::Download()
{
	Process clientApp("PilotRecord.exe");

	QElapsedTimer _timer;
	_timer.start();
	qint64 elapsedTime = 0;
	while (clientApp.FindApplicationOpen())
	{
		auto timeRemain = 10 - (elapsedTime / 1000);
		if (timeRemain <= 0)
		{
			clientApp.KillTheClient();
			break;
		}

		ui.statusLabel->setStyleSheet("color: Red; font-weight: bold;");
		ui.statusLabel->setText(QString("Waiting for PilotRecord Client to close before updating.\n Closing in (%1s).").arg(timeRemain));

		QApplication::processEvents();
		QThread::msleep(10);
		elapsedTime += _timer.restart();
	}
	

	ui.statusLabel->setText("Initializing...");
	ui.statusLabel->setStyleSheet("color: black; font-weight: normal;");

	QtConcurrent::run([&](){
	if (!pCurl)
	{
		SendError(nullptr, "PilotRecord Update Error", "There was a problem updating PilotRecord.", ErrorMSG::CRITICAL_MSG);
		SendExitCmd();
		return;
	}
	CURLcode res;

	QString strURL(QApplication::arguments().at(1));
	QString verURL = strURL + "ver.txt";
	curl_easy_setopt(pCurl, CURLOPT_URL, verURL.toStdString().c_str());
	curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, Updater::WriteFunction);
	curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, this);

	res = curl_easy_perform(pCurl);
	if (res != CURLE_OK)
	{
		SendError(this, "Error!", "There was an error getting the right version info", ErrorMSG::CRITICAL_MSG);
		SendExitCmd();
		return;
	}

	stage = Updater::DOWNLOAD_STAGE;
	verURL = strURL + strVersionURL.c_str();
	pZipFile = fopen(strVersionURL.c_str(), "wb");
	if (!pZipFile)
	{
		SendError(this, "Error!", "There was an error creating the file", ErrorMSG::CRITICAL_MSG);
		SendExitCmd();
		return;
	}

	curl_easy_setopt(pCurl, CURLOPT_URL, verURL.toStdString().c_str());
	curl_easy_setopt(pCurl, CURLOPT_NOPROGRESS, 0);
	curl_easy_setopt(pCurl, CURLOPT_XFERINFOFUNCTION, Updater::XferInfo);
	curl_easy_setopt(pCurl, CURLOPT_XFERINFODATA, this);

	res = curl_easy_perform(pCurl);
	if (res != CURLE_OK)
	{
		SendError(this, "Error!", "There was an error downloading the file", ErrorMSG::CRITICAL_MSG);
		SendExitCmd();
		return;
	}

	fclose(pZipFile);
	

	// QApplication::activeWindow();
	// Unzip file
	mz_zip_archive zipFile;
	memset(&zipFile, 0, sizeof(mz_zip_archive));
	auto zipHND = mz_zip_reader_init_file(&zipFile, strVersionURL.c_str(), 0);
	if (!zipHND)
	{
		SendError(this, "Error!", "Error extracting files. err01", ErrorMSG::CRITICAL_MSG);
		QFile::remove(strVersionURL.c_str());
		SendExitCmd();
		return;
	}

	for (int i = 0; i < (int)mz_zip_reader_get_num_files(&zipFile); i++)
	{
		char szStrFile[250];

		if (!mz_zip_reader_get_filename(&zipFile, i, szStrFile, 250))
		{
			SendError(this, "Error!", "Error extracting files. err02", ErrorMSG::CRITICAL_MSG);
			mz_zip_reader_end(&zipFile);
			SendExitCmd();
			return;
		}
		
		QString qstrFile(szStrFile);
		if (qstrFile.count("/") > 0)
		{
			QString qstrPath = qstrFile.mid(0, qstrFile.lastIndexOf("/"));
			if(!QDir(qstrPath).exists())
				QDir().mkpath(qstrPath);
		}

		ui.progLabel->setText(QString("Extracting: %1").arg(szStrFile));
		if (!mz_zip_reader_extract_file_to_file(&zipFile, szStrFile, szStrFile, 0))
			SendError(this, "Error!", "Error extracting files. err03", ErrorMSG::CRITICAL_MSG);

	}

	mz_zip_reader_end(&zipFile);

	// Start Deleting Files
	std::fstream remFileList("rem.txt", std::ios_base::out);
	if (remFileList.is_open())
	{
		std::string line;
		while (std::getline(remFileList, line))
		{
			QFile::remove(line.c_str());
		}

		remFileList.close();
		QFile::remove("rem.txt");
	}	

	QFile::remove(strVersionURL.c_str());
	SendError(this, "Success", "Successfully updated!", ErrorMSG::ABOUT_MSG);
	SendExitCmd();
	});
}

void Updater::SendError(void * pObj, QString strTitle, QString strContent, ErrorMSG::typeMSG type)
{
	ErrorMSG _msg_;
	_msg_.pObj = pObj;
	_msg_.strTitle = strTitle;
	_msg_.strMSG = strContent;
	_msg_.errType = type;
	
	std::lock_guard<std::mutex> guardLOCK(errorLOCK);
	errorQueue.push(_msg_);
}

void Updater::SendExitCmd()
{
	SendError(nullptr, "", "", ErrorMSG::QUIT_MSG);
}

void Updater::UpdateLoop()
{
	QApplication::processEvents();
	static Updater::CURR_STAGE _stg;

	if (_stg != stage)
	{
		switch (stage)
		{
		case Updater::GETINFO_STAGE:
		{
			ui.statusLabel->setText("Getting Download Info");
			break;
		}
		case Updater::DOWNLOAD_STAGE:
		{
			ui.statusLabel->setText(QString("Downloading: ").append(strVersionURL.c_str()));
			break;
		}
		case Updater::EXTRACTING_STAGE:
		{
			ui.statusLabel->setText(QString("Extracting Update").append(strVersionURL.c_str()));
			break;
		}
		}
		_stg = stage;
	}

	if (errorQueue.size() > 0)
	{
		for (auto it = errorQueue.front(); errorQueue.size() > 0;)
		{
			std::lock_guard<std::mutex> guardLOCK(errorLOCK);
			switch (it.errType)
			{
			case ErrorMSG::ABOUT_MSG:
			{
				QMessageBox::about((QWidget*)(it.pObj), it.strTitle, it.strMSG);
				break;
			}
			case ErrorMSG::CRITICAL_MSG:
			{
				QMessageBox::critical((QWidget*)(it.pObj), it.strTitle, it.strMSG);
				break;
			}
			case ErrorMSG::WARNING_MSG:
			{
				QMessageBox::warning((QWidget*)(it.pObj), it.strTitle, it.strMSG);
				break;
			}
			case ErrorMSG::QUIT_MSG:
			{
				QTimer::singleShot(0, qApp, SLOT(quit()));
				break;
			}
			}

			errorQueue.pop();

			if(errorQueue.size() > 0)
				it = errorQueue.front();
		}
	}

}


QString Updater::BytesFormat(size_t nBytes)
{
	int i;
	double dblByte = nBytes;
	for (i = 0; i < 5 && nBytes >= 1024; i++, nBytes /= 1024)
		dblByte = nBytes / 1024.0;

	auto strVal = QString("%1").arg(dblByte, 0, 'f', 2);

	return QString("%1 %2").arg(strVal).arg(bytesFormat[i]);
}
