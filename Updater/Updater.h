#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Updater.h"
#include <QTimer>
#include <curl/curl.h>
#include <queue>
#include <mutex>

struct ErrorMSG {
	enum typeMSG {
		CRITICAL_MSG,
		WARNING_MSG,
		ABOUT_MSG,
		QUIT_MSG
	};
	void *pObj;
	QString strTitle;
	QString strMSG;
	typeMSG errType;
};

extern QStringList bytesFormat;

class Updater : public QMainWindow
{
	enum CURR_STAGE {
		GETINFO_STAGE,
		DOWNLOAD_STAGE,
		EXTRACTING_STAGE
	};
	Q_OBJECT

public:
	Updater(QWidget *parent = Q_NULLPTR);
	~Updater();

	static int WriteFunction(char *pData, size_t size, size_t nmemb, void *pointer);
	static int XferInfo(void *pClient, size_t dltotal, size_t dlnow, size_t ultotal, size_t ulnow);
	inline CURR_STAGE GetStage() { return stage; }

	int GetInfoStage(char *pData, size_t size, size_t nmemb);
	int DownloadStage(char *pData, size_t size, size_t nmemb);

	void SetProgressInfo(size_t dltotal, size_t dlnow);

protected:
	void showEvent(QShowEvent *event) override;

	void SendError(void *pObj, QString strTitle, QString strContent, ErrorMSG::typeMSG type);
	void SendExitCmd();

	QString BytesFormat(size_t nBytes);

public slots:
	void UpdateLoop();
	void Download(); // called by invokeMethod

private:
	Ui::UpdaterClass ui;
	QTimer *updateTimer;
	std::queue<ErrorMSG> errorQueue;

	CURL *pCurl;
	CURR_STAGE stage;

	std::string strVersionURL;
	FILE *pZipFile;
	size_t downloadSize;

	std::mutex errorLOCK;
};
