#include "Updater.h"
#include <QtWidgets/QApplication>
#include <QMessageBox>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	if (argc < 2)
	{
		QMessageBox::critical(nullptr, "Error", "The updater can�t start without the proper config!");
		return 0;
	}

	Updater w;
	w.show();
	return a.exec();
}
