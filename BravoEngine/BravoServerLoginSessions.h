#ifndef BRAVO_SERVER_LOGIN_SESSIONS_H
#define BRAVO_SERVER_LOGIN_SESSIONS_H

#if defined(_MSC_VER) 
#pragma once
#endif

#include <map>
#include "BravoDBStructure.h"

namespace Bravo 
{
	class ServerLoginSessions
	{
	public:
		ServerLoginSessions();
		~ServerLoginSessions();

		std::string AddSession(UserAccount &acc);
		bool FindAccount(const std::string &strLoginID, UserAccount &account);
		void DeleteSession(const std::string &strLoginID);
	protected:
		std::string GenerateSessionID();
	
	private:
		std::map<const std::string, UserAccount> m_SessionMap;
	};
}

#endif // BRAVO_SERVER_LOGIN_SESSIONS_H