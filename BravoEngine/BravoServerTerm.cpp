#include "BravoServerTerm.h"
#include "BravoUtility.h"
#include <iostream>
#include <thread>

#include "BravoServer.h"


namespace Bravo 
{
	ServerTerminal::ServerTerminal(Server *server)
		: m_pServer(server)
	{
	}


	ServerTerminal::~ServerTerminal()
	{
	}

	void ServerTerminal::StartTerminal()
	{
		std::thread ([&]() {
			TerminalInit();
		}).detach();

		std::thread([&]() {
			StatCheck();
		}).detach();
	}

	void ServerTerminal::StatCheck()
	{
		while (m_pServer->isRunning())
		{
			std::this_thread::sleep_for(std::chrono::seconds(120));
			m_pServer->CheckAliveClients();
		}
	}

	void ServerTerminal::TerminalInit()
	{
		while (m_pServer->isRunning())
		{
			std::string strCommand, strArgs, strCmd;
			std::getline(std::cin, strCmd);

			ParseCommand(strCmd.c_str(), strCommand, strArgs);
			if (strCommand.compare("check") == 0)
			{
				if (strArgs.compare("stats") == 0)
				{
					m_pServer->Check(0);
				}
				else if (strArgs.compare("clients") == 0)
				{
					m_pServer->CheckAliveClients();
				}
				else
					m_pServer->Check(-1);
			}
			else if (strCommand.compare("shutdown") == 0)
			{
				m_pServer->Shutdown();
			}
			else
			{
				SERVER_OUT("[NOTICE]: Unknown command sent!\n");
			}
		}
	}

	void ServerTerminal::ParseCommand(const char *strLine, std::string &strCommand, std::string & strArgs)
	{
		std::string strTemp = strLine;
		
		auto pos = strTemp.find(" ");
		if (pos != std::string::npos)
		{
			strCommand = strTemp.substr(0, pos);
			strArgs = strTemp.substr(pos+1);
		}
		else
		{
			strCommand = strLine;
			strArgs = "";
		}

		std::transform(strCommand.begin(), strCommand.end(), strCommand.begin(), [](unsigned char c) { return std::tolower(c); });
		std::transform(strArgs.begin(), strArgs.end(), strArgs.begin(), [](unsigned char c) { return std::tolower(c); });
	}
}
