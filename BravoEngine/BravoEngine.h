/* Bravo Engine
* Heart and Soul of the application, provides you access to modules
* concerning core data processors
*/
#ifndef BRAVO_ENGINE_H
#define BRAVO_ENGINE_H

#if defined(_MSC_VER)
#pragma once
#endif

#include "Bravo.h"

class BravoEngine
{
public:
	enum BravoEngineState {
		STATE_LOCKED,
		STATE_LOGGEDIN
	};

	static BravoEngine *instance() {
		if (!_instance)
			_instance = new BravoEngine;
		return _instance;
	}

	BravoEngine();
	~BravoEngine();

public:
	// Modules Controllers
	//Bravo::BravoAccounts *getAccounts() { return _accounts; };
	//Bravo::BravoDatabase *getDatabase() { return _database; };
	Bravo::BravoEntry *getEntry() { return _entry; };

	// Configuration
	void Reconfig(const std::string &strKey, const std::string &strVal);
	// bool Configure();

	bool Initialize();
	bool CheckUpdates(std::string &strUpdaterFile);

	void LogOut();
	void ReconnectClient();
	bool IsClientOnline();
	BravoEngineState State();

	void addError(const std::string &strTitle, const std::string &strContent, const int &ntype);
	bool fetchError(std::string &strTitle, std::string &strContent, int &nType, int &nAct);

	void Login(std::string &strUser, std::string &strPass);
	//void RetrieveAccounts(std::function<void(std::string, int)> func);

	template<typename S>
	void RetrieveAccounts(S func)
	{
		_accounts->RetrieveUsers(func);
	}

private:
	static BravoEngine *_instance;

	BravoEngineState _state;

	// Modules
	Bravo::BravoDatabase *_database;
	Bravo::BravoAccounts *_accounts;
	Bravo::BravoEntry *_entry;
	
	// Handlers
	Bravo::BravoConfigHandler *_config;
	Bravo::BravoErrorHandler *_errorHandler;
	Bravo::BravoClient *_netClient;
};

#endif // !BRAVO_ENGINE_H

