#ifndef BRAVO_DEF_H
#define BRAVO_DEF_H

#ifdef _MSC_VER
#pragma once   
#endif // _MSC_VER

#define BRAVO_CLIENT_VERSION 0x003

namespace Bravo {
	enum ActionType {
		ERROR_NO_ACTION,
		ERROR_RECONNECT,
		ERROR_RESET_LOGIN,
		ERROR_FATAL,
		ERROR_LAST_ACTION
	};
}

#endif // !BRAVO_DEF_H