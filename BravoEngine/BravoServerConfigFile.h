#ifndef BRAVO_SERVER_CONFIG_FILE_H
#define BRAVO_SERVER_CONFIG_FILE_H

#ifdef _MSC_VER
#pragma once  
#endif // _MSC_VER

#include "Crypt.h"
#include "BravoFileHandler.h"
#include <map>

namespace Bravo
{
	const char serverConfFile[] = { "servConf.conf" };


	class BravoServerConfigFile : public BravoFileHandler
	{
	public:
		BravoServerConfigFile();
		~BravoServerConfigFile();

		bool Initialize();
		bool Find(const std::string &strKey);
		std::string operator[](const char *cKey);
	protected:
		void ParseConfig();

		std::map<std::string, std::string>	configMap;
	};
}

#endif // BRAVO_SERVER_CONFIG_FILE_H

