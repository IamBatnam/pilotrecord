#include "BravoServerLoginSessions.h"
#include <random>

namespace Bravo 
{
	ServerLoginSessions::ServerLoginSessions()
	{
	}


	ServerLoginSessions::~ServerLoginSessions()
	{
	}

	std::string ServerLoginSessions::AddSession(UserAccount &acc)
	{
		auto id = GenerateSessionID();

		m_SessionMap[id] = acc;

		return id;
	}

	bool ServerLoginSessions::FindAccount(const std::string & strLoginID, UserAccount &account)
	{
		auto it = m_SessionMap.find(strLoginID);
		if (it != m_SessionMap.end()) 
		{
			account = it->second;
			return true;
		}

		return false;
	}

	void ServerLoginSessions::DeleteSession(const std::string & strLoginID)
	{
		m_SessionMap.erase(strLoginID);
	}

	std::string ServerLoginSessions::GenerateSessionID()
	{
		//char idOut[4];
		std::random_device rd;
		std::mt19937 e1(rd());
		std::uniform_int_distribution<unsigned int> r(48, 122);

		generate:
		char id[5] = { (char)r(e1), (char)r(e1), (char)r(e1), (char)r(e1), '\0' };

		//for (int i = 0; i < 4; ++i) {
		//	idOut[i] = id[i];
		//}
		//
		//*idOut = '\0';
		if (m_SessionMap.find(id) != m_SessionMap.end())
			goto generate;

		std::string strReturn(id);
		return strReturn;
	}

}