#include "BravoClient.h"
#include "network.pb.h"

namespace Bravo {
	NetworkPackets::DataPacket BravoClient::SendLogOut(LoginSession *activeUser)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);
		
		auto requestPacket = packet.mutable_messagerequest();
		requestPacket->set_reqtype(NetworkPackets::Request::MSG_LOGOUT);
		requestPacket->mutable_session()->set_sessionid(activeUser->sessionid);
		

		SendMsg(packet);

		auto result = GetMessage(NetworkPackets::DataPacket::MESSAGE_RESULT);

		return result;
	}

	bool BravoClient::SendFindUser(const std::string &strUser)
	{
		ASSERT(0);
		//NetworkPackets::DataPacket packet;
		//packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_LOGIN);

		//auto loginMsg = packet.mutable_messagelogin();
		//loginMsg->set_user(strUser);
		//loginMsg->set_logintype(NetworkPackets::Login::LOGIN_CHECKUSER);

		//SendMsg(packet);

		//auto resultPacket = GetMessage(NetworkPackets::DataPacket::MESSAGE_LOGIN);

		//auto result = resultPacket.mutable_messagelogin()->mutable_resultmsg()->result();

		//if (result > 0)
		//	return true;

		return false;
	}

	NetworkPackets::DataPacket BravoClient::SendCheckLogin(const std::string &strUser, const std::string &strPass)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_LOGIN);
		
		auto loginMsg = packet.mutable_messagelogin();
		loginMsg->set_user(strUser);
		loginMsg->set_pass(strPass);
		loginMsg->set_logintype(NetworkPackets::Login::LOGIN_LOGIN);

		SendMsg(packet);

		auto resultPacket = GetMessage(NetworkPackets::DataPacket::MESSAGE_LOGIN);

		return resultPacket;
	}

	NetworkPackets::DataPacket BravoClient::RetrieveAccounts()
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_REQUEST_ACCOUNTS);

		SendMsg(packet);

		auto results = GetMessage(NetworkPackets::DataPacket::MESSAGE_ACCOUNTS);

		return results;
	}

	NetworkPackets::DataPacket BravoClient::RetrieveDuties(LoginSession *activeUser, const BravoDate &startDate, const BravoDate &endDate)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_REQUEST_DUTIES);
		
		// Active Session
		auto activeSession = msg->mutable_session();
		activeSession->set_sessionid(activeUser->sessionid);
		
		// Date range
		auto requiredData = msg->mutable_dutydata();
		auto sdate = requiredData->mutable_startdate();
		sdate->set_year(startDate.year); sdate->set_month(startDate.month); sdate->set_day(startDate.day);
		auto edate = requiredData->mutable_enddate();
		edate->set_year(endDate.year); edate->set_month(endDate.month); edate->set_day(endDate.day);

		SendMsg(packet);

		return GetMessage(NetworkPackets::DataPacket::MESSAGE_DUTY);
	}

	NetworkPackets::DataPacket BravoClient::RetrieveDuties(LoginSession *activeUser, const BravoDate &date)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_REQUEST_DUTIES);

		// Active Session
		auto activeSession = msg->mutable_session();
		activeSession->set_sessionid(activeUser->sessionid);

		// duty data
		auto requiredData = msg->mutable_dutydata();

		// type
		requiredData->set_reqdutytype(NetworkPackets::Request::DutyRequestOptions::DUTY_REQ_SPEC);

		// date
		auto sdate = requiredData->mutable_startdate();
		sdate->set_year(date.year); sdate->set_month(date.month); sdate->set_day(date.day);		
		

		SendMsg(packet);

		return GetMessage(NetworkPackets::DataPacket::MESSAGE_DUTY);
	}

	NetworkPackets::DataPacket BravoClient::CommitDuty(LoginSession * activeUser, const BravoDataDuty &duty)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_COMMIT_DUTIES);

		// Active Session
		auto activeSession = msg->mutable_session();
		activeSession->set_sessionid(activeUser->sessionid);

		// duty data
		auto requiredData = msg->mutable_dutycommitdata();
		PackDuty(duty, requiredData);

		SendMsg(packet);

		return GetMessage(NetworkPackets::DataPacket::MESSAGE_RESULT);
	}

	NetworkPackets::DataPacket BravoClient::RetrieveEntries(LoginSession *activeUser)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_REQUEST_ENTRIES);
		auto requiredData = msg->mutable_session();
		requiredData->set_sessionid(activeUser->sessionid);

		SendMsg(packet);

		auto results = GetMessage(NetworkPackets::DataPacket::MESSAGE_ENTRY);
		
		return results;
	}

	NetworkPackets::DataPacket BravoClient::RetrievePreviousEntries(LoginSession * activeUser, int &entryId)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_REQUEST_PREVIOUS_ENTRIES);
		auto requiredData = msg->mutable_session();
		requiredData->set_sessionid(activeUser->sessionid);

		msg->set_entrynum(entryId);
		SendMsg(packet);

		auto results = GetMessage(NetworkPackets::DataPacket::MESSAGE_ENTRY);

		return results;
	}

	NetworkPackets::DataPacket BravoClient::AddEntry(LoginSession *activeUser, const BravoDataEntry &entry)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_ADD_ENTRY);

		auto requiredData = msg->mutable_session();
		requiredData->set_sessionid(activeUser->sessionid);

		PackEntry(entry, msg->mutable_entrydata());
		SendMsg(packet);

		auto result = GetMessage(NetworkPackets::DataPacket::MESSAGE_RESULT);

		return result;
	}

	NetworkPackets::DataPacket BravoClient::EditEntry(LoginSession *activeUser, const BravoDataEntry &entry)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto msg = packet.mutable_messagerequest();
		msg->set_reqtype(NetworkPackets::Request::MSG_EDIT_ENTRY);

		auto requiredData = msg->mutable_session();
		requiredData->set_sessionid(activeUser->sessionid);

		PackEntry(entry, msg->mutable_entrydata());
		SendMsg(packet);

		auto result = GetMessage(NetworkPackets::DataPacket::MESSAGE_RESULT);


		return result;
	}

	void BravoClient::PackEntry(const BravoDataEntry &account, NetworkPackets::Entries::Entry *entry)
	{
		entry->set_id(account.id);
		entry->set_date(account._date);
		entry->set_cplanetype(account.cPlaneType);
		entry->set_cplanereg(account.cPlaneReg);
		entry->set_cpilotname(account.cPilotName);
		entry->set_cpax(account.cPax);
		entry->set_strremarks(account.strRemarks);

		entry->set_se_day_dual(account.SE_DAY_DUAL);
		entry->set_se_night_dual(account.SE_NIGHT_DUAL);
		entry->set_se_day_pic(account.SE_DAY_PIC);
		entry->set_se_night_pic(account.SE_NIGHT_PIC);

		entry->set_me_day_dual(account.ME_DAY_DUAL);
		entry->set_me_night_dual(account.ME_NIGHT_DUAL);
		entry->set_me_day_pic(account.ME_DAY_PIC);
		entry->set_me_night_pic(account.ME_NIGHT_PIC);
		entry->set_me_day_sic(account.ME_DAY_SIC);
		entry->set_me_night_sic(account.ME_NIGHT_SIC);

		entry->set_xcun_day_dual(account.XCUN_DAY_DUAL);
		entry->set_xcun_day_pic(account.XCUN_DAY_PIC);
		entry->set_xcun_night_dual(account.XCUN_NIGHT_DUAL);
		entry->set_xcun_night_pic(account.XCUN_NIGHT_PIC);

		entry->set_nto_land_day(account.nTO_LAND_DAY);
		entry->set_nto_land_night(account.nTO_LAND_NIGHT);

		entry->set_ifr_approach(account.IFR_APPROACH);
		entry->set_sim_time(account.SIM_TIME);
		entry->set_hood_time(account.HOOD_TIME);
		entry->set_actual_time(account.ACTUAL_TIME);
	}

	void BravoClient::PackDuty(const BravoDataDuty &bravoDuty, NetworkPackets::Duties::Duty *packetDuty)
	{
		packetDuty->set_dutyid(bravoDuty.id);

		auto date = packetDuty->mutable_date();
		date->set_year(bravoDuty.date.year);
		date->set_month(bravoDuty.date.month);
		date->set_day(bravoDuty.date.day);

		packetDuty->set_dutydayoff((int)bravoDuty.dayOffDuty);
		packetDuty->set_dutysplitday((int)bravoDuty.dutySplit);

		auto startdate = packetDuty->mutable_dutystarttime();
		startdate->set_year(bravoDuty.startDutyTime.year);
		startdate->set_month(bravoDuty.startDutyTime.month);
		startdate->set_day(bravoDuty.startDutyTime.day);
		startdate->set_hour(bravoDuty.startDutyTime.hour);
		startdate->set_minutes(bravoDuty.startDutyTime.min);

		auto middayenddate = packetDuty->mutable_dutymiddayend();
		middayenddate->set_year(bravoDuty.endMiddayDutyTime.year);
		middayenddate->set_month(bravoDuty.endMiddayDutyTime.month);
		middayenddate->set_day(bravoDuty.endMiddayDutyTime.day);
		middayenddate->set_hour(bravoDuty.endMiddayDutyTime.hour);
		middayenddate->set_minutes(bravoDuty.endMiddayDutyTime.min);

		auto middaystartdate = packetDuty->mutable_dutymiddaystart();
		middaystartdate->set_year(bravoDuty.startMiddayDutyTime.year);
		middaystartdate->set_month(bravoDuty.startMiddayDutyTime.month);
		middaystartdate->set_day(bravoDuty.startMiddayDutyTime.day);
		middaystartdate->set_hour(bravoDuty.startMiddayDutyTime.hour);
		middaystartdate->set_minutes(bravoDuty.startMiddayDutyTime.min);

		auto enddate = packetDuty->mutable_dutyendtime();
		enddate->set_year(bravoDuty.endDutyTime.year);
		enddate->set_month(bravoDuty.endDutyTime.month);
		enddate->set_day(bravoDuty.endDutyTime.day);
		enddate->set_hour(bravoDuty.endDutyTime.hour);
		enddate->set_minutes(bravoDuty.endDutyTime.min);

		// Flight legs
		for (auto it : *(bravoDuty.flightLegs))
		{
			auto flightLegs = packetDuty->add_flightlegs();
			
			flightLegs->set_flightlegid(it.nFlightLegId);

			// Start Time
			auto startLegTime = flightLegs->mutable_starttime();
			startLegTime->set_year(it.flightStart.year);
			startLegTime->set_month(it.flightStart.month);
			startLegTime->set_day(it.flightStart.day);
			startLegTime->set_hour(it.flightStart.hour);
			startLegTime->set_minutes(it.flightStart.min);

			// End Time
			auto endLegTime = flightLegs->mutable_endtime();
			endLegTime->set_year(it.flightEnd.year);
			endLegTime->set_month(it.flightEnd.month);
			endLegTime->set_day(it.flightEnd.day);
			endLegTime->set_hour(it.flightEnd.hour);
			endLegTime->set_minutes(it.flightEnd.min);

			// Crew
			flightLegs->set_crew(it.strCrew);

			flightLegs->set_aircraft(it.strAircraft);
			flightLegs->set_route(it.strFlightRoute);
			flightLegs->set_fltnum(it.strFlightNum);
			flightLegs->set_mileage(it.nMileage);
		}
	}
}