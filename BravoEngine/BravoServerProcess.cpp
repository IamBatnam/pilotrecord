#include "BravoServer.h"

#include "BravoNetwork.h"
#include "BravoEngine.h"

#include "nanodbc/nanodbc.h"
#include "network.pb.h"

namespace Bravo {
	void Server::ProcessLoginRequest(const NetworkPackets::Login & loginPacket, Network::Session * client)
	{
		using namespace NetworkPackets;

		if (client == nullptr)
			return;

		switch (loginPacket.logintype())
		{
		case Login::LOGIN_LOGIN:
		{
			auto strUser = loginPacket.user();
			auto strPass = loginPacket.pass();

			nanodbc::result result;

			try {
				if (!m_pDatabase->connected())
					ReconnectToDatabase();

				nanodbc::statement query(*m_pDatabase);

				query.prepare("SELECT * FROM `Users` WHERE `Username` = ? AND `Password` = ?");
				query.bind(0, strUser.c_str());
				query.bind(1, strPass.c_str());

				result = query.execute();
			}
			catch (const nanodbc::database_error &error)
			{
				std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
				SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
				return;
			}

			// Send Client login confirm!
			DataPacket packet;
			packet.set_messagetype(DataPacket::MESSAGE_LOGIN);
			auto msgLogin = packet.mutable_messagelogin();
			msgLogin->set_logintype(Login::LOGIN_SERVER_RESULT);

			auto serverResult = msgLogin->mutable_resultmsg();

			if (result.next())
			{
				serverResult->set_result(Login::ServerResult::LOGIN_ACCEPT);

				// Generate Session id
				UserAccount accountInfo;
				accountInfo.nUserId = result.get<int>(0);
				accountInfo.User = result.get<std::string>(1);

				std::string sessionid = LoggedInSessions.AddSession(accountInfo);

				auto acc = serverResult->mutable_session();
				acc->set_sessionid(sessionid);

				SERVER_OUT("Successful login from [IP]:%s [User]:%s [SessionID]: %s\n",
					client->GetSocket()->remote_endpoint().address().to_string().c_str(),
					strUser.c_str(),
					sessionid.c_str());
			}
			else
			{
				serverResult->set_result(Login::ServerResult::LOGIN_ERROR);
				SERVER_OUT("Login failed from [IP]:%s [User]:%s\n",
					client->GetSocket()->remote_endpoint().address().to_string().c_str(),
					strUser.c_str());
			}

			SendMsg(packet, client);

			break;
		}
		case NetworkPackets::Login::LOGIN_UNKNOWN:
		default:
		{
			SERVER_OUT("LOGIN TYPE FROM CLIENT UNKNOWN!");
			DataPacket packet;
			packet.set_messagetype(DataPacket::MESSAGE_ERROR);
			auto error = packet.mutable_messageerror();
			error->set_errorwhat("Error Occured While logging in\n");

			SendMsg(packet, client);
			break;
		}
		}
	}

	void Server::ProcessRequests(const NetworkPackets::Request & requestPacket, Network::Session * client)
	{
		using namespace NetworkPackets;

		if (client == nullptr)
			return;

		switch (requestPacket.reqtype())
		{
		case Request::MSG_LOGOUT:
		{
			auto strLoginSessionID = requestPacket.session().sessionid();
			LoggedInSessions.DeleteSession(strLoginSessionID);

			DataPacket packet;
			packet.set_messagetype(DataPacket::MESSAGE_RESULT);
			auto packetResult = packet.mutable_messageresult();
			packetResult->set_resulttype(Result::RESULT_GOOD);

			SERVER_OUT("Session terminated ['%s'], [IP]: %s\n",
				requestPacket.session().sessionid().c_str(),
				client->GetSocket()->remote_endpoint().address().to_string().c_str());

			SendMsg(packet, client);

			break;
		}

		case Request::MSG_REQUEST_ENTRIES:
		{
			MSG_REQUEST_ENTRIES(requestPacket, client);
			break;
		}

		case Request::MSG_REQUEST_PREVIOUS_ENTRIES:
		{
			MSG_REQUEST_PREVIOUS_ENTRIES(requestPacket, client);
			break;
		}

		case Request::MSG_REQUEST_DUTIES:
		{
			MSG_REQUEST_DUTIES(requestPacket, client);
			break;
		}

		case Request::MSG_COMMIT_DUTIES:
		{
			MSG_COMMIT_DUTIES(requestPacket, client);
			break;
		}

		case Request::MSG_ADD_ENTRY:
		{
			MSG_ADD_ENTRY(requestPacket, client);
			break;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// MSG_EDIT_ENTRY
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case Request::MSG_EDIT_ENTRY:
		{
			auto entry = requestPacket.entrydata();

			nanodbc::result result;
			UserAccount userAcc;

			try {
				if (!m_pDatabase->connected())
					ReconnectToDatabase();

				auto strLoginSessionID = requestPacket.session().sessionid();
				if (LoggedInSessions.FindAccount(strLoginSessionID, userAcc))
				{
					{
						nanodbc::statement query(*m_pDatabase);
						int entryId = (int)entry.id();

						query.prepare("SELECT * FROM `Entries` WHERE `EntryOwner` = ? AND `EntryNum` = ?");
						query.bind<int>(0, &userAcc.nUserId);
						query.bind<int>(1, &entryId);

						result = query.execute();

						while (result.next())
						{
							auto id = result.get<int>(0);
							auto entryowner = result.get<int>(1);
							nanodbc::date date = result.get<nanodbc::date>(2);
							auto cplanetype = result.get<std::string>(3);
							auto cplanereg = result.get<std::string>(4);
							auto cpilotname = result.get<std::string>(5);
							auto cpax = result.get<std::string>(6);
							auto strremarks = result.get<std::string>(7);

							auto se_day_dual = result.get<float>(8);
							auto se_night_dual = result.get<float>(9);
							auto se_day_pic = result.get<float>(10);
							auto se_night_pic = result.get<float>(11);

							auto me_day_dual = result.get<float>(12);
							auto me_night_dual = result.get<float>(13);
							auto me_day_pic = result.get<float>(14);
							auto me_night_pic = result.get<float>(15);
							auto me_day_sic = result.get<float>(16);
							auto me_night_sic = result.get<float>(17);

							auto xcun_day_dual = result.get<float>(18);
							auto xcun_day_pic = result.get<float>(19);
							auto xcun_night_dual = result.get<float>(20);
							auto xcun_night_pic = result.get<float>(21);

							auto nto_land_day = result.get<int>(22);
							auto nto_land_night = result.get<int>(23);

							auto ifr_approach = result.get<int>(24);
							auto sim_time = result.get<float>(25);
							auto hood_time = result.get<float>(26);
							auto actual_time = result.get<float>(27);


							nanodbc::statement query1(*m_pDatabase);
							query1.prepare("INSERT INTO `PreviousEntries`(`EntryNum`, `EntryOwner`, `EntryDate`, `PlaneType`, `PlaneReg`, `PilotName`, `PaxName`, `Remarks`, `SE_DAY_DUAL`, `SE_NIGHT_DUAL`, `SE_DAY_PIC`, `SE_NIGHT_PIC`, `ME_DAY_DUAL`, `ME_NIGHT_DUAL`, `ME_DAY_PIC`, `ME_NIGHT_PIC`, `ME_DAY_SIC`, `ME_NIGHT_SIC`, `XCUN_DAY_DUAL`, `XCUN_NIGHT_DUAL`, `XCUN_DAY_PIC`, `XCUN_NIGHT_PIC`, `TO_LAND_DAY`, `TO_LAND_NIGHT`, `IFR_APPROACH`, `SIM_TIME`, `HOOD_TIME`, `ACTUAL_TIME`, `EntryModDate`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())");
							query1.bind<int>(0, &id);
							query1.bind<int>(1, &entryowner);
							query1.bind<nanodbc::date>(2, &date);
							query1.bind(3, cplanetype.c_str());
							query1.bind(4, cplanereg.c_str());
							query1.bind(5, cpilotname.c_str());
							query1.bind(6, cpax.c_str());
							query1.bind(7, strremarks.c_str());

							query1.bind<float>(8, &se_day_dual);
							query1.bind<float>(9, &se_night_dual);
							query1.bind<float>(10, &se_day_pic);
							query1.bind<float>(11, &se_night_pic);

							query1.bind<float>(12, &me_day_dual);
							query1.bind<float>(13, &me_night_dual);
							query1.bind<float>(14, &me_day_pic);
							query1.bind<float>(15, &me_night_pic);
							query1.bind<float>(16, &me_day_sic);
							query1.bind<float>(17, &me_night_sic);

							query1.bind<float>(18, &xcun_day_dual);
							query1.bind<float>(19, &xcun_day_pic);
							query1.bind<float>(20, &xcun_night_dual);
							query1.bind<float>(21, &xcun_night_pic);

							query1.bind<int>(22, &nto_land_day);
							query1.bind<int>(23, &nto_land_night);

							query1.bind<int>(24, &ifr_approach);
							query1.bind<float>(25, &sim_time);
							query1.bind<float>(26, &hood_time);
							query1.bind<float>(27, &actual_time);

							query1.execute();
						}

					}
					{
						nanodbc::statement query(*m_pDatabase);

						query.prepare("UPDATE `Entries` SET `EntryDate` = ?, `PlaneType` = ?, `PlaneReg` = ?, `PilotName` = ?, `PaxName` = ?, `Remarks` = ?, `SE_DAY_DUAL` = ?, `SE_NIGHT_DUAL` = ?, `SE_DAY_PIC` = ?, `SE_NIGHT_PIC` = ?, `ME_DAY_DUAL` = ?, `ME_NIGHT_DUAL` = ?, `ME_DAY_PIC` = ?, `ME_NIGHT_PIC` = ?, `ME_DAY_SIC` = ?, `ME_NIGHT_SIC` = ?, `XCUN_DAY_DUAL` = ?, `XCUN_NIGHT_DUAL` = ?, `XCUN_DAY_PIC` = ?, `XCUN_NIGHT_PIC` = ?, `TO_LAND_DAY` = ?, `TO_LAND_NIGHT` = ?, `IFR_APPROACH` = ?, `SIM_TIME` = ?, `HOOD_TIME` = ?, `ACTUAL_TIME` = ? WHERE EntryNum = ?");

						unsigned short month, day;
						unsigned int year;
						BravoDataEntry::LongToDate((long)entry.date(), month, day, year);
                        nanodbc::date dateData = { (std::int16_t)year, (std::int16_t)month, (std::int16_t)day };
						query.bind<nanodbc::date>(0, &dateData);

						std::string cplanetype = entry.cplanetype();
						std::string cplanereg = entry.cplanereg();
						std::string cpilotname = entry.cpilotname();
						std::string cpax = entry.cpax();
						std::string strremarks = entry.strremarks();

						query.bind(1, cplanetype.c_str());
						query.bind(2, cplanereg.c_str());
						query.bind(3, cpilotname.c_str());
						query.bind(4, cpax.c_str());
						query.bind(5, strremarks.c_str());

						float se_day_dual = entry.se_day_dual();
						float se_night_dual = entry.se_night_dual();
						float se_day_pic = entry.se_day_pic();
						float se_night_pic = entry.se_night_pic();

						float me_day_dual = entry.me_day_dual();
						float me_night_dual = entry.me_night_dual();
						float me_day_pic = entry.me_day_pic();
						float me_night_pic = entry.me_night_pic();
						float me_day_sic = entry.me_day_sic();
						float me_night_sic = entry.me_night_sic();

						float xcun_day_dual = entry.xcun_day_dual();
						float xcun_day_pic = entry.xcun_day_pic();
						float xcun_night_dual = entry.xcun_night_dual();
						float xcun_night_pic = entry.xcun_night_pic();

						int nto_land_day = entry.nto_land_day();
						int nto_land_night = entry.nto_land_night();

						int ifr_approach = entry.ifr_approach();
						float sim_time = entry.sim_time();
						float hood_time = entry.hood_time();
						float actual_time = entry.actual_time();

						query.bind<float>(6, &se_day_dual);
						query.bind<float>(7, &se_night_dual);
						query.bind<float>(8, &se_day_pic);
						query.bind<float>(9, &se_night_pic);

						query.bind<float>(10, &me_day_dual);
						query.bind<float>(11, &me_night_dual);
						query.bind<float>(12, &me_day_pic);
						query.bind<float>(13, &me_night_pic);
						query.bind<float>(14, &me_day_sic);
						query.bind<float>(15, &me_night_sic);

						query.bind<float>(16, &xcun_day_dual);
						query.bind<float>(17, &xcun_day_pic);
						query.bind<float>(18, &xcun_night_dual);
						query.bind<float>(19, &xcun_night_pic);

						query.bind<int>(20, &nto_land_day);
						query.bind<int>(21, &nto_land_night);

						query.bind<int>(22, &ifr_approach);
						query.bind<float>(23, &sim_time);
						query.bind<float>(24, &hood_time);
						query.bind<float>(25, &actual_time);

						int nEntry = entry.id();
						query.bind<int>(26, &nEntry);

						result = query.execute();
					}
				}
				else
					SendClientServerError(client, BravoServerError::SERVER_UNKNOWN_LOGIN_SESSION);

			}
			catch (const nanodbc::database_error &error)
			{
				std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
				SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
				return;
			}

			DataPacket packet;
			packet.set_messagetype(DataPacket::MESSAGE_RESULT);
			auto packetResult = packet.mutable_messageresult();
			packetResult->set_resulttype(Result::RESULT_GOOD_EDIT_ENTRY);

			SERVER_OUT("Entry [ID]:%d has been updated by [USER]:%s, [IP]:%s\n",
				(int)entry.id(),
				userAcc.User.c_str(),
				client->GetSocket()->remote_endpoint().address().to_string().c_str());

			SendMsg(packet, client);
			break;
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// MSG_REQUEST_ACCOUNTS
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case Request::MSG_REQUEST_ACCOUNTS:
		{
			nanodbc::result result;

			try {
				if (!m_pDatabase->connected())
					ReconnectToDatabase();

				result = nanodbc::execute(*m_pDatabase, "SELECT * FROM `Users`");
			}
			catch (const nanodbc::database_error &error)
			{
				std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
				SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
				return;
			}

			DataPacket packet;
			packet.set_messagetype(DataPacket::MESSAGE_ACCOUNTS);
			auto accounts = packet.mutable_messageaccounts();

			int i = 0;
			for (; result.next(); i++)
			{
				auto acc = accounts->add_accounts();
				auto userid = result.get<int>(0);
				auto username = result.get<std::string>(1);
				auto usertype = result.get<int>(3);
				acc->set_userid(userid);
				acc->set_username(username);
				acc->set_accnttype(usertype);
			}
			accounts->set_totalaccounts(i);

			SendMsg(packet, client);

			break;
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// MSG_REQUEST_UPDATE
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case Request::MSG_REQUEST_UPDATE:
		{
			DataPacket packet;
			packet.set_messagetype(DataPacket::MESSAGE_RESULT);
			auto resultMsg = packet.mutable_messageresult();
			auto urlMsg = resultMsg->mutable_urlmsg();
			
			// UNDONE
			urlMsg->set_latestversion(Util::CharToInt(serverConfig["PatchVersion"].c_str()));
			urlMsg->set_strurl(serverConfig["PatchPath"]);

			SendMsg(packet, client);

			break;
		}
		}
	}
}
