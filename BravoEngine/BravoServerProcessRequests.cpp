#include "BravoServer.h"

#include "BravoNetwork.h"
#include "BravoEngine.h"

#include "nanodbc/nanodbc.h"
#include "network.pb.h"

namespace Bravo {
	using namespace NetworkPackets;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MSG_REQUEST_DUTIES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Server::MSG_REQUEST_DUTIES(const NetworkPackets::Request & requestPacket, Network::Session * client)
	{
		nanodbc::result result;
		UserAccount userAcc;

		auto strLoginSessionID = requestPacket.session().sessionid();
		auto dutyData = requestPacket.dutydata();

		try {
			if (!m_pDatabase->connected())
				ReconnectToDatabase();

			if (LoggedInSessions.FindAccount(strLoginSessionID, userAcc))
			{
				nanodbc::statement query(*m_pDatabase);
				if (dutyData.reqdutytype() == NetworkPackets::Request::DutyRequestOptions::DUTY_REQ_MIN) // Range base retrieve
				{
					query.prepare("SELECT * FROM `Duties` WHERE `DutyOwner` = ? AND `DutyDate` >= ? AND `DutyDate` <= ? ORDER BY `DutyDate` ASC");
					query.bind<int>(0, &userAcc.nUserId);

					const nanodbc::date startDate = { (short)dutyData.startdate().year(), (short)dutyData.startdate().month(), (short)dutyData.startdate().day() };
					query.bind<nanodbc::date>(1, &startDate);

					const nanodbc::date endDate = { (short)dutyData.enddate().year(), (short)dutyData.enddate().month(), (short)dutyData.enddate().day() };
					query.bind<nanodbc::date>(2, &endDate);
				}
				else // Specific date retrieve DUTY_REQ_SPEC
				{
					query.prepare("SELECT * FROM `Duties` WHERE `DutyOwner` = ? AND `DutyDate` = ? ORDER BY `DutyDate` ASC");
					query.bind<int>(0, &userAcc.nUserId);

					const nanodbc::date startDate = { (short)dutyData.startdate().year(), (short)dutyData.startdate().month(), (short)dutyData.startdate().day() };
					query.bind<nanodbc::date>(1, &startDate);
				}

				result = query.execute();
			}
			else
				SendClientServerError(client, BravoServerError::SERVER_UNKNOWN_LOGIN_SESSION);

		}
		catch (const nanodbc::database_error &error)
		{
			std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
			SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
			return;
		}

		//Send Results back to the waiting client
		DataPacket packet;
		packet.set_messagetype(DataPacket::MESSAGE_DUTY);
		auto msgDuties = packet.mutable_messageduties();

		while (result.next())
		{
			auto duty = msgDuties->add_duties();

			// Set Duty ID
			auto DutyID = result.get<int>(0);
			duty->set_dutyid(DutyID);

			// Set Duty Date
			nanodbc::date date = result.get<nanodbc::date>(1);
			auto dateDuty = duty->mutable_date();
			dateDuty->set_year(date.year);
			dateDuty->set_month(date.month);
			dateDuty->set_day(date.day);

			// DayOff
			auto dayoffDuty = result.get<int>(2);
			duty->set_dutydayoff(dayoffDuty);

			// Split Duty Day
			auto dutyDaySplit = result.get<int>(3);
			duty->set_dutysplitday(dutyDaySplit);

			// Retrieve start Time
			nanodbc::timestamp startTime = result.get<nanodbc::timestamp>(5);
			auto sTime = duty->mutable_dutystarttime();
			sTime->set_year(startTime.year);
			sTime->set_month(startTime.month);
			sTime->set_day(startTime.day);
			sTime->set_hour(startTime.hour);
			sTime->set_minutes(startTime.min);

			// Midday End
			nanodbc::timestamp midEndTime = result.get<nanodbc::timestamp>(6, { date.year, date.month, date.day });
			auto midETime = duty->mutable_dutymiddayend();
			midETime->set_year(midEndTime.year);
			midETime->set_month(midEndTime.month);
			midETime->set_day(midEndTime.day);
			midETime->set_hour(midEndTime.hour);
			midETime->set_minutes(midEndTime.min);

			// Midday Start
			nanodbc::timestamp midStartTime = result.get<nanodbc::timestamp>(7, { date.year, date.month, date.day });
			auto midSTime = duty->mutable_dutymiddaystart();
			midSTime->set_year(midStartTime.year);
			midSTime->set_month(midStartTime.month);
			midSTime->set_day(midStartTime.day);
			midSTime->set_hour(midStartTime.hour);
			midSTime->set_minutes(midStartTime.min);

			// Retrieve End Time
			nanodbc::timestamp endTime = result.get<nanodbc::timestamp>(8);
			auto eTime = duty->mutable_dutyendtime();
			eTime->set_year(endTime.year);
			eTime->set_month(endTime.month);
			eTime->set_day(endTime.day);
			eTime->set_hour(endTime.hour);
			eTime->set_minutes(endTime.min);

			// Retrieve Flight Legs
			// Dont retrieve for DUTY_REQ_SPEC
			if (dutyData.reqdutytype() != NetworkPackets::Request::DutyRequestOptions::DUTY_REQ_MIN)
			{
				nanodbc::statement query(*m_pDatabase);
				query.prepare("SELECT * FROM `FlightLegs` WHERE LegDutyOwner = ? AND LegDutyId = ? AND LegDeleted = FALSE");
				query.bind<int>(0, &userAcc.nUserId);
				query.bind<int>(1, &DutyID);

				auto res1 = query.execute();
				while (res1.next())
				{
					auto flytLegs = duty->add_flightlegs();
					flytLegs->set_flightlegid(res1.get<int>(0));

					// LegDutyId

					// LegUserOwner

					// Start Time
					nanodbc::timestamp timeStart;
					timeStart = res1.get<nanodbc::timestamp>(3);
					auto stime = flytLegs->mutable_starttime();
					stime->set_year(timeStart.year);
					stime->set_month(timeStart.month);
					stime->set_day(timeStart.day);
					stime->set_hour(timeStart.hour);
					stime->set_minutes(timeStart.min);

					// End Time
					nanodbc::timestamp timeEnd;
					timeEnd = res1.get<nanodbc::timestamp>(4);
					auto etime = flytLegs->mutable_endtime();
					etime->set_year(timeEnd.year);
					etime->set_month(timeEnd.month);
					etime->set_day(timeEnd.day);
					etime->set_hour(timeEnd.hour);
					etime->set_minutes(timeEnd.min);

					// Aircraft
					flytLegs->set_aircraft(res1.get<std::string>(5));

					// Crew
					flytLegs->set_crew(res1.get<std::string>(6));

					// Route
					flytLegs->set_route(res1.get<std::string>(7));

					// LegFlightNum
					flytLegs->set_fltnum(res1.get<std::string>(8));

					// LegMileage
					flytLegs->set_mileage(res1.get<int>(9));
				}
			}
		}

		SERVER_OUT("Duties [TOTAL]:%d, Requested by [USER]:%s, [IP]:%s]\n",
			msgDuties->duties().size(),
			userAcc.User.c_str(),
			client->GetSocket()->remote_endpoint().address().to_string().c_str());
		SendMsg(packet, client);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MSG_REQUEST_DUTIES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Server::MSG_COMMIT_DUTIES(const NetworkPackets::Request & requestPacket, Network::Session * client)
	{
		nanodbc::result result;
		UserAccount userAcc;

		try {
			if (!m_pDatabase->connected())
				ReconnectToDatabase();

			// transactions of queries
			nanodbc::transaction trans(*m_pDatabase);

			auto strLoginSessionID = requestPacket.session().sessionid();
			if (LoggedInSessions.FindAccount(strLoginSessionID, userAcc))
			{
				nanodbc::statement query(*m_pDatabase);

				int64_t nDutyId;

				if (requestPacket.dutycommitdata().dutyid() > 0) // update entry
				{
					query.prepare("UPDATE `Duties` SET `DutyDate` = ?, `DutyDayOff` = ?, `DutySplit` = ?, `DutyStart` = ?, `DutyMiddayEnd` = ?, `DutyMiddayStart` = ?, `DutyEnd` = ? WHERE `DutyNum` = ? AND `DutyOwner` = ?");

					nanodbc::date dutyDate = { (short)requestPacket.dutycommitdata().date().year(), (short)requestPacket.dutycommitdata().date().month(), (short)requestPacket.dutycommitdata().date().day() };
					query.bind<nanodbc::date>(0, &dutyDate);

					int dutyDayOff = requestPacket.dutycommitdata().dutydayoff();
					query.bind<int>(1, &dutyDayOff);

					int dutySplitDay = requestPacket.dutycommitdata().dutysplitday();
					query.bind<int>(2, &dutySplitDay);

					nanodbc::timestamp dutySart = { 
						(short)requestPacket.dutycommitdata().dutystarttime().year(),
						(short)requestPacket.dutycommitdata().dutystarttime().month(),
						(short)requestPacket.dutycommitdata().dutystarttime().day(),
						(short)requestPacket.dutycommitdata().dutystarttime().hour(),
						(short)requestPacket.dutycommitdata().dutystarttime().minutes() };
					query.bind<nanodbc::timestamp>(3, &dutySart);

					nanodbc::timestamp dutyMidEnd = {
						(short)requestPacket.dutycommitdata().dutymiddayend().year(),
						(short)requestPacket.dutycommitdata().dutymiddayend().month(),
						(short)requestPacket.dutycommitdata().dutymiddayend().day(),
						(short)requestPacket.dutycommitdata().dutymiddayend().hour(),
						(short)requestPacket.dutycommitdata().dutymiddayend().minutes() };
					query.bind<nanodbc::timestamp>(4, &dutyMidEnd);

					nanodbc::timestamp dutyMidStart = {
						(short)requestPacket.dutycommitdata().dutymiddaystart().year(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().month(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().day(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().hour(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().minutes() };
					query.bind<nanodbc::timestamp>(5, &dutyMidStart);

					nanodbc::timestamp dutyEnd = {
						(short)requestPacket.dutycommitdata().dutyendtime().year(),
						(short)requestPacket.dutycommitdata().dutyendtime().month(),
						(short)requestPacket.dutycommitdata().dutyendtime().day(),
						(short)requestPacket.dutycommitdata().dutyendtime().hour(),
						(short)requestPacket.dutycommitdata().dutyendtime().minutes() };
					query.bind<nanodbc::timestamp>(6, &dutyEnd);

					nDutyId = requestPacket.dutycommitdata().dutyid();
					query.bind<int64_t>(7, &nDutyId);
					query.bind<int>(8, &userAcc.nUserId);

					result = query.execute();
				}
				else // insert duty entry
				{
					query.prepare("INSERT INTO `Duties` (`DutyDate`, `DutyDayOff`, `DutySplit`, `DutyStart`, `DutyMiddayEnd`, `DutyMiddayStart`, `DutyEnd`, `DutyOwner`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

					nanodbc::date dutyDate = { (short)requestPacket.dutycommitdata().date().year(), (short)requestPacket.dutycommitdata().date().month(), (short)requestPacket.dutycommitdata().date().day() };
					query.bind<nanodbc::date>(0, &dutyDate);

					int dutyDayOff = requestPacket.dutycommitdata().dutydayoff();
					query.bind<int>(1, &dutyDayOff);

					int dutySplitDay = requestPacket.dutycommitdata().dutysplitday();
					query.bind<int>(2, &dutySplitDay);

					nanodbc::timestamp dutySart = {
						(short)requestPacket.dutycommitdata().dutystarttime().year(),
						(short)requestPacket.dutycommitdata().dutystarttime().month(),
						(short)requestPacket.dutycommitdata().dutystarttime().day(),
						(short)requestPacket.dutycommitdata().dutystarttime().hour(),
						(short)requestPacket.dutycommitdata().dutystarttime().minutes() };
					query.bind<nanodbc::timestamp>(3, &dutySart);

					nanodbc::timestamp dutyMidEnd = {
						(short)requestPacket.dutycommitdata().dutymiddayend().year(),
						(short)requestPacket.dutycommitdata().dutymiddayend().month(),
						(short)requestPacket.dutycommitdata().dutymiddayend().day(),
						(short)requestPacket.dutycommitdata().dutymiddayend().hour(),
						(short)requestPacket.dutycommitdata().dutymiddayend().minutes() };
					query.bind<nanodbc::timestamp>(4, &dutyMidEnd);

					nanodbc::timestamp dutyMidStart = {
						(short)requestPacket.dutycommitdata().dutymiddaystart().year(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().month(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().day(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().hour(),
						(short)requestPacket.dutycommitdata().dutymiddaystart().minutes() };
					query.bind<nanodbc::timestamp>(5, &dutyMidStart);

					nanodbc::timestamp dutyEnd = {
						(short)requestPacket.dutycommitdata().dutyendtime().year(),
						(short)requestPacket.dutycommitdata().dutyendtime().month(),
						(short)requestPacket.dutycommitdata().dutyendtime().day(),
						(short)requestPacket.dutycommitdata().dutyendtime().hour(),
						(short)requestPacket.dutycommitdata().dutyendtime().minutes() };
					query.bind<nanodbc::timestamp>(6, &dutyEnd);

					// Duty Owner
					query.bind<int>(7, &userAcc.nUserId);

					result = query.execute();

					auto resultSQLID = nanodbc::execute(*m_pDatabase, "SELECT LAST_INSERT_ID()");
					resultSQLID.next();

					auto resultID = resultSQLID.get<int>(0);
					nDutyId = resultID;
				}
				
				ProcessFlightLegs(requestPacket.dutycommitdata(), userAcc, nDutyId);

				// Commit all queries
				trans.commit();

				DataPacket packet;
				packet.set_messagetype(DataPacket::MESSAGE_RESULT);
				auto packetResult = packet.mutable_messageresult();
				packetResult->set_resulttype(Result::RESULT_GOOD_COMMIT_DUTY);

				BravoDate dutyDate = { (unsigned int)requestPacket.dutycommitdata().date().year(), (unsigned int)requestPacket.dutycommitdata().date().month(), (unsigned int)requestPacket.dutycommitdata().date().day() };

				SERVER_OUT("Duty for %s has been updated by [USER]:%s, [IP]:%s\n",
					dutyDate.GetDateFmt().c_str(),
					userAcc.User.c_str(),
					client->GetSocket()->remote_endpoint().address().to_string().c_str());

				SendMsg(packet, client);
			}
			else // No login found
				SendClientServerError(client, BravoServerError::SERVER_UNKNOWN_LOGIN_SESSION);

		}
		catch (const nanodbc::database_error &error)
		{
			std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
			SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
			return;
		}

	}

	void Server::ProcessFlightLegs(const NetworkPackets::Duties::Duty & fligtLegsPcket, const UserAccount & usrAcct, const int64_t nDutyId)
	{
		std::vector<int64_t> legIdVector;
		nanodbc::statement sqlStatement(*m_pDatabase);
		sqlStatement.prepare("SELECT `LegNum` FROM `FlightLegs` WHERE `LegDutyId` = ? AND `LegDutyOwner` = ? AND `LegDeleted` = FALSE");

		sqlStatement.bind<int64_t>(0, &nDutyId);
		sqlStatement.bind<int>(1, &usrAcct.nUserId); // LegOwner = leg user id

		auto result = sqlStatement.execute();

		while (result.next())
		{
			auto legId = result.get<int64_t>(0);
			legIdVector.push_back(legId);
		}

		std::vector<int64_t> foundLegs;
		// Check if the id from client is in the database
		auto flightLegs = fligtLegsPcket.flightlegs();
		for (auto it : flightLegs)
		{
			{
				nanodbc::statement statement(*m_pDatabase);
				statement.prepare("SELECT COUNT(*) FROM `FlightLegs`" \
					"WHERE `LegNum` = ? AND `LegDutyId` = ? AND `LegDeleted` = FALSE");

				int64_t nFlightLegId = it.flightlegid();
				statement.bind<int64_t>(0, &nFlightLegId);

				int64_t nduty = nDutyId;
				statement.bind<int64_t>(1, &nduty);

				result = statement.execute();
			}

			if (result.next())
			{
				auto flightLegCount = result.get<int>(0);
				if (flightLegCount > 0)
				{
					// Update the flight leg
					nanodbc::statement statement(*m_pDatabase);
					statement.prepare("UPDATE `FlightLegs` SET `LegStartTime` = ?, `LegEndTime` = ?, `LegAircraft` = ?, `LegCrew` = ?, `LegRoute` = ?, `LegFlightNum` = ?, `LegMileage` = ? WHERE `LegNum` = ? AND `LegDutyOwner` = ?");

					nanodbc::timestamp startTime = { (short)it.starttime().year(), (short)it.starttime().month(), (short)it.starttime().day(),
					(short)it.starttime().hour(), (short)it.starttime().minutes() };
					statement.bind<nanodbc::timestamp>(0, &startTime);

					nanodbc::timestamp endTime = { (short)it.endtime().year(), (short)it.endtime().month(), (short)it.endtime().day(),
					(short)it.endtime().hour(), (short)it.endtime().minutes() };
					statement.bind<nanodbc::timestamp>(1, &endTime);

					statement.bind(2, it.aircraft().c_str());
					statement.bind(3, it.crew().c_str());
					statement.bind(4, it.route().c_str());
					statement.bind(5, it.fltnum().c_str());

					int64_t nFltMileage = it.mileage();
					statement.bind<int64_t>(6, &nFltMileage);

					int64_t nLegNum = it.flightlegid();
					statement.bind<int64_t>(7, &nLegNum);

					int64_t nAcctNum = usrAcct.nUserId;
					statement.bind<int64_t>(8, &nAcctNum);

					statement.execute();

					foundLegs.push_back(nLegNum);
				}
				else
				{
					// insert the flight leg
					nanodbc::statement statement(*m_pDatabase);
					statement.prepare("INSERT INTO `FlightLegs` (`LegStartTime`, `LegEndTime`, `LegAircraft`, `LegCrew`, `LegRoute`, `LegFlightNum`, `LegMileage`, `LegDutyOwner` , `LegDutyId`) VALUES (?, ? ,? ,? ,? ,? ,?, ?, ?)");

					nanodbc::timestamp startTime = { (short)it.starttime().year(), (short)it.starttime().month(), (short)it.starttime().day(),
					(short)it.starttime().hour(), (short)it.starttime().minutes() };
					statement.bind<nanodbc::timestamp>(0, &startTime);

					nanodbc::timestamp endTime = { (short)it.endtime().year(), (short)it.endtime().month(), (short)it.endtime().day(),
					(short)it.endtime().hour(), (short)it.endtime().minutes() };
					statement.bind<nanodbc::timestamp>(1, &endTime);

					statement.bind(2, it.aircraft().c_str());
					statement.bind(3, it.crew().c_str());
					statement.bind(4, it.route().c_str());
					statement.bind(5, it.fltnum().c_str());

					int64_t nFltMileage = it.mileage();
					statement.bind<int64_t>(6, &nFltMileage);

					int64_t nAcctNum = usrAcct.nUserId;
					statement.bind<int64_t>(7, &nAcctNum);

					int64_t nduty = nDutyId;
					statement.bind<int64_t>(8, &nduty);

					statement.execute();
				}
			}

		}

		for (auto it : legIdVector)
		{
			auto found = std::find(foundLegs.begin(), foundLegs.end(), it);
			if (found == foundLegs.end())
			{
				nanodbc::statement statement(*m_pDatabase);
				statement.prepare("UPDATE `FlightLegs` SET `LegDeleted` = TRUE WHERE `LegNum` = ?");

				statement.bind<int64_t>(0, &it);
				statement.execute();
			}
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MSG_REQUEST_ENTRIES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Server::MSG_REQUEST_ENTRIES(const NetworkPackets::Request & requestPacket, Network::Session * client)
	{
		nanodbc::result result;
		UserAccount userAcc;

		try {
			if (!m_pDatabase->connected())
				ReconnectToDatabase();

			auto strLoginSessionID = requestPacket.session().sessionid();
			if (LoggedInSessions.FindAccount(strLoginSessionID, userAcc))
			{
				nanodbc::statement query(*m_pDatabase);

				query.prepare("SELECT * FROM `Entries` WHERE `EntryOwner` = ? ORDER BY `EntryDate` DESC");
				query.bind<int>(0, &userAcc.nUserId);

				result = query.execute();
			}
			else
				SendClientServerError(client, BravoServerError::SERVER_UNKNOWN_LOGIN_SESSION);

		}
		catch (const nanodbc::database_error &error)
		{
			std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
			SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
			return;
		}

		//Send Results back to the waiting client
		DataPacket packet;
		packet.set_messagetype(DataPacket::MESSAGE_ENTRY);
		auto msgEntries = packet.mutable_messageentries();
		msgEntries->set_entrytype(Entries::ENTRY_VERSION_NORMAL);

		while (result.next())
		{
			auto entry = msgEntries->add_entries();

			entry->set_id(result.get<int>(0));
			nanodbc::date date = result.get<nanodbc::date>(2);
			auto dateLong = BravoDataEntry::DateToLong((short)date.month, (short)date.day, (short)date.year);
			entry->set_date(dateLong);
			entry->set_cplanetype(result.get<std::string>(3));
			entry->set_cplanereg(result.get<std::string>(4));
			entry->set_cpilotname(result.get<std::string>(5));
			entry->set_cpax(result.get<std::string>(6));
			entry->set_strremarks(result.get<std::string>(7));

			entry->set_se_day_dual(result.get<float>(8));
			entry->set_se_night_dual(result.get<float>(9));
			entry->set_se_day_pic(result.get<float>(10));
			entry->set_se_night_pic(result.get<float>(11));

			entry->set_me_day_dual(result.get<float>(12));
			entry->set_me_night_dual(result.get<float>(13));
			entry->set_me_day_pic(result.get<float>(14));
			entry->set_me_night_pic(result.get<float>(15));
			entry->set_me_day_sic(result.get<float>(16));
			entry->set_me_night_sic(result.get<float>(17));

			entry->set_xcun_day_dual(result.get<float>(18));
			entry->set_xcun_day_pic(result.get<float>(19));
			entry->set_xcun_night_dual(result.get<float>(20));
			entry->set_xcun_night_pic(result.get<float>(21));

			entry->set_nto_land_day(result.get<int>(22));
			entry->set_nto_land_night(result.get<int>(23));

			entry->set_ifr_approach(result.get<int>(24));
			entry->set_sim_time(result.get<float>(25));
			entry->set_hood_time(result.get<float>(26));
			entry->set_actual_time(result.get<float>(27));
		}

		SERVER_OUT("Entries [TOTAL]:%d, Requested by [USER]:%s, [IP]:%s\n",
			msgEntries->entries().size(),
			userAcc.User.c_str(),
			client->GetSocket()->remote_endpoint().address().to_string().c_str());
		SendMsg(packet, client);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MSG_REQUEST_PREVIOUS_ENTRIES
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void Server::MSG_REQUEST_PREVIOUS_ENTRIES(const NetworkPackets::Request & requestPacket, Network::Session * client)
	{
		nanodbc::result result;
		UserAccount userAcc;

		try {
			if (!m_pDatabase->connected())
				ReconnectToDatabase();

			auto strLoginSessionID = requestPacket.session().sessionid();
			if (LoggedInSessions.FindAccount(strLoginSessionID, userAcc))
			{
				nanodbc::statement query(*m_pDatabase);
				int entryId = (int)requestPacket.entrynum();

				query.prepare("SELECT * FROM `PreviousEntries` WHERE `EntryOwner` = ? AND `EntryNum` = ?");
				query.bind<int>(0, &userAcc.nUserId);
				query.bind<int>(1, &entryId);

				result = query.execute();
			}
			else
			{
				// TODO: Add Handler for unknown login
			}

		}
		catch (const nanodbc::database_error &error)
		{
			std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
			SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
			return;
		}

		//Send Results back to the waiting client
		DataPacket packet;
		packet.set_messagetype(DataPacket::MESSAGE_ENTRY);
		auto msgEntries = packet.mutable_messageentries();
		msgEntries->set_entrytype(Entries::ENTRY_PREVIOUS_VERSION);

		while (result.next())
		{
			auto entry = msgEntries->add_entries();

			entry->set_id(result.get<int>(1));
			nanodbc::date moddate = result.get<nanodbc::date>(3);
			auto datemodLong = BravoDataEntry::DateToLong((short)moddate.month, (short)moddate.day, (short)moddate.year);
			entry->set_moddate(datemodLong);
			nanodbc::date date = result.get<nanodbc::date>(4);
			auto dateLong = BravoDataEntry::DateToLong((short)date.month, (short)date.day, (short)date.year);
			entry->set_date(dateLong);
			entry->set_cplanetype(result.get<std::string>(5));
			entry->set_cplanereg(result.get<std::string>(6));
			entry->set_cpilotname(result.get<std::string>(7));
			entry->set_cpax(result.get<std::string>(8));
			entry->set_strremarks(result.get<std::string>(9));

			entry->set_se_day_dual(result.get<float>(10));
			entry->set_se_night_dual(result.get<float>(11));
			entry->set_se_day_pic(result.get<float>(12));
			entry->set_se_night_pic(result.get<float>(13));

			entry->set_me_day_dual(result.get<float>(14));
			entry->set_me_night_dual(result.get<float>(15));
			entry->set_me_day_pic(result.get<float>(16));
			entry->set_me_night_pic(result.get<float>(17));
			entry->set_me_day_sic(result.get<float>(18));
			entry->set_me_night_sic(result.get<float>(19));

			entry->set_xcun_day_dual(result.get<float>(20));
			entry->set_xcun_day_pic(result.get<float>(21));
			entry->set_xcun_night_dual(result.get<float>(22));
			entry->set_xcun_night_pic(result.get<float>(23));

			entry->set_nto_land_day(result.get<int>(24));
			entry->set_nto_land_night(result.get<int>(25));

			entry->set_ifr_approach(result.get<int>(26));
			entry->set_sim_time(result.get<float>(27));
			entry->set_hood_time(result.get<float>(28));
			entry->set_actual_time(result.get<float>(29));
		}

		SERVER_OUT("Previous Entries Request of [ID]:%d [TOTAL]:%d, Requested by [USER]:%s, [IP]:%s\n",
			(int)requestPacket.entrynum(),
			msgEntries->entries().size(),
			userAcc.User.c_str(),
			client->GetSocket()->remote_endpoint().address().to_string().c_str());
		SendMsg(packet, client);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// MSG_ADD_ENTRY
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// TODO: ADD ERROR HANDLER IF INSERT FAILS
	void Server::MSG_ADD_ENTRY(const NetworkPackets::Request & requestPacket, Network::Session * client)
	{
		auto entry = requestPacket.entrydata();

		nanodbc::result result;
		UserAccount userAcc;

		try {
			if (!m_pDatabase->connected())
				ReconnectToDatabase();

			auto strLoginSessionID = requestPacket.session().sessionid();
			if (LoggedInSessions.FindAccount(strLoginSessionID, userAcc))
			{
				nanodbc::statement query(*m_pDatabase);

				query.prepare("INSERT INTO `Entries`(`EntryOwner`, `EntryDate`, `PlaneType`, `PlaneReg`, `PilotName`, `PaxName`, `Remarks`, `SE_DAY_DUAL`, `SE_NIGHT_DUAL`, `SE_DAY_PIC`, `SE_NIGHT_PIC`, `ME_DAY_DUAL`, `ME_NIGHT_DUAL`, `ME_DAY_PIC`, `ME_NIGHT_PIC`, `ME_DAY_SIC`, `ME_NIGHT_SIC`, `XCUN_DAY_DUAL`, `XCUN_NIGHT_DUAL`, `XCUN_DAY_PIC`, `XCUN_NIGHT_PIC`, `TO_LAND_DAY`, `TO_LAND_NIGHT`, `IFR_APPROACH`, `SIM_TIME`, `HOOD_TIME`, `ACTUAL_TIME`) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				query.bind<int>(0, &userAcc.nUserId);

				unsigned short month, day;
				unsigned int year;
				BravoDataEntry::LongToDate((long)entry.date(), month, day, year);
				nanodbc::date dateData = { (short)year, (short)month, (short)day };
				query.bind<nanodbc::date>(1, &dateData);

				std::string cplanetype = entry.cplanetype();
				std::string cplanereg = entry.cplanereg();
				std::string cpilotname = entry.cpilotname();
				std::string cpax = entry.cpax();
				std::string strremarks = entry.strremarks();

				query.bind(2, cplanetype.c_str());
				query.bind(3, cplanereg.c_str());
				query.bind(4, cpilotname.c_str());
				query.bind(5, cpax.c_str());
				query.bind(6, strremarks.c_str());

				float se_day_dual = entry.se_day_dual();
				float se_night_dual = entry.se_night_dual();
				float se_day_pic = entry.se_day_pic();
				float se_night_pic = entry.se_night_pic();

				float me_day_dual = entry.me_day_dual();
				float me_night_dual = entry.me_night_dual();
				float me_day_pic = entry.me_day_pic();
				float me_night_pic = entry.me_night_pic();
				float me_day_sic = entry.me_day_sic();
				float me_night_sic = entry.me_night_sic();

				float xcun_day_dual = entry.xcun_day_dual();
				float xcun_day_pic = entry.xcun_day_pic();
				float xcun_night_dual = entry.xcun_night_dual();
				float xcun_night_pic = entry.xcun_night_pic();

				int nto_land_day = entry.nto_land_day();
				int nto_land_night = entry.nto_land_night();

				int ifr_approach = entry.ifr_approach();
				float sim_time = entry.sim_time();
				float hood_time = entry.hood_time();
				float actual_time = entry.actual_time();
				query.bind<float>(7, &se_day_dual);
				query.bind<float>(8, &se_night_dual);
				query.bind<float>(9, &se_day_pic);
				query.bind<float>(10, &se_night_pic);

				query.bind<float>(11, &me_day_dual);
				query.bind<float>(12, &me_night_dual);
				query.bind<float>(13, &me_day_pic);
				query.bind<float>(14, &me_night_pic);
				query.bind<float>(15, &me_day_sic);
				query.bind<float>(16, &me_night_sic);

				query.bind<float>(17, &xcun_day_dual);
				query.bind<float>(18, &xcun_day_pic);
				query.bind<float>(19, &xcun_night_dual);
				query.bind<float>(20, &xcun_night_pic);

				query.bind<int>(21, &nto_land_day);
				query.bind<int>(22, &nto_land_night);

				query.bind<int>(23, &ifr_approach);
				query.bind<float>(24, &sim_time);
				query.bind<float>(25, &hood_time);
				query.bind<float>(26, &actual_time);

				result = query.execute();
			}
			else
				SendClientServerError(client, BravoServerError::SERVER_UNKNOWN_LOGIN_SESSION);

		}
		catch (const nanodbc::database_error &error)
		{
			std::cout << "[DATABASE ERROR]: [CODE]: " << error.native() << " " << error.what() << std::endl;
			SendClientServerError(client, BravoServerError::SERVER_DB_ERROR);
			return;
		}

		DataPacket packet;
		packet.set_messagetype(DataPacket::MESSAGE_RESULT);
		auto packetResult = packet.mutable_messageresult();
		packetResult->set_resulttype(Result::RESULT_GOOD_ADD_ENTRY);

		SERVER_OUT("Entry from [USER]:%s has been added to the DB, [IP]:%s\n",
			userAcc.User.c_str(),
			client->GetSocket()->remote_endpoint().address().to_string().c_str());

		SendMsg(packet, client);
	}
}
