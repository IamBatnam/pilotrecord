#ifndef BRAVO_UPDATE_H
#define BRAVO_UPDATE_H


#if defined(_MSC_VER)
#pragma once
#endif

#include "BravoClient.h"
#include "BravoErrorHandler.h"

namespace Bravo
{
	class BravoUpdate
	{
	public:
		BravoUpdate(BravoClient *pNetClient, BravoErrorHandler *pErrorModule);

		void getUpdateUrl(std::string &strURL);
	protected:
		BravoClient *m_pNetClient;
		BravoErrorHandler *m_pErrorModule;
	};
}

#endif // !BRAVO_UPDATE_H

