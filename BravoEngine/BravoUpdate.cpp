#include "BravoUpdate.h"
#include "BravoDef.h"

namespace Bravo {
	BravoUpdate::BravoUpdate(BravoClient * pNetClient, BravoErrorHandler *pErrorModule)
		: m_pNetClient(pNetClient)
		, m_pErrorModule(pErrorModule)
	{
	}

	void BravoUpdate::getUpdateUrl(std::string & strURL)
	{
		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);

		auto updateRqMsg = packet.mutable_messagerequest();

		updateRqMsg->set_reqtype(NetworkPackets::Request::MSG_REQUEST_UPDATE);
		updateRqMsg->set_version(BRAVO_CLIENT_VERSION);

		m_pNetClient->SendMsg(packet);

		auto result = m_pNetClient->GetMessage(NetworkPackets::DataPacket::MESSAGE_RESULT);
		strURL = result.messageresult().urlmsg().strurl();
	}
}