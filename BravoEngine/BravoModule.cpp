#include "BravoModule.h"
#include "BravoDatabase.h"
#include "BravoErrorHandler.h"


namespace Bravo 
{
	BravoModule::BravoModule()
		: m_pDatabase(nullptr)
		, m_pErrorModule(nullptr)
	{
	}


	BravoModule::~BravoModule()
	{
	}
}