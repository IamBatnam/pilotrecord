/*  BRAVO DATABASE MODULE
* Database Object that handles Client-Server Interaction and Data Management
*
*/
#ifndef BRAVO_DATABASE_H
#define BRAVO_DATABASE_H

#if defined(_MSC_VER)
#pragma once
#endif

//#include "BravoDatabaseFile.h"
#include "BravoClient.h"
#include "BravoDBStructure.h"
#include <vector>
#include "BravoModule.h"

namespace Bravo
{
	typedef std::vector<UserAccount> LoginDB;
	class BravoDatabase : /*public BravoDatabaseFile,*/ public BravoModule
	{
	public:
		BravoDatabase(BravoClient *client, BravoErrorHandler *pError);
		~BravoDatabase();

		bool Init();
		void Logout();

//////////////////////////////////////////////////////////////////
// User
//////////////////////////////////////////////////////////////////
		void AddLoginUser(const UserAccount &User);
		std::vector<UserAccount> &RetrieveUsers();
		bool CheckLogin(const std::string &strUser, const std::string &strPass);
		bool FindUser(const std::string &strUser);

//////////////////////////////////////////////////////////////////
// Entry
//////////////////////////////////////////////////////////////////
		void AddEntry(const BravoDataEntry &entry);
		void UpdateEntry(const BravoDataEntry &entry);
		std::vector<BravoDataEntry> FetchEntries() const;
		template<typename T> void FetchInitialEntries(T &func);
		void FetchPreviousEntries(std::vector<PreviousVersionDataEntry> &prevEntryArray, int nEntryId);

//////////////////////////////////////////////////////////////////
// Duties
//////////////////////////////////////////////////////////////////
        template <typename T> 
		void FetchDutiesMinimal(
			const BravoDate &startDateRange, 
			const BravoDate &endDateRange, 
			void *pObj,
			T& func); // Get range date

		int FetchDutiesMinimal(
			const BravoDate &startDateRange, 
			const BravoDate &endDateRange); // Get range date
        
        bool FetchableDutiesMinimalCache(const BravoDate &startDateRange, const BravoDate &endDateRange);
        

        const std::vector<BravoDataDuty> GetDutyContainer();
		BravoDataDuty *GetDuty(const BravoDate &date);
		bool CommitDuty(const BravoDataDuty &duty);

	protected:
		
		
		BravoClient *mClient;
		DatabaseData dbConfig; // Holds all the data
		LoginSession *activeUserSession;
	};

	//////////////////////////////////////////////////////////////////
	// Template Methods
	//////////////////////////////////////////////////////////////////
	template<typename T>
	inline void BravoDatabase::FetchInitialEntries(T & func)
	{
		if (mClient->IsConnected())
		{
			auto entryPackets = mClient->RetrieveEntries(activeUserSession);
			auto entry = entryPackets.messageentries();

			dbConfig.entries.clear();
			dbConfig.nTotalEntries = 0;

			for (auto ent : entry.entries())
			{
				BravoDataEntry strucEntry;
				strucEntry.ConvertPacketToEntry(ent);

				dbConfig.entries.push_back(strucEntry);
				func(strucEntry);
			}
		}
	}

	template<typename T>
	inline void BravoDatabase::FetchDutiesMinimal(
		const BravoDate & startDateRange, 
		const BravoDate & endDateRange, 
		void* pObj,
		T& func)
	{
		// if buffer is empty get from server
		if (mClient->IsConnected())
		{
			auto dutyPackets = mClient->RetrieveDuties(activeUserSession, startDateRange, endDateRange);
			auto msgDuties = dutyPackets.messageduties();

			dbConfig.duties.clear();

			for (auto it : msgDuties.duties())
			{
				BravoDataDuty dutyStruct;
				dutyStruct.ConvertPacketToDuty(it);

				dbConfig.duties.push_back(dutyStruct);


				if(pObj != nullptr)
					func(pObj,
						{ (int)dutyStruct.date.year, (int)dutyStruct.date.month, (int)dutyStruct.date.day },
							{
								{ dutyStruct.startDutyTime.year, dutyStruct.startDutyTime.month, dutyStruct.startDutyTime.day },
								{ dutyStruct.startDutyTime.hour, dutyStruct.startDutyTime.min }
							},
							{
								{ dutyStruct.endDutyTime.year, dutyStruct.endDutyTime.month, dutyStruct.endDutyTime.day },
								{ dutyStruct.endDutyTime.hour, dutyStruct.endDutyTime.min }
							},
						dutyStruct.dayOffDuty);
			}
		}

		
	}

}
#endif // !BRAVO_DATABASE_H
