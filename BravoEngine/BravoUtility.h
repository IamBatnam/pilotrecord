#ifndef BRAVO_UTILITY_H
#define BRAVO_UTILITY_H

#ifdef _MSC_VER 
	#pragma once  
#endif

#ifdef __linux__
#define BRAVO_LINUX
#endif

#ifdef _WIN32
#define BRAVO_WINDOWS
#endif

#ifdef BRAVO_LINUX
#include <stdio.h>
#endif

#include <string>
#include <memory>
#include <functional>
#include <system_error>
#include <list>
#include <chrono>
#include <ctime>

#define MAX_STR_CONTENT 256

namespace Bravo {

	/*@ String copy. Provides a new buffer
	*	for strings.
	*/
	void StringCopy(char *dest, const char *src);


	/*@ StrLen returns length of a string
	*/
	size_t StrLen(const char *str);

	/*@ Sprint function that could be used within
	*	the engine. Used to format strings
	*/
	template <typename... Args>
	inline std::string Sprint(const char *sFormat, Args&&... args)
	{
		char *sReturn = new char[MAX_STR_CONTENT];
#ifdef BRAVO_WINDOWS
		sprintf_s(sReturn, MAX_STR_CONTENT, sFormat, std::forward<Args>(args)...);
#else
		sprintf(sReturn, sFormat, std::forward<Args>(args)...);
#endif
		return std::string(sReturn);
	}

	// Returns void
	template <typename... Args>
	inline void Sprint(char *dst, const char *sFormat, Args&&... args)
	{
		char sReturn[MAX_STR_CONTENT];
#ifdef BRAVO_WINDOWS
		sprintf_s(&sReturn, MAX_STR_CONTENT, sFormat, std::forward<Args>(args)...);
#else
		sprintf(&sReturn, sFormat, std::forward<Args>(args)...);
#endif
		StringCopy(dst, sReturn);
	}

	namespace Util {
		/* Declaration of DateToStr
		*	Provides string format of date
		*/
		std::string GetDateTimeStr(const std::string &strFormat, const std::tm &time);

		/*@ Provides bind function to our namespace
		* alias of std::bind; we alias this
		* to have possibility of porting to boost???
		*/
		template <typename... Args>
		auto bind(Args&&... args) -> decltype(std::bind(std::forward<Args>(args)...)) {
			return std::bind(std::forward<Args>(args)...);
		}


		///*@ Converst int and floats to string
		//*/
		//static char *IntToChar(int nNum)
		//{
		//	char *cText = new char[MAX_STR_CONTENT];
		//	Sprint(cText, "%i", nNum);
		//	return cText;
		//}

		//static char *FloatToChar(double fNum)
		//{
		//	char *cText = new char[MAX_STR_CONTENT];
		//	Sprint(cText, "%f", fNum);
		//	return cText;
		//}


		/*@ Converts string to int and float
		*/
		static double CharToFloat(const char * fChar)
		{
			return atof(fChar);
		}

		template <typename... Args>
		auto CharToInt(Args&&... args) -> decltype(atoi(std::forward<Args>(args)...)) {
			return atoi(std::forward<Args>(args)...);
		}

		//static std::string GetDateTime(const std::string &strFormat, const std::tm &time)
		//{
		//	std::unique_ptr<char[]> strTime(new char[MAX_STR_CONTENT]);
		//	auto sampletime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

		//	// Format ("Mon MmmDDYYYY 00:00:00")
		//	strftime(strTime.get(), MAX_STR_CONTENT, strFormat.c_str(), &time);

		//	return std::string(strTime.get());
		//}

		static std::string GetDateTime()
		{
			std::unique_ptr<char []> strTime(new char[MAX_STR_CONTENT]);
			auto sampletime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

			std::tm time;
#ifdef BRAVO_WINDOWS
			localtime_s(&time, &sampletime);
#else
			time = *(std::localtime(&sampletime));
#endif
			// Format ("Mon MmmDDYYYY 00:00:00")
			strftime(strTime.get(), MAX_STR_CONTENT, "%a %b%d%Y %T", &time);

			return std::string(strTime.get());
		}
	}

	template <typename T>
	class Enable_Shared_From_This {
	public:
		typedef std::enable_shared_from_this<T> std;
	};

	template <typename T>
	class Shared_ptr {
	public:
		typedef std::shared_ptr<T> std;
	};

	template <typename T, typename... Args>
	auto Make_Shared(Args&&... args) -> decltype(std::make_shared<T>(std::forward<Args>(args)...)) {
		return std::make_shared<T>(std::forward<Args>(args)...);
	}

	typedef std::error_code ErrorHandler;
}

#ifndef ASSERT
#define ASSERT assert
#endif

#ifdef DEBUG
	#define DEBUG_PRINT(fmt, ...) printf(fmt, ##__VA_ARGS__)

	#ifdef BRAVO_WINDOWS
		#pragma warning(push)
		#pragma warning(disable: 4996)

		#include <tchar.h>
		#include <sstream>
		#include <string>
		#include <stdarg.h>
		

		inline std::string string_format(const std::string fmt_str, ...) {
			int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
			std::unique_ptr<char[]> formatted;
			va_list ap;

			while (1) {
				formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
				strcpy(&formatted[0], fmt_str.c_str());
				va_start(ap, fmt_str);
				final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
				va_end(ap);
				if (final_n < 0 || final_n >= n)
					n += abs(final_n - n + 1);
				else
					break;
			}
			return std::string(formatted.get());
		}
	
		typedef std::basic_stringstream<TCHAR> tstringstream;
		template<typename QQQ> tstringstream& operator,(tstringstream& tss, QQQ t) { tss << _T(" ") << t; return tss; }
		#define OUTPUT_DEBUG(fmt, ...) ::OutputDebugString((tstringstream(), _T("[DEBUG OUTPUT]: "), string_format(fmt, __VA_ARGS__).c_str(), _T("\n")).str().c_str());
		#pragma warning(pop)
	#else
		#define OUTPUT_DEBUG(frmt, ...) printf(frmt, ##__VA_ARGS__)
	#endif
#else
	#define DEBUG_PRINT(fmt, ...)    /* Don't do anything in release builds */
	#define OUTPUT_DEBUG(...)
#endif



#define SERVER_OUT(fmt, ...) printf("[SERVER][%s]: " fmt, Bravo::Util::GetDateTime().c_str(), ##__VA_ARGS__)

#endif // !BRAVO_UTILITY_H

