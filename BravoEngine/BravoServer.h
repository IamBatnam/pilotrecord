#ifndef BRAVO_SERVER_H
#define BRAVO_SERVER_H

#if defined(_MSC_VER) 
#pragma once
#endif

#include "BravoNetwork.h"
#include <mutex>
#include <atomic>

#include "BravoServerConfigFile.h"
#include "BravoServerLoginSessions.h"
#include "BravoServerTerm.h"
#include "BravoErrorHandler.h"

namespace nanodbc { class connection; }

namespace Bravo {
	class Server : public Network::ServerSocket
	{
		friend ServerTerminal;
	public:
		Server();
		~Server();

		void Start();
		bool isRunning();
		void Shutdown();

		void SendPacket(const NetworkPackets::DataPacket &packetData);
	protected:
		
		void OnAfterConnect(Network::Session *client) override;
		void MessageSorter(const NetworkPackets::DataPacket &pckData, Network::Session *client);
		void ErrorHandler(const Bravo::ErrorHandler &error, Network::Session *client = nullptr);

		void ProcessLoginRequest(const NetworkPackets::Login &loginPacket, Network::Session *client);

		void ProcessRequests(const NetworkPackets::Request &requestPacket, Network::Session *client);

		// Duties
		void MSG_REQUEST_DUTIES(const NetworkPackets::Request &requestPacket, Network::Session *client);
		void MSG_COMMIT_DUTIES(const NetworkPackets::Request &requestPacket, Network::Session *client);
		void ProcessFlightLegs(const NetworkPackets::Duties::Duty &fligtLegsPcket, const UserAccount& usrAcct, const int64_t nDutyId);

		// Entries
		void MSG_REQUEST_ENTRIES(const NetworkPackets::Request &requestPacket, Network::Session *client);
		void MSG_REQUEST_PREVIOUS_ENTRIES(const NetworkPackets::Request &requestPacket, Network::Session *client);
		void MSG_ADD_ENTRY(const NetworkPackets::Request &requestPacket, Network::Session *client);


		void ReconnectToDatabase();
		void SendClientServerError(Network::Session *client, unsigned int nErrorVal = BravoServerError::SERVER_UNKNOWN_ERROR, std::string strMsg = std::string());

	private:
		void CloseServerMsg();
		void CheckAliveClients();
		void Check(int n);
	

	private:
		nanodbc::connection *m_pDatabase;
		std::mutex genRandom_mutex;

		BravoServerConfigFile serverConfig;
		ServerLoginSessions LoggedInSessions;
		ServerTerminal	*m_pTerminalCommands;
		std::atomic<bool> _bServer;
	};
}
  
#endif // BRAVO_SERVER_H
