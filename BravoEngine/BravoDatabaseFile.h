#ifndef BRAVO_DATABASE_FILE_H
#define BRAVO_DATABASE_FILE_H

#ifdef _MSC_VER
#pragma once  
#endif // _MSC_VER

#include "BravoFileHandler.h"
#include "BravoDBStructure.h"
#include <vector>

namespace Bravo
{
	class BravoDatabaseFile
	{
	public:
		BravoDatabaseFile();
		~BravoDatabaseFile();

		bool Init();
	protected:
		bool CheckFileExist();
		void Create();
		void ReadData(DatabaseData &config);
		void SaveData(DatabaseData &config);

		// OFFLINE DATABASE SEARCH
		bool FindUser(const std::string &strUser);
		UserAccount *FindUser(const std::string &strUser, const std::string &strPass);

		void CloseDatabase();

	protected:
		DatabaseData dbConfig;;
	
	private:
		BravoFileHandler *_file;
	};
}

#endif // !BRAVO_DATABASE_FILE_H