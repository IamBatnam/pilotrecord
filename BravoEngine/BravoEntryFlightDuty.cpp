#include "BravoEntryFlightDuty.h"

namespace Bravo
{
	BravoEntryFlightDuty::BravoEntryFlightDuty()
		: pActiveDuty(nullptr)
		, bActiveDutyChanged(false)
	{
	}


	BravoEntryFlightDuty::~BravoEntryFlightDuty()
	{
		if (pActiveDuty)
			delete pActiveDuty;
	}
    
    /**
     * @Brief: Get the Duties to be loaded in memory and save the requested start and end date
     * @Param:
     *  - startDate: start of range
     *  - endDate: end of range
     *  - bAbleCached: determines whether to grab cahced or not - DEFAULT: true
    */
    int BravoEntryFlightDuty::GetDutiesMinInfoContained(const BravoDate& startDate, const BravoDate& endDate, bool bAbleCached)
    {
        if (m_pDatabase == nullptr)
            return -1;
        
        requestedStartDate = startDate;
        requestedEndDate = endDate;
        
        // check if cache is available
        if(bAbleCached && m_pDatabase->FetchableDutiesMinimalCache(startDate, endDate))
            return m_pDatabase->GetDutyContainer().size();

        return m_pDatabase->FetchDutiesMinimal(startDate, endDate);
    }

    /**
     *  @Brief: Returns a copy of the flight duty min info
     *  @return: BravoDataEntry construct
     */
    BravoDataDuty BravoEntryFlightDuty::AccessDutyMinInfo(const BravoDate& date)
    {
        for(auto it : m_pDatabase->GetDutyContainer())
        {
            if(it.date == date)
                return it;
        }
        
        return BravoDataDuty();
    }

	void BravoEntryFlightDuty::CommitActiveDuty()
	{
		if (m_pDatabase->CommitDuty(*pActiveDuty))
		{
			setActiveDutyChanged(false);

			// get new data from previous requested data
			GetDutiesMinInfoContained(requestedStartDate, requestedEndDate, false);
		}
	}

	void BravoEntryFlightDuty::AddDutyLeg(const BravoTimestamp startTime, 
		const BravoTimestamp endTime,
		const std::string strAircraft,
		const std::string strCrew, 
		const std::string strRoute, 
		const std::string strFltNum, 
		const int nMileage)
	{
		if (!pActiveDuty)
		{
			// m_pErrorModule->Error()
			ASSERT(0);
			return;
		}


		if (!pActiveDuty->flightLegs)
		{
			// m_pErrorModule->Error()
			ASSERT(0);
			return;
		}

		BravoDataFlightLeg duty;
		duty.flightStart = const_cast<BravoTimestamp&>(startTime);
		duty.flightEnd = const_cast<BravoTimestamp&>(endTime);
		duty.strAircraft = strAircraft;
		duty.strCrew = strCrew;
		duty.strFlightRoute = strRoute;
		duty.strFlightNum = strFltNum;
		duty.nMileage = nMileage;

		pActiveDuty->flightLegs->push_back(duty);
		setActiveDutyChanged(true);
	}

	bool BravoEntryFlightDuty::DeleteDutyLeg(const int nIndex)
	{
		if (!pActiveDuty && !pActiveDuty->flightLegs)
			return false;
		
		if(nIndex >= pActiveDuty->flightLegs->size())
			return false;

		
		pActiveDuty->flightLegs->erase(pActiveDuty->flightLegs->begin() + nIndex);
		setActiveDutyChanged(true);
		return true;
	}

	void BravoEntryFlightDuty::UpdateDutyLeg(int nIndex,
		const BravoTimestamp startTime,
		const BravoTimestamp endTime,
		const std::string strAircraft,
		const std::string strCrew,
		const std::string strRoute,
		const std::string strFltNum,
		const int nMileage)
	{
		if (!pActiveDuty)
		{
			// m_pErrorModule->Error()
			ASSERT(0);
			return;
		}


		if (!pActiveDuty->flightLegs)
		{
			// m_pErrorModule->Error()
			ASSERT(0);
			return;
		}

		auto &currentLeg = (*pActiveDuty->flightLegs)[nIndex];

		if (currentLeg.flightStart != startTime || currentLeg.flightEnd != endTime ||
			currentLeg.strAircraft != strAircraft || currentLeg.strCrew != strCrew ||
			currentLeg.strFlightRoute != strRoute || currentLeg.strFlightNum != strFltNum ||
			currentLeg.nMileage != nMileage)
			setActiveDutyChanged(true);

		currentLeg.flightStart = const_cast<BravoTimestamp&>(startTime);
		currentLeg.flightEnd = const_cast<BravoTimestamp&>(endTime);
		currentLeg.strAircraft = strAircraft;
		currentLeg.strCrew = strCrew;
		currentLeg.strFlightRoute = strRoute;
		currentLeg.strFlightNum = strFltNum;
		currentLeg.nMileage = nMileage;
	}

/*  SetActiveDuty
 *  @Brief - Sets the active duty in the engine for I/O of Duty
 */
	void BravoEntryFlightDuty::SetActiveDuty(unsigned int month, unsigned int day, unsigned int year)
	{
		if (pActiveDuty)
			delete pActiveDuty;

		pActiveDuty = m_pDatabase->GetDuty({year, month, day});

		if (!pActiveDuty) {
			ASSERT(0);
			//m_pErrorModule->Error();
		}

		setActiveDutyChanged(false);
	}

	BravoTimestamp &BravoEntryFlightDuty::getActiveDutyStartTime()
	{
		return pActiveDuty->startDutyTime;
	}

	BravoTimestamp &BravoEntryFlightDuty::getActiveDutyMiddayEndTime()
	{
		return pActiveDuty->endMiddayDutyTime;
	}

	BravoTimestamp &BravoEntryFlightDuty::getActiveDutyMiddayStartTime()
	{
		return pActiveDuty->startMiddayDutyTime;
	}

	BravoTimestamp &BravoEntryFlightDuty::getActiveDutyEndTime()
	{
		return pActiveDuty->endDutyTime;
	}

	bool BravoEntryFlightDuty::getDayOff()
	{
		return pActiveDuty->dayOffDuty;
	}

	bool BravoEntryFlightDuty::getActiveDutySplitDuty()
	{
		return pActiveDuty->dutySplit;
	}

	void BravoEntryFlightDuty::setActiveDutyStartTime(const BravoTimestamp &timestamp)
	{
		if (pActiveDuty->startDutyTime != timestamp)
			setActiveDutyChanged(true);
		pActiveDuty->startDutyTime = timestamp;
	}

	void BravoEntryFlightDuty::setActiveDutyMiddayEndTime(const BravoTimestamp & timestamp)
	{
		if (pActiveDuty->endMiddayDutyTime != timestamp)
			setActiveDutyChanged(true);
		pActiveDuty->endMiddayDutyTime = timestamp;
	}

	void BravoEntryFlightDuty::setActiveDutyMiddayStartTime(const BravoTimestamp &timestamp)
	{
		if (pActiveDuty->startMiddayDutyTime != timestamp)
			setActiveDutyChanged(true);
		pActiveDuty->startMiddayDutyTime = timestamp;
	}

	void BravoEntryFlightDuty::setActiveDutyEndTime(const BravoTimestamp & timestamp)
	{
		if (pActiveDuty->endDutyTime != timestamp)
			setActiveDutyChanged(true);
		pActiveDuty->endDutyTime = timestamp;
	}

	void BravoEntryFlightDuty::setActiveDutyDayOff(const bool & bStatus)
	{
		if (pActiveDuty->dayOffDuty != bStatus)
			setActiveDutyChanged(true);
		pActiveDuty->dayOffDuty = bStatus;
	}

	void BravoEntryFlightDuty::setActiveDutySplitDuty(const bool &bStatus)
	{
		if (pActiveDuty->dutySplit != bStatus)
			setActiveDutyChanged(true);
		pActiveDuty->dutySplit = bStatus;
	}

	bool BravoEntryFlightDuty::isActiveDutyChanged()
	{
		return bActiveDutyChanged;
	}

	void BravoEntryFlightDuty::setActiveDutyChanged(bool bChanged)
	{
		bActiveDutyChanged = bChanged;
	}

    /**
     * @Brief: Method to retrieve whether or not we have Flight legs in the active duty
     * @Return: returns the number of flight legs
     */
    int BravoEntryFlightDuty::getActiveDutyLegsCount()
    {
        if (pActiveDuty != nullptr && pActiveDuty->flightLegs != nullptr)
        {
            return (int)pActiveDuty->flightLegs->size();
        }
        return 0;
    }

    const BravoDataFlightLeg &BravoEntryFlightDuty::getActiveDutyFlightLeg(int nIndex)
    {
        if (pActiveDuty != nullptr && pActiveDuty->flightLegs != nullptr)
        {
            return pActiveDuty->flightLegs->at(nIndex);
        }
        return BravoDataFlightLeg();
    }
}
