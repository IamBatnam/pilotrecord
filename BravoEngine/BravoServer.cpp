#include "BravoServer.h"
#include "BravoServerTerm.h"

#include "BravoNetwork.h"
#include "BravoEngine.h"

#include "nanodbc/nanodbc.h"
#include "network.pb.h"

#include <thread>

namespace Bravo {
	Server::Server()
		: m_pDatabase(nullptr)
		, m_pTerminalCommands(nullptr)
		, _bServer(false)
	{

	}

	Server::~Server()
	{
	}

	void Server::SendPacket(const NetworkPackets::DataPacket &packetData)
	{
		SendMsg(packetData);
	}

	void Server::ReconnectToDatabase()
	{
		if (m_pDatabase)
			m_pDatabase->connect("PilotRecordTest", "batnam_pilot", "12345");
	}

	void Server::SendClientServerError(Network::Session * client, unsigned int errorVal, std::string strMsg)
	{
		if (errorVal > BravoServerError::SERVER_UNKNOWN_ERROR)
			errorVal = BravoServerError::SERVER_UNKNOWN_ERROR;

		using namespace NetworkPackets;

		DataPacket packet;
		packet.set_messagetype(DataPacket::MESSAGE_ERROR);
		auto pDataPacket = packet.mutable_messageerror();

		pDataPacket->set_errortype(ErrorMsg::SERVER_RESPONSE_ERROR);
		pDataPacket->set_errorval(errorVal);
		pDataPacket->set_errorwhat(strMsg);

		SendMsg(packet, client);
	}

	void Server::Start()
	{
		if (!serverConfig.Initialize())
		{
			std::cout << "[CONFIG]: There was a problem opening config file" << std::endl;
			CloseServerMsg();
			return;
		}
		SERVER_OUT("[CONFIG]: SUCCESSFULLY Initialized!\n");

		// Connect to Database
		try {
			m_pDatabase = new nanodbc::connection("PilotRecordTest", "batnam_pilot", "12345");
			SERVER_OUT("[DATABASE]: SUCCESSFULLY Initialized!\n");
		}
		catch (const nanodbc::database_error &error)
		{
			std::cout << "[DATABASE ERROR:] " << error.native() << std::endl;
			CloseServerMsg();
			return;
		}

		// Network Start
		if (!Initialize())
		{
			// Server Ended
			CloseServerMsg();
			return;
		}
		std::thread t1([this]() {
			Socket::Start();
			SERVER_OUT("********** IO_RUN ENDED! **********\n");
		});

		_bServer = true;

		// Check Server start!
		SERVER_OUT("========================================================\n");
		SERVER_OUT("SERVER SUCCESSFULLY STARTED!\n");
		SERVER_OUT("========================================================\n");

		m_pTerminalCommands = new ServerTerminal(this);
		m_pTerminalCommands->StartTerminal();

		t1.join();

		CloseServerMsg();
		
	}

	bool Server::isRunning()
	{
		return _bServer;
	}

	void Server::Shutdown()
	{
		for (auto it : m_clientList)
		{
			if(it)
				it->ShutdownProcess();
		}

		ServerSocket::Shutdown();
		_bServer = false;
	}

	void Server::OnAfterConnect(Network::Session *client)
	{
		using namespace Network;

		NetworkPackets::DataPacket packet;
		packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);
		auto packetData = packet.mutable_messagerequest();
		packetData->set_reqtype(NetworkPackets::Request::MSG_VERSION);
		packetData->set_version(BRAVO_CLIENT_VERSION);
		
		std::vector<uint32_t> buffer;
		buffer.resize(packet.ByteSize());
		packet.SerializeToArray(&buffer[0], packet.ByteSize());

		SERVER_OUT("Client Count: %i\n", (int)m_clientList.size());
		
		// Send this Packet
		SendMsg(packet, client);
	}

	void Server::MessageSorter(const NetworkPackets::DataPacket &packet, Network::Session *session)
	{
		using namespace Network;

		switch (packet.messagetype())
		{
		case NetworkPackets::DataPacket::MESSAGE_REQUEST:
		{
			ProcessRequests(packet.messagerequest(), session);
			break;
		}
		case NetworkPackets::DataPacket::MESSAGE_LOGIN:
		{
			ProcessLoginRequest(packet.messagelogin(), session);
			break;
		}
		case NetworkPackets::DataPacket::MESSAGE_RESULT:
		{
			break;
		}
		default:
		{
			SERVER_OUT("An unknown type of message was recieved. [type]: %d\n", packet.messagetype());
			break;
		}
		}
	}

	void Server::ErrorHandler(const Bravo::ErrorHandler & error, Network::Session *client)
	{
		if(client != nullptr)
			m_clientList.remove(client);

		using namespace asio::error;
		switch (error.value())
		{
		case address_in_use:
		{
			SERVER_OUT("[ERROR]: Port in used! \"%s\"\n", error.message().c_str());
			break;
		}
		case operation_aborted:
		case connection_reset: // Client force disconect disconnected!
		case eof:
		{
			//UpdateClients();
			SERVER_OUT("[Network]: %s\n", error.message().c_str());
			SERVER_OUT("Client Count: %i\n", (int)m_clientList.size());
			break;
		}
		default:
		{
			SERVER_OUT("[ERROR]: [%i], %s\n", error.value(), error.message().c_str());
			//ASSERT(0 && "An error has occured!");
			break;
		}
		}

	}

	void Server::CloseServerMsg()
	{
		SERVER_OUT("********************************************************\n");
		SERVER_OUT("SERVER ENDED!\n");
		SERVER_OUT("********************************************************\n");
	}

	void Server::CheckAliveClients()
	{
		if (m_clientList.size() > 0)
		{
			SERVER_OUT("[NOTICE]: Sending Health check!\n");
			NetworkPackets::DataPacket packet;
			packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_REQUEST);
			auto req = packet.mutable_messagerequest();
			req->set_reqtype(NetworkPackets::Request::MSG_HEALTH_CHECK);

			SendPacket(packet);
			for (auto it : m_clientList)
			{
				if (!it->CheckHeartBeat())
				{
					SERVER_OUT("Expired Client!\n");
					it->ShutdownProcess();
					m_clientList.remove(it);
				}

				if (m_clientList.size() == 0)
					break;
			}
		}
	}

	void Server::Check(int n)
	{
		switch (n)
		{
		case 0:
		{
			SERVER_OUT("[NOTICE]: Client count: %i\n", (int)m_clientList.size());
			break;
		}
		default:
		{
			SERVER_OUT("[NOTICE]: Unknown Check!\n");
			break;
		}
		}
	}
}