#include "BravoNetwork.h"
#include <functional>
#include <thread>

#include "network.pb.h"

namespace Bravo {
	namespace Network {

		const char *SERVER_DEFAULT_PORT = "5002";
		//const unsigned int HEADER_PACKET_SIZE = 4;

		using asio::ip::tcp;

		Socket::Socket()
			: io_service(*(new asio::io_service))
		{
			GOOGLE_PROTOBUF_VERIFY_VERSION;
		}

		Socket::~Socket()
		{
			google::protobuf::ShutdownProtobufLibrary();
		}

		void Socket::Start()
		{
			io_service.run();
		}


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// SESSION!
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		Session::Session(asio::io_service &io_service)
			: m_sock(new asio::ip::tcp::socket(io_service))
			, sessionState(Session::STATE_HEALTH_NORMAL)
		{
		}

		Session::~Session()
		{
		}

		void Session::StartProcess(Session::SendMsgCB func)
		{
			callbackFunc = func;
			RecieveMsg();
		}

		void Session::ShutdownProcess()
		{
			Bravo::ErrorHandler ec;
			m_sock->shutdown(asio::ip::tcp::socket::shutdown_both, ec);
			m_sock->close(ec);
		}

		void Session::SendMsg(const NetworkPackets::DataPacket & data)
		{
			if (data.messagetype() == NetworkPackets::DataPacket::MESSAGE_REQUEST)
			{
				if (data.messagerequest().reqtype() == NetworkPackets::Request::MSG_HEALTH_CHECK)
					sessionState = Session::STATE_HEALTH_CHECKING;
			}

			sendBuffer.clear();
			
			sendBuffer.resize(HEADER_PACKET_SIZE + data.ByteSize());
			EncodeHeader(sendBuffer, data.ByteSize());
			data.SerializeToArray(&sendBuffer[HEADER_PACKET_SIZE], data.ByteSize());

			Bravo::ErrorHandler error;
			asio::write(*m_sock.get(),
				asio::buffer(sendBuffer, sendBuffer.size()),
				error);

			if (error)
				error_callback(error);
		}

		void Session::RecieveMsg()
		{
			// Recieve Header First
			// And then 
			recvBuffer.clear();
			recvBuffer.resize(HEADER_PACKET_SIZE);

			asio::async_read(*m_sock.get(),
				asio::buffer(recvBuffer, recvBuffer.size()),
				Bravo::Util::bind(&Session::AfterHeaderRecieve, this, std::placeholders::_1, std::placeholders::_2));
		}

		void Session::AfterHeaderRecieve(const Bravo::ErrorHandler &error, size_t bytes_recieve)
		{
			if (error)
			{
				error_callback(error);
				return;
			}

			sessionState = Session::STATE_HEALTH_GOOD;

			auto msgSize = DecodeHeader(recvBuffer);
			recvBuffer.resize(HEADER_PACKET_SIZE + msgSize);

			asio::async_read(*m_sock.get(),
				asio::buffer(&recvBuffer[HEADER_PACKET_SIZE], msgSize),
				Bravo::Util::bind(&Session::AfterRecieve, this, std::placeholders::_1, std::placeholders::_2));
		}

		void Session::AfterRecieve(const Bravo::ErrorHandler &error, size_t bytes_recieve)
		{
			// TODO: When error occurs it might be better to skip
			//	processing and call RecieveMsg instead
			if (error)
			{
				error_callback(error);
				//DEBUG_PRINT("Error Detected: %s\n", error.message().c_str());
				return;
				//ASSERT(0 && Bravo::Util::Sprint("An Error is detected while recieving data: %s", error.value()));
			}

			/// Dissect the data recieved
			if (!error && bytes_recieve > 0)
			{
				NetworkPackets::DataPacket packet;
				packet.ParseFromArray(&recvBuffer[HEADER_PACKET_SIZE], (int)(recvBuffer.size() - HEADER_PACKET_SIZE));

				/// Sort the message
				callbackFunc(packet, this);

				RecieveMsg();
			}

		}

		shared_socket Session::GetSocket()
		{
			return m_sock;
		}

		bool Session::CheckHeartBeat()
		{
			auto startTime = std::chrono::steady_clock::now();

			while (true)
			{
				if (sessionState == Session::STATE_HEALTH_GOOD)
				{
					sessionState = Session::STATE_HEALTH_NORMAL;
					return true;
				}

				auto timeElapsed = std::chrono::steady_clock::now() - startTime;
				if (timeElapsed >= std::chrono::seconds(30))
				{
					sessionState = Session::STATE_HEALTH_TIMEOUT;
					return false;
				}

				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
		}

		void Session::EncodeHeader(NetDataContainer &buffer, unsigned int size)
		{
			ASSERT(buffer.size() >= HEADER_PACKET_SIZE);
			buffer[0] = static_cast<uint8_t>((size >> 24) & 0xFF);
			buffer[1] = static_cast<uint8_t>((size >> 16) & 0xFF);
			buffer[2] = static_cast<uint8_t>((size >> 8) & 0xFF);
			buffer[3] = static_cast<uint8_t>(size & 0xFF);
		}

		const unsigned int Session::DecodeHeader(NetDataContainer & buffer)
		{
			if (buffer.size() < HEADER_PACKET_SIZE)
				return 0;

			unsigned int msg_size = 0;
			for (unsigned i = 0; i < HEADER_PACKET_SIZE; ++i)
				msg_size = msg_size * 256 + (static_cast<unsigned int>(buffer[i]) & 0xFF);
			return msg_size;
		}


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// SERVER SOCKET!
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		ServerSocket::ServerSocket()
			: m_acceptor(io_service)
		{
		}
		
		ServerSocket::~ServerSocket()
		{
		}

		bool ServerSocket::Initialize()
		{
			auto endpoint = tcp::endpoint(tcp::v4(), Bravo::Util::CharToInt(SERVER_DEFAULT_PORT));
			m_acceptor.open(endpoint.protocol());
			m_acceptor.set_option(asio::socket_base::reuse_address(false));

			Bravo::ErrorHandler ec;
			
			m_acceptor.bind(endpoint, ec);
			
			if (ec)
			{
				ErrorHandler(ec);
				return false;
			}
			
			m_acceptor.listen();
			SERVER_OUT("[NETWORK]: SUCCESSFULLY Initialized\n");
			
			Accept();	
			return true;
		}

		void ServerSocket::Accept()
		{
			auto sess = new Session(m_acceptor.get_io_service());
			m_acceptor.async_accept(*sess->GetSocket(), Bravo::Util::bind(&ServerSocket::OnAccept, this, sess, std::placeholders::_1));
		}

		void ServerSocket::OnAccept(Session *sess, const Bravo::ErrorHandler &error)
		{
			if (error)
			{
				ErrorHandler(error);
				return;
			}

			asio::error_code _err;
			auto endp = sess->GetSocket()->remote_endpoint(_err);

			if (!_err)
			{
				SERVER_OUT("Client connected: %s\n", endp.address().to_string().c_str());
			}

			//sess->GetSocket()->set_option(asio::socket_base::keep_alive(true));

			/// start service
			m_clientList.push_back(sess);
			sess->StartProcess(Bravo::Util::bind(&ServerSocket::MessageSorter, this, std::placeholders::_1, sess));
			sess->error_callback = Bravo::Util::bind(&ServerSocket::ErrorHandler, this, std::placeholders::_1, sess);
			OnAfterConnect(sess);			

			Accept();
		}

		void ServerSocket::SendMsg(const NetworkPackets::DataPacket & packet, Session * session)
		{
			if (session == nullptr)
			{
				for (auto it : m_clientList)
				{
					if (it)
						it->SendMsg(packet);
				}
			}
			else
			{
				session->SendMsg(packet);
			}
		}

		void ServerSocket::Shutdown()
		{
			Bravo::ErrorHandler ec;
			//m_acceptor.cancel(ec);
			m_acceptor.close(ec);
			if (ec)
				ErrorHandler(ec);
		}


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Client SOCKET!
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		ClientSocket::ClientSocket()
			: session(new Session(io_service))
		{
		}

		ClientSocket::~ClientSocket()
		{
		}

		bool ClientSocket::Initialize(const std::string & strHostAddr/*, const std::string & strPort*/)
		{
			m_strHostAddr = strHostAddr;
			return Initialize();
		}

		bool ClientSocket::Initialize()
		{
			Connect();
			return true;
		}

		void ClientSocket::RenewSession()
		{
			if (session)
				delete session;

			session = new Session(io_service);

			io_service.reset();
		}

		void ClientSocket::Connect()
		{
			Bravo::ErrorHandler ec;

			tcp::resolver resolver(io_service); // I/O Object
			auto endpoint_iterator = resolver.resolve({ m_strHostAddr.c_str(), SERVER_DEFAULT_PORT }, ec);
			
			if (ec)
			{
				ErrorHandler(ec);
				return;
			}

			asio::connect(*session->GetSocket(), endpoint_iterator, ec);

			if (ec)
			{
				ErrorHandler(ec);
				return;
			}

			session->StartProcess(Util::bind(&ClientSocket::MessageSorter, this, std::placeholders::_1, session));
			session->error_callback = Util::bind(&ClientSocket::ErrorHandler, this, std::placeholders::_1, nullptr);

			OnAfterConnect(session);
		}

		//void ClientSocket::SendMsg(const NetworkPackets::DataPacket &pcktData)
		//{
		//	if (pcktData.IsInitialized())
		//		session->SendMsg(pcktData);
		//}

		//void ClientSocket::Async_Timeout_Wait(long seconds) try
		//{
		//	asio::high_resolution_timer timer(io_service);
		//	timer.expires_from_now(std::chrono::seconds(seconds));
		//	timer.wait();

		//} catch (const Bravo::ErrorHandler &error) {
		//	ErrorHandler(error);
		//}

	}
}