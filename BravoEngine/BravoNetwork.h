#ifndef BRAVO_NETWORK_H
#define BRAVO_NETWORK_H

#if defined(_MSC_VER)
#pragma once
#endif

#ifdef BRAVO_WINDOWS
#ifdef _DEBUG
#pragma comment(lib, "libprotocd.lib")
#pragma comment(lib, "libprotobufd.lib")
#else
#pragma comment(lib, "libprotoc.lib")
#pragma comment(lib, "libprotobuf.lib")
#endif // _DEBUG
#endif // BRAVO_WINDOWS

//#if defined(BRAVO_WINDOWS) && !defined(_WIN32_WINNT)
//#define _WIN32_WINNT=0x0501
//#endif // DEBUG

#ifndef ASIO_STANDALONE
#define ASIO_STANDALONE
#endif
//#define ASIO_STANDALONE 
//#define ASIO_HAS_STD_ADDRESSOF
//#define ASIO_HAS_STD_ARRAY
//#define ASIO_HAS_CSTDINT
//#define ASIO_HAS_STD_SHARED_PTR
//#define ASIO_HAS_STD_TYPE_TRAITS

#include <asio.hpp>

#ifndef ASIO_HAS_STD_CHRONO
#define ASIO_HAS_STD_CHRONO 1
#endif // ASIO_HAS_STD_CHRONO

#include <asio/high_resolution_timer.hpp>
#include <asio/steady_timer.hpp>
#include "BravoUtility.h"
#include <functional>
#include <atomic>

#include "network.pb.h"

#define HEADER_PACKET_SIZE 4

namespace Bravo {
	namespace Network {
		// Server Settings
		extern const char *SERVER_DEFAULT_PORT;

		// Vector
		template<typename T>
		using DataContainer = std::vector<T>;
		typedef DataContainer<uint8_t> NetDataContainer;


		/* Simplyfies shared socket shared_ptr
		*/
		typedef Bravo::Shared_ptr<asio::ip::tcp::socket>::std shared_socket;


		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// SESSION!
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		class Session : public Bravo::Enable_Shared_From_This<Session>::std
		{
		public:
			enum STATE {
				STATE_HEALTH_NORMAL,
				STATE_HEALTH_CHECKING,
				STATE_HEALTH_GOOD,
				STATE_HEALTH_TIMEOUT
			};
			Session(asio::io_service &io_service);
			~Session();

			typedef std::function<void(const NetworkPackets::DataPacket &, Session *sess)> SendMsgCB;
			typedef std::function<void(const ErrorHandler &)> ErrorHandleCB;

			ErrorHandleCB error_callback;

			void StartProcess(SendMsgCB func);
			void ShutdownProcess();

			void SendMsg(const NetworkPackets::DataPacket &data);
			shared_socket GetSocket();

			bool CheckHeartBeat();

		private:
			void RecieveMsg();

			void AfterHeaderRecieve(const Bravo::ErrorHandler &error, size_t bytes_recieved);
			void AfterRecieve(const Bravo::ErrorHandler &error, size_t bytes_recieved);

			void EncodeHeader(NetDataContainer &buffer, unsigned int size);
			const unsigned int DecodeHeader(NetDataContainer &buffer);

			//void AfterSend(const Bravo::ErrorHandler &error, size_t bytes_transfered);
		private:
			shared_socket m_sock;
			SendMsgCB callbackFunc;

			NetDataContainer sendBuffer;
			NetDataContainer recvBuffer;
			std::atomic<STATE> sessionState;
		};

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// SOCKET!
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		class Socket
		{
		public:
			Socket();
			~Socket();

			void Start();

		protected:
			virtual void MessageSorter(const NetworkPackets::DataPacket &pckData, Session *session) = 0;
			virtual void ErrorHandler(const Bravo::ErrorHandler &error, Session* client = nullptr) = 0;
			virtual bool Initialize() = 0;
			virtual void OnAfterConnect(Session *client) = 0;

		protected:
			asio::io_service &io_service;
		};

		//////////////////////////////////////
		// Server
		//////////////////////////////////////
		class ServerSocket : public Socket 
		{
		public:
			ServerSocket();
			~ServerSocket();
		protected:
			bool Initialize() override;
			void Accept();
			void OnAccept(Session *sock, const Bravo::ErrorHandler &error);
			void SendMsg(const NetworkPackets::DataPacket &packet, Session* session = nullptr);
			void Shutdown();

		protected:
			asio::ip::tcp::acceptor m_acceptor;
			std::list<Session*> m_clientList;
		};

		///////////////////////////////////////
		// Client
		//////////////////////////////////////
		class ClientSocket : public Socket 
		{
		public:
			ClientSocket();
			~ClientSocket();
		protected:
			bool Initialize(const std::string &strHostAddr/*, const std::string &strPort*/);
			bool Initialize() override;
			void Connect();
			void RenewSession();
			//void SendMsg(const NetworkPackets::DataPacket &pcktData);

			//bool Async_Timeout_Wait(long seconds = 5);

			Session *session;
			std::string m_strHostAddr;
			std::string m_strServerPort;
		};

		/* @DESC: Use to serialize PacketData struct
		*/
		//static void SerializePacket(const PacketData &data, NetDataContainer &vector)
		//{
		//	ASSERT(vector.size() != 0 && "The size of the vector is not defined!");
		//	vector[0] = ((uint16_t)data.msgType);
		//	vector[1] = ((uint16_t)data.dataSize);

		//	for (int i = 0; i <= (int)strlen(data.data); i++)
		//	{
		//		vector[i + 2] = data.data[i];
		//	}
		//}



		/* @ DESC: Use to deserialize PacketData struct
		*
		*/
		//static void DeserializePacket(const NetDataContainer &vector, PacketData &data)
		//{
		//	ASSERT(vector.size() && "The size of the recieved vector is 0");
		//	data.msgType = (MessageType)vector[0];
		//	data.dataSize = (size_t)vector[1];

		//	auto pData = data.data;

		//	for (int i = 0; i <= (int)data.dataSize; i++)
		//	{
		//		if (i + 2 == data.dataSize)
		//		{
		//			*pData = '\0';	
		//			break;
		//		}
		//		*pData = (char)vector[i + 2];
		//		pData++;
		//	}
		//}

		/* @ DESC: NetToToken combines all variable
		*		to a single string
		*/
		//static void NetToToken(DataContainer<std::string> &data, char *tempBuffer)
		//{
		//	for (auto it = data.begin(); it != data.end(); it++)
		//	{
		//		for (int i = 0; i < (int)strlen(it->c_str()); i++)
		//		{
		//			*tempBuffer = (*it)[i];
		//			tempBuffer++;
		//		}

		//		*tempBuffer = ';';
		//		tempBuffer++;
		//	}

		//	*tempBuffer = '\0';
		//}

		/* @ DESC: Make a pair [key] => [value] of the data from
		*  PacketData.data
		*/
		//static DataContainer<std::string> SplitData(char *tempBuffer)
		//{
		//	std::string strData(tempBuffer);
		//	DataContainer<std::string> pairs;

		//	size_t nOffset = 0;
		//	size_t cur = strData.find(";");

		//	if (cur == std::string::npos)
		//		pairs.push_back(strData);

		//	while (cur != std::string::npos)
		//	{
		//		std::string strValue(strData.substr(nOffset, cur));
		//		pairs.push_back(strValue);

		//		nOffset = cur + 1;
		//		cur = strData.find(";", nOffset);
		//	}

		//	return pairs;
		//}

	}
}

#endif
