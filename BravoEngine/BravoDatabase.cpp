#include "BravoDatabase.h"
#include "BravoErrorHandler.h"

#include "network.pb.h"


namespace Bravo
{
	BravoDatabase::BravoDatabase(BravoClient *client, BravoErrorHandler *pError)
		: mClient(client)
		, activeUserSession(nullptr)
	{
		m_pErrorModule = pError;
	}


	BravoDatabase::~BravoDatabase()
	{
		// TODO: Clean the Databases once destroyed
	}

	bool BravoDatabase::Init()
	{		
		return true;
	}

	void BravoDatabase::Logout()
	{
		auto result = mClient->SendLogOut(activeUserSession);
		//if(result.messageresult().resulttype() == NetworkPackets::Result::RESULT_GOOD)
		delete activeUserSession;
	}


//////////////////////////////////////////////////////////////////
// User
//////////////////////////////////////////////////////////////////
	void BravoDatabase::AddLoginUser(const UserAccount & User)
	{
		ASSERT(0);
		dbConfig.accounts.push_back(User);
		dbConfig.nTotalAccounts = (int)dbConfig.accounts.size();
	}

	std::vector<UserAccount> &BravoDatabase::RetrieveUsers()
	{
		if (mClient->IsConnected())
		{
			auto Packet = mClient->RetrieveAccounts();
			auto accounts = Packet.messageaccounts();
			
			dbConfig.accounts.clear();
			dbConfig.nTotalAccounts = 0;

			for (auto account : accounts.accounts())
			{
				UserAccount uAccount;
				uAccount.convertProtobufToUserAccount(account);

				dbConfig.accounts.push_back(uAccount);
			}
		}

		return dbConfig.accounts;
	}

	bool BravoDatabase::FindUser(const std::string &strUser)
	{
		bool bResult = false/* = BravoDatabaseFile::FindUser(strUser)*/;
		if (mClient->IsConnected())
		{
			bResult = mClient->SendFindUser(strUser);
		}
		
		return bResult;
	}

	bool BravoDatabase::CheckLogin(const std::string &strUser, const std::string &strPass)
	{
		//UserAccount Result/* = BravoDatabaseFile::FindUser(strUser, strPass)*/;
		if (mClient->IsConnected())
		{
			//UserAccount *uAcc = new UserAccount;
			auto resultUser = mClient->SendCheckLogin(strUser, strPass);
			auto resultObj = resultUser.messagelogin().resultmsg();

			if (resultObj.result() > 0)
			{
				activeUserSession = new LoginSession(resultObj.session().sessionid());
				//uAcc->convertProtobufToUserAccount(*(resultUser.mutable_messagelogin()->mutable_resultmsg()->mutable_account()));
				return true;
			}
		}
		return false;
	}

//////////////////////////////////////////////////////////////////
// Entry
//////////////////////////////////////////////////////////////////
	void BravoDatabase::AddEntry(const BravoDataEntry & entry)
	{
		if (mClient->IsConnected())
		{
			auto result = mClient->AddEntry(activeUserSession, entry);

			if (result.messageresult().resulttype() != NetworkPackets::Result::RESULT_GOOD_ADD_ENTRY)
			{
				m_pErrorModule->Error("Adding an entry resulted in an error", BravoErrorHandler::ERROR_OKAY_ONLY);
			}
		}
	}

	void BravoDatabase::UpdateEntry(const BravoDataEntry & entry)
	{
		if (mClient->IsConnected())
		{
			auto result = mClient->EditEntry(activeUserSession, entry);

			if (result.messageresult().resulttype() != NetworkPackets::Result::RESULT_GOOD_EDIT_ENTRY)
			{
				m_pErrorModule->Error("Adding an entry resulted in an error", BravoErrorHandler::ERROR_OKAY_ONLY);
			}
		}
	}

	std::vector<BravoDataEntry> BravoDatabase::FetchEntries() const
	{
		return dbConfig.entries;
	}

	void BravoDatabase::FetchPreviousEntries(std::vector<PreviousVersionDataEntry>& prevEntryArray, int nEntryId)
	{
		if (mClient->IsConnected())
		{
			auto entryPackets = mClient->RetrievePreviousEntries(activeUserSession, nEntryId);
			auto entriesmsg = entryPackets.messageentries();
			if (entriesmsg.entrytype() == NetworkPackets::Entries::ENTRY_PREVIOUS_VERSION)
			{
				for (auto e : entriesmsg.entries())
				{
					PreviousVersionDataEntry prevEntry;
					prevEntry.ConvertPacketToEntry(e);
					prevEntryArray.push_back(prevEntry);
				}
			}
		}
	}
	
//////////////////////////////////////////////////////////////////
// Duty
//////////////////////////////////////////////////////////////////
	int BravoDatabase::FetchDutiesMinimal(const BravoDate &startDateRange, const BravoDate &endDateRange)
    {
        // Placeholder function
        auto _func = [](void*, _BravoTimeStamp::_date, _BravoTimeStamp, _BravoTimeStamp, bool) {};
        
		FetchDutiesMinimal<decltype(_func)>(startDateRange,
			endDateRange, 
			nullptr, 
			_func);

		if (dbConfig.duties.size() > 0)
			return dbConfig.duties.size();
		else
			return -1;
    }

    /**
     * @Brief: Checks whether the start and end ranges matches are cached
     */
    bool BravoDatabase::FetchableDutiesMinimalCache(const BravoDate &startDateRange, const BravoDate &endDateRange)
    {
        if(dbConfig.duties.size() > 0 &&
           dbConfig.duties.front().date == startDateRange &&
           dbConfig.duties.back().date == endDateRange)
            return true;
        
        return false;
    }
    
    /** Get the duty if available or create in the buffer
	*/
	BravoDataDuty *BravoDatabase::GetDuty(const BravoDate &date)
	{
		if (mClient->IsConnected())
		{
			auto dutyPackets = mClient->RetrieveDuties(activeUserSession, date);
			auto dutymsg = dutyPackets.messageduties().duties();

			if (dutymsg.size() > 0)
			{
				BravoDataDuty *pDuty = new BravoDataDuty;
				pDuty->ConvertPacketToDuty(dutymsg[0]);

				return pDuty;
			}
		}
		
        return (new BravoDataDuty(date));
	}

	bool BravoDatabase::CommitDuty(const BravoDataDuty &duty)
	{
		if (mClient->IsConnected())
		{
			auto dutyPacket = mClient->CommitDuty(activeUserSession, duty);
			// RMK: Verify that the server request is successful
			if (dutyPacket.messageresult().resulttype() == NetworkPackets::Result::RESULT_GOOD_COMMIT_DUTY)
				return true;
		}

		return false;
	}
    
    /* GetDutyContainer
     @Brief - Creates a constant copy of the container
     */
    const std::vector<BravoDataDuty> BravoDatabase::GetDutyContainer()
    {
        return (const std::vector<BravoDataDuty>)dbConfig.duties;
    }
}
