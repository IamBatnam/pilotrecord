#include "BravoEntry.h"


namespace Bravo
{
	BravoEntry::BravoEntry(BravoDatabase *db, BravoErrorHandler *pError)
	{
		m_pDatabase = db;
		m_pErrorModule = pError;
	}


	BravoEntry::~BravoEntry()
	{
	}

	bool BravoEntry::Init()
	{
		if(!m_pDatabase)
			return false;

		return true;
	}

}
