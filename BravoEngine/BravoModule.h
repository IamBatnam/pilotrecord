#ifndef _BRAVO_MODULE_H
#define _BRAVO_MODULE_H

#if defined(_MSC_VER)
#pragma once
#endif

#include "BravoUtility.h"

namespace Bravo {
	class BravoDatabase;
	class BravoErrorHandler;
	class BravoModule
	{
	public:
		BravoModule();
		virtual ~BravoModule();

	protected:
		BravoDatabase *m_pDatabase;
		BravoErrorHandler *m_pErrorModule;

	public:
		virtual bool Init() = 0;
	};
}
#endif // _BRAVO_MODULE_H

