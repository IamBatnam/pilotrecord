#include "BravoClient.h"

#include "BravoDef.h"

#include <thread>

namespace Bravo 
{
	double fMaxTimeout = 30.0f;


	BravoClient::BravoClient(BravoErrorHandler *pErrorModule, BravoConfigHandler *pConfHndlr)
		: clientState(new BravoClient::ClientState(BravoClient::CLIENT_DISCONNECTED))
		, m_pErrorModule(pErrorModule)
		, m_pConfigHndlr(pConfHndlr)
	{
	}


	BravoClient::~BravoClient()
	{
	}

	bool BravoClient::IsConnected()
	{
		if (GetClientState() != CLIENT_DISCONNECTED)
			return true;
		return false;
	}

	void BravoClient::Start()
	{
        if(GetClientState() == CLIENT_PROCESSING)
            OUTPUT_DEBUG("Client in process!");
            
        while(GetClientState() == CLIENT_PROCESSING)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        
        if (GetClientState() == CLIENT_CONNECTED)
        {
            OUTPUT_DEBUG("Operation Already Connected!\n");
            return;
        }
		
        Initialize((*m_pConfigHndlr)["servIP"]);
        std::thread t1([this]() {
            Socket::Start(); // blocking method
                
            OUTPUT_DEBUG("\n\n\n[CLIENT]: THE IO_RUN HAS ENDED!!!\n\n\n");
            
            std::lock_guard<std::mutex> lock(clientStateMutex);
            *clientState = BravoClient::CLIENT_DISCONNECTED;
        });
        t1.detach();
	}

	bool BravoClient::Reconnect()
	{
		Disconnect();
		RenewSession();
		
		auto start = std::chrono::high_resolution_clock::now();
		
		Start();
		while (!IsConnected())
		{
			std::chrono::duration<double> timeElapsed = std::chrono::high_resolution_clock::now() - start;
			if (timeElapsed >= std::chrono::duration<double>(fMaxTimeout))
			{
				m_pErrorModule->Error("The application is unable to reconnect to the server.", 0, 2);
				return false;
			}

			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		return true;
	}

	void BravoClient::Disconnect()
	{
        std::lock_guard<std::mutex> lock(clientStateMutex);
        *clientState = BravoClient::CLIENT_PROCESSING;
        
        OUTPUT_DEBUG("Shutting Down Client\n");
		session->ShutdownProcess();
	}

	BravoClient::ClientState BravoClient::GetClientState()
	{
		return *(clientState.get());
	}

	//void BravoClient::SendPacket(const NetworkPackets::DataPacket &packetData)
	//{
	//	SendMsg(packetData);
	//}

	void BravoClient::OnAfterConnect(Network::Session * client)
	{
		if (*(clientState.get()) != BravoClient::CLIENT_OUTDATED)
		{
			std::lock_guard<std::mutex> lock(clientStateMutex);
			*clientState = BravoClient::CLIENT_CONNECTED;
		}
	}

	void BravoClient::MessageSorter(const NetworkPackets::DataPacket & pckData, Network::Session *client)
	{
		switch (pckData.messagetype())
		{
		case NetworkPackets::DataPacket::MESSAGE_REQUEST:
		{
			auto msg = pckData.messagerequest();
			RequestMessageSorter(msg, client);			
			break;
		}

		case NetworkPackets::DataPacket::MESSAGE_LOGIN:
		{
			AddMessage(pckData);			
			break;
		}

		case NetworkPackets::DataPacket::MESSAGE_ERROR:
		{
			auto msgErr = pckData.messageerror();
			unsigned int errorType = msgErr.errortype();
			unsigned int errorVal = msgErr.errorval();

			m_pErrorModule->ErrorFromServer(errorType, errorVal);
			break;
		}
		case NetworkPackets::DataPacket::MESSAGE_RESULT:
		case NetworkPackets::DataPacket::MESSAGE_ENTRY:
		case NetworkPackets::DataPacket::MESSAGE_ACCOUNTS:
		case NetworkPackets::DataPacket::MESSAGE_DUTY:
		{
			AddMessage(pckData);
			break;
		}

		default:
		{
			ASSERT(0 && "Unknown message was recieved!");
			break;
		}
		}
	}

	void BravoClient::ErrorHandler(const Bravo::ErrorHandler & error, Network::Session * sess)
	{
		if (error.value() == asio::error::operation_aborted)
		{
			OUTPUT_DEBUG("The connection was disconnected! %s\n", error.message().c_str());
			return;
		}

		switch(error.value())
		{ 
		case asio::error::host_unreachable:
		case asio::error::host_not_found:
		{
			m_pErrorModule->Error(Sprint("[Error]: %s", "There was a problem connecting to the server"), 0);
			break;
		}
		case asio::error::connection_reset:
		{
			std::lock_guard<std::mutex> lock(clientStateMutex);
			*clientState = BravoClient::CLIENT_DISCONNECTED;
			m_pErrorModule->Error("The application has disconnected to the server.", 1);
			break;
		}
		default:
		{
			m_pErrorModule->Error(Sprint("[Error]: %s", error.message().c_str()), 0);
			break;
		}
		}

		//OUTPUT_DEBUG(error.message().c_str());
		//ASSERT(0);
		
	}

	void BravoClient::RequestMessageSorter(const NetworkPackets::Request & reqData, Network::Session * client)
	{
		switch (reqData.reqtype())
		{
		case NetworkPackets::Request::MSG_HEALTH_CHECK:
		{
			NetworkPackets::DataPacket packet;
			packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_RESULT);
			packet.mutable_messageresult()->set_resulttype(NetworkPackets::Result::RESULT_GOOD);
			SendMsg(packet);
			break;
		}
		case NetworkPackets::Request::MSG_VERSION:
		default:
		{
			if (reqData.version() > BRAVO_CLIENT_VERSION)
			{
				std::lock_guard<std::mutex> lock(clientStateMutex);
				*clientState = BravoClient::CLIENT_OUTDATED;
			}
			OUTPUT_DEBUG("CONNECTED TO THE SERVER!\tVersion: %i\n", reqData.version());
			break;
		}
		}
	}

	void BravoClient::SendMsg(const NetworkPackets::DataPacket & pcktData)
	{
		if (io_service.stopped())
		{
			m_pErrorModule->Error("The connection service is currently not running", 2);
			return;
		}

		if (pcktData.IsInitialized())
			session->SendMsg(pcktData);
	}

	const NetworkPackets::DataPacket BravoClient::GetMessage(NetworkPackets::DataPacket::MessageType type)
	{
		auto start = std::chrono::high_resolution_clock::now();
		while (true)
		{
			std::chrono::duration<double> timeElapsed = std::chrono::high_resolution_clock::now() - start;

			if (timeElapsed >= std::chrono::duration<double>(fMaxTimeout))
			{
				NetworkPackets::DataPacket packet;
				packet.set_messagetype(NetworkPackets::DataPacket::MESSAGE_ERROR);
				auto data = packet.mutable_messageerror();
				data->set_errorval(BravoServerError::SERVER_TIMEOUT);
				data->set_errortype(NetworkPackets::ErrorMsg::TIMEOUT_ERROR);

				MessageSorter(packet, session);

				return NetworkPackets::DataPacket();
			}

			if (m_msgs.size() > 0)
			{
				std::pair<NetworkPackets::DataPacket, std::chrono::milliseconds> msg = m_msgs.front();

				// If message is expired then delete message
				if (msg.second < std::chrono::duration_cast<std::chrono::milliseconds>
					(std::chrono::high_resolution_clock::now().time_since_epoch()))
				{
					m_msgs.pop();
					//delete msg;
					goto thread_sleep;
				}

				// If not the type being searched for then try to move msgs around
				{
					std::lock_guard<std::mutex> lockMsg(msgMutex);

					/// TODO: Consider using for(val:conainer)
					size_t msgSize = m_msgs.size(), i = 0;
					while (type != msg.first.messagetype() && i <= msgSize)
					{
						m_msgs.pop();
						m_msgs.push(msg);
						msg = m_msgs.front();
						i++;
					}
				}

				// Confirm msg type before returning
				if (type == msg.first.messagetype())
				{
					m_msgs.pop();
					return msg.first;
				}
			}
		thread_sleep:
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
	}

	void BravoClient::AddMessage(const NetworkPackets::DataPacket & packet)
	{
		auto now = std::chrono::high_resolution_clock::now();
		std::chrono::milliseconds nano = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) +
			std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::duration<double>(fMaxTimeout));
		{
			std::lock_guard<std::mutex> g_lockmsg(msgMutex);
			auto makePair = std::make_pair(packet, nano);
			m_msgs.push(makePair);
		}
	}
}
