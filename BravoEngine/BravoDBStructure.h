#ifndef BRAVO_DB_STRUCTURE_H
#define BRAVO_DB_STRUCTURE_H

#if defined(_MSC_VER)
#pragma once
#endif


#include "Crypt.h"
#include <vector>
#include "network.pb.h"
#include <stdint.h>

namespace Bravo {
	const short BravoCurrentVersion = 0x01;

	struct BravoTimestamp
	{
		int year;   //!< Year [0-inf).
		int month;  //!< Month of the year [1-12].
		int day;    //!< Day of the month [1-31].
		int hour;   //!< Hours since midnight [0-23].
		int min;    //!< Minutes after the hour [0-59].
		int sec;    //!< Seconds after the minute.
		int fract;  //!< Fractional seconds.

		bool operator== (const BravoTimestamp &bravo1)
		{
			if ((year == bravo1.year) && (month == bravo1.month) &&
				(day == bravo1.day) && (hour == bravo1.hour) &&
				(min == bravo1.min) && (sec == bravo1.sec) &&
				(fract == bravo1.fract))
				return true;
			else
				return false;
		}

		bool operator!= (const BravoTimestamp &bravo1)
		{
			if ((year != bravo1.year) || (month != bravo1.month) ||
				(day != bravo1.day) || (hour != bravo1.hour) ||
				(min != bravo1.min) || (sec != bravo1.sec) ||
				(fract != bravo1.fract))
				return true;
			else
				return false;
		}
	};

	struct BravoDate 
	{
		unsigned int year;   //!< Year [0-inf).
		unsigned int month;  //!< Month of the year [1-12].
		unsigned int day;    //!< Day of the month [1-31].

		bool operator == (const BravoDate &dComp)
		{
			return ((year == dComp.year) && (month == dComp.month) && (day == dComp.day));
		}

		std::string GetDateFmt()
		{
			std::tm _tm;
			_tm.tm_year = year - 1900;
			_tm.tm_mon = month - 1;
			_tm.tm_mday = day;
			return Util::GetDateTimeStr("%F", _tm);
		}
	};

	/**
	* @Brief: Struct holder for QTimestamp for callbacks should not be used for data handling
	*/
	struct _BravoTimeStamp {
		struct _date {
			int year;
			int month;
			int day;
		};
		struct _time {
			int hour;
			int minutes;
		};
		_date date;
		_time time;
	};

	struct PreviousVersionDataEntry;

	struct BravoDataEntry {
		int id;
		long _date;
		std::string cPlaneType;
		std::string cPlaneReg;
		std::string cPilotName;
		std::string cPax;
		std::string strRemarks;

		// Hours Info
		float SE_DAY_DUAL;
		float SE_NIGHT_DUAL;
		float SE_DAY_PIC;
		float SE_NIGHT_PIC;

		float ME_DAY_DUAL;
		float ME_NIGHT_DUAL;
		float ME_DAY_PIC;
		float ME_NIGHT_PIC;
		float ME_DAY_SIC;
		float ME_NIGHT_SIC;

		float XCUN_DAY_DUAL;
		float XCUN_DAY_PIC;
		float XCUN_NIGHT_DUAL;
		float XCUN_NIGHT_PIC;

		int nTO_LAND_DAY;
		int nTO_LAND_NIGHT;

		// Instrument
		int IFR_APPROACH;
		float SIM_TIME;
		float HOOD_TIME;
		float ACTUAL_TIME; // Instrument
		std::vector<PreviousVersionDataEntry> *PreviousVersion;
		
		BravoDataEntry()
			// Default Times
			: SE_DAY_DUAL(0.0f)
			, SE_NIGHT_DUAL(0.0f)
			, SE_DAY_PIC(0.0f)
			, SE_NIGHT_PIC(0.0f)
			, ME_DAY_DUAL(0.0f)
			, ME_NIGHT_DUAL(0.0f)
			, ME_DAY_PIC(0.0f)
			, ME_NIGHT_PIC(0.0f)
			, ME_DAY_SIC(0.0f)
			, ME_NIGHT_SIC(0.0f)
			, XCUN_DAY_DUAL(0.0f)
			, XCUN_DAY_PIC(0.0f)
			, XCUN_NIGHT_DUAL(0.0f)
			, XCUN_NIGHT_PIC(0.0f)
			, nTO_LAND_DAY(0)
			, nTO_LAND_NIGHT(0)
			, IFR_APPROACH(0)
			, SIM_TIME(0.0f)
			, HOOD_TIME(0.0f)
			, ACTUAL_TIME(0.0f)
			, PreviousVersion(nullptr)
		{
		}
		~BravoDataEntry()
		{
			if (PreviousVersion && PreviousVersion->size() > 0)
				delete PreviousVersion;
		}

		virtual void ConvertPacketToEntry(NetworkPackets::Entries::Entry &entry)
		{
			id = entry.id();
			_date = (long)entry.date();
			cPlaneType = entry.cplanetype();
			cPlaneReg = entry.cplanereg();
			cPilotName = entry.cpilotname();
			cPax = entry.cpax();
			strRemarks = entry.strremarks();

			SE_DAY_DUAL = entry.se_day_dual();
			SE_NIGHT_DUAL = entry.se_night_dual();
			SE_DAY_PIC = entry.se_day_pic();
			SE_NIGHT_PIC = entry.se_night_pic();

			ME_DAY_DUAL = entry.me_day_dual();
			ME_NIGHT_DUAL = entry.me_night_dual();
			ME_DAY_PIC = entry.me_day_pic();
			ME_NIGHT_PIC = entry.me_night_pic();
			ME_DAY_SIC = entry.me_day_sic();
			ME_NIGHT_SIC = entry.me_night_sic();

			XCUN_DAY_DUAL = entry.xcun_day_dual();
			XCUN_DAY_PIC = entry.xcun_day_pic();
			XCUN_NIGHT_DUAL = entry.xcun_night_dual();
			XCUN_NIGHT_PIC = entry.xcun_night_pic();

			nTO_LAND_DAY = entry.nto_land_day();
			nTO_LAND_NIGHT = entry.nto_land_night();

			// Instrument
			IFR_APPROACH = entry.ifr_approach();
			SIM_TIME = entry.sim_time();
			HOOD_TIME = entry.hood_time();
			ACTUAL_TIME = entry.actual_time();  // Instrument
		}

		static long DateToLong(unsigned short nMonth, unsigned short nDay, unsigned int nYear)
		{
			return (nYear << 12) | (nMonth << 8) | nDay;
		}

		static void LongToDate(long date, unsigned short &nMonth, unsigned short &nDay, unsigned int &nYear)
		{
			nMonth = (date >> 8) & 0xf;
			nDay = date & 0xff;
			nYear = (int)(date >> 12);
		}

		static std::string GetDateStrFormat(int nDay, int nMonth, int nYear)
		{
			std::string strReturn;
			std::string strDay;

			if (nDay < 10)
				strDay = Sprint("0%i", nDay);
			else
				strDay = Sprint("%i", nDay);

			strReturn = Sprint("%s%s%i", strDay.c_str(), GetMonthStr(nMonth).c_str(), nYear);

			return strReturn;
		}

		static const std::string GetMonthStr(int nMonth)
		{
			std::string strBuffer;
			switch (nMonth)
			{
			case 1: {
				strBuffer = "JAN";
				break;
			}
			case 2: {
				strBuffer = "FEB";
				break;
			}
			case 3: {
				strBuffer = "MAR";
				break;
			}
			case 4: {
				strBuffer = "APR";
				break;
			}
			case 5: {
				strBuffer = "MAY";
				break;
			}
			case 6: {
				strBuffer = "JUN";
				break;
			}
			case 7: {
				strBuffer = "JUL";
				break;
			}
			case 8: {
				strBuffer = "AUG";
				break;
			}
			case 9: {
				strBuffer = "SEP";
				break;
			}
			case 10: {
				strBuffer = "OCT";
				break;
			}
			case 11: {
				strBuffer = "NOV";
				break;
			}
			case 12: {
				strBuffer = "DEC";
				break;
			}
			default: {
				// TODO: Add exception error
				strBuffer = "UKN";
				break;
			}
			}
			return strBuffer;
		}

		void SetDate(unsigned short nMonth, unsigned short nDay, unsigned short nYear)
		{
			_date =   (nYear << 12) | (nMonth << 8)| nDay;
		}

		void GetDateInt(unsigned int &nMonth, unsigned int &nDay, unsigned int &nYear)
		{
			nYear = GetDateYear();
			nMonth = GetDateMonthInt();
			nDay = GetDateDay();
		}

		int GetDateMonthInt() const
		{
			return (_date >> 8) & 0xf;
		}

		int GetDateDay() const
		{
			return _date & 0xff;
		}

		int GetDateYear() const
		{
			return (int)(_date >> 12);
		}

		std::string GetDateStrFormat() const
		{
			return GetDateStrFormat(GetDateDay(), GetDateMonthInt(), GetDateYear());
		}

	};

	struct PreviousVersionDataEntry : public BravoDataEntry {
		long dateModified;

		void ConvertPacketToEntry(NetworkPackets::Entries::Entry &entry) override
		{
			dateModified = (long)entry.moddate();
			BravoDataEntry::ConvertPacketToEntry(entry);
		}
	};

	/*	BravoDataFlightLeg
	*		Struct for legs
	*/
	struct BravoDataFlightLeg {
		int nFlightLegId;
		BravoTimestamp flightStart;
		BravoTimestamp flightEnd;
		std::string strAircraft;
		std::string strCrew;
		std::string strFlightRoute;
		std::string strFlightNum;
		int nMileage;

		BravoDataFlightLeg()
			: nFlightLegId(-1) 
			, nMileage(0)
		{
		}

		void ConvertPacketToFlightLeg(NetworkPackets::Duties::FlightLeg &flightLeg)
		{
			nFlightLegId = flightLeg.flightlegid();
			flightStart = {	(std::int16_t)flightLeg.starttime().year(), 
							(std::int16_t)flightLeg.starttime().month(),
							(std::int16_t)flightLeg.starttime().day(),
							(std::int16_t)flightLeg.starttime().hour(), 
							(std::int16_t)flightLeg.starttime().minutes() };
			flightEnd = { (std::int16_t)flightLeg.endtime().year(), 
							(std::int16_t)flightLeg.endtime().month(), 
							(std::int16_t)flightLeg.endtime().day(),
							(std::int16_t)flightLeg.endtime().hour(), 
							(std::int16_t)flightLeg.endtime().minutes() };
			strAircraft = flightLeg.aircraft();
			strCrew = flightLeg.crew();
			strFlightRoute = flightLeg.route();
			strFlightNum = flightLeg.fltnum();
			nMileage = flightLeg.mileage();
		}
	};

	/* BravoDataDuty
	*		Duty Data Struct
	*/
	struct BravoDataDuty {
		int id;
		BravoDate date;
		bool dayOffDuty;
		bool dutySplit;
		BravoTimestamp startDutyTime;
		BravoTimestamp endMiddayDutyTime;
		BravoTimestamp startMiddayDutyTime;
		BravoTimestamp endDutyTime;

		std::vector<BravoDataFlightLeg> *flightLegs;

		BravoDataDuty()
			: flightLegs(nullptr)
		{
		}
		
		// Constructor with default value.. This is usually called when the 
		BravoDataDuty(const BravoDate &dte) 
			: id(-1) // -1 means it's not set
			, date({ dte.year, dte.month, dte.day })
			, dayOffDuty(false)
			, dutySplit(false)
			, startDutyTime({ (int)dte.year, (int)dte.month, (int)dte.day, 0, 0 })
			, endMiddayDutyTime({ (int)dte.year, (int)dte.month, (int)dte.day, 4, 0 })
			, startMiddayDutyTime({ (int)dte.year, (int)dte.month, (int)dte.day, 8, 0 })
			, endDutyTime({ (int)dte.year, (int)dte.month, (int)dte.day, 12, 0 })
			, flightLegs(new std::vector<BravoDataFlightLeg>)
		{
		}

		~BravoDataDuty()
		{
			if (flightLegs && flightLegs->size() > 0)
				delete flightLegs;
		}

		void ConvertPacketToDuty(NetworkPackets::Duties::Duty &dutyPacket)
		{
			id = dutyPacket.dutyid();
			date = {(unsigned int)dutyPacket.date().year(), (unsigned int)dutyPacket.date().month(), (unsigned int)dutyPacket.date().day()};

			dayOffDuty = (dutyPacket.dutydayoff() > 0) ? true : false;
			dutySplit = (dutyPacket.dutysplitday() > 0) ? true : false;

			startDutyTime = { (std::int16_t)dutyPacket.dutystarttime().year(),
			(std::int16_t)dutyPacket.dutystarttime().month(),
			(std::int16_t)dutyPacket.dutystarttime().day(),
			(std::int16_t)dutyPacket.dutystarttime().hour(),
			(std::int16_t)dutyPacket.dutystarttime().minutes() };

			endMiddayDutyTime = { (std::int16_t)dutyPacket.dutymiddayend().year(),
			(std::int16_t)dutyPacket.dutymiddayend().month(),
			(std::int16_t)dutyPacket.dutymiddayend().day(),
			(std::int16_t)dutyPacket.dutymiddayend().hour(),
			(std::int16_t)dutyPacket.dutymiddayend().minutes() };

			startMiddayDutyTime = { (std::int16_t)dutyPacket.dutymiddaystart().year(),
			(std::int16_t)dutyPacket.dutymiddaystart().month(),
			(std::int16_t)dutyPacket.dutymiddaystart().day(),
			(std::int16_t)dutyPacket.dutymiddaystart().hour(),
			(std::int16_t)dutyPacket.dutymiddaystart().minutes() };

			endDutyTime = { (std::int16_t)dutyPacket.dutyendtime().year(),
			(std::int16_t)dutyPacket.dutyendtime().month(),
			(std::int16_t)dutyPacket.dutyendtime().day(),
			(std::int16_t)dutyPacket.dutyendtime().hour(),
			(std::int16_t)dutyPacket.dutyendtime().minutes() };

			flightLegs = new std::vector<BravoDataFlightLeg>;

			if (dutyPacket.flightlegs().size() > 0)
			{
				for (auto fl : dutyPacket.flightlegs())
				{
					BravoDataFlightLeg flightLeg;
					flightLeg.ConvertPacketToFlightLeg(fl);

					flightLegs->push_back(flightLeg);
				}
			}
		}
	};

	struct LoginSession {
		const std::string sessionid;

		LoginSession(std::string _sessID)
			: sessionid(_sessID)
		{

		}
	};

	struct UserAccount
	{
		int nUserId;
		std::string User;
		std::string Pass;
		int nType;

		UserAccount()
			: nUserId(0)
			, nType(0)
		{
		}

		void convertProtobufToUserAccount(NetworkPackets::Accounts::Account &acc)
		{
			nUserId = acc.userid();
			User = acc.username();
			Pass = acc.pass();
			nType = acc.accnttype();
		}
	};

	/* DatabaseData Object
	* Object that BravoDatabase Module uses to handle data for the session
	*/
	struct DatabaseData {
		short nVersion;
		int nTotalAccounts;
		std::vector<UserAccount> accounts;

		int nTotalEntries; // TODO: this member might be useless
		std::vector<BravoDataEntry> entries;
		std::vector<BravoDataDuty> duties;

		DatabaseData()
			: nVersion(BravoCurrentVersion)
		{}
	};
}

#endif // !BRAVO_DB_STRUCTURE_H


