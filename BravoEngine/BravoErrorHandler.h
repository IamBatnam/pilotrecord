#ifndef BRAVO_ERROR_MODULE
#define BRAVO_ERROR_MODULE

#if defined(_MSC_VER)
#pragma once
#endif

#include <functional>
#include <queue>
#include <string>
#include "BravoDef.h"

namespace Bravo {
	extern const char* cServerErrorsContent[];

	/* These Enums should only be used when recieving error from the server
	*/
	enum BravoServerError {
		SERVER_DB_ERROR,
		SERVER_UNKNOWN_LOGIN_SESSION,
		SERVER_UNKNOWN_ERROR,
		SERVER_TIMEOUT
	};
	struct BravoError {
		std::string strTitle;
		std::string strContent;
		unsigned int nMsgType; // Type needed for messagebox
		ActionType nAction;

		void setAction(unsigned int nAct)
		{
			if (nAct >= ERROR_LAST_ACTION)
			{
				nAction = ERROR_NO_ACTION;
				return;
			}

			nAction = (ActionType)nAct;
		}
	};

	class BravoErrorHandler
	{
	public:
		enum MessageType {
			ERROR_OKAY_ONLY,
			NOTICE,
			NOTICE_YESNO
		};

		BravoErrorHandler();
		~BravoErrorHandler();

		void ErrorFromServer(unsigned int type, unsigned nVal);
		void Error(const std::string strContent, unsigned int type, int nAct = 0);
		void ShowError(const std::string strTitle, const std::string strContent, unsigned int type, int nAct = 0);
		bool fetchError(std::string &strTitle, std::string &strContent, int &nType, int &nAction);

	protected:
		std::queue<BravoError> errorList;
	};
}

#endif // BRAVO_ERROR_MODULE
