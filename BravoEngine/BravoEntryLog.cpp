#include "BravoEntryLog.h"


namespace Bravo
{
	BravoEntryLog::BravoEntryLog()
		: fTotalHours(0.0f)
		, fTotalSE(0.0f)
		, fTotalME(0.0f)
		, fTotalXCountry(0.0f)
		, fSEDayDual(0.0f)
		, fSEDayPIC(0.0f)
		, fSENightDual(0.0f)
		, fSENightPIC(0.0f)
		, fMEDayDual(0.0f)
		, fMEDayPIC(0.0f)
		, fMEDaySIC(0.0f)
		, fMENightDual(0.0f)
		, fMENightPIC(0.0f)
		, fMENightSIC(0.0f)
		, fXCountryDayDual(0.0f)
		, fXCountryDayPIC(0.0f)
		, fXCountryNightDual(0.0f)
		, fXCountryNightPIC(0.0f)
		, fInstSim(0.0f)
		, fInstHood(0.0f)
		, fInstIFR(0.0f)
		, nInstAppr(0)
	{
	}


	BravoEntryLog::~BravoEntryLog()
	{
	}

	BravoDataEntry BravoEntryLog::GetEntry(int nIndex)
	{
		auto entries = m_pDatabase->FetchEntries();
		return entries[nIndex];
	}

	std::vector<PreviousVersionDataEntry> *BravoEntryLog::GetEntryVersions(int nEntryId)
	{
		std::vector<PreviousVersionDataEntry> oldEntries;

		if (m_pDatabase == nullptr) return nullptr;
		m_pDatabase->FetchPreviousEntries(oldEntries, nEntryId);

		if (oldEntries.size() > 0)
			return new std::vector<PreviousVersionDataEntry>(oldEntries);

		return nullptr;
	}

	void BravoEntryLog::AddEntry(const BravoDataEntry & entry)
	{
		if (m_pDatabase == nullptr) return;
		m_pDatabase->AddEntry(entry);

		AddHoursTotal(entry);
	}

	void BravoEntryLog::UpdateEntry(const BravoDataEntry &updatedEntry)
	{
		if (m_pDatabase == nullptr) return;
		m_pDatabase->UpdateEntry(updatedEntry);
	}

	void BravoEntryLog::ResetTotal()
	{
		fTotalSE = 0;
		fTotalME = 0;
		fTotalHours = 0;
		fTotalXCountry = 0;

		fSEDayDual = 0;
		fSEDayPIC = 0;
		fSENightDual = 0;
		fSENightPIC = 0;

		fMEDayDual = 0;
		fMEDayPIC = 0;
		fMEDaySIC = 0;
		fMENightDual = 0;
		fMENightPIC = 0;
		fMENightSIC = 0;

		fXCountryDayDual = 0;
		fXCountryDayPIC = 0;
		fXCountryNightDual = 0;
		fXCountryNightPIC = 0;

		fInstSim = 0;
		fInstHood = 0;
		fInstIFR = 0;
		nInstAppr = 0;
	}

	void BravoEntryLog::AddHoursTotal(const BravoDataEntry & entry)
	{
		//fTotalHours += entry.;
		float fEntryTotalSE, fEntryTotalME;
		fEntryTotalSE = (entry.SE_DAY_DUAL + entry.SE_DAY_PIC + entry.SE_NIGHT_DUAL + entry.SE_NIGHT_PIC);
		fEntryTotalME = (entry.ME_DAY_DUAL +
			entry.ME_DAY_PIC +
			entry.ME_DAY_SIC +
			entry.ME_NIGHT_DUAL +
			entry.ME_NIGHT_PIC +
			entry.ME_NIGHT_SIC);

		fTotalSE += fEntryTotalSE;
		fTotalME += fEntryTotalME;
		fTotalHours += fEntryTotalSE + fEntryTotalME;
		fTotalXCountry += (entry.XCUN_DAY_DUAL + entry.XCUN_DAY_PIC + entry.XCUN_NIGHT_DUAL + entry.XCUN_NIGHT_PIC);

		fSEDayDual += entry.SE_DAY_DUAL;
		fSEDayPIC += entry.SE_DAY_PIC;
		fSENightDual += entry.SE_NIGHT_DUAL;
		fSENightPIC += entry.SE_NIGHT_PIC;

		fMEDayDual += entry.ME_DAY_DUAL;
		fMEDayPIC += entry.ME_DAY_PIC;
		fMEDaySIC += entry.ME_DAY_SIC;
		fMENightDual += entry.ME_NIGHT_DUAL;
		fMENightPIC += entry.ME_NIGHT_PIC;
		fMENightSIC += entry.ME_NIGHT_SIC;

		fXCountryDayDual += entry.XCUN_DAY_DUAL;
		fXCountryDayPIC += entry.XCUN_DAY_PIC;
		fXCountryNightDual += entry.XCUN_NIGHT_DUAL;
		fXCountryNightPIC += entry.XCUN_NIGHT_PIC;

		fInstSim += entry.SIM_TIME;
		fInstHood += entry.HOOD_TIME;
		fInstIFR += entry.ACTUAL_TIME;
		nInstAppr += entry.IFR_APPROACH;
	}
}
