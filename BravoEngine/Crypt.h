#ifndef ENCRYPT_H
#define ENCRYPT_H

#if defined(_MSC_VER)
#pragma once
#endif

#include "BravoUtility.h"

namespace Bravo
{
	class Crypt
	{
	public:
		Crypt();
		~Crypt();
		static std::string Hash_256(const char* str);
	};
}


#endif // ENCRYPT_H
