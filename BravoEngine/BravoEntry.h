#ifndef BRAVO_ENTRY_H
#define BRAVO_ENTRY_H

#if defined(_MSC_VER)
#pragma once
#endif

#include "BravoEntryFlightDuty.h"
#include "BravoEntryLog.h"
#include "BravoDatabase.h"

namespace Bravo 
{
	class BravoEntry : public BravoEntryLog, public BravoEntryFlightDuty
	{
	public:
		BravoEntry(BravoDatabase *db, BravoErrorHandler *pError);
		virtual ~BravoEntry();

		bool Init() override;

	};
}
#endif // BRAVO_ENTRY_H
