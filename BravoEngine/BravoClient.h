#ifndef BRAVO_CLIENT_H
#define BRAVO_CLIENT_H

#if defined(_MSC_VER) 
#pragma once
#endif

#include "BravoUtility.h"
#include "BravoNetwork.h"
#include "BravoErrorHandler.h"
#include "BravoConfig.h"
#include <mutex>
#include <queue>

#include "BravoDBStructure.h"

namespace Bravo 
{
	// Provides the general timeout for all messages
	extern double fMaxTimeout;
	class BravoClient : public Network::ClientSocket
	{
		friend class BravoUpdate;
	public:
		typedef std::pair<const NetworkPackets::DataPacket, std::chrono::milliseconds> MsgPair;

		enum ClientState {
			CLIENT_DISCONNECTED,
			CLIENT_CONNECTED,
            CLIENT_PROCESSING,
			CLIENT_OUTDATED
		};

		BravoClient(BravoErrorHandler *pErrorModule, BravoConfigHandler *pConfHndlr);
		~BravoClient();

		bool IsConnected();
		void Start();
		bool Reconnect();
		void Disconnect();
		//void SendPacket(const NetworkPackets::DataPacket &packetData);

		ClientState GetClientState();

		////////////////////////////////////////////////////////////////////////////
		// Public Message Processor
		////////////////////////////////////////////////////////////////////////////
		NetworkPackets::DataPacket SendLogOut(LoginSession * activeUser);
		bool SendFindUser(const std::string &strUser);
		NetworkPackets::DataPacket SendCheckLogin(const std::string &strUser, const std::string &strPass);
		NetworkPackets::DataPacket RetrieveAccounts();

		
		NetworkPackets::DataPacket RetrieveEntries(LoginSession *activeUser);
		NetworkPackets::DataPacket RetrievePreviousEntries(LoginSession *activeUser, int &entryId);
		NetworkPackets::DataPacket AddEntry(LoginSession *activeUser, const BravoDataEntry &entry);
		NetworkPackets::DataPacket EditEntry(LoginSession *activeUser, const BravoDataEntry &entry);

		NetworkPackets::DataPacket RetrieveDuties(LoginSession *activeUser, const BravoDate &startDate, const BravoDate &endDate);
		NetworkPackets::DataPacket RetrieveDuties(LoginSession *activeUser, const BravoDate &date);
		NetworkPackets::DataPacket CommitDuty(LoginSession *activeUser, const BravoDataDuty &duty);

	private:
		void PackEntry(const BravoDataEntry &account, NetworkPackets::Entries::Entry *entry);
		void PackDuty(const BravoDataDuty &bravoDuty, NetworkPackets::Duties::Duty *packetDuty);

	protected:
		void OnAfterConnect(Network::Session *client) override;
		void MessageSorter(const NetworkPackets::DataPacket &pckData, Network::Session *client);
		void ErrorHandler(const Bravo::ErrorHandler &error, Network::Session *sess = nullptr);

		void RequestMessageSorter(const NetworkPackets::Request &reqData, Network::Session *client);

		void SendMsg(const NetworkPackets::DataPacket &pcktData);

		const NetworkPackets::DataPacket GetMessage(NetworkPackets::DataPacket::MessageType type);
		void AddMessage(const NetworkPackets::DataPacket &packet);

		std::shared_ptr<ClientState> clientState;
		std::mutex clientStateMutex;

		std::mutex msgMutex;

		std::queue<MsgPair> m_msgs;

		BravoErrorHandler *m_pErrorModule;
		BravoConfigHandler	*m_pConfigHndlr;
	};
}

#endif

