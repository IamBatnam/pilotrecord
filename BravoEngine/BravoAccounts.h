#ifndef BRAVO_ACCOUNTS_H
#define BRAVO_ACCOUNTS_H

#ifdef _MSC_VER
#pragma once  
#endif // _MSC_VER

#include "BravoModule.h"
#include "BravoDBStructure.h"
#include <vector>

namespace Bravo
{
	class BravoAccounts : public BravoModule
	{
	public:
		enum {
			USER,
			_PLACEHOLDER_1,
			_PLACEHOLDER_2,
			_PLACEHOLDER_3,
			ADMIN,
		};

		BravoAccounts(BravoDatabase *db, BravoErrorHandler *error);
		virtual ~BravoAccounts();

		bool Init() override;
		void CheckActiveUser();
		bool CheckLogin(const std::string &strUser, const std::string &strPass);
		void AddUser(std::string user, std::string pass, unsigned int type = ADMIN);
		void RetrieveUsers(std::function<void(std::string, int)> func);
		std::vector<UserAccount> &RetrieveUsers();
	};

}

#endif // !BRAVO_ACCOUNTS_H
