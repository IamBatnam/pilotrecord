#include "BravoDatabaseFile.h"

#include <cctype>
#include <algorithm>

std::string strFile = "PilotRecordSample.ptr";

namespace Bravo
{
	BravoDatabaseFile::BravoDatabaseFile()
		: _file(nullptr)
	{
		ASSERT(0);
		_file = new BravoFileHandler;
	}


	BravoDatabaseFile::~BravoDatabaseFile()
	{
		CloseDatabase();
	}

	bool BravoDatabaseFile::Init()
	{
		if (_file == 0) return false;

		if (!dbConfig.accounts.empty())
			dbConfig.accounts.clear();

		_file->OpenFile(::strFile);
		ReadData(dbConfig);	
		return true;
	}

	void BravoDatabaseFile::Create()
	{
		_file->Create(::strFile);

		if (!(*_file >> BravoCurrentVersion))
			throw std::runtime_error("Error writing version!");


		// Add accounts
		if (!(*_file >> (int)dbConfig.accounts.size()))
			throw std::runtime_error("Error writing initial entry");

		if (dbConfig.accounts.size() > 0)
		{
			// Write accounts to file
			for (auto it : dbConfig.accounts)
			{
				*_file >> it.User;
				*_file >> it.Pass;
				*_file >> it.nType;
			}
		}
		
		if (!(*_file >> dbConfig.entries.size()))
			throw std::runtime_error("Error writing initial entry");

		if (dbConfig.entries.size() > 0)
		{
			// Write accounts to file
			for (auto it : dbConfig.entries)
			{
				
			}
		}

		_file->CloseFile();
	}

	bool BravoDatabaseFile::CheckFileExist()
	{
		return _file->CheckFileExist(strFile);
	}

	void BravoDatabaseFile::CloseDatabase()
	{
		if (_file)
			_file->CloseFile();
	}
	
	void BravoDatabaseFile::ReadData(DatabaseData &config)
	{
		if (!_file) return;

		*_file << config.nVersion;
		*_file << config.nTotalAccounts;

		if (config.nTotalAccounts > 0)
		{
			for (int i = 0; i < config.nTotalAccounts; i++)
			{
				UserAccount acc;
				*_file << acc.User;
				*_file << acc.Pass;
				*_file << acc.nType;

				config.accounts.push_back(acc);
			}
		}

		*_file << config.nTotalEntries;

		if (config.nTotalEntries > 0)
		{
			for (int i = 0; i < config.nTotalEntries; i++)
			{
				BravoDataEntry entry;
				*_file << entry._date;
				*_file << entry.cPilotName;
				*_file << entry.cPax;
				*_file << entry.cPlaneType;
				*_file << entry.cPlaneReg;
				*_file << entry.strRemarks;

				*_file << entry.SE_DAY_DUAL;

				config.entries.push_back(entry);
			}
		}
	}
	
	void BravoDatabaseFile::SaveData(DatabaseData &config)
	{
		if (!_file) return;
		_file->SetSeekSet();

		*_file >> config.nVersion;
		*_file >> (int)config.accounts.size();

		if (config.accounts.size() > 0)
		{
			// Write accounts to file
			for (auto it : config.accounts)
			{
 				*_file >> it.User;
				*_file >> it.Pass;
				*_file >> it.nType;
			}
		}

		*_file >> config.nTotalEntries;

		if (config.nTotalEntries > 0)
		{
			for (auto it : config.entries)
			{
				*_file >> it._date;
				*_file >> it.cPilotName;
				*_file >> it.cPax;
				*_file >> it.cPlaneType;
				*_file >> it.cPlaneReg;
				*_file >> it.strRemarks;

				*_file >> it.SE_DAY_DUAL;

				// hours
				
			}
		}
	}

	bool BravoDatabaseFile::FindUser(const std::string & strUser)
	{
		if (dbConfig.accounts.size() > 0)
		{
			for (auto one : dbConfig.accounts)
			{
				auto it = std::search(one.User.begin(), one.User.end(), strUser.begin(), strUser.end(),
					[](char ch1, char ch2) { return std::tolower(ch1) == std::tolower(ch2); });
				if (it != one.User.end())
				{
					return true;
				}
			}
		}

		return false;
	}

	UserAccount *BravoDatabaseFile::FindUser(const std::string & strUser, const std::string & strPass)
	{
		assert(0);
		//if (dbConfig.accounts.size() > 0)
		//{
		//	for (auto one : dbConfig.accounts)
		//	{
		//		auto it = std::search(one.User.begin(), one.User.end(), strUser.begin(), strUser.end(),
		//			[](char ch1, char ch2) { return std::tolower(ch1) == std::tolower(ch2); });
		//		if (it != one.User.end())
		//		{
		//			auto it2 = std::search(one.Pass.begin(), one.Pass.end(), strPass.begin(), strPass.end(),
		//				[](char c1, char c2) { return std::toupper(c1) == std::toupper(c2); });
		//			if (it2 != one.Pass.end())
		//			{
		//				return &one;
		//			}
		//		}
		//	}
		//}

		return nullptr;
	}
}
