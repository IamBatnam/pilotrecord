#include "BravoErrorHandler.h"

namespace Bravo 
{
	const char* cServerErrorsContent[] = {
		"There was a problem getting the data from the server.",
		"The Login session was not found.",
		"Unknown error from the server.",
		"The server didn't respond within the alloted time resulting in a time out"
	};

	BravoErrorHandler::BravoErrorHandler()
	{
	}


	BravoErrorHandler::~BravoErrorHandler()
	{
	}

	void BravoErrorHandler::ErrorFromServer(unsigned int type, unsigned nVal)
	{
		BravoError _error;
		_error.strTitle = "Server Error!";
		_error.strContent = cServerErrorsContent[nVal];
		_error.nMsgType = BravoErrorHandler::ERROR_OKAY_ONLY;
		_error.setAction(0);

		errorList.push(_error);
	}

	void BravoErrorHandler::Error(const std::string strContent, unsigned int type, int nAct)
	{
		ShowError("Error!", strContent, type, nAct);
	}

	void BravoErrorHandler::ShowError(const std::string strTitle, const std::string strContent, unsigned int type, int nAct)
	{
		BravoError _error;
		_error.strTitle = strTitle;
		_error.strContent = strContent;
		_error.nMsgType = type;
		_error.setAction(nAct);

		errorList.push(_error);
	}

	bool BravoErrorHandler::fetchError(std::string &strTitle, std::string &strContent, int &nType, int &nAction)
	{
		if (errorList.size() > 0)
		{
			auto error = errorList.front();
			strTitle = error.strTitle;
			strContent = error.strContent;
			nType = error.nMsgType;
			nAction = error.nAction;

			errorList.pop();

			return true;
		}
			
		return false;
	}

}