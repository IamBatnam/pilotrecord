#include "BravoFileHandler.h"
#include "BravoServerConfigFile.h"

namespace Bravo {
	BravoServerConfigFile::BravoServerConfigFile()
	{
	}

	BravoServerConfigFile::~BravoServerConfigFile()
	{
	}

	bool BravoServerConfigFile::Initialize()
	{
		if (!CheckFileExist(serverConfFile))
			return false;

		if (!OpenFile(serverConfFile))
			return false;

		ParseConfig();

		return true;
	}

	void BravoServerConfigFile::ParseConfig()
	{
		// UNDONE Parsing file
		std::string line, strkey, strval;
		while (std::getline(*_file, line))
		{
			auto pos = line.find(" = ");
			strkey = line.substr(0, pos);
			strval = line.substr(pos + 3);

			auto ret = configMap.insert({ strkey, strval });

			if (ret.second == false)
				SERVER_OUT("[CONFIG]: Duplicate '%s' entry. Retaining first value!\n", strkey.c_str());
			else
				SERVER_OUT("[CONFIG]: ['%s']['%s']\n", strkey.c_str(), strval.c_str());
		}
	}

	std::string BravoServerConfigFile::operator[](const char * cKey)
	{
		if (configMap.find(cKey) == configMap.end())
		{
			SERVER_OUT("[CONFIG]: Invalid key '%s' is being accessed.\n", cKey);
			return std::string();
		}

		return configMap[cKey];
	}

	bool BravoServerConfigFile::Find(const std::string &strKey)
	{
		return configMap.find(strKey.c_str()) != configMap.end();
	}
}