#ifndef BRAVO_ENTRY_FLIGHT_DUTY_H
#define BRAVO_ENTRY_FLIGHT_DUTY_H

#if defined(_MSC_VER)
#pragma once
#endif

#include "BravoModule.h"
#include "BravoDatabase.h"

namespace Bravo
{
	class BravoEntryFlightDuty : virtual public BravoModule
	{
	public:
		BravoEntryFlightDuty();
		virtual ~BravoEntryFlightDuty();
		
		template<typename T>
		void GetDutiesMinInfo(const BravoDate& startDate, const BravoDate& endDate, void* pObj, T func);
        int GetDutiesMinInfoContained(const BravoDate& startDate, const BravoDate& endDate, bool bAbleCached = true);
        BravoDataDuty AccessDutyMinInfo(const BravoDate &date);
       
        
        void CommitActiveDuty();

		void AddDutyLeg(const BravoTimestamp startTime,
			const BravoTimestamp endTime,
			const std::string strAircraft,
			const std::string strCrew, 
			const std::string strRoute, 
			const std::string strFltNum, 
			const int nMileage);

		bool DeleteDutyLeg(const int nIndex);
		void UpdateDutyLeg(int nIndex, 
			const BravoTimestamp startTime,
			const BravoTimestamp endTime,
			const std::string strAircraft,
			const std::string strCrew,
			const std::string strRoute,
			const std::string strFltNum,
			const int nMileage);
		
		// Active duty
		void SetActiveDuty(unsigned int month, unsigned int day, unsigned int year);
		BravoTimestamp &getActiveDutyStartTime();
		BravoTimestamp &getActiveDutyMiddayEndTime();
		BravoTimestamp &getActiveDutyMiddayStartTime();
		BravoTimestamp &getActiveDutyEndTime();
		bool getDayOff();
		bool getActiveDutySplitDuty();
        
		template<typename T>
		void getActiveFlightLegs(T func);
        int getActiveDutyLegsCount();
        const BravoDataFlightLeg &getActiveDutyFlightLeg(int nIndex);

		void setActiveDutyStartTime(const BravoTimestamp &timestamp);
		void setActiveDutyEndTime(const BravoTimestamp &timestamp);
		void setActiveDutyDayOff(const bool &bStatus);
		void setActiveDutySplitDuty(const bool &bStatus);
		void setActiveDutyMiddayEndTime(const BravoTimestamp &timestamp);
		void setActiveDutyMiddayStartTime(const BravoTimestamp &timestamp);

		bool isActiveDutyChanged();
		void setActiveDutyChanged(bool bChanged);
	
	private:
		BravoDataDuty *pActiveDuty;
		bool bActiveDutyChanged;
        
        // dates of previosly requested datta
        BravoDate requestedStartDate;
        BravoDate requestedEndDate;
	};

	/* GetDutiesMinInfo
			DESC:	Get the MinInfo which only includes start and end times
				NO Legs included for effciency
	*/
	template<typename T>
	inline void BravoEntryFlightDuty::GetDutiesMinInfo(
		const BravoDate& startDate, 
		const BravoDate& endDate,
		void* pObj, 
		T func)
	{
		if (m_pDatabase == nullptr)
			return;
        
        requestedStartDate = startDate;
        requestedEndDate = endDate;

		m_pDatabase->FetchDutiesMinimal(startDate, endDate, pObj, func);
	}

	template<typename T>
	inline void BravoEntryFlightDuty::getActiveFlightLegs(T func)
	{
		if (pActiveDuty == nullptr || pActiveDuty->flightLegs == nullptr)
			return;

		for (auto it : *pActiveDuty->flightLegs)
		{
			func({ { it.flightStart.year, it.flightStart.month, it.flightStart.day },
				{ it.flightStart.hour, it.flightStart.min } },
				{ { it.flightEnd.year, it.flightEnd.month, it.flightEnd.day }, 
				{ it.flightEnd.hour, it.flightEnd.min } },
				it.strCrew.c_str(),
				it.strAircraft.c_str(),
				it.strFlightRoute.c_str(),
				it.strFlightNum.c_str(),
				it.nMileage);
		}
	}
}
#endif // BRAVO_ENTRY_FLIGHT_DUTY_H
