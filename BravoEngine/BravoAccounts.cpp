#include "BravoAccounts.h"

#include "BravoEngine.h"

#include "Crypt.h"

namespace Bravo
{
	BravoAccounts::BravoAccounts(BravoDatabase *db, BravoErrorHandler *error)
	{
		m_pDatabase = db;
		m_pErrorModule = error;
	}


	BravoAccounts::~BravoAccounts()
	{
	}

	bool BravoAccounts::Init()
	{
		if(m_pDatabase == nullptr)
			return false;

		return true;
	}

	bool BravoAccounts::CheckLogin(const std::string & strUser, const std::string & strPass)
	{
		if (m_pDatabase == nullptr) return false;
		auto strHashed = Crypt::Hash_256(strPass.c_str());

		auto foundAccount = m_pDatabase->CheckLogin(strUser, strHashed);

		if (foundAccount)
			return true;

		return false;
	}

	// TODO:
	//	- Whis is this not returning a bool type result?
	//	- This was designed when the database was based of a file and not coming from the server.
	//		* REDESIGN - to be supported when Edit Account function becomes available
	//
	void BravoAccounts::AddUser(std::string user, std::string pass, unsigned int type)
	{
		if (!m_pDatabase) return;

		UserAccount User;
		User.User = user;
		User.Pass = Crypt::Hash_256(pass.c_str());
		User.nType = type;
		

		if (!m_pDatabase->FindUser(User.User))
			m_pDatabase->AddLoginUser(User);
		
		//return m_pDatabase->GetLoginSize();
	}

	void BravoAccounts::RetrieveUsers(std::function<void(std::string, int)> func)
	{
		for (auto user : RetrieveUsers())
		{
			func(user.User, user.nType);
		}
	} 

	std::vector<UserAccount> &BravoAccounts::RetrieveUsers()
	{
		//if (!m_pDatabase) return std::vector<UserAccount>();
		return m_pDatabase->RetrieveUsers();
	}
}