#ifndef BRAVO_SERVER_TERM_H
#define BRAVO_SERVER_TERM_H

#if defined(_MSC_VER) 
#pragma once
#endif

#include <string>

namespace Bravo { 
	class Server;
	class ServerTerminal
	{
	public:
		ServerTerminal(Server *server);
		~ServerTerminal();

		void StartTerminal();

	protected:
		void StatCheck();
		void TerminalInit();
		void ParseCommand(const char *strLine, std::string &strCommand, std::string & strArgs);
	
	private:
		Server * m_pServer;
	};

}

#endif // BRAVO_SERVER_TERM_H
