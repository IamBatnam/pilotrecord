/* BravoFileHandler
* Class that handles files directly	
* CHANGELOGS
* - Updated 18AUG2017, changed file handler from C style FILE to
*	C++ style fstream
*/
#include "BravoFileHandler.h"


namespace Bravo
{
	BravoFileHandler::BravoFileHandler()
		: _file(new std::fstream)
		, bBinary(false)
	{
	}


	BravoFileHandler::~BravoFileHandler()
	{
	}

	bool BravoFileHandler::OpenFile(std::string strFileName)
	{
		//fopen_s(&_file, strFileName.c_str(), "rb+");
		//if (!_file)
		//	return false;
		if (_file->is_open())
			_file->close();

		_file->open(strFileName.c_str(), std::ios_base::in | std::ios_base::out |((bBinary) ? std::ios_base::binary : (std::ios_base::openmode)0));
		if(_file->is_open())
			return true;

		return false;
	}

	void BravoFileHandler::CloseFile()
	{
		//fclose(_file);
		_file->close();
	}

	/* @- Create a file and replace if exists and leave it opened.
	*Remeber to call close!
	*/
	bool BravoFileHandler::Create(std::string strFile)
	{
		//fopen_s(&_file, strFile.c_str(), "wb+");
		//if (_file == 0)
		//{
		//	fclose(_file);
		//	return false;
		//}
		_file->open(strFile, std::ios_base::out | std::ios_base::binary);
		if (_file->bad())
			throw std::runtime_error("Bad() returned true while opening a file");


		return true;
	}

	bool BravoFileHandler::CheckFileExist(std::string strFileName)
	{
		//FILE *pFile;
		//fopen_s(&pFile, strFileName.c_str(), "r");
		std::fstream pFile (strFileName, std::ios_base::out | std::ios_base::in | std::ios_base::binary);
		if (pFile.is_open())
		{
			//fclose(pFile);
			pFile.close();
			return true;
		}

		return false;
	}

	void BravoFileHandler::SetSeekSet()
	{
		if (_file->is_open())
			_file->seekg(0, _file->beg);
	}

////////////////////////////////////////////////////////////////////////////////////
// Operators
////////////////////////////////////////////////////////////////////////////////////

	void BravoFileHandler::operator <<(short &out)
	{
		/*auto sample = new short[sizeof(short)];
		fread_s(sample, sizeof(short), sizeof(short), 1, _file);
		out = *sample;
		return *sample;*/
		short *ret = new short[sizeof(short)];
		_file->read((char*)ret, sizeof(short));
		out = *ret;
	}

	void BravoFileHandler::operator <<(long &out)
	{
		/*auto sample = new long[sizeof(long)];
		fread_s(sample, sizeof(long), sizeof(long), 1, _file);
		out = *sample;
		return *sample;*/
		long *ret = new long[sizeof(long)];
		_file->read((char*)ret, sizeof(long));
		out = *ret;
	}

	void BravoFileHandler::operator <<(int &out)
	{
		//auto sample = new int[sizeof(int)];
		//fread_s(sample, sizeof(int), sizeof(int), 1, _file);
		//out = *sample;
		//return *sample;
		int *ret = new int[sizeof(int)];
		_file->read((char*)ret, sizeof(int));
		out = *ret;
	}

	void BravoFileHandler::operator <<(size_t & out)
	{
		//auto sample = new size_t[sizeof(size_t)];
		//auto result = fread_s(sample, sizeof(size_t), sizeof(size_t), 1, _file);
		//out = *sample;
		//return *sample;
		size_t *ret = new size_t[sizeof(size_t)];
		_file->read((char*)ret, sizeof(size_t));
		out = *ret;
	}

	void BravoFileHandler::operator<<(float & out)
	{
		//char *cBuff = new char[4];
		//operator << (cBuff);

		//auto fBuff = reinterpret_cast<float*>(cBuff);
		//return *fBuff;
		float *ret = new float[sizeof(float)];
		_file->read((char*)ret, sizeof(float));
		out = *ret;
	}

	void BravoFileHandler::operator <<(char * & out)
	{
		short length;
		operator << (length);

		char *ret = new char[length];
		_file->read(ret, length);
		ret[length] = 0;
		out = ret;
		//auto buffer = new char[length];
		//auto result = fread_s(buffer, length, sizeof(char), length, _file);
		//buffer[length] = 0;
		//out = buffer;

		//return buffer;
	}

	void BravoFileHandler::operator <<(std::string & out)
	{
		short length;
		operator << (length);

		auto buffer = new char[length];
		//auto result = fread_s(buffer, length, sizeof(char), length, _file);
		_file->read(buffer, length);
		buffer[length] = 0;
		out = buffer;
	}

////////////////////////////////////////////////////////////////////////////////////
// Write functions
////////////////////////////////////////////////////////////////////////////////////
	bool BravoFileHandler::operator >>(const short &in)
	{
		//if (!_file) return false;
		//fwrite((void *)&in, sizeof(short), 1, _file);
		//*(_file) >> const_cast<short&>(in);
		_file->write((char*)&in, sizeof(short));
		return true;
	}

	bool BravoFileHandler::operator >>(const long &in)
	{
		//if (!_file) return false;
		//fwrite((void *)&in, sizeof(long), 1, _file);
		_file->write((char*)&in, sizeof(long));
		return true;
	}

	bool BravoFileHandler::operator >>(const int &in)
	{
		//if (!_file) return false;
		//fwrite((void *)&in, sizeof(int), 1, _file);
		_file->write((char*)&in, sizeof(int));
		return true;
	}

	bool BravoFileHandler::operator >>(const size_t & in)
	{
		//if (!_file) return false;
		//fwrite((void *)&in, sizeof(size_t), 1, _file);
		_file->write((char*)&in, sizeof(size_t));
		return true;
	}

	bool BravoFileHandler::operator >>(const char * in)
	{
		if (!(operator >> ((short)StrLen(in)))) return false;

		//if (!_file)
		//	return false;
		//fwrite(in, sizeof(char), strlen(in), _file);
		_file->write(in, StrLen(in));

		return true;
	}

	bool BravoFileHandler::operator >> (const std::string &in)
	{
		//if(!(operator >> ( (short)in.length() ))) return false;
		//if (!_file)
		//	return false;
		//fwrite(in.c_str(), sizeof(char), in.length(), _file);
		operator >> (in.c_str());

		return true;
	}

	bool BravoFileHandler::operator >> (const float & in)
	{
		//if (!_file) return false;
		//auto charBuff = reinterpret_cast<char *>(const_cast<float*> (&in));
		//operator >> (charBuff);
		auto sample = reinterpret_cast<const char*>(&in);
		_file->write(sample, sizeof(float));

		return true;
	}
	
}
