#ifndef BRAVO_FILE_HANDLER_H
#define BRAVO_FILE_HANDLER_H

#ifdef _MSC_VER
#pragma once  
#endif // _MSC_VER

#include "Crypt.h"
#include <fstream>

namespace Bravo
{
	class BravoFileHandler
	{
		friend class BravoServerConfigFile;
	public:
		bool OpenFile(std::string strFileName);
		void CloseFile();

		bool Create(std::string strFileName);
		bool CheckFileExist(std::string strFileName);
		void SetSeekSet();

		void operator <<(short &out);
		void operator <<(long &out);
		void operator <<(int &out);
		void operator <<(size_t &out);
		void operator <<(float &out);
		void operator <<(char * &out);
		void operator <<(std::string &out);

		bool operator >>(const short &in);
		bool operator >>(const long &in);
		bool operator >>(const int &in);
		bool operator >> (const size_t &in);
		bool operator >> (const char* in);
		bool operator >> (const std::string &in);
		bool operator >> (const float &in);

	public:
		BravoFileHandler();
		~BravoFileHandler();
	
	private:
		std::fstream *_file;
		bool bBinary;
	};
}

#endif // BRAVO_FILE_HANDLER_H

