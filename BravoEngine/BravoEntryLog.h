#ifndef BRAVO_ENTRY_LOG_H
#define BRAVO_ENTRY_LOG_H

#if defined(_MSC_VER)
#pragma once
#endif

#include "BravoModule.h"
#include "BravoDatabase.h"

namespace Bravo
{
	class BravoEntryLog : virtual public BravoModule
	{
	public:
		BravoEntryLog();
		virtual ~BravoEntryLog();

		void AddEntry(const BravoDataEntry &entry);
		void UpdateEntry(const BravoDataEntry &entry);
		template<typename T> void GetInitialEntries(T func);
		BravoDataEntry GetEntry(int nIndex);
		std::vector<PreviousVersionDataEntry> *GetEntryVersions(int nEntryId);

		float GetTotalHours() { return fTotalHours; };
		float GetTotalSEHours() { return fTotalSE; };
		float GetTotalMEHours() { return fTotalME; };
		float GetTotalXCunHours() { return fTotalXCountry; };

		float GetTotalSEDayDual() { return fSEDayDual; };
		float GetTotalSEDayPIC() { return fSEDayPIC; };
		float GetTotalSENightDual() { return fSENightDual; };
		float GetTotalSENightPIC() { return fSENightPIC; };

		float GetTotalMEDayDual() { return fMEDayDual; };
		float GetTotalMEDayPIC() { return fMEDayPIC; };
		float GetTotalMEDaySIC() { return fMEDaySIC; };
		float GetTotalMENightDual() { return fMENightDual; };
		float GetTotalMENightPIC() { return fMENightPIC; };
		float GetTotalMENightSIC() { return fMENightSIC; };

		float GetTotalXCountryDayDual() { return fXCountryDayDual; };
		float GetTotalXCountryDayPIC() { return fXCountryDayPIC; };
		float GetTotalXCountryNightDual() { return fXCountryNightDual; };
		float GetTotalXCountryNightPIC() { return fXCountryNightPIC; };

		float GetTotalSim() { return fInstSim; };
		float GetTotalHood() { return fInstHood; };
		float GetTotalIFR() { return fInstIFR; };
		int GetTotalInstrumentApproach() { return nInstAppr; };

	protected:
		void AddHoursTotal(const BravoDataEntry &entry);
		void ResetTotal();
	private:
		float fTotalHours;
		float fTotalSE, fTotalME, fTotalXCountry;
		float fSEDayDual, fSEDayPIC, fSENightDual, fSENightPIC;
		float fMEDayDual, fMEDayPIC, fMEDaySIC, fMENightDual, fMENightPIC, fMENightSIC;
		float fXCountryDayDual, fXCountryDayPIC, fXCountryNightDual, fXCountryNightPIC;
		float fInstSim, fInstHood, fInstIFR;
		int nInstAppr;
	};

	/* GetEntries
	* - Calls a lambda function which recieves the following:
	*	* const char* _date - "01JAN2017" format // TODO:UNP - Have set
	*	* const char* _ACtype - AC type
	*	* const char* _ACreg - AC reg
	*	* const char* _remarks - comments about the flight limits to 30 chars
	* TODO:
	*	- Limit the amount of chars of remarks
	*/
	template<typename T>
	inline void BravoEntryLog::GetInitialEntries(T func)
	{
		ResetTotal();
		m_pDatabase->FetchInitialEntries([=](const BravoDataEntry &entry) {
			func(entry.GetDateStrFormat().c_str(),
				entry.cPilotName.c_str(),
				entry.cPlaneType.c_str(),
				entry.cPlaneReg.c_str(),
				entry.strRemarks.c_str());
			AddHoursTotal(entry);
		});
	}
}
#endif // BRAVO_ENTRY_LOG_H
