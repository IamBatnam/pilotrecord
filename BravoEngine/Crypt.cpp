#include "Crypt.h"

#include <cryptopp/sha.h>
#include <cryptopp/hex.h>

namespace Bravo
{
	Crypt::Crypt()
	{
	}


	Crypt::~Crypt()
	{
	}

	std::string Crypt::Hash_256(const char * str)
	{
		byte digest[CryptoPP::SHA256::DIGESTSIZE];

		CryptoPP::SHA256 hash;
		hash.CalculateDigest(digest, (const byte*)str, strlen(str));

		CryptoPP::HexEncoder encoder;
		std::string output;

		encoder.Attach(new CryptoPP::StringSink(output));
		encoder.Put(digest, sizeof(digest));
		encoder.MessageEnd();

		return output;
	}

}