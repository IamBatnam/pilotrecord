#ifndef BRAVO_CONFIG_H
#define BRAVO_CONFIG_H

#if defined(_MSC_VER) 
#pragma once
#endif

#include <string>
#include "BravoErrorHandler.h"
#include <map>

namespace Bravo {
	class BravoConfigHandler
	{
	public:
		BravoConfigHandler(BravoErrorHandler *pErrorHnd);

		/* Reconfig 
		*	- replaces or add aditional config on runtime
		*/
		void Reconfig(const std::string strKey, const std::string strVal);

		/* Operator []
		* Access the map through method
		*/
		const std::string& operator [](const std::string &strKey);

	protected:
		BravoErrorHandler *m_pErrorHndler;
		std::map<std::string, std::string> m_configMap;
	};
}

#endif // BRAVO_CONFIG_H
