#include "BravoConfig.h"

namespace Bravo {
	BravoConfigHandler::BravoConfigHandler(BravoErrorHandler * pErrorHnd)
	{
		m_pErrorHndler = pErrorHnd;

		m_configMap.insert(std::make_pair("servIP", "pilot.raviuz.com"));
		// m_configMap.insert(std::make_pair("servPort", "5002"));
	}

	void BravoConfigHandler::Reconfig(std::string strKey, std::string strVal)
	{
		auto it_found = m_configMap.find(strKey);

		if (it_found != m_configMap.end())	// If key exist replace
			it_found->second = strVal;
		else	// add new config if not found
			m_configMap.insert(std::make_pair(strKey, strVal));

	}

	const std::string & BravoConfigHandler::operator[](const std::string &strKey)
	{
		return m_configMap[strKey];
	}

}