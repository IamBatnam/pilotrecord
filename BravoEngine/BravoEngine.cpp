#include "BravoEngine.h"

BravoEngine *BravoEngine::_instance = 0;

BravoEngine::BravoEngine()
	: _errorHandler(nullptr)
	, _database(nullptr)
	, _accounts(nullptr)
	, _entry(nullptr)
	, _netClient(nullptr)
	, _state(STATE_LOCKED)
{
	_errorHandler = new Bravo::BravoErrorHandler;
	_config = new Bravo::BravoConfigHandler(_errorHandler);
}


BravoEngine::~BravoEngine()
{	
}

/* Reconfig
*	Replace exisiting config or Inserts if not set
*/
void BravoEngine::Reconfig(const std::string &strKey, const std::string &strVal)
{
	_config->Reconfig(strKey, strVal);
}

bool BravoEngine::Initialize()
{
    // Check first if client already connected
    if(_netClient)
    {
        if(_netClient->GetClientState() == Bravo::BravoClient::CLIENT_CONNECTED)
            return true;
    }
    
	_netClient = new Bravo::BravoClient(_errorHandler, _config);
	_netClient->Start();

	if (_netClient->GetClientState() == Bravo::BravoClient::CLIENT_DISCONNECTED)
	{
		OUTPUT_DEBUG("CLIENT FAILED TO CONNECT TO SERVER!\n");
        return false;
	}

	_database = new Bravo::BravoDatabase(_netClient, _errorHandler);
 	_accounts = new Bravo::BravoAccounts(_database, _errorHandler);
	_entry = new Bravo::BravoEntry(_database, _errorHandler);

	if (!_database->Init() && !IsClientOnline())
		return false;

	if (!_accounts->Init())
		return false;

	if (!_entry->Init())
		return false;

	return true;
}

bool BravoEngine::CheckUpdates(std::string &strUrlDownload)
{
	if (_netClient->GetClientState() == Bravo::BravoClient::CLIENT_OUTDATED)
	{
		Bravo::BravoUpdate _update(_netClient, _errorHandler);
		_update.getUpdateUrl(strUrlDownload);

		return true;
	}
	return false;
}


void BravoEngine::LogOut()
{
	if (_state == STATE_LOCKED) return;
	_database->Logout();
	_netClient->Disconnect();
	
	
	_state = STATE_LOCKED;

	//delete _netClient;
}

bool BravoEngine::IsClientOnline()
{
	return _netClient->IsConnected();
}

void BravoEngine::ReconnectClient()
{
	if (!_netClient->IsConnected())
		_netClient->Reconnect();
}

BravoEngine::BravoEngineState BravoEngine::State()
{
	return _state;
}

void BravoEngine::Login(std::string &strUser, std::string &strPass)
{
	if (_accounts->CheckLogin(strUser, strPass))
	{
		_state = STATE_LOGGEDIN;
	}
}

void BravoEngine::addError(const std::string & strTitle, const std::string & strContent, const int & ntype)
{
	if (_errorHandler)
	{
		_errorHandler->ShowError(strTitle, strContent, ntype);
	}
}

bool BravoEngine::fetchError(std::string &strTitle, std::string &strContent, int &nType, int &nAct)
{
	return _errorHandler->fetchError(strTitle, strContent, nType, nAct);
}

//void BravoEngine::RetrieveAccounts(std::function<void(std::string, int)> func)
//{
//	_accounts->RetrieveUsers(func);
//}


