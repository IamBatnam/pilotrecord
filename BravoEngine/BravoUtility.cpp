#include "BravoUtility.h"	
//#ifdef BRAVO_LINUX
#include <stdio.h>
#include <cstring>
//#endif

namespace Bravo 
{
	void StringCopy(char *dest, const char *src)
	{
#ifdef BRAVO_WINDOWS
		strcpy_s(dest, sizeof(dest), src);
#else
		::strcpy(dest, src);
#endif
	}

	size_t StrLen(const char *str)
	{
		return ::strlen(str);
	}

	std::string Util::GetDateTimeStr(const std::string &strFormat, const std::tm &time)
	{
		std::unique_ptr<char[]> strTime(new char[MAX_STR_CONTENT]);

		// Format ("Mon MmmDDYYYY 00:00:00")
		strftime(strTime.get(), MAX_STR_CONTENT, strFormat.c_str(), &time);

		return std::string(strTime.get());
	}
}