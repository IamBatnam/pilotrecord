#ifndef BRAVO_WRAPPER_H
#define  BRAVO_WRAPPER_H


#ifdef __cplusplus
extern "C" {
#endif
    struct CTimestamp{
        int year;
        int month;
        int day;
        int hour;
        int minute;
    };

    void *BravoEngineObj();
    bool BravoInitialize(void *pObj);
    void BravoLogin(void *pObj, const char *szUsername, const char *szPassword);
    void BravoLogout(void *pObj);
	int BravoEngineState(void *pObj);

    // Getting minimum info of duties
    void BravoGetMinMonthlyDuties(void *pObj, unsigned int sYear, unsigned int sMonth, unsigned int sDay, unsigned int eYear, unsigned int eMonth, unsigned int eDay);
    int BravoGetMinDutyStatus(void *pObj, unsigned int sYear, unsigned int sMonth, unsigned int sDay);

    // Active Duties
    void BravoSetActiveDuty(void *pObj, unsigned int sYear, unsigned int sMonth, unsigned int sDay);
    bool BravoActiveDutyGetDayOff(void *pObj);
    bool BravoActiveDutyGetSplitDuty(void *pObj);
    void *BravoActiveDutyGetStartTime(void *pObj);
    void *BravoActiveDutyGetMidEndTime(void *pObj);
    void *BravoActiveDutyGetMidStartTime(void *pObj);
    void *BravoActiveDutyGetEndTime(void *pObj);

    int BravoActiveDutyCountFlightLegs(void *pObj);
    void *BravoActiveDutyFlightLeg(void *pObj, int nIndex);

    void BravoActiveDutySetDayOff(void *pObj, bool bDayOff);
    void BravoActiveDutySetSplitDuty(void *pObj, bool bSplitShift);
    void BravoActiveDutySetStartTime(void *pObj, const struct CTimestamp timeStamp);
    void BravoActiveDutySetMidEndTime(void *pObj, const struct CTimestamp timeStamp);
    void BravoActiveDutySetMidStartTime(void *pObj, const struct CTimestamp timeStamp);
    void BravoActiveDutySetEndTime(void *pObj, const struct CTimestamp timeStamp);


    void BravoActiveDutyCommitActiveDuty(void *pObj);


//***********************************************************************************
// BravoUtil
//***********************************************************************************
    int BravoUtil_GetTimestampYear(void *timestampObj);
    int BravoUtil_GetTimestampMonth(void *timestampObj);
    int BravoUtil_GetTimestampDay(void *timestampObj);
    int BravoUtil_GetTimestampHour(void *timestampObj);
    int BravoUtil_GetTimestampMinute(void *timestampObj);

    const char *BravoUtil_GetFlightLegAircraft(void *pFlightLeg);
    const char *BravoUtil_GetFlightLegRoute(void *pFlightLeg);
    const char *BravoUtil_GetFlightLegFlightNum(void *pFlightLeg);
    const char *BravoUtil_GetFlightLegCrew(void *pFlightLeg);
    int BravoUtil_GetFlightLegMileage(void *pFlightLeg);
    void *BravoUtil_GetFlightLegUpTimestamp(void *pFlightLeg);
    void *BravoUtil_GetFlightLegDownTimestamp(void *pFlightLeg);

    
    /*void bravo_duty_add_to_buffer(const Bravo::BravoDate &dutyDate,
                                  const Bravo::BravoTimeStamp &start,
                                  const Bravo::BravoTimeStamp &end);*/
#ifdef __cplusplus
}
#endif

#endif // BRAVO_WRAPPER_H
