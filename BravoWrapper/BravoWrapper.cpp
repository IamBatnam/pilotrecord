#include "BravoWrapper.h"
#include "BravoEngine.h"
#include <string>

#define MAX_STRING 2048

#ifdef __cplusplus
extern "C" {
#endif

	void *BravoEngineObj()
	{
		BravoEngine *pEngine = new BravoEngine();
		return (void *)pEngine;
	}
    
    bool BravoInitialize(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return pEngine->Initialize();
    }

    void BravoLogin(void *pObj, const char* szUsername, const char *szPassword)
	{
		BravoEngine *pEngine = (BravoEngine*)pObj;
        std::string strUser, strPass;
        strUser = szUsername;
        strPass = szPassword;
		pEngine->Login(strUser, strPass);
	}
    
    void BravoLogout(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        pEngine->LogOut();
    }

	int BravoEngineState(void *pObj)
	{
		BravoEngine *pEngine = (BravoEngine*)pObj;
		return pEngine->State();
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Active Duty
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    void BravoSetActiveDuty(void *pObj, unsigned int sYear, unsigned int sMonth, unsigned int sDay)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        pEngine->getEntry()->SetActiveDuty(sMonth, sDay, sYear);
    }

    bool BravoActiveDutyGetDayOff(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return pEngine->getEntry()->getDayOff();
    }

    /**
    @Brief: Split duty status of particular duty
    @Return: returns true if duty is split day
    */
    bool BravoActiveDutyGetSplitDuty(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return pEngine->getEntry()->getActiveDutySplitDuty();
    }

    /**
     * @Brief: Retrieves the Start time timestamp object
     * @Return: a converted void pointer timestamp obj
     */
    void *BravoActiveDutyGetStartTime(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return (void *)&(pEngine->getEntry()->getActiveDutyStartTime());
    }

    /**
     * @Brief: Retrieves the Midday End time timestamp object
     * @Return: a converted void pointer timestamp obj
     */
    void *BravoActiveDutyGetMidEndTime(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return (void *)&(pEngine->getEntry()->getActiveDutyMiddayEndTime());
    }

    /**
     * @Brief: Retrieves the Midday Start time timestamp object
     * @Return: a converted void pointer timestamp obj
     */
    void *BravoActiveDutyGetMidStartTime(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return (void *)&(pEngine->getEntry()->getActiveDutyMiddayStartTime());
    }

    /**
     * @Brief: Retrieves the END time timestamp object
     * @Return: a converted void pointer timestamp obj
     */
    void *BravoActiveDutyGetEndTime(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return (void *)&pEngine->getEntry()->getActiveDutyEndTime();
    }

    /**
     * @Brief: Provides the total of the current active duty's flight legs
     * @Return: The total number of the active duty's flight legs avialable
     */
    int BravoActiveDutyCountFlightLegs(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return pEngine->getEntry()->getActiveDutyLegsCount();
    }

    /**
     * @Brief: Provides the total of the current active duty's flight legs
     * @Return: The total number of the active duty's flight legs avialable
     */
    void *BravoActiveDutyFlightLeg(void *pObj, int nIndex)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        return (void *)&pEngine->getEntry()->getActiveDutyFlightLeg(nIndex);
    }

    /**
     * @Brief: Setting the active duty's day off
     */
    void BravoActiveDutySetDayOff(void *pObj, bool bDayOff)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        pEngine->getEntry()->setActiveDutyDayOff(bDayOff);
    }

    /**
     * @Brief: Sets the active duty's split duty
     */
    void BravoActiveDutySetSplitDuty(void *pObj, bool bSplitShift)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        pEngine->getEntry()->setActiveDutySplitDuty(bSplitShift);
    }

    /**
     * @Brief: Sets the activeduty's start time
     */
    void BravoActiveDutySetStartTime(void *pObj, const CTimestamp timeStamp)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        
        Bravo::BravoTimestamp _time = {
            timeStamp.year,
            timeStamp.month,
            timeStamp.day,
            timeStamp.hour,
            timeStamp.minute };
        
        pEngine->getEntry()->setActiveDutyStartTime(_time);
    }

    /**
     * @Brief: Sets the active duty's mid end time
    */
    void BravoActiveDutySetMidEndTime(void *pObj, const struct CTimestamp timeStamp)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        
        Bravo::BravoTimestamp _time = {
            timeStamp.year,
            timeStamp.month,
            timeStamp.day,
            timeStamp.hour,
            timeStamp.minute };
        
        pEngine->getEntry()->setActiveDutyMiddayEndTime(_time);
    }

    /**
     * @Brief: Sets the activeduty's start time
     */
    void BravoActiveDutySetMidStartTime(void *pObj, const CTimestamp timeStamp)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        
        Bravo::BravoTimestamp _time = {
            timeStamp.year,
            timeStamp.month,
            timeStamp.day,
            timeStamp.hour,
            timeStamp.minute };
        
        pEngine->getEntry()->setActiveDutyMiddayStartTime(_time);
    }

    /**
     * @Brief: Sets the activeduty's end time
     */
    void BravoActiveDutySetEndTime(void *pObj, const CTimestamp timeStamp)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        
        Bravo::BravoTimestamp _time = {
            timeStamp.year,
            timeStamp.month,
            timeStamp.day,
            timeStamp.hour,
            timeStamp.minute };
        
        pEngine->getEntry()->setActiveDutyEndTime(_time);
    }


    /**
     * @Brief: Commit and send the changed active duty to the server
     */
    void BravoActiveDutyCommitActiveDuty(void *pObj)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        pEngine->getEntry()->CommitActiveDuty();
    }

// ***********************************************************************
// Monthly Duties
// ***********************************************************************

    void BravoGetMinMonthlyDuties(void *pObj, unsigned int sYear, unsigned int sMonth, unsigned int sDay, unsigned int eYear, unsigned int eMonth, unsigned int eDay)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        int nCount = pEngine->getEntry()->GetDutiesMinInfoContained({sYear, sMonth, sDay},
                                              {eYear, eMonth, eDay});
    }

    int BravoGetMinDutyStatus(void *pObj, unsigned int sYear, unsigned int sMonth, unsigned int sDay)
    {
        BravoEngine *pEngine = (BravoEngine*)pObj;
        
        Bravo::BravoDate date;// = {sYear, sMonth, sDay};
        date.year = sYear;
        date.month = sMonth;
        date.day = sDay;
        
        Bravo::BravoDataDuty resultingDuty = pEngine->getEntry()->AccessDutyMinInfo(date);
        
        if(resultingDuty.date == date && resultingDuty.dutySplit)
            return 2;
        else if(resultingDuty.date == date && !resultingDuty.dayOffDuty)
            return 1;
        else if(resultingDuty.date == date && resultingDuty.dayOffDuty)
            return 0;
        else
            return -1;
        
        // return 1 for Duty Data valid
        // return 0 for Duty Day off
        // return -1 for Duty non existent
        
    }

// ***********************************************************************
// BravoUtil
//  ***********************************************************************

    int BravoUtil_GetTimestampYear(void *timestampObj)
    {
        Bravo::BravoTimestamp *timestamp = (Bravo::BravoTimestamp *)timestampObj;
        return timestamp->year;
    }

    int BravoUtil_GetTimestampMonth(void *timestampObj)
    {
        Bravo::BravoTimestamp *timestamp = (Bravo::BravoTimestamp *)timestampObj;
        return timestamp->month;
    }

    int BravoUtil_GetTimestampDay(void *timestampObj)
    {
        Bravo::BravoTimestamp *timestamp = (Bravo::BravoTimestamp *)timestampObj;
        return timestamp->day;
    }

    int BravoUtil_GetTimestampHour(void *timestampObj)
    {
        Bravo::BravoTimestamp *timestamp = (Bravo::BravoTimestamp *)timestampObj;
        return timestamp->hour;
    }

    int BravoUtil_GetTimestampMinute(void *timestampObj)
    {
        Bravo::BravoTimestamp *timestamp = (Bravo::BravoTimestamp *)timestampObj;
        return timestamp->min;
    }

    /**
     * @Brief: Provides the C string of strAC
     */
    const char *BravoUtil_GetFlightLegAircraft(void *pFlightLeg)
    {
        Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
        return _pFlightLeg->strAircraft.c_str();
    }

    /**
     * @Brief: Provides copy of Route string
     */
    const char *BravoUtil_GetFlightLegRoute(void *pFlightLeg)
    {
        Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
        return _pFlightLeg->strFlightRoute.c_str();
    }

    /**
     * @Brief: Provides copy of Route string
     */
    const char *BravoUtil_GetFlightLegFlightNum(void *pFlightLeg)
    {
        Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
        return _pFlightLeg->strFlightNum.c_str();
    }

    /**
     * @Brief: Provides copy of Route string
     */
    const char *BravoUtil_GetFlightLegCrew(void *pFlightLeg)
    {
        Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
        return _pFlightLeg->strCrew.c_str();
    }

    /**
     * @Brief: Provides copy ofmileage
     * @Return: Int value type mileage
     */
    int BravoUtil_GetFlightLegMileage(void *pFlightLeg)
    {
        Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
        return _pFlightLeg->nMileage;
    }

    /**
     * @Brief: Provides Up timestamp
     * @Return: void pointer to timestamp object
     */
    void *BravoUtil_GetFlightLegUpTimestamp(void *pFlightLeg)
    {
        Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
        return (void*)&_pFlightLeg->flightStart;
    }

/**
 * @Brief: Provides Up timestamp
 * @Return: void pointer to timestamp object
 */
void *BravoUtil_GetFlightLegDownTimestamp(void *pFlightLeg)
{
    Bravo::BravoDataFlightLeg *_pFlightLeg = (Bravo::BravoDataFlightLeg *)pFlightLeg;
    return (void*)&_pFlightLeg->flightEnd;
}

    
#ifdef __cplusplus
}
#endif
