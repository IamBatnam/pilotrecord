#include "DOFRiskAssesment.h"
#include <QLineEdit>

DOFRiskAssesment::DOFRiskAssesment(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	connect(ui.pushButton, SIGNAL(pressed()), this, SLOT(OkayPressed()));
	connect(ui.lineEdit, SIGNAL(textChanged(const QString &)), this, SLOT(checkVal(const QString &)));
	connect(ui.lineEdit_2, SIGNAL(textEdited(const QString &)), this, SLOT(checkVal(const QString &)));
	connect(ui.lineEdit_3, SIGNAL(textEdited(const QString &)), this, SLOT(checkVal(const QString &)));
	connect(ui.lineEdit_4, SIGNAL(textEdited(const QString &)), this, SLOT(checkVal(const QString &)));
	connect(ui.lineEdit_5, SIGNAL(textEdited(const QString &)), this, SLOT(checkVal(const QString &)));
	connect(ui.lineEdit_6, SIGNAL(textEdited(const QString &)), this, SLOT(checkVal(const QString &)));
	connect(ui.lineEdit_7, SIGNAL(textEdited(const QString &)), this, SLOT(checkVal(const QString &)));
}

DOFRiskAssesment::~DOFRiskAssesment()
{
}


void DOFRiskAssesment::doModal(QString &qstrReturn)
{
	this->exec();
	qstrReturn += QString("%1,").arg(ui.lineEdit->text());
	qstrReturn += QString("%1,").arg(ui.lineEdit_2->text());
	qstrReturn += QString("%1,").arg(ui.lineEdit_3->text());
	qstrReturn += QString("%1,").arg(ui.lineEdit_4->text());
	qstrReturn += QString("%1,").arg(ui.lineEdit_5->text());
	qstrReturn += QString("%1,").arg(ui.lineEdit_6->text());
	qstrReturn += QString("%1").arg(ui.lineEdit_7->text());
}

void DOFRiskAssesment::checkVal(const QString & str)
{
	QLineEdit *le = qobject_cast<QLineEdit *>(sender());
	if (le)
	{
		auto curPos = le->cursorPosition();
		if (le->objectName().compare("lineEdit_7") == 0 && le->text().compare("--") == 0)
		{
			// do nothing
		}
		else if (le->text().compare("-") != 0 && le->text().compare("+") != 0)
		{
			if (le->text().length() > 1)
			{
				if (le->text()[0] == "-" || le->text()[0] == "+")
				{
					le->setText(QString(le->text()[0]));
				}
			}
			else
				le->setText("");
		}

		le->setCursorPosition(curPos);
	}
}

void DOFRiskAssesment::OkayPressed()
{
	accept();
}