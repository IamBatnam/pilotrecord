#include <QtWidgets>
#include <cmath>

#include "PRCalendarItem.h"


PRCalendarItem::PRCalendarItem(QDate qdate)
{
	dateData = qdate;
	bDeadItem = false;
	m_style = PRCalendarItem::ITEM_NO_STYLE;
	listTimeline.clear();
}

void PRCalendarItem::addTimeline(TimeInfo info)
{
	listTimeline.push_back(info);
}

void PRCalendarItem::setFontStyle(PRCalendarItem::STYLE style)
{ 
	m_style |= style; 
}

void PRCalendarItem::paint(QPainter *painter, const QRect &rect,
                       const QPalette &palette) const
{
	int day = dateData.day();
	QString strText = QString("%1").arg(day);

	//auto optionCopy = option;
	auto cellRect = rect;
	cellRect.setHeight(20);
	cellRect.setWidth(20);

	painter->save();

	if (m_style & PRCalendarItem::ITEM_STYLE_BOLD)
	{
		QFont font = painter->font();
		font.setBold(true);
		painter->setFont(font);
	}

	if (bDeadItem)
	{
		QFont font = painter->font();
		font.setItalic(true);
		painter->setPen(QColor(150,150,150));
		painter->setFont(font);
	}

	painter->drawText(cellRect, Qt::AlignCenter, strText);

    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setPen(Qt::NoPen);
	
	if(bDeadItem)
		painter->setBrush(QColor(210, 72, 72, 50)); // TODO: Change to userSet
	else
		painter->setBrush(QColor(210, 72, 72)); // TODO: Change to userSet
	
	for (auto it : listTimeline)
	{
		if (it.dayOff)
		{
			QFont font = painter->font();
			font.setItalic(true);
			font.setPointSize(20);

			if(bDeadItem)
				painter->setPen(QColor(150, 150, 150, 50));
			else
				painter->setPen(QColor(150, 150, 150));

			painter->setFont(font);
			painter->drawText(rect, Qt::AlignCenter, "OFF");
		}
		else
		{
			auto rectIt = it.getRectOfTime(rect);
			painter->drawRect(rectIt);
		}
	}

    painter->restore();
}
