/* TODO: Online support
*	- Server to client connection for multi-platform support
*/
#include "PilotRecordSetup.h"

#include "PRMsgBox.h"

#include "Bravo.h"

#include <QMainWindow>
#include <QCloseEvent>


PilotRecordSetup::PilotRecordSetup(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	connect(ui.closeButton, SIGNAL(pressed()), this, SLOT(closePressed()));
	connect(ui.createDatabase, SIGNAL(pressed()), this, SLOT(createDatabase()));
}

PilotRecordSetup::~PilotRecordSetup()
{
}

void PilotRecordSetup::closeEvent(QCloseEvent * event)
{
	PRMsgBox alertConfirm(this);
	if (alertConfirm.doModal(tr("Close PilotRecord"), 
		tr("Do you want to close the application without setting up?")) == PRMsgBox::RESULT::CANCEL)
	{
		event->ignore();
		return;
	}

	event->accept();
}

void PilotRecordSetup::createDatabase()
{
	auto user = ui.UserName->text();
	auto pass = ui.Password->text();
	auto repass = ui.Password_2->text();

	if(user.isEmpty() || pass.isEmpty() || repass.isEmpty())
	{
		PRMsgBox alert(this, tr("Empty Input"),
			tr("You must insert a value on all the fields."),
			PRMsgBox::ERROROKONLY);
		alert.doModal();
		return;
	}

	if (pass.compare(repass) != 0)
	{
		PRMsgBox alert(this, tr("Retype Password"),
			tr("The passwords does not match! Please type them again."),
			PRMsgBox::ERROROKONLY);
		alert.doModal();
		return;
	}

	auto strUser = user.toStdString();
	auto strPass = pass.toStdString();
	//BravoEngine::instance()->CreateDatabase(strUser, strPass);
	
	// re INIT the engine
	BravoEngine::instance()->Initialize();

	accept();
}

void PilotRecordSetup::closePressed()
{
	close();
}