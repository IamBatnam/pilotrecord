#ifndef PRDUTYDELEGATE_H
#define PRDUTYDELEGATE_H

#include <QStyledItemDelegate>

class PRDutyDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    PRDutyDelegate(QWidget *parent = 0) : QStyledItemDelegate(parent) {}

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

protected:
	bool eventFilter(QObject *obj, QEvent *event) override;
};

#endif
