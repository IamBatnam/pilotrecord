#include <QtWidgets>
#include <QMessageBox>

#include "PRDutyDelegate.h"

void PRDutyDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                         const QModelIndex &index) const
{
	//QStyledItemDelegate::paint(painter, option, index);
	if (option.state & QStyle::State_Selected)
		painter->fillRect(option.rect, option.palette.highlight());

	if (index.column() == 2) {
		painter->save();
		painter->setPen(QColor(Qt::gray));
		painter->drawText(option.rect, Qt::AlignRight | Qt::AlignVCenter, index.data().toString());
		painter->restore();
		return;
	}

	if (index.column() == 0 || index.column() == 1)
	{
		auto textToDraw = qvariant_cast<QDateTime>(index.data());
		painter->drawText(option.rect, Qt::AlignHCenter | Qt::AlignVCenter, textToDraw.toString("hh:mm")); // TODO: Change to userSet
	}
	else
		painter->drawText(option.rect, Qt::AlignHCenter | Qt::AlignVCenter, index.data().toString());
}

QSize PRDutyDelegate::sizeHint(const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
	return QStyledItemDelegate::sizeHint(option, index);
}

QWidget *PRDutyDelegate::createEditor(QWidget *parent,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const

{
	switch (index.column())
	{
	case 0:
	case 1:
	{
		QDateTimeEdit *editor = new QDateTimeEdit(parent);
		editor->setTimeSpec(Qt::UTC);
		editor->setDisplayFormat("MM/dd hh:mm"); // TODO: Change to userSet
		auto dateData = qvariant_cast<QDateTime>(index.data());
		//editor->setDateTime(QDateTime::fromString(.toString(), "hh:mm AP")); // TODO: Change to userSet
		editor->setDateTime(dateData); // TODO: Change to userSet

		return editor;
		break;
	}
	default:
		return QStyledItemDelegate::createEditor(parent, option, index);
	}

}

void PRDutyDelegate::setEditorData(QWidget *editor,
                                 const QModelIndex &index) const
{
	QObject qobj;

	switch (index.column())
	{
	case 0:
	case 1:
	{
		auto currentTime = qvariant_cast<QDateTime>(index.data());
		QDateTimeEdit *pTimeEditor = qobject_cast<QDateTimeEdit*>(editor);
		pTimeEditor->setDateTime(currentTime);
		//pTimeEditor->setSelectedSection(QDateTimeEdit::HourSection);

		connect(&qobj, &QObject::destroyed, pTimeEditor, [pTimeEditor]() {
			pTimeEditor->setSelectedSection(QDateTimeEdit::HourSection);
		}, Qt::QueuedConnection);

		break;
	}
	default:
		QStyledItemDelegate::setEditorData(editor, index);
		break;
	}
	
}

/* setModelData
* @brief: Gets the data from the editor widget then stores it to the item
*/
void PRDutyDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                const QModelIndex &index) const
{
	switch (index.column())
	{
	case 0:
	case 1:
	{
		QDateTimeEdit *peditor = qobject_cast<QDateTimeEdit *>(editor);
		model->setData(index, peditor->dateTime());
		break;
	}
	default:
		QStyledItemDelegate::setModelData(editor, model, index);
		break;
	}
	
}

bool PRDutyDelegate::eventFilter(QObject *obj, QEvent *event)
{
	if (event->type() == QEvent::KeyPress) 
	{
		QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
		QDateTimeEdit *pTimeEdit = qobject_cast<QDateTimeEdit*>(obj);
		if(keyEvent->key() == Qt::Key_Tab && pTimeEdit)
		{
			switch (pTimeEdit->currentSection())
			{
			case QDateTimeEdit::MonthSection:
				pTimeEdit->setSelectedSection(QDateTimeEdit::DaySection);
				break;
			case QDateTimeEdit::DaySection:
				pTimeEdit->setSelectedSection(QDateTimeEdit::HourSection);
				break;
			case QDateTimeEdit::HourSection:
				pTimeEdit->setSelectedSection(QDateTimeEdit::MinuteSection);
				break;
			case QDateTimeEdit::MinuteSection:
				if (pTimeEdit->displayedSections().testFlag(QDateTimeEdit::AmPmSection))
				{
					pTimeEdit->setSelectedSection(QDateTimeEdit::AmPmSection);
					break;
				}
				goto nextItem;
			case QDateTimeEdit::AmPmSection:
			default:
			nextItem:
				emit commitData(pTimeEdit);
				emit closeEditor(pTimeEdit);


				QTableWidget *pTblWidget = qobject_cast<QTableWidget*>(parent());
				auto item = pTblWidget->currentItem();
				QTableWidgetItem *pNextItem;
				
				if (item->column() == 1) {
					pNextItem = pTblWidget->item(item->row(), 3); 
					pTblWidget->setCurrentCell(item->row(), 3);
					pTblWidget->editItem(pNextItem);
				}
				else
				{
					pNextItem = pTblWidget->item(item->row(), item->column() + 1);
					pTblWidget->setCurrentCell(item->row(), 1);
					pTblWidget->editItem(pNextItem);
				}
			}
			return true;
		}

		QLineEdit *pLineEdit = qobject_cast<QLineEdit*>(obj);
		if (keyEvent->key() == Qt::Key_Tab && pLineEdit)
		{
			emit commitData(pLineEdit);
			emit closeEditor(pLineEdit);

			QTableWidget *pTblWidget = qobject_cast<QTableWidget*>(parent());
			auto item = pTblWidget->currentItem();
			QTableWidgetItem *pNextItem = pTblWidget->item(item->row(), item->column() + 1);

			pTblWidget->setCurrentCell(item->row(), item->column() + 1);
			pTblWidget->editItem(pNextItem);
			return true;
		}
	}

	// standard event processing
	return QObject::eventFilter(obj, event);
}