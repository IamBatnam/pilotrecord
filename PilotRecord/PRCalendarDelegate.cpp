#include <QtWidgets>

#include "PRCalendarDelegate.h"
#include "PRCalendarItem.h"

void PRCalendarDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                         const QModelIndex &index) const
{
	if (index.data().canConvert<PRCalendarItem>())
	{
		PRCalendarItem calendarItem = qvariant_cast<PRCalendarItem>(index.data());

		if (option.state & QStyle::State_Selected)
			painter->fillRect(option.rect, option.palette.highlight());

		calendarItem.paint(painter, option.rect, option.palette);
	}
	else {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

QSize PRCalendarDelegate::sizeHint(const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
	return QStyledItemDelegate::sizeHint(option, index);
}

QWidget *PRCalendarDelegate::createEditor(QWidget *parent,
                                    const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const

{
	return QStyledItemDelegate::createEditor(parent, option, index);
}

void PRCalendarDelegate::setEditorData(QWidget *editor,
                                 const QModelIndex &index) const
{
	QStyledItemDelegate::setEditorData(editor, index);
}

void PRCalendarDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                const QModelIndex &index) const
{
	QStyledItemDelegate::setModelData(editor, model, index);
}
