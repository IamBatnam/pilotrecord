#include "PilotRecordLogin.h"
#include "PilotRecord.h"
#include "PRMsgBox.h"

#include <QMainWindow>
#include <QCloseEvent>
#include <qtimer.h>

PilotRecordLogin::PilotRecordLogin(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	ui.statuslabel->setVisible(false);
	connect(ui.loginButton, SIGNAL(pressed()), this, SLOT(login_pressed()));
	connect(ui.closeButton, SIGNAL(pressed()), this, SLOT(close_pressed()));
}

PilotRecordLogin::~PilotRecordLogin()
{
}

void PilotRecordLogin::closeEvent(QCloseEvent * event)
{
	if (((PilotRecord*)parentWidget())->close())
		event->accept();
	else
		event->ignore();
}

void PilotRecordLogin::showEvent(QShowEvent * event)
{
	if (!BravoEngine::instance()->IsClientOnline())
	{
		ui.statuslabel->setVisible(true);
		ui.logolabel->setDisabled(true);
		this->setDisabled(true);
	}

	QWidget::showEvent(event);

	ui.userEdit->clear();
	ui.passEdit->clear();
	ui.userEdit->setFocus();
}

void PilotRecordLogin::reject()
{
	return; // Prevents the window from closing until logged in
}

void PilotRecordLogin::close_pressed()
{
	close();
}

void PilotRecordLogin::login_pressed()
{
	auto struser = ui.userEdit->text().toStdString();
	auto strpass = ui.passEdit->text().toStdString();
	
	// TODO: have the engine do the checking!
	auto bravoEngine = BravoEngine::instance();
	MOUSE_WAIT
	bravoEngine->Login(struser, strpass);

	if (bravoEngine->State() == BravoEngine::STATE_LOGGEDIN)
	{
		MOUSE_RST
		((PilotRecord*)parentWidget())->statusBar()->showMessage(tr("Logged in!"));
		// TODO: Make this line to be exec after hours updated!
		((PilotRecord*)parentWidget())->togglePage(PilotRecord::CURRENT_STATE::SUMMARY_PAGE);
		((PilotRecord*)parentWidget())->setFocus();
		this->accept();	
	}
	else
	{
		MOUSE_RST
		PRMsgBox msgBox;
		msgBox.SetType(PRMsgBox::ERROROKONLY);
		msgBox.SetMessage(tr("Login Error!"), tr("The username and password combination used could not be found! Please try again."));
		msgBox.doModal();

		ui.passEdit->clear();

		//QApplication::restoreOverrideCursor();
	}

	//ui.userEdit->setFocus();
	ui.passEdit->setFocus();
}
