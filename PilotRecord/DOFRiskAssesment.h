#pragma once

#include <QDialog>
#include "ui_DOFRiskAssesment.h"

class DOFRiskAssesment : public QDialog
{
	Q_OBJECT

public:
	DOFRiskAssesment(QWidget *parent = Q_NULLPTR);
	~DOFRiskAssesment();

	void doModal(QString &qstrReturn);

public slots:
	void OkayPressed();
	void checkVal(const QString &str);

private:
	Ui::DOFRiskAssesment ui;
};
