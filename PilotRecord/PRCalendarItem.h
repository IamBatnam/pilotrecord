#ifndef PRCALENDAR_ITEM_H
#define PRCALENDAR_ITEM_H

#include <QMetaType>
#include <QPointF>
#include <QVector>
#include <QDate>
#include <QTime>

struct TimeInfo 
{
	QTime startTime;
	QTime endTime;
	bool dayOff;
	
	QRect getRectOfTime(const QRect &rectRef)
	{
		const qint64 DAY = 86400;
//		QTime timeSpan = QTime(0, 0).addSecs(startTime.secsTo(endTime) % DAY);
		qreal timeSpanSecs = (qreal)startTime.secsTo(endTime) / (qreal)DAY;

		int desiredHeight = 15;
		auto center = rectRef.center();
		QRect resRect;
		qreal xVal = rectRef.x() + ((qreal)rectRef.width() * ((qreal)QTime(0, 0).secsTo(startTime) / (qreal)DAY));
		resRect.setX(xVal);
		resRect.setY(center.y() - (desiredHeight / 2));
		resRect.setHeight(desiredHeight);
		resRect.setWidth(timeSpanSecs * (qreal)rectRef.width());

		return resRect;


	}
};

/* PRCalendarItem
*	Provides a handler for the timelines in the calendar widget
*/
class PRCalendarItem
{
public:
	enum STYLE {
		ITEM_NO_STYLE,
		ITEM_STYLE_BOLD
	};

	explicit PRCalendarItem(QDate date = QDate::currentDate());

    void paint(QPainter *painter, const QRect &rect, const QPalette &palette) const;

	QDate &getDate() { return dateData; };
	void addTimeline(TimeInfo info);
	void setItemInactive(bool bact) { bDeadItem = bact; };
	void setFontStyle(STYLE style);

private:
	unsigned int m_style;
	QDate dateData;
	bool bDeadItem;
	QList<TimeInfo> listTimeline;
};

Q_DECLARE_METATYPE(PRCalendarItem)

#endif
