#include "PRWaitBox.h"

#include <QTimer>

PRWaitBox::PRWaitBox(QWidget *parent)
	: QDialog(parent)
	, qTimer(nullptr)
{
	ui.setupUi(this);
	
	qTimer = new QTimer;
	connect(qTimer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
}

PRWaitBox::~PRWaitBox()
{
}

void PRWaitBox::SetMinMaxVal(int min, int max)
{
	if (ui.progressBar->maximum() != max)
		QMetaObject::invokeMethod(ui.progressBar, "setRange", Qt::QueuedConnection, Q_ARG(int, min), Q_ARG(int, max));
}

void PRWaitBox::SetValue(int nVal)
{
	QMetaObject::invokeMethod(ui.progressBar, "setValue", Qt::QueuedConnection, Q_ARG(int, nVal));
}

void PRWaitBox::showEvent(QShowEvent * event)
{
	QWidget::showEvent(event);
	qTimer->start(100);
}

void PRWaitBox::timerUpdate()
{
	if (updateCheck())
	{
		qTimer->stop();
		QMetaObject::invokeMethod(this, "close", Qt::QueuedConnection);
	}
}