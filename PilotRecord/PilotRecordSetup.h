#pragma once

#include <QDialog>
#include "ui_PilotRecordSetup.h"

class PilotRecordSetup : public QDialog
{
	Q_OBJECT

public:
	PilotRecordSetup(QWidget *parent = Q_NULLPTR);
	~PilotRecordSetup();

protected:
	void closeEvent(QCloseEvent *event) override;

public slots:
	void closePressed();
	void createDatabase();

private:
	Ui::PilotRecordSetup ui;
};
