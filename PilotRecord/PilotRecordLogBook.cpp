#include "PilotRecordLogBook.h"
#include "Bravo.h"

#include "PilotRecord.h"

#include "PRMsgBox.h"
#include <QApplication>
#include <QDesktopWidget>

PilotRecordLogBook::PilotRecordLogBook(QWidget *parent, PilotRecordLogBook::STATE state)
	: QDialog(parent)
	, m_State(state)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	switch (m_State)
	{
	case PilotRecordLogBook::SUMMARIZE_MODE:
	{
		ui.dateModLabel->setVisible(false);
		ui.dateModEdit->setVisible(false);
	}
	case PilotRecordLogBook::COMPARE_MODE:
	{
		if(m_State == PilotRecordLogBook::COMPARE_MODE)
			connect(ui.tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabWidgetChange()));

		if (m_State == PilotRecordLogBook::SUMMARIZE_MODE)
		{
			this->setWindowTitle("Review Log book Entry");
			connect(ui.editEntry, SIGNAL(pressed()), this, SLOT(edit_pressed()));
		}
		else
			this->setWindowTitle("Comparing Old Entry");

		ui.tabWidget->removeTab(1);
		ui.tabWidget->removeTab(0);
		ui.dateEdit->setEnabled(false);
		ui.okayButton->setVisible(false);

		connect(ui.previousList, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(reviewPreviousItem(QTreeWidgetItem*, int)));
		break;
	}
	case PilotRecordLogBook::ADD_MODE:
	{
		ui.dateEdit->setDate(QDate::currentDate());
	}
	case PilotRecordLogBook::EDIT_MODE:
	default:
	{
		if (m_State == PilotRecordLogBook::EDIT_MODE)
			this->setWindowTitle("Editing Entry");

		ui.tabWidget->removeTab(4);
		ui.tabWidget->removeTab(3);
		ui.tabWidget->removeTab(2);
		ui.editEntry->setVisible(false);
		ui.dateModLabel->setVisible(false);
		ui.dateModEdit->setVisible(false);
		
		auto dbValidator = new QDoubleValidator(this);
		dbValidator->setNotation(QDoubleValidator::StandardNotation);
		dbValidator->setDecimals(2);

		ui.SE_DAY_DUAL->setValidator(dbValidator);
		ui.SE_NIGHT_DUAL->setValidator(dbValidator);
		ui.SE_DAY_PIC->setValidator(dbValidator);
		ui.SE_NIGHT_PIC->setValidator(dbValidator);

		// Multi-Engine
		ui.ME_DAY_DUAL->setValidator(dbValidator);
		ui.ME_NIGHT_DUAL->setValidator(dbValidator);
		ui.ME_DAY_PIC->setValidator(dbValidator);
		ui.ME_NIGHT_PIC->setValidator(dbValidator);
		ui.ME_DAY_SIC->setValidator(dbValidator);
		ui.ME_NIGHT_SIC->setValidator(dbValidator);

		// Cross country
		ui.XCUN_DAY_DUAL->setValidator(dbValidator);
		ui.XCUN_NIGHT_DUAL->setValidator(dbValidator);
		ui.XCUN_DAY_PIC->setValidator(dbValidator);
		ui.XCUN_NIGHT_PIC->setValidator(dbValidator);

		auto intValidator = new QIntValidator(this);

		// Take-offs and Landings
		ui.TO_LAND_DAY->setValidator(intValidator);
		ui.TO_LAND_NIGHT->setValidator(intValidator);

		// Instruments
		ui.IFR_APPR->setValidator(intValidator);
		ui.SIM_TIME->setValidator(dbValidator);
		ui.HOOD_TIME->setValidator(dbValidator);
		ui.ACTUAL_TIME->setValidator(dbValidator);

		connect(ui.okayButton, SIGNAL(pressed()), this, SLOT(okay_pressed()));

		connect(ui.ACtype, SIGNAL(editTextChanged(const QString &)), SLOT(toUpper(const QString &)));
		connect(ui.ACreg, SIGNAL(editTextChanged(const QString &)), SLOT(toUpper(const QString &)));

		connect(ui.PICname, SIGNAL(textEdited(const QString &)), SLOT(toUpper(const QString &)));
		connect(ui.PAXname, SIGNAL(textEdited(const QString &)), SLOT(toUpper(const QString &)));
		break;
	}
	}
	
	connect(ui.cancelButton, SIGNAL(pressed()), this, SLOT(cancel_pressed()));
}

PilotRecordLogBook::~PilotRecordLogBook()
{
}

void PilotRecordLogBook::okay_pressed()
{
	Bravo::BravoDataEntry entry;
	auto date = ui.dateEdit->date();
	entry.SetDate(date.month(), date.day(), date.year());

	entry.cPilotName = ui.PICname->text().toStdString();
	entry.cPax = ui.PAXname->text().toStdString();
	entry.cPlaneType = ui.ACtype->lineEdit()->text().toStdString();
	entry.cPlaneReg = ui.ACreg->lineEdit()->text().toStdString();

	entry.strRemarks = ui.Remarks->toPlainText().toStdString();

	if(!ui.SE_DAY_DUAL->text().isEmpty())
		entry.SE_DAY_DUAL = ui.SE_DAY_DUAL->text().toFloat();
	if (!ui.SE_NIGHT_DUAL->text().isEmpty())
		entry.SE_NIGHT_DUAL = ui.SE_NIGHT_DUAL->text().toFloat();
	if (!ui.SE_DAY_PIC->text().isEmpty())
		entry.SE_DAY_PIC = ui.SE_DAY_PIC->text().toFloat();
	if (!ui.SE_NIGHT_PIC->text().isEmpty())
		entry.SE_NIGHT_PIC = ui.SE_NIGHT_PIC->text().toFloat();

	if (!ui.ME_DAY_DUAL->text().isEmpty())
		entry.ME_DAY_DUAL = ui.ME_DAY_DUAL->text().toFloat();
	if (!ui.ME_NIGHT_DUAL->text().isEmpty())
		entry.ME_NIGHT_DUAL = ui.ME_NIGHT_DUAL->text().toFloat();
	if (!ui.ME_DAY_PIC->text().isEmpty())
		entry.ME_DAY_PIC = ui.ME_DAY_PIC->text().toFloat();
	if (!ui.ME_NIGHT_PIC->text().isEmpty())
		entry.ME_NIGHT_PIC = ui.ME_NIGHT_PIC->text().toFloat();
	if (!ui.ME_DAY_SIC->text().isEmpty())
		entry.ME_DAY_SIC = ui.ME_DAY_SIC->text().toFloat();
	if (!ui.ME_NIGHT_SIC->text().isEmpty())
		entry.ME_NIGHT_SIC = ui.ME_NIGHT_SIC->text().toFloat();

	if (!ui.XCUN_DAY_DUAL->text().isEmpty())
		entry.XCUN_DAY_DUAL = ui.XCUN_DAY_DUAL->text().toFloat();
	if (!ui.XCUN_NIGHT_DUAL->text().isEmpty())
		entry.XCUN_NIGHT_DUAL = ui.XCUN_NIGHT_DUAL->text().toFloat();
	if (!ui.XCUN_DAY_PIC->text().isEmpty())
		entry.XCUN_DAY_PIC = ui.XCUN_DAY_PIC->text().toFloat();
	if (!ui.XCUN_NIGHT_PIC->text().isEmpty())
		entry.XCUN_NIGHT_PIC = ui.XCUN_NIGHT_PIC->text().toFloat();

	if (!ui.TO_LAND_DAY->text().isEmpty())
		entry.nTO_LAND_DAY = ui.TO_LAND_DAY->text().toInt();
	if (!ui.TO_LAND_NIGHT->text().isEmpty())
		entry.nTO_LAND_NIGHT = ui.TO_LAND_NIGHT->text().toInt();

	if (!ui.IFR_APPR->text().isEmpty())
		entry.IFR_APPROACH = ui.IFR_APPR->text().toInt();
	if (!ui.SIM_TIME->text().isEmpty())
		entry.SIM_TIME = ui.SIM_TIME->text().toFloat();
	if (!ui.HOOD_TIME->text().isEmpty())
		entry.HOOD_TIME = ui.HOOD_TIME->text().toFloat();
	if (!ui.ACTUAL_TIME->text().isEmpty())
		entry.ACTUAL_TIME = ui.ACTUAL_TIME->text().toFloat();

	if (ui.SE_DAY_DUAL->text().isEmpty() && ui.SE_NIGHT_DUAL->text().isEmpty() && ui.SE_DAY_PIC->text().isEmpty() && ui.SE_NIGHT_PIC->text().isEmpty()
		&& ui.ME_DAY_DUAL->text().isEmpty() && ui.ME_DAY_SIC->text().isEmpty() && ui.ME_DAY_PIC->text().isEmpty()
		&& ui.ME_NIGHT_PIC->text().isEmpty() && ui.ME_NIGHT_SIC->text().isEmpty() && ui.ME_NIGHT_PIC->text().isEmpty()
		&& ui.SIM_TIME->text().isEmpty())
	{
		PRMsgBox errorMsg(this, "Error!", "Hours information incomplete!", PRMsgBox::ERROROKONLY);
		errorMsg.doModal();
		return;
	}

	auto entryControl = BravoEngine::instance()->getEntry();
	if (m_State == PilotRecordLogBook::EDIT_MODE)
	{
		entry.id = m_tempEntry.id;
		entryControl->UpdateEntry(entry);
	}
	else
		entryControl->AddEntry(entry);

	((PilotRecord *)(this->parentWidget()))->UpdateHoursLog();
	this->accept();
}

void PilotRecordLogBook::cancel_pressed()
{
	// TODO: Catch the reject event for confirmation msg box
	reject();
}

void PilotRecordLogBook::edit_pressed()
{
	PilotRecordLogBook editEntry(parentWidget(), PilotRecordLogBook::EDIT_MODE);
	editEntry.EditEntry(m_tempEntry);
	this->accept();
	editEntry.exec();
}

void PilotRecordLogBook::showEvent(QShowEvent * event)
{
	QWidget::showEvent(event);
}

void PilotRecordLogBook::SummarizeData(int nRow)
{
	m_tempEntry = BravoEngine::instance()->getEntry()->GetEntry(nRow);

	m_tempEntry.PreviousVersion = BravoEngine::instance()->getEntry()->GetEntryVersions(m_tempEntry.id);
	if (m_tempEntry.PreviousVersion)
	{
		for (auto prev : *m_tempEntry.PreviousVersion)
		{
			unsigned short nMonth, nDay;
			unsigned int nYear;
			Bravo::BravoDataEntry::LongToDate(prev.dateModified, nMonth, nDay, nYear);
			QString strDateMod(Bravo::BravoDataEntry::GetDateStrFormat(nDay, nMonth, nYear).c_str());
			auto toAddEntry = new QTreeWidgetItem(QStringList() << strDateMod << prev.strRemarks.c_str());

			ui.previousList->addTopLevelItem(toAddEntry);
		}
	}
	else
		ui.tabWidget->removeTab(2);


	ui.dateEdit->setDate(QDate(m_tempEntry.GetDateYear(), m_tempEntry.GetDateMonthInt(), m_tempEntry.GetDateDay()));
	
	ui.T_PICname->setText(m_tempEntry.cPilotName.c_str());
	ui.T_PAXname->setText(m_tempEntry.cPax.c_str());
	ui.T_ACtype->setText(m_tempEntry.cPlaneType.c_str());
	ui.T_ACreg->setText(m_tempEntry.cPlaneReg.c_str());

	ui.T_Remarks->setPlainText(m_tempEntry.strRemarks.c_str());

	auto floatFormat = [](float f)-> QString {
		return QString("%1").arg(f, 0, 'f', 2);
	};

	auto checkSetText = [&](auto T, auto &val) {
		if (val == 0)
			T->setStyleSheet("font-weight: normal;");

		if (typeid(decltype(val)) == typeid(float))
			T->setText(floatFormat(val));
		else
			T->setText(QString("%1").arg(val));
	};

	checkSetText(ui.T_SE_DAY_DUAL, m_tempEntry.SE_DAY_DUAL);
	checkSetText(ui.T_SE_NIGHT_DUAL, m_tempEntry.SE_NIGHT_DUAL);
	checkSetText(ui.T_SE_DAY_PIC, m_tempEntry.SE_DAY_PIC);
	checkSetText(ui.T_SE_NIGHT_PIC, m_tempEntry.SE_NIGHT_PIC);

	// Multi-Engine
	checkSetText(ui.T_ME_DAY_DUAL, m_tempEntry.ME_DAY_DUAL);
	checkSetText(ui.T_ME_NIGHT_DUAL, m_tempEntry.ME_NIGHT_DUAL);
	checkSetText(ui.T_ME_DAY_PIC, m_tempEntry.ME_DAY_PIC);
	checkSetText(ui.T_ME_NIGHT_PIC, m_tempEntry.ME_NIGHT_PIC);
	checkSetText(ui.T_ME_DAY_SIC, m_tempEntry.ME_DAY_SIC);
	checkSetText(ui.T_ME_NIGHT_SIC, m_tempEntry.ME_NIGHT_SIC);

	// Cross country
	checkSetText(ui.T_XCUN_DAY_DUAL, m_tempEntry.XCUN_DAY_DUAL);
	checkSetText(ui.T_XCUN_NIGHT_DUAL, m_tempEntry.XCUN_NIGHT_DUAL);
	checkSetText(ui.T_XCUN_DAY_PIC, m_tempEntry.XCUN_DAY_PIC);
	checkSetText(ui.T_XCUN_NIGHT_PIC, m_tempEntry.XCUN_NIGHT_PIC);

	// Take-offs and Landings
	checkSetText(ui.T_TO_LAND_DAY, m_tempEntry.nTO_LAND_DAY);
	checkSetText(ui.T_TO_LAND_NIGHT, m_tempEntry.nTO_LAND_NIGHT);

	// Instruments
	checkSetText(ui.T_IFR_APPR, m_tempEntry.IFR_APPROACH);
	checkSetText(ui.T_SIM_TIME, m_tempEntry.SIM_TIME);
	checkSetText(ui.T_HOOD_TIME, m_tempEntry.HOOD_TIME);
	checkSetText(ui.T_ACTUAL_TIME, m_tempEntry.ACTUAL_TIME);
}

void PilotRecordLogBook::EditEntry(Bravo::BravoDataEntry &entry)
{
	auto checkSetText = [&](auto T, auto &val) {
		if (val != 0)
		{
			T->setText(QString("%1").arg(val));
		}
		else
			T->setText("");
	};

	m_tempEntry = entry;

	ui.dateModLabel->setVisible(true);
	ui.dateModEdit->setVisible(true);
	ui.dateModEdit->setDate(QDate::currentDate());

	ui.dateEdit->setDate(QDate(entry.GetDateYear(), entry.GetDateMonthInt(), entry.GetDateDay()));
	ui.PICname->setText(entry.cPilotName.c_str());
	ui.PAXname->setText(entry.cPax.c_str());
	ui.ACtype->lineEdit()->setText(entry.cPlaneType.c_str());
	ui.ACreg->lineEdit()->setText(entry.cPlaneReg.c_str());

	ui.Remarks->setPlainText(entry.strRemarks.c_str());

	checkSetText(ui.SE_DAY_DUAL, entry.SE_DAY_DUAL);
	checkSetText(ui.SE_NIGHT_DUAL, entry.SE_NIGHT_DUAL);
	checkSetText(ui.SE_DAY_PIC, entry.SE_DAY_PIC);
	checkSetText(ui.SE_NIGHT_PIC, entry.SE_NIGHT_PIC);

	// Multi-Engine
	checkSetText(ui.ME_DAY_DUAL, entry.ME_DAY_DUAL);
	checkSetText(ui.ME_NIGHT_DUAL, entry.ME_NIGHT_DUAL);
	checkSetText(ui.ME_DAY_PIC, entry.ME_DAY_PIC);
	checkSetText(ui.ME_NIGHT_PIC, entry.ME_NIGHT_PIC);
	checkSetText(ui.ME_DAY_SIC, entry.ME_DAY_SIC);
	checkSetText(ui.ME_NIGHT_SIC, entry.ME_NIGHT_SIC);

	// Cross country
	checkSetText(ui.XCUN_DAY_DUAL, entry.XCUN_DAY_DUAL);
	checkSetText(ui.XCUN_NIGHT_DUAL, entry.XCUN_NIGHT_DUAL);
	checkSetText(ui.XCUN_DAY_PIC, entry.XCUN_DAY_PIC);
	checkSetText(ui.XCUN_NIGHT_PIC, entry.XCUN_NIGHT_PIC);

	// Take-offs and Landings
	checkSetText(ui.TO_LAND_DAY, entry.nTO_LAND_DAY);
	checkSetText(ui.TO_LAND_NIGHT, entry.nTO_LAND_NIGHT);

	// Instruments
	checkSetText(ui.IFR_APPR, entry.IFR_APPROACH);
	checkSetText(ui.SIM_TIME, entry.SIM_TIME);
	checkSetText(ui.HOOD_TIME, entry.HOOD_TIME);
	checkSetText(ui.ACTUAL_TIME, entry.ACTUAL_TIME);
}

void PilotRecordLogBook::reviewPreviousItem(QTreeWidgetItem * item, int nRow)
{
	item = ui.previousList->currentItem();
	int nrow = ui.previousList->indexOfTopLevelItem(item);

	PilotRecordLogBook previousEntryWindow(this, PilotRecordLogBook::COMPARE_MODE);
	previousEntryWindow.CompareData((*m_tempEntry.PreviousVersion)[nrow], m_tempEntry);
	auto curgeom = this->geometry();
	
	auto global = QApplication::desktop()->screenGeometry(this);
	move(global.x() + ((global.width() - (curgeom.width()*2)) / 2), global.y() + ((global.height() - curgeom.height()) / 2));
	previousEntryWindow.move(
		global.x() + ((global.width() - (curgeom.width()*2)) / 2) + curgeom.width(),
		global.y() + ((global.height() - curgeom.height()) / 2));

	previousEntryWindow.exec();
	this->setGeometry(curgeom);

}

void PilotRecordLogBook::CompareData(Bravo::PreviousVersionDataEntry &prevEntry, Bravo::BravoDataEntry &currentEntry)
{
	ui.tabWidget->removeTab(2);
	ui.editEntry->setVisible(false);

	unsigned short nDay, nMonth;
	unsigned int nYear;
	Bravo::BravoDataEntry::LongToDate(prevEntry.dateModified, nMonth, nDay, nYear);
	ui.dateModEdit->setDate(QDate(nYear, nMonth, nDay));
	
	auto compareData = [](auto first, auto second) -> bool 
	{
		if (first.compare(second) == 0)
			return true;

		return false;
	};

	auto colorMeDiffer = [&](auto *uiObj, auto &first, auto &second) 
	{
		if(!compareData(first, second))
			uiObj->setStyleSheet("color: Red;");

		uiObj->setText(first.c_str());
	};

	auto colorMeDifferNum = [&](auto *uiObj, auto &first, auto &second) 
	{
		if (first != second)
			uiObj->setStyleSheet("color: Red;");

		if (first == 0)
			uiObj->setStyleSheet("font-weight: normal;");

		if (typeid(decltype(first)) == typeid(float))
			uiObj->setText(QString("%1").arg((float)first, 0, 'f', 2));
		else
			uiObj->setText(QString("%1").arg(first));
	};

	ui.dateEdit->setDate(QDate(prevEntry.GetDateYear(), prevEntry.GetDateMonthInt(), prevEntry.GetDateDay()));
	if (prevEntry._date != currentEntry._date)
		ui.dateEdit->setStyleSheet("color: Red;");

	colorMeDiffer(ui.T_PICname, prevEntry.cPilotName, currentEntry.cPilotName);
	colorMeDiffer(ui.T_PAXname, prevEntry.cPax, currentEntry.cPax);
	colorMeDiffer(ui.T_ACtype, prevEntry.cPlaneType, currentEntry.cPlaneType);
	colorMeDiffer(ui.T_ACreg, prevEntry.cPlaneReg, currentEntry.cPlaneReg);

	ui.T_Remarks->setPlainText(prevEntry.strRemarks.c_str());
	if (!compareData(prevEntry.strRemarks, currentEntry.strRemarks))
		ui.T_Remarks->setStyleSheet("color: Red;");


	colorMeDifferNum(ui.T_SE_DAY_DUAL, prevEntry.SE_DAY_DUAL, currentEntry.SE_DAY_DUAL);
	colorMeDifferNum(ui.T_SE_NIGHT_DUAL, prevEntry.SE_NIGHT_DUAL, currentEntry.SE_NIGHT_DUAL);
	colorMeDifferNum(ui.T_SE_DAY_PIC, prevEntry.SE_DAY_PIC, currentEntry.SE_DAY_PIC);
	colorMeDifferNum(ui.T_SE_NIGHT_PIC, prevEntry.SE_NIGHT_PIC, currentEntry.SE_NIGHT_PIC);

	// Multi-Engine
	colorMeDifferNum(ui.T_ME_DAY_DUAL, prevEntry.ME_DAY_DUAL, currentEntry.ME_DAY_DUAL);
	colorMeDifferNum(ui.T_ME_NIGHT_DUAL, prevEntry.ME_NIGHT_DUAL, currentEntry.ME_NIGHT_DUAL);
	colorMeDifferNum(ui.T_ME_DAY_PIC, prevEntry.ME_DAY_PIC, currentEntry.ME_DAY_PIC);
	colorMeDifferNum(ui.T_ME_NIGHT_PIC, prevEntry.ME_NIGHT_PIC, currentEntry.ME_NIGHT_PIC);
	colorMeDifferNum(ui.T_ME_DAY_SIC, prevEntry.ME_DAY_SIC, currentEntry.ME_DAY_SIC);
	colorMeDifferNum(ui.T_ME_NIGHT_SIC, prevEntry.ME_NIGHT_SIC, currentEntry.ME_NIGHT_SIC);

	// Cross country
	colorMeDifferNum(ui.T_XCUN_DAY_DUAL, prevEntry.XCUN_DAY_DUAL, currentEntry.XCUN_DAY_DUAL);
	colorMeDifferNum(ui.T_XCUN_NIGHT_DUAL, prevEntry.XCUN_NIGHT_DUAL, currentEntry.XCUN_NIGHT_DUAL);
	colorMeDifferNum(ui.T_XCUN_DAY_PIC, prevEntry.XCUN_DAY_PIC, currentEntry.XCUN_DAY_PIC);
	colorMeDifferNum(ui.T_XCUN_NIGHT_PIC, prevEntry.XCUN_NIGHT_PIC, currentEntry.XCUN_NIGHT_PIC);

	// Take-offs and Landings
	colorMeDifferNum(ui.T_TO_LAND_DAY, prevEntry.nTO_LAND_DAY, currentEntry.nTO_LAND_DAY);
	colorMeDifferNum(ui.T_TO_LAND_NIGHT, prevEntry.nTO_LAND_NIGHT, currentEntry.nTO_LAND_NIGHT);

	// Instruments
	colorMeDifferNum(ui.T_IFR_APPR, prevEntry.IFR_APPROACH, currentEntry.IFR_APPROACH);
	colorMeDifferNum(ui.T_SIM_TIME, prevEntry.SIM_TIME, currentEntry.SIM_TIME);
	colorMeDifferNum(ui.T_HOOD_TIME, prevEntry.HOOD_TIME, currentEntry.HOOD_TIME);
	colorMeDifferNum(ui.T_ACTUAL_TIME, prevEntry.ACTUAL_TIME, currentEntry.ACTUAL_TIME);
}


void PilotRecordLogBook::toUpper(const QString & str)
{
	QComboBox *cle = qobject_cast<QComboBox *>(sender());
	if (cle)
	{
		auto curPos = cle->lineEdit()->cursorPosition();
		cle->lineEdit()->setText(str.toUpper());
		cle->lineEdit()->setCursorPosition(curPos);
	}

	QLineEdit *le = qobject_cast<QLineEdit *>(sender());
	if (le)
	{
				auto curPos = le->cursorPosition();
		le->setText(str.toUpper());
		le->setCursorPosition(curPos);
	}
}

void PilotRecordLogBook::setTabIndex(int nIndex)
{
	ui.tabWidget->setCurrentIndex(nIndex);
}

void PilotRecordLogBook::tabWidgetChange()
{
	((PilotRecordLogBook*)parentWidget())->setTabIndex(ui.tabWidget->currentIndex());
}
