#include "PilotRecordSplash.h"
#include "buildversion.h"
#include <QTextStream>
#include <QTimer>

#include "PilotRecord.h"
#include "PRMsgBox.h"
#include "PRWaitBox.h"

#include <QApplication>
#include <QThread>
#include <QtConcurrent>
#include <QStyle>
#include <QDesktopWidget>
#include <QFile>
#include <QProcess>

#include <curl/curl.h>
#include <miniz.h>

#include <Bravo.h>

#include <QMessageBox>

extern "C" {
	int Xferinfo_DL(void *point, size_t dltotal, size_t dlnow, size_t ultotal, size_t ulnow)
	{
		PRWaitBox *dlgPoint = (PRWaitBox*)point;
		dlgPoint->SetMinMaxVal(0, dltotal);
		dlgPoint->SetValue(dlnow);

		return 0;
	}

	int FileWriter_DL(char *pData, size_t size, size_t nmemb, void *pointer)
	{
		FILE *pFile = (FILE *)pointer;
		return fwrite(pData, size, nmemb, pFile);
	}
}

PilotRecordSplash::PilotRecordSplash(QWidget *parent)
	: QDialog(parent)
	, timer(nullptr)
	, nElapsedTime(0)
	, bDoneInit(false)
{
	timer = new QTimer;
	connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));

	ui.setupUi(this);
	setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

	QString strBuild;
	QTextStream(&strBuild) << "Build " <<
		MAJOR_VER << "." <<
		MINOR_VER << "." <<
		PATCH_VER << "." <<
		BUILD_NUMBER;

	ui.buildNumber->setText(strBuild);
}

PilotRecordSplash::~PilotRecordSplash()
{
	if(timer)
		delete timer;
}

/*	Main Application Initialization
*		Main Engine is started here
*		Called when the window is shown
*/
void PilotRecordSplash::Initialize()
{
	timer->start(100);

	QFuture<void> f1 = QtConcurrent::run([this]() 
	{
		auto appArg = QApplication::arguments();
		auto configArg = appArg.filter("-");
		for (auto it : configArg)
		{
			auto nDivider = it.indexOf("=");
			QStringRef qstrKey(&it, 1, nDivider - 1);
			QStringRef qstrVal(&it, nDivider + 1, it.length() - nDivider + 1);
			BravoEngine::instance()->Reconfig(qstrKey.toString().toStdString(), qstrVal.toString().toStdString());
		}

		if (!BravoEngine::instance()->Initialize())
		{
			BravoEngine::instance()->addError("Initialization Error!", "There was a problem initializing the app.", Bravo::ERROR_FATAL);
		}

		this->SetDone(true);
	});
}

void PilotRecordSplash::timerUpdate()
{
	nElapsedTime += timer->interval();
	if (nElapsedTime >= 3000 && bDoneInit)
	{
		timer->stop();
		std::string strUrlUpdater;

		if (BravoEngine::instance()->CheckUpdates(strUrlUpdater))
		{
			PRMsgBox msg(this,
				"Update Notice",
				"A newer version of the application is available.",
				PRMsgBox::NOTICEOKONLY);
			msg.doModal();

			DownloadUpdater(QString(strUrlUpdater.c_str()));
			return;
		}
		QTimer::singleShot(0, this, SLOT(accept()));
	}
}

void PilotRecordSplash::keyPressEvent(QKeyEvent *e)
{
	if (e->key() != Qt::Key_Escape)
		QDialog::keyPressEvent(e);
}

void PilotRecordSplash::showEvent(QShowEvent * event)
{
	this->setGeometry(
		QStyle::alignedRect(
			Qt::LeftToRight,
			Qt::AlignCenter,
			this->size(),
			qApp->desktop()->availableGeometry()
		)
	);

	QDialog::showEvent(event);
	QCoreApplication::processEvents();

	Initialize();
}

void PilotRecordSplash::DownloadUpdater(QString & strUpdaterURL)
{
	// Wait Msg create!
	QString fileName = "up.bin";
	QString updaterFile = strUpdaterURL + fileName;
	PRWaitBox downloadBox(this);
	downloadBox.Initialize("Update", "Downloading Updater");

	QFuture<void> d1;

	downloadBox.SetDoneChecker([&]()->bool {return d1.isFinished(); });
	d1 = QtConcurrent::run([&]()
	{
		FILE *pFile = fopen(fileName.toStdString().c_str(), "wb");

		auto pCurl = curl_easy_init();
		if (!pCurl)
		{
			BravoEngine::instance()->addError("Update Error", "Initializing update failed 001", Bravo::ERROR_FATAL);
			return;
		}
		CURLcode res;

		curl_easy_setopt(pCurl, CURLOPT_URL, updaterFile.toStdString().c_str());
		curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, FileWriter_DL);
		curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, pFile);

		curl_easy_setopt(pCurl, CURLOPT_NOPROGRESS, 0);
		curl_easy_setopt(pCurl, CURLOPT_XFERINFOFUNCTION, Xferinfo_DL);
		curl_easy_setopt(pCurl, CURLOPT_XFERINFODATA, &downloadBox);

		res = curl_easy_perform(pCurl);
		if (res != CURLE_OK)
		{
			BravoEngine::instance()->addError("Update Error", "Initializing update failed 002", Bravo::ERROR_FATAL);
			return;
		}

		fclose(pFile);
		curl_easy_cleanup(pCurl);

		// Unzip file
		mz_zip_archive zipFile;
		memset(&zipFile, 0, sizeof(mz_zip_archive));
		auto zipHND = mz_zip_reader_init_file(&zipFile, fileName.toStdString().c_str(), 0);
		if (!zipHND)
		{
			BravoEngine::instance()->addError("Update Error", "Initializing update failed 003", Bravo::ERROR_FATAL);
			QFile::remove(fileName.toStdString().c_str());
			return;
		}

		for (int i = 0; i < (int)mz_zip_reader_get_num_files(&zipFile); i++)
		{
			char szStrFile[250];

			if (!mz_zip_reader_get_filename(&zipFile, i, szStrFile, 250))
			{
				BravoEngine::instance()->addError("Update Error", "Initializing update failed", Bravo::ERROR_FATAL);
				mz_zip_reader_end(&zipFile);
				return;
			}

			if (!mz_zip_reader_extract_file_to_file(&zipFile, szStrFile, szStrFile, 0))
				BravoEngine::instance()->addError("Update Error", "Initializing update failed", Bravo::ERROR_FATAL);
		}

		mz_zip_reader_end(&zipFile);

		QFile::remove(fileName.toStdString().c_str());
		
		// Shell open the updater!!!!
		// close!
		QProcess::startDetached("updater.exe", QStringList(strUpdaterURL));
		QMetaObject::invokeMethod(this, "reject", Qt::QueuedConnection);
	});
	
	downloadBox.exec();
}
