#pragma once

#include <QDialog>
#include "ui_PilotRecordLogBook.h"
#include "BravoDBStructure.h"

class PilotRecordLogBook : public QDialog
{
	Q_OBJECT

public:

	enum STATE {
		ADD_MODE,
		EDIT_MODE,
		SUMMARIZE_MODE,
		COMPARE_MODE
	};

	PilotRecordLogBook(QWidget *parent = Q_NULLPTR, STATE = ADD_MODE);
	//PilotRecordLogBook(QWidget *parent = Q_NULLPTR, );
	~PilotRecordLogBook();

	void setTabIndex(int nTabIndex);
	void SummarizeData(int nIndex);
	void EditEntry(Bravo::BravoDataEntry &entry);
	void CompareData(Bravo::PreviousVersionDataEntry &prevEntry, Bravo::BravoDataEntry &currentEntry);

public slots:
	void okay_pressed();
	void cancel_pressed();
	void edit_pressed();
	void reviewPreviousItem(QTreeWidgetItem* item, int nRow);
	void toUpper(const QString &str);
	void tabWidgetChange();

protected:
	void showEvent(QShowEvent *event) override;


private:
	STATE m_State;
	Ui::PilotRecordLogBook ui;

	//std::vector<Bravo::PreviousVersionDataEntry> m_tempPrevious;
	Bravo::BravoDataEntry m_tempEntry;
};
