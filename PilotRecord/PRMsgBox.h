#pragma once

#include <QDialog>
#include "ui_PRMsgBox.h"

class PRMsgBox : public QDialog
{
	Q_OBJECT

public:
	enum TYPE {
		ERRORTYPE, 
		ERROROKONLY,
		NOTICETYPE,
		NOTICEYESNO,
		NOTICEOKONLY
	};

	enum RESULT {
		OKAY,
		CANCEL,
	};

	PRMsgBox(QWidget *parent = Q_NULLPTR, QString strTitle = QString(), 
		QString strMsg = QString(), PRMsgBox::TYPE type = PRMsgBox::NOTICETYPE);
	~PRMsgBox();

	static TYPE intToType(int nType)
	{
		switch (nType)
		{
		case 4:
		{
			return NOTICEOKONLY;
			break;
		}
		case 3:
		{
			return NOTICEYESNO;
			break;
		}
		case 2:
		{
			return NOTICETYPE;
			break;
		}
		case 1:
		{
			return ERROROKONLY;
			break;
		}
		case 0:
		default:
		{
			return ERRORTYPE;
			break;
		}
		}
	}

	void SetMessage(QString strTitle, QString strMsg);
	void SetType(TYPE type);
	RESULT doModal(QString &strTitle = QString(), QString &strMessage = QString());


private:
	Ui::PRMsgBox ui;
	TYPE mType;
	QString m_strMessage;
	QString m_strTitle;
};
