#pragma once

#include <QDialog>
#include "ui_PilotRecordAccounts.h"

class PilotRecordAccounts : public QDialog
{
	Q_OBJECT

public:
	PilotRecordAccounts(QWidget *parent = Q_NULLPTR);
	~PilotRecordAccounts();

	void addAccount(std::string strUser, int nAccType);

protected:
	void showEvent(QShowEvent *event) override;


private:
	Ui::PilotRecordAccounts ui;

	const char* userType[5] = { "User", "Unkown_1", "Unkown_2", "Unkown_3", "Admin" };
};
