#include "PRCalendarWidget.h"
#include "PRCalendarDelegate.h"
#include "PRCalendarItem.h"
#include <QHeaderView>
#include <QTime>

PRCalendarWidget::PRCalendarWidget(QWidget *parent)
	: nRows (6)
	, nColumns (7)
	, QTableWidget(parent)
{
	setRowCount(nRows);
	setColumnCount(nColumns);
	headerLabels << "Monday" << "Tuesday" << "Wednesday" << "Thursday" << "Friday" << "Saturday" << "Sunday";
	nStartDayOfWeek = Qt::Monday;

	QTableWidget::setItemDelegate(new PRCalendarDelegate);
	QTableWidget::setEditTriggers(QAbstractItemView::NoEditTriggers);
	//QTableWidget::setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::SelectedClicked);
	QTableWidget::setSelectionBehavior(QAbstractItemView::SelectItems);

	QTableWidget::horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	QTableWidget::verticalHeader()->hide();
	QTableWidget::verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

PRCalendarWidget::~PRCalendarWidget()
{
}

void PRCalendarWidget::Initialize(Qt::DayOfWeek startDay, QDate dateInit)
{
	nStartDayOfWeek = startDay;
	dateActive = dateInit;

	addHeader();
	populateCalendar(dateInit);
	emit activePageChanged(dateActive.year(), dateActive.month());
}

QDate & PRCalendarWidget::getMonthActive()
{
	return dateActive;
}

QDate & PRCalendarWidget::getDayWidgetStart()
{
	return qvariant_cast<PRCalendarItem>(QTableWidget::item(0, 0)->data(0)).getDate();
}

QDate & PRCalendarWidget::getDayWidgetEnd()
{
	return qvariant_cast<PRCalendarItem>(QTableWidget::item(nRows-1, nColumns-1)->data(0)).getDate();
}

/** Get the item reference for Timeline data
@param date - date to find
@return nullptr if not found, pointer to calendar item
*/
void PRCalendarWidget::addDataCalendarItem(const QDate &date, const TimeInfo &data)
{
	for(int r = 0; r < rowCount(); r++)
		for (int c = 0; c < columnCount(); c++)
		{
			auto calItem = item(r, c);
			PRCalendarItem newData = qvariant_cast<PRCalendarItem>(calItem->data(0));
			if (newData.getDate() == date)
			{
				newData.addTimeline(data);
				calItem->setData(0, QVariant::fromValue(newData));
			}
		}
	//int i = 0;
	//for (QList<PRCalendarItem>::Iterator it = calendarItems.begin();
	//	it != calendarItems.end();
	//	it++, i++) {
	//	if (it->getDate() == date)
	//	{
	//		it->addTimeline(data);
	//		return;
	//	}
	//}
}

void PRCalendarWidget::addHeader()
{
	QStringList strListNewSort;
	for (int i = 0, labelIndex = nStartDayOfWeek - 1; i < headerLabels.size(); i++, labelIndex++)
	{
		if (labelIndex > 6)
			labelIndex = 0;

		strListNewSort << headerLabels[labelIndex];
	}
	QTableWidget::setHorizontalHeaderLabels(strListNewSort);
}

void PRCalendarWidget::populateCalendar(const QDate & monthInit)
{
	calendarItems.clear();

	QDate &currentDate = QDate::currentDate();

	int day, weekCounter;
	for (int day = 1, weekCounter = 0; day <= monthInit.daysInMonth(); day++)
	{
		QDate dateDay(monthInit.year(), monthInit.month(), day);
		QTableWidgetItem *dayWidget = new QTableWidgetItem;

		// Timeline data
		PRCalendarItem dateData(dateDay);
		if (currentDate == dateDay)
			dateData.setFontStyle(PRCalendarItem::ITEM_STYLE_BOLD);
		dayWidget->setData(0, QVariant::fromValue(dateData));
		calendarItems.push_back(dateData);

		// Day offset calculation
		int dayOfWeek = CalculateOffset((dateDay.dayOfWeek() - 1), (nStartDayOfWeek - 1));

		// previous Month Calculation
		if (day == 1)
		{
			QDate prevMonth(dateDay);
			for (int pd = dayOfWeek; pd > 0; pd--)
			{
				prevMonth = prevMonth.addDays(-1);
				PRCalendarItem prevMonthData(prevMonth);
				prevMonthData.setItemInactive(true);

				QTableWidgetItem *prevDayWidget = new QTableWidgetItem;
				prevDayWidget->setData(0, QVariant::fromValue(prevMonthData));
				QTableWidget::setItem(weekCounter, pd - 1, prevDayWidget);
			}
		}

		QTableWidget::setItem(weekCounter, dayOfWeek, dayWidget);
		
		if (currentDate == dateDay)
			QTableWidget::setCurrentItem(dayWidget);


		// Next Month Calculation
		if (day == dateDay.daysInMonth()
			&& weekCounter != QTableWidget::rowCount())
		{
			if (dayOfWeek == 6)
				weekCounter++;

			QDate nextDay = dateDay.addDays(1);
			while (weekCounter <= QTableWidget::rowCount())
			{
				for (int i = CalculateOffset(nextDay.dayOfWeek() - 1, nStartDayOfWeek - 1); i <= 6; i++)
				{
					PRCalendarItem NextMonthData(nextDay);
					NextMonthData.setItemInactive(true);

					QTableWidgetItem *nextDayWidget = new QTableWidgetItem;
					nextDayWidget->setData(0, QVariant::fromValue(NextMonthData));
					QTableWidget::setItem(weekCounter, i, nextDayWidget);
					nextDay = nextDay.addDays(1);
				}
				weekCounter++;
			}

		}

		if (dayOfWeek == 6)
			weekCounter++;
	}
}

int PRCalendarWidget::CalculateOffset(int nDayOfWeek, int nOffset)
{
	int dayOfWeek = nDayOfWeek - nOffset;
	if (dayOfWeek < 0)
	{
		dayOfWeek = dayOfWeek + 7;
	}

	return dayOfWeek;
}


void PRCalendarWidget::previousMonth()
{
	QTableWidget::clearContents();
	dateActive = dateActive.addMonths(-1);

	populateCalendar(dateActive);
	emit activePageChanged(dateActive.year(), dateActive.month());
}

void PRCalendarWidget::nextMonth()
{
	QTableWidget::clearContents();
	dateActive = dateActive.addMonths(1);

	populateCalendar(dateActive);
	emit activePageChanged(dateActive.year(), dateActive.month());
}
