#pragma once

#include <QDialog>
#include "ui_PilotRecordFlightDuty.h"

#include "PRCalendarItem.h"
#include <QGraphicsRectItem>

class QGraphicsScene;

class customRectItem : public QGraphicsRectItem
{
private:
	bool bSplitDuty;
public:
	inline void setSplitDuty(bool bVal)
	{
		bSplitDuty = bVal;
	}

protected:
	void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override
	{
		QColor col(brush().color());
		col.setAlpha(255);
		setBrush(col);
		setPen(QColor(220, 50, 50));
	}

	void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override
	{
		QColor col(brush().color());
		col.setAlpha(128);
		setBrush(col);
		setPen(Qt::NoPen);
	}
};

class PilotRecordFlightDuty : public QDialog
{
	Q_OBJECT

public:
	PilotRecordFlightDuty(QWidget *parent = Q_NULLPTR, PRCalendarItem *dateItem = nullptr);
	~PilotRecordFlightDuty();

public slots:
	void AddLeg();
	void SaveButton();
	void CloseButton();
	void reject() override;
	void DeleteButton();
	void itemChanged(QTableWidgetItem *pItem);
	void itemSelected();
	void dutyTimesCheck();
	void dayOffHandler(int state);
	void splitDutyHandler(int nState);
	void generateDOF(const QString &strLink);

signals:
	void refreshSignal();


protected:
	void showEvent(QShowEvent *pShowEvent) override;
	void closeEvent(QCloseEvent *event) override;

	void InitDlg();
	void InitGraphicsView();
	void CalcTotalFltTime();

private:
	Ui::PilotRecordFlightDuty ui;

	bool m_dlgChanged;
	void SetDlgChanged();
	void configureDutyDayDisplayItem();

	PRCalendarItem *pActiveDateItem;
	QGraphicsScene *m_pScene; // Scene Object
	QList<QGraphicsItem *> m_ItemList; // Item List
	customRectItem *m_pDayDutyItem1; // Duty Duration Bar
	customRectItem *m_pDayDutyItem2; // Duty Duration Bar
	QGraphicsTextItem *m_pOffLabel; // Off Label
};
