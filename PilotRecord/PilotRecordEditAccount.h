#pragma once

#include <QWidget>
#include "ui_PilotRecordEditAccount.h"

class PilotRecordEditAccount : public QWidget
{
	Q_OBJECT

public:
	PilotRecordEditAccount(QWidget *parent = Q_NULLPTR);
	~PilotRecordEditAccount();

private:
	Ui::PilotRecordEditAccount ui;
};
