/* PRCalendarWidget
*	Custom QT Table that shows bars to the calendar view
*	
*/
#pragma once

#include <QWidget>
#include <QTableWidget>
#include <QDate>
#include <QString>

#include "PRCalendarItem.h"

class PRCalendarWidget : public QTableWidget
{
	Q_OBJECT

public:
	PRCalendarWidget(QWidget *parent = nullptr);
	~PRCalendarWidget();
	/* Initial call setting up the widget
	*	populates them too
	*/
	void Initialize(Qt::DayOfWeek startDay = Qt::Monday, QDate dateInit = QDate::currentDate());
	
	QDate& getMonthActive();
	QDate& getDayWidgetStart();
	QDate& getDayWidgetEnd();

	void addDataCalendarItem(const QDate &date, const TimeInfo &data);

public slots:
	void previousMonth();
	void nextMonth();

signals:
	void activePageChanged(int year, int month);

protected:
	void addHeader();
	/* populateCalendar
	*	Clears the calendar and populate with new date
	*/
	void populateCalendar(const QDate &monthInit);

	int CalculateOffset(int nDayOfWeek, int nOffset);

	QStringList headerLabels;
	int nStartDayOfWeek;
	QDate dateActive; // Placeholder for active/shown month

	QList<PRCalendarItem> calendarItems;

private:
	int nRows;
	int nColumns;
};
