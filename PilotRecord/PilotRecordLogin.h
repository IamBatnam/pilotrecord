#pragma once

#include <QDialog>
#include "ui_PilotRecordLogin.h"

#include "Bravo.h"

class PilotRecordLogin : public QDialog
{
	Q_OBJECT

public:
	PilotRecordLogin(QWidget *parent = Q_NULLPTR);
	~PilotRecordLogin();

protected:
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

	void reject();

public slots:
	void login_pressed();
	void close_pressed();

private:
	Ui::PilotRecordLogin ui;
};
