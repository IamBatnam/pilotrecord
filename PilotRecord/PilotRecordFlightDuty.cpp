#include "PilotRecordFlightDuty.h"
#include "PRDutyDelegate.h"
#include "PRMsgBox.h"
#include "DOFRiskAssesment.h"

#include "Bravo.h"

#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QCloseEvent>

#include <curl/curl.h>
#include <nlohmann/json.hpp>

#include <qdebug.h>

int buffer_writer(char *ptr, size_t dsize, size_t nmemb, std::string *pStrUserData)
{
	if (pStrUserData == nullptr)
		return 0;

	pStrUserData->append(ptr, dsize * nmemb);
	return (int)(dsize * nmemb);
};


PilotRecordFlightDuty::PilotRecordFlightDuty(QWidget *parent, PRCalendarItem *dateItem)
	: QDialog(parent)
	, pActiveDateItem(dateItem)
	, m_pDayDutyItem1(nullptr)
	, m_pDayDutyItem2(nullptr)
	, m_pOffLabel(nullptr)
	//, m_bDayOff(false)
	//, m_bSplitDuty(false)
	, m_dlgChanged(false)
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	ui.setupUi(this);

	ui.graphicsView->setAlignment(Qt::AlignLeft | Qt::AlignBottom);
	ui.graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	m_pScene = new QGraphicsScene;
	ui.graphicsView->setScene(m_pScene);

	ui.dateLabel->setText(dateItem->getDate().toString("MMMM d, yyyy"));
	
	ui.tableWidget->setFocusPolicy(Qt::NoFocus);
	ui.tableWidget->setItemDelegate(new PRDutyDelegate(ui.tableWidget));
	ui.tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui.tableWidget->setColumnWidth(0, 70); // Up Time
	ui.tableWidget->setColumnWidth(1, 70); // Down Time
	ui.tableWidget->setColumnWidth(2, 50); // Flt Time
	ui.tableWidget->setColumnWidth(3, 70); // Crew
	ui.tableWidget->setColumnWidth(4, 70); // Aircraft
	ui.tableWidget->setColumnWidth(5, 115); // Flight Route
	ui.tableWidget->setColumnWidth(6, 90); // Flight Number
	ui.tableWidget->setColumnWidth(7, 50); // Mileage
	ui.tableWidget->horizontalHeader()->setHighlightSections(false);


	connect(ui.addLegButton, SIGNAL(pressed()), this, SLOT(AddLeg()));
	connect(ui.closeButton, SIGNAL(pressed()), this, SLOT(CloseButton()));
	connect(ui.deleteButton, SIGNAL(pressed()), this, SLOT(DeleteButton()));
	connect(ui.saveButton, SIGNAL(pressed()), this, SLOT(SaveButton()));
	connect(ui.tableWidget, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(itemChanged(QTableWidgetItem*)));
	connect(ui.tableWidget, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelected()));
	connect(ui.InTime, SIGNAL(editingFinished()), this, SLOT(dutyTimesCheck()));
	connect(ui.middayDutyOut, SIGNAL(editingFinished()), this, SLOT(dutyTimesCheck()));
	connect(ui.middayDutyStart, SIGNAL(editingFinished()), this, SLOT(dutyTimesCheck()));
	connect(ui.EndTime, SIGNAL(editingFinished()), this, SLOT(dutyTimesCheck()));
	connect(ui.offCheck, SIGNAL(stateChanged(int)), this, SLOT(dayOffHandler(int)));

	connect(ui.splitCheckBox, SIGNAL(stateChanged(int)), this, SLOT(splitDutyHandler(int)));
	ui.middayDutyOutLabel->setHidden(true);
	ui.middayDutyOut->setHidden(true);
	ui.middayDutyStartLabel->setHidden(true);
	ui.middayDutyStart->setHidden(true);

	connect(ui.linkDOFGen, SIGNAL(linkActivated(const QString &)), this, SLOT(generateDOF(const QString &)));

	auto &_date = dateItem->getDate();

	// Engine Set up
	BravoEngine::instance()->getEntry()->SetActiveDuty(_date.month(), _date.day(), _date.year());
	InitDlg();
}

PilotRecordFlightDuty::~PilotRecordFlightDuty()
{
}

/* reject - slot from QT and prevents esc
*/
void PilotRecordFlightDuty::reject()
{
	close();
}

void PilotRecordFlightDuty::CloseButton()
{
	close();
}

void PilotRecordFlightDuty::closeEvent(QCloseEvent *event)
{
	if (BravoEngine::instance()->getEntry()->isActiveDutyChanged())
	{
		PRMsgBox alert;
		if (alert.doModal(
			tr("Save Confirmation"),
			tr("Do you wish to proceed without saving?")) != PRMsgBox::RESULT::OKAY)
		{
			event->ignore();
			return;
		}
	}

	event->accept();
}

/* InitDlg
* @brief - Data initialiazation
*/
void PilotRecordFlightDuty::InitDlg()
{
	auto startTime = BravoEngine::instance()->getEntry()->getActiveDutyStartTime();
	ui.InTime->setTime({ startTime.hour, startTime.min });

	auto endMiddayTime = BravoEngine::instance()->getEntry()->getActiveDutyMiddayEndTime();
	ui.middayDutyOut->setTime({ endMiddayTime.hour, endMiddayTime.min });

	auto startMiddayTime = BravoEngine::instance()->getEntry()->getActiveDutyMiddayStartTime();
	ui.middayDutyStart->setTime({ startMiddayTime.hour, startMiddayTime.min });

	auto endTime = BravoEngine::instance()->getEntry()->getActiveDutyEndTime();
	ui.EndTime->setTime({ endTime.hour, endTime.min });

	//m_bDayOff = BravoEngine::instance()->getEntry()->getDayOff();
	//m_bSplitDuty = BravoEngine::instance()->getEntry()->getActiveDutySplitDuty();

	BravoEngine::instance()->getEntry()->getActiveFlightLegs<std::function<void(const QDateTime &,
		const QDateTime &,
		const char*,
		const char*,
		const char*,
		const char*,
		int)>>
		([&] (const QDateTime &startTime,
			const QDateTime &endTime,
			const char* szCrew,
			const char* szAC,
			const char* szFltRoute,
			const char* szFltNum,
			int nMileage)
	{
		customRectItem *pItem = new customRectItem;
		pItem->setRect(0, 10, 0, 20);
		pItem->setPen(Qt::NoPen);
		pItem->setBrush(QColor(200, 95, 95, 128));
		m_pScene->addItem((QGraphicsItem*)pItem);

		pItem->setAcceptHoverEvents(true);

		m_ItemList.push_back((QGraphicsItem*)pItem);

		int nRowCount = ui.tableWidget->rowCount();
		ui.tableWidget->insertRow(nRowCount);

		//QTableWidgetItem *pItemOut = new QTableWidgetItem(endTime.toString("hh:mm AP")); // TODO: change to userSet
		//QTableWidgetItem *pItemIn = new QTableWidgetItem(startTime.toString("hh:mm AP")); // TODO: change to userSet
		QTableWidgetItem *pItemIn = new QTableWidgetItem; // TODO: change to userSet
		QDateTime startTimeUTC = startTime;
		startTimeUTC.setTimeSpec(Qt::UTC);
		pItemIn->setData(0, startTimeUTC);

		QTableWidgetItem *pItemOut = new QTableWidgetItem; // TODO: change to userSet
		QDateTime endTimeUTC = endTime;
		endTimeUTC.setTimeSpec(Qt::UTC);		
		pItemOut->setData(0, endTimeUTC);

		QTableWidgetItem *pItemTotal = new QTableWidgetItem;
		pItemTotal->setFlags(pItemTotal->flags() ^ (Qt::ItemIsEditable));
		QTableWidgetItem *pItemCrew = new QTableWidgetItem(szCrew);
		QTableWidgetItem *pItemAC = new QTableWidgetItem(szAC);
		QTableWidgetItem *pItemFltNum = new QTableWidgetItem(szFltNum);
		QTableWidgetItem *pItemFltRoute = new QTableWidgetItem(szFltRoute);
		QTableWidgetItem *pItemMileage = new QTableWidgetItem(QString("%1 SM").arg(nMileage));

		ui.tableWidget->setItem(nRowCount, 2, pItemTotal);
		ui.tableWidget->setItem(nRowCount, 0, pItemIn);
		ui.tableWidget->setItem(nRowCount, 1, pItemOut);
		
		ui.tableWidget->setItem(nRowCount, 3, pItemCrew); // Column 3 is Crew
		ui.tableWidget->setItem(nRowCount, 4, pItemAC);
		ui.tableWidget->setItem(nRowCount, 5, pItemFltRoute); // Column 3 is Crew// Column 5 is route
		ui.tableWidget->setItem(nRowCount, 6, pItemFltNum);
		ui.tableWidget->setItem(nRowCount, 7, pItemMileage);
	});
}

void PilotRecordFlightDuty::SaveButton()
{
	BravoEngine::instance()->getEntry()->CommitActiveDuty();
	emit refreshSignal();
	accept();
}

void PilotRecordFlightDuty::DeleteButton()
{
	auto selectedItems = ui.tableWidget->selectedItems();
	if (selectedItems.size() > 0)
	{
		do
		{
			auto currentItem = ui.tableWidget->selectedItems()[0];
			int nRow = currentItem->row();
			ui.tableWidget->removeRow(nRow);

			auto graphicItem = m_ItemList[nRow];
			m_ItemList.removeAt(nRow);
			if (graphicItem)
				delete graphicItem;

			BravoEngine::instance()->getEntry()->DeleteDutyLeg(nRow);
		} while (ui.tableWidget->selectedItems().size() > 0);

		CalcTotalFltTime();
		SetDlgChanged();
	}
}

/** itemChanged - when the leg item changes
* @brief Code executes when a flight leg cell is changed
*/
void PilotRecordFlightDuty::itemChanged(QTableWidgetItem *pItem)
{
	if (pItem->column() == 0 || pItem->column() == 1)
	{
		SetDlgChanged();
		QDateTime InTime, OutTime;
		if (pItem->column() == 0) {
			//auto qInTime = QTime::fromString(qvariant_cast<QString>(pItem->data(0)), "hh:mm AP"); // TODO: Change to userSet
			InTime = qvariant_cast<QDateTime>(pItem->data(0));

			auto out = ui.tableWidget->item(pItem->row(), 1);
			if (out)
				OutTime = qvariant_cast<QDateTime>(out->data(0));
		}
		else // pItem->column() is endtime
		{
			auto In = ui.tableWidget->item(pItem->row(), 0);
			InTime = qvariant_cast<QDateTime>(In->data(0));
			OutTime = qvariant_cast<QDateTime>(pItem->data(0));
			
			if (InTime.isValid() && OutTime.isValid() && InTime > OutTime)
			{
				PRMsgBox msgBox(nullptr, "Time Error!", "The End time can't be earlier than the start time.", PRMsgBox::NOTICEOKONLY);
				msgBox.doModal();
			}
		}

		// If InTime is later than OutTime
		if ((InTime.isValid() && OutTime.isValid()) && InTime > OutTime)
		{
			auto nextColumn = ui.tableWidget->item(pItem->row(), 1);
			if (nextColumn != nullptr)
			{
				nextColumn->setData(0, InTime);
				OutTime = InTime;
			}
		}

		// Calculate by the hour
		auto pTotalItem = ui.tableWidget->item(pItem->row(), 2);
		if (pTotalItem)
		{
			// Change times from UTC to Local
			QDateTime startTimeLCL = InTime.toLocalTime();
			QDateTime endTimeLCL = OutTime.toLocalTime();

			qDebug() << "local start: " << startTimeLCL << "local end: " << endTimeLCL;

			double fStartTime, fFinishTime;
			fStartTime = startTimeLCL.time().hour() + (startTimeLCL.time().minute() / 60.0f);
			fFinishTime = (endTimeLCL.time().hour() + (endTimeLCL.time().minute() / 60.0f)) - fStartTime;

			auto viewPortRect = ui.graphicsView->rect();
			customRectItem *graphicBox = (customRectItem*)m_ItemList[pItem->row()];
			graphicBox->setRect(QRectF((viewPortRect.width() / 24.0f) * fStartTime, 10, (viewPortRect.width() / 24.0f) * fFinishTime, 20));

			pTotalItem->setData(0, QString("%1").arg(fFinishTime, 0, 'f', 1));
		}

		CalcTotalFltTime();
	}

	// Send the changes to the engine
	auto startTimeItem = ui.tableWidget->item(pItem->row(), 0);
	auto endTimeItem = ui.tableWidget->item(pItem->row(), 1);
	auto crewItem = ui.tableWidget->item(pItem->row(), 3);
	auto ACItem = ui.tableWidget->item(pItem->row(), 4);
	auto routeItem = ui.tableWidget->item(pItem->row(), 5);
	auto flightNumItem = ui.tableWidget->item(pItem->row(), 6);
	auto mileageItem = ui.tableWidget->item(pItem->row(), 7);

	if (startTimeItem && endTimeItem 
		&& ACItem && crewItem 
		&& routeItem && flightNumItem 
		&& mileageItem)
	{
		auto startLegTime = qvariant_cast<QDateTime>(startTimeItem->data(0));
		auto endLegTime = qvariant_cast<QDateTime>(endTimeItem->data(0));
		BravoEngine::instance()->getEntry()->UpdateDutyLeg(pItem->row(),
			{ startLegTime.date().year(),
				startLegTime.date().month(),
				startLegTime.date().day(),
				startLegTime.time().hour(),
				startLegTime.time().minute() },
			{ endLegTime.date().year(),
				endLegTime.date().month(),
				endLegTime.date().day(),
				endLegTime.time().hour(),
				endLegTime.time().minute() },
			ACItem->data(0).toString().toStdString(),
			crewItem->data(0).toString().toStdString(), // Crew
			routeItem->data(0).toString().toStdString(), // Route
			flightNumItem->data(0).toString().toStdString(),
			mileageItem->data(0).toInt());
	}
}

void PilotRecordFlightDuty::itemSelected()
{
	if (ui.tableWidget->selectedItems().size() > 0)
		ui.deleteButton->setEnabled(true);
	else
		ui.deleteButton->setEnabled(false);
}

void PilotRecordFlightDuty::showEvent(QShowEvent * pShowEvent)
{
	InitGraphicsView();
	dutyTimesCheck();
}

/** Link Activated generation of Daily Ops Form
*/
void PilotRecordFlightDuty::generateDOF(const QString &strLink)
{
	QString qstrRiskAssesment;
	if (ui.tableWidget->rowCount() > 0)
	{
		// TODO:NOW add row flight legs

		qstrRiskAssesment += "&drisk=";
		DOFRiskAssesment riskAsses;
		riskAsses.doModal(qstrRiskAssesment);		
	}

	using json = nlohmann::json;
	QString strPOSTData("name=Erana&");

	if (BravoEngine::instance()->getEntry()->getDayOff())
		strPOSTData += "dtype=do&";
	else
	{
		strPOSTData += "dtype=dd&";
		strPOSTData += QString("dtstart=%1&dtend=%2&").arg(ui.InTime->time().toString("HH:mm")).arg(ui.EndTime->time().toString("HH:mm"));
	}

	// Add dates to URL
	strPOSTData += QString("dday=%1&dmon=%2&dyear=%3").arg(pActiveDateItem->getDate().toString("dd")).arg(pActiveDateItem->getDate().toString("MM")).arg(pActiveDateItem->getDate().toString("yy"));

	// Add duty times
	strPOSTData += qstrRiskAssesment;

	auto pCurl = curl_easy_init();
	CURLcode curlRes;
	std::string strBuffer;


	if (pCurl)
	{
		strPOSTData = QUrl::toPercentEncoding(strPOSTData, "=&");
		char *szPOSTData = new char[strPOSTData.length()];
		strcpy(szPOSTData, strPOSTData.toStdString().c_str());

		curl_easy_setopt(pCurl, CURLOPT_POST, 1L);
		curl_easy_setopt(pCurl, CURLOPT_POSTFIELDS, szPOSTData);
		//curl_easy_setopt(pCurl, CURLOPT_POSTFIELDSIZE, strPOSTData.length());

		curl_easy_setopt(pCurl, CURLOPT_URL, "http://dof.raviuz.com/");

		curl_easy_setopt(pCurl, CURLOPT_WRITEFUNCTION, buffer_writer);
		curl_easy_setopt(pCurl, CURLOPT_WRITEDATA, &strBuffer);

		curlRes = curl_easy_perform(pCurl);
		curl_easy_cleanup(pCurl);

		if (curlRes != CURLE_OK)
		{
			PRMsgBox errorMsg(this, "Error!", "Error generating form #5", PRMsgBox::ERROROKONLY);
			errorMsg.doModal();
			return;
		}

		try {
			auto jSon = json::parse(strBuffer);

			PRMsgBox userMsg(this, "Generated Report", QString("The form generated is a \"%1\" URL: %2").arg(
				jSon["Result"].get<std::string>().c_str(),
				jSon["URL"].get<std::string>().c_str()));
			userMsg.doModal();
		}
		catch (...)
		{
			PRMsgBox errorMsg(this, "Error!", "Error getting response from server!", PRMsgBox::ERROROKONLY);
			errorMsg.doModal();
			return;
		}
	}
}

/* InitGraphicsView
* @brief - Initialize the view window with default values
*/
void PilotRecordFlightDuty::InitGraphicsView()
{
	// m_pScene->clear();
	// m_ItemList.clear();

	auto viewPortRect = ui.graphicsView->rect();

	// Guidelines/Hours color
	QBrush guideBrush(QColor(165, 165, 165));

	qreal  x = 0, y = 0;
	for (int i = 0; i < 24; i++)
	{
		if (i == 0)
		{
			// Text
			auto *text = m_pScene->addText(QString("%1h").arg(i, 2, 10, QChar(0)));
			text->setPos(QPointF(0.0f, -16.0f));
			text->setDefaultTextColor(guideBrush.color());

			// sub line
			QLineF startline(x + ((viewPortRect.width() / 24.0f) / 2), 15.0f,
				x + ((viewPortRect.width() / 24.0f) / 2), 30.0f);
			auto *startlineItem = m_pScene->addLine(startline);
			startlineItem->setPen(guideBrush.color());

			x += (viewPortRect.width() / 24.0f);
			continue;
		}

		// Line
		QLineF line(x, 0, x, 30);
		auto *lineItem = m_pScene->addLine(line);
		lineItem->setPen(guideBrush.color());

		// Text
		auto *text = m_pScene->addText(QString("%1h").arg(i));
		text->setPos(QPointF(x - 7.0f, -16.0f));
		text->setDefaultTextColor(guideBrush.color());

		// sub line
		QLineF subline(x + ((viewPortRect.width() / 24.0f) / 2), 15.0f,
			x + ((viewPortRect.width() / 24.0f) / 2), 30.0f);
		auto *sublineItem = m_pScene->addLine(subline);
		sublineItem->setPen(guideBrush.color());

		x += (viewPortRect.width() / 24.0f);
	}

	// Yellow line for 1st duty
	m_pDayDutyItem1 = new customRectItem;
	m_pDayDutyItem1->setSplitDuty(false);
	m_pDayDutyItem1->setRect(0, 10, 0, 20); // TODO: this would be from the engine
	m_pDayDutyItem1->setPen(Qt::NoPen);
	m_pDayDutyItem1->setBrush(QColor(220, 190, 47, 128));
	m_pScene->addItem((QGraphicsItem*)m_pDayDutyItem1);

	m_pDayDutyItem2 = new customRectItem;
	m_pDayDutyItem2->setSplitDuty(false);
	m_pDayDutyItem2->setRect(0, 10, 0, 20); // TODO: this would be from the engine
	m_pDayDutyItem2->setPen(Qt::NoPen);
	m_pDayDutyItem2->setBrush(QColor(220, 190, 47, 128));
	m_pScene->addItem((QGraphicsItem*)m_pDayDutyItem2);

	if (BravoEngine::instance()->getEntry()->getActiveDutySplitDuty())
		ui.splitCheckBox->setChecked(true);
	configureDutyDayDisplayItem();

	// OFF SIGN
	m_pOffLabel = m_pScene->addText("Off Today");
	auto offFont = m_pOffLabel->font();
	offFont.setBold(true);
	offFont.setPointSize(20);
	m_pOffLabel->setFont(offFont);
	m_pOffLabel->setPos(QPointF((viewPortRect.width() / 2) - (m_pOffLabel->boundingRect().width() / 2), -12.0f));
	m_pOffLabel->hide();

	if (BravoEngine::instance()->getEntry()->getDayOff())
		ui.offCheck->setChecked(true);

	// recalculate the legs
	for (int i = 0; i < ui.tableWidget->rowCount(); i++)
	{
		auto startTimeItem = ui.tableWidget->item(i, 0);
		auto endTimeItem = ui.tableWidget->item(i, 1);

		QDateTime startTime = qvariant_cast<QDateTime>(startTimeItem->data(0));
		QDateTime endTime = qvariant_cast<QDateTime>(endTimeItem->data(0));

		// Change times from UTC to Local
		const QDateTime &startTimeLCL = startTime.toLocalTime();
		const QDateTime &endTimeLCL = endTime.toLocalTime();


		double fStartTime, fFinishTime;
		fStartTime = startTimeLCL.time().hour() + (startTimeLCL.time().minute() / 60.0f);
		fFinishTime = (endTimeLCL.time().hour() + (endTimeLCL.time().minute() / 60.0f)) - fStartTime;

		auto viewPortRect = ui.graphicsView->rect();
		customRectItem *graphicBox = (customRectItem*)m_ItemList[i];
		graphicBox->setRect(QRectF((viewPortRect.width() / 24.0f) * fStartTime, 10, (viewPortRect.width() / 24.0f) * fFinishTime, 20));
	}
	
}

/* configureDutyDayDisplayItem
* @brief - Set The graphics window according to the split duty settings
*/
void PilotRecordFlightDuty::configureDutyDayDisplayItem()
{
	if (BravoEngine::instance()->getEntry()->getDayOff())
	{
		m_pDayDutyItem1->hide();
		m_pDayDutyItem2->hide();
		return;
	}

	if (BravoEngine::instance()->getEntry()->getActiveDutySplitDuty())
	{
		m_pDayDutyItem1->show();
		m_pDayDutyItem2->show();
	}
	else
	{
		m_pDayDutyItem1->show();
		m_pDayDutyItem2->hide();
	}
}

void PilotRecordFlightDuty::AddLeg()
{
	customRectItem *pItem = new customRectItem;
	pItem->setRect(0, 10, 0, 20);
	pItem->setPen(Qt::NoPen);
	pItem->setBrush(QColor(200, 95, 95, 128));
	m_pScene->addItem((QGraphicsItem*)pItem);

	pItem->setAcceptHoverEvents(true);

	m_ItemList.push_back((QGraphicsItem*)pItem);

	int nRowCount = ui.tableWidget->rowCount();
	ui.tableWidget->insertRow(nRowCount);

	QString qstrStartTime("12:00 AM");
	QTime startTime = QTime::fromString(qstrStartTime);

	QString qstrEndTime("12:00 PM");
	QTime endTime = QTime::fromString(qstrEndTime);

	BravoEngine::instance()->getEntry()->AddDutyLeg({ pActiveDateItem->getDate().year(),
			pActiveDateItem->getDate().month(),
			pActiveDateItem->getDate().day(),
			startTime.hour(),
			startTime.minute() },
		{ pActiveDateItem->getDate().year(),
			pActiveDateItem->getDate().month(),
			pActiveDateItem->getDate().day(),
			endTime.hour(),
			endTime.minute() },
		"C-",
		"", // Crew
		"", // Route
		"HW",
		0);

	QTableWidgetItem *pItemIn = new QTableWidgetItem;
	QDateTime startDateTime({ pActiveDateItem->getDate().year(), pActiveDateItem->getDate().month(), pActiveDateItem->getDate().day() },
		{ startTime.hour(),
			startTime.minute() });
	pItemIn->setData(0, startDateTime);

	QTableWidgetItem *pItemOut = new QTableWidgetItem;
	QDateTime endDateTime({ pActiveDateItem->getDate().year(), pActiveDateItem->getDate().month(), pActiveDateItem->getDate().day() },
		{ endTime.hour(),
			endTime.minute() });
	pItemOut->setData(0, endDateTime);

	QTableWidgetItem *pItemTotal = new QTableWidgetItem(QString("0.0"));
	pItemTotal->setFlags(pItemTotal->flags() ^ (Qt::ItemIsEditable));
	QTableWidgetItem *pItemCrew = new QTableWidgetItem(QString("")); // Null value required for item 
	QTableWidgetItem *pItemAC = new QTableWidgetItem("C-");
	QTableWidgetItem *pItemRoute = new QTableWidgetItem(QString(""));
	QTableWidgetItem *pItemFltNum = new QTableWidgetItem("HW");
	QTableWidgetItem *pItemMileage = new QTableWidgetItem("0 SM");

	ui.tableWidget->setItem(nRowCount, 0, pItemIn);
	ui.tableWidget->setItem(nRowCount, 1, pItemOut);
	ui.tableWidget->setItem(nRowCount, 2, pItemTotal);
	ui.tableWidget->setItem(nRowCount, 3, pItemCrew);
	ui.tableWidget->setItem(nRowCount, 4, pItemAC);
	ui.tableWidget->setItem(nRowCount, 5, pItemRoute);
	ui.tableWidget->setItem(nRowCount, 6, pItemFltNum);
	ui.tableWidget->setItem(nRowCount, 7, pItemMileage);

	ui.tableWidget->setCurrentItem(pItemIn);
	ui.tableWidget->editItem(pItemIn);	

	SetDlgChanged();
}

/** dutyTimesCheck
* @brief - Code executes when any of the duty time boxes changes (Start, mid-end, mid-start, end)
*		- Event handler
*		- Calculates the new position of the duty display bars
*/
void PilotRecordFlightDuty::dutyTimesCheck()
{
	//auto prevRect = m_pDayDutyItem->rect();
	QObject *timeEdit = sender();
	if (timeEdit == ui.InTime)
	{
		if (ui.InTime->time() > ui.EndTime->time())
			ui.EndTime->setTime(ui.InTime->time());
	}

	if (timeEdit == ui.EndTime)
	{
		if (ui.EndTime->time() < ui.InTime->time())
		{
			PRMsgBox msgBox;
			msgBox.SetType(PRMsgBox::NOTICEOKONLY);
			msgBox.SetMessage(tr("Time Warning!"), tr("The start time can not be later than the end time."));
			msgBox.doModal();
			ui.EndTime->setTime(ui.InTime->time());
		}
	}

	auto activeDay = pActiveDateItem->getDate();
	if (!BravoEngine::instance()->getEntry()->getDayOff())
	{
		BravoEngine::instance()->getEntry()->setActiveDutyStartTime({ activeDay.year(), activeDay.month(), activeDay.day(),
			ui.InTime->time().hour(), ui.InTime->time().minute()});

		BravoEngine::instance()->getEntry()->setActiveDutyMiddayEndTime({ activeDay.year(), activeDay.month(), activeDay.day(),
			ui.middayDutyOut->time().hour(), ui.middayDutyOut->time().minute() });

		BravoEngine::instance()->getEntry()->setActiveDutyMiddayStartTime({ activeDay.year(), activeDay.month(), activeDay.day(),
			ui.middayDutyStart->time().hour(), ui.middayDutyStart->time().minute() });

		BravoEngine::instance()->getEntry()->setActiveDutyEndTime({ activeDay.year(), activeDay.month(), activeDay.day(),
			ui.EndTime->time().hour(), ui.EndTime->time().minute() });
	}

	// Calculate the duty day display
	if (BravoEngine::instance()->getEntry()->getActiveDutySplitDuty())
	{
		double fStartTime, fFinishTime, fMiddayEnd, fMiddayStart;
		auto viewPortRect = ui.graphicsView->rect();
		fStartTime = ui.InTime->time().hour() + (ui.InTime->time().minute() / 60.0f);
		fMiddayEnd = ui.middayDutyOut->time().hour() + (ui.middayDutyOut->time().minute() / 60.0f) - fStartTime;

		m_pDayDutyItem1->setRect(QRectF((viewPortRect.width() / 24.0f) * fStartTime, 10, (viewPortRect.width() / 24.0f) * fMiddayEnd, 20));

		fMiddayStart = ui.middayDutyStart->time().hour() + (ui.middayDutyStart->time().minute() / 60.0f);
		fFinishTime = (ui.EndTime->time().hour() + (ui.EndTime->time().minute() / 60.0f)) - fMiddayStart;
		m_pDayDutyItem2->setRect(QRectF((viewPortRect.width() / 24.0f) * fMiddayStart, 10, (viewPortRect.width() / 24.0f) * fFinishTime, 20));

		ui.dutyDayLabel->setText(QString("%1").arg(fMiddayEnd + fFinishTime, 0, 'f', 1));
	}
	else
	{
		double fStartTime, fFinishTime;
		fStartTime = ui.InTime->time().hour() + (ui.InTime->time().minute() / 60.0f);
		fFinishTime = (ui.EndTime->time().hour() + (ui.EndTime->time().minute() / 60.0f)) - fStartTime;
		auto viewPortRect = ui.graphicsView->rect();
		m_pDayDutyItem1->setRect(QRectF((viewPortRect.width() / 24.0f) * fStartTime, 10, (viewPortRect.width() / 24.0f) * fFinishTime, 20));

		ui.dutyDayLabel->setText(QString("%1").arg(fFinishTime, 0, 'f', 1));
	}

	//if (prevRect != m_pDayDutyItem->rect())
	SetDlgChanged();
}

void PilotRecordFlightDuty::dayOffHandler(int state)
{
	if (state == Qt::Checked)
	{
		BravoEngine::instance()->getEntry()->setActiveDutyDayOff(true);
		m_pOffLabel->show();
		configureDutyDayDisplayItem();
		ui.manageDutiesBox->setDisabled(true);
	}
	else // Unchecked
	{
		BravoEngine::instance()->getEntry()->setActiveDutyDayOff(false);
		m_pOffLabel->hide();
		configureDutyDayDisplayItem();
		ui.manageDutiesBox->setDisabled(false);
	}

	SetDlgChanged();
}

/* splitDutyCheckHandler
* @brief - The handler for when the state of split duty check box changes
*/
void PilotRecordFlightDuty::splitDutyHandler(int nState)
{
	if (nState == Qt::Checked)
	{
		ui.middayDutyOutLabel->setHidden(false);
		ui.middayDutyOut->setHidden(false);
		ui.middayDutyStartLabel->setHidden(false);
		ui.middayDutyStart->setHidden(false);
		BravoEngine::instance()->getEntry()->setActiveDutySplitDuty(true);
	}
	else // Unchecked
	{
		ui.middayDutyOutLabel->setHidden(true);
		ui.middayDutyOut->setHidden(true);
		ui.middayDutyStartLabel->setHidden(true);
		ui.middayDutyStart->setHidden(true);
		BravoEngine::instance()->getEntry()->setActiveDutySplitDuty(false);
	}

	configureDutyDayDisplayItem();
	dutyTimesCheck();
}

void PilotRecordFlightDuty::CalcTotalFltTime()
{
	double fTotalTime = 0.0f;
	for (int i = 0; i < ui.tableWidget->rowCount(); i++)
	{
		auto dataItem = ui.tableWidget->item(i, 2);
		if(dataItem)
			fTotalTime += dataItem->data(0).toDouble();
	}

	ui.totalFlightLabel->setText(QString("%1").arg(fTotalTime, 0, 'f', 1));
}

void PilotRecordFlightDuty::SetDlgChanged()
{
	if (BravoEngine::instance()->getEntry()->isActiveDutyChanged() && !m_dlgChanged) {
		m_dlgChanged = true;
		setWindowTitle(QString("%1*").arg(windowTitle()));
	}
}