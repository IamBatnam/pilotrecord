#pragma once

#include <QtWidgets/QMainWindow>
#include <QListWidgetItem>
#include "ui_PilotRecord.h"

#include "PilotRecordLogin.h"
#include "PilotRecordLogBook.h"
#include "PilotRecordAccounts.h"

#ifndef MOUSE_WAIT
#	define MOUSE_WAIT  QApplication::setOverrideCursor(Qt::WaitCursor); 
#endif

#ifndef MOUSE_RST
#	define MOUSE_RST  QApplication::restoreOverrideCursor(); 
#endif


class PilotRecord : public QMainWindow
{
    Q_OBJECT

public:

	enum CURRENT_STATE {
		SUMMARY_PAGE,
		LOGBOOK_PAGE,
		FLIGHT_DUTY,
		HIDE_ALL,
		NEXT_PAGE
	};

    PilotRecord(QWidget *parent = Q_NULLPTR);
	~PilotRecord();
	void UpdateHoursLog();
	void UpdateCalendar(bool bInitialize = false);
	void Show();
	void RunErrorLoop();

	static void engineCallbackCalendarUpdate(void * pObj, QDate date, QDateTime startTime, QDateTime endTime, bool bDayOff);

protected:
	void closeEvent(QCloseEvent *event) override;

public slots:
	void togglePageToolbar();
	void lockLogoutToolbar();
	void addLogToolbar();
	void manageAccntsDlgToolbar();
	void flightDutyToolbar();
	
	void calendarChanged(int nYear, int nMonth);
	void calendarSelectChange();
	void calendarDetails();
	void calendarDateDbleClicked(QTableWidgetItem *pItem);
	void calendarNextMonth();
	void calendarPreviousMonth();

	void reviewItem(QTreeWidgetItem *item = nullptr, int nRow = 0);
	void ErrorLoopUpdate();

	void refreshDutyCalendar();

public:
	void ErrorMsgShow(std::string strTitle, std::string strContent, int errorType);
	void togglePage(CURRENT_STATE cpage = CURRENT_STATE::NEXT_PAGE);

private:
    Ui::PilotRecordClass ui;

	CURRENT_STATE curPage;
	
	PilotRecordLogin *login;

	QIcon lookBook;
	QIcon pilotBook;

	bool bAppRunning;

	QTimer *loopTimer;

	void InitFlightDutyPage();
	
};
