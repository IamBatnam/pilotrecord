#include "PRMsgBox.h"

PRMsgBox::PRMsgBox(QWidget * parent, QString strTitle, QString strMsg, PRMsgBox::TYPE type)
	: QDialog(parent)
	, mType(type)
	, m_strTitle(strTitle)
	, m_strMessage(strMsg)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	setWindowTitle(m_strTitle);
	SetType(mType);
}

PRMsgBox::~PRMsgBox()
{
}

void PRMsgBox::SetMessage(QString strTitle, QString strMsg)
{
	m_strMessage = strMsg;
	m_strTitle = strTitle;
	setWindowTitle(m_strTitle);
}

void PRMsgBox::SetType(TYPE type)
{
	mType = type;

	switch (mType)
	{
	case PRMsgBox::ERROROKONLY:
	{
		ui.OkButton->setVisible(true);
		ui.CancelButton->setVisible(false);
	}
	case PRMsgBox::ERRORTYPE:
	{
		QPixmap pix(":/PilotRecord/errorAlert.png");
		ui.logoLabel->setPixmap(pix);
		
		QIcon errorIcon(pix);
		setWindowIcon(errorIcon);
		break;
	}
	case PRMsgBox::NOTICEOKONLY:
	{
		ui.OkButton->setVisible(true);
		ui.CancelButton->setVisible(false);
	}
	case PRMsgBox::NOTICETYPE:
	case PRMsgBox::NOTICEYESNO:
	default:
	{
		QPixmap pix(":/PilotRecord/Alert.png");
		ui.logoLabel->setPixmap(pix);

		QIcon alertIcon(pix);
		setWindowIcon(alertIcon);
		break;
	}
	}
}

PRMsgBox::RESULT PRMsgBox::doModal(QString &strTitle, QString &strMessage)
{
	if(!strMessage.isNull())
		ui.msgText->setText(strMessage);
	else
		ui.msgText->setText(m_strMessage);


	if (!strTitle.isNull())
	{
		setWindowTitle(strTitle);
		ui.titleText->setText(strTitle);
	}
	else
		ui.titleText->setText(m_strTitle);

	adjustSize();
	auto winSize = frameSize();
	setMinimumSize(winSize);
	setMaximumSize(winSize);

	QApplication::beep();

	if (exec() == QDialog::Accepted)
		return RESULT::OKAY;
	
	return RESULT::CANCEL;
}
