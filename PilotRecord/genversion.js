var fileName = 'buildversion.h';

var fs = require('fs');
var lineReader = require('readline').createInterface({
  input: fs.createReadStream(fileName)
});

var commentedLines = [];
var lines = []; // Lines that are commented
var addedLines = []; // Already configured

lineReader.on('line', function (line) {
	if(line.search("//") >= 0) {
		commentedLines.push(line);

		var x = line.search(" ") + 1;
		line = line.substring(x, line.length);
		
		if((i = line.search(/=/g)) >= 0)
		{
			var obj = {'key' : line.substring(0, i), 'type' : 0, 'value' : line.substring(i+1, line.length)};
			lines.push(obj);
		}
		else if((i = line.search(/\+\+/g)) >= 0)
		{
			var obj = {'key' : line.substring(0, i), 'type' : 1, 'value' : 1};
			lines.push(obj);
		}
	}


	if(l = line.search("#define") >= 0) {
		l += 7;
		line = line.substring(l, line.length);
		l = line.search(" ");

		addedLines.push({'key' : line.substring(0, l), 'value' : line.substring(l+1, line.length)});
	}

  //console.log('Line from file:', line.substring(x, line.length));
}).on('close', function()
{
	if(addedLines.length > 0) {
		var foundLines = [];
		// Compare if listed in is listed
		lines.forEach(function(item, index) {
			addedLines.forEach(function(item2, index2) {
				if(item['key'] == item2['key'])
				{
					if(item['type'] == 0)
					{
						foundLines.push(item2);
					}
					else if(item['type'] == 1)
					{
						item2['value']++;
						foundLines.push(item2);
					}
				}
			});
		});

		addedLines = foundLines;
	}

	var findInLine = function(itemFind) {
			var result = false;
			addedLines.forEach(function(item2, index2) {
				if(itemFind == item2['key'])
				{
					result = true;
				}
			});

			return result;
		};

	lines.forEach(function(item, index) {
		if(!findInLine(item['key']))
		{
			addedLines.push(item);
		}
	});

	var bufferArray = [];

	commentedLines.forEach(function(item, index) {
		bufferArray.push(item);
	});

	bufferArray.push('\n');

	addedLines.forEach(function(item, index) {

		bufferArray.push("#define " + item['key'] + " " + item['value']);
	});

	var file = fs.createWriteStream(fileName);

	bufferArray.forEach(function(item, index) {
		file.write(item + "\n")
	});

	file.end();
});