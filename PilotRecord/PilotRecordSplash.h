#pragma once

#include <QDialog>
#include "ui_PilotRecordSplash.h"

extern "C" { 
	extern int Xferinfo_DL(void *point, size_t dltotal, size_t dlnow, size_t ultotal, size_t ulnow);
	extern int FileWriter_DL(char *pData, size_t size, size_t nmemb, void *pointer);
}

class PilotRecordSplash : public QDialog
{
	Q_OBJECT

public:
	PilotRecordSplash(QWidget *parent = Q_NULLPTR);
	~PilotRecordSplash();

	void Initialize();

public slots:
	void timerUpdate();

protected:
	void keyPressEvent(QKeyEvent *e) override;
	void showEvent(QShowEvent *event) override;
	
	void DownloadUpdater(QString & strUpdaterURL);

public:
	inline void SetDone(bool bDone) { bDoneInit = bDone; };

private:
	Ui::PilotRecordSplash ui;

	bool bDoneInit;
	QTimer *timer;
	int nElapsedTime;
};
