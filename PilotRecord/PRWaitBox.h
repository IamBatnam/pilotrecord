#pragma once

#include <QDialog>
#include "ui_PRWaitBox.h"
#include <functional>

class PRWaitBox : public QDialog
{
	Q_OBJECT

public:
	PRWaitBox(QWidget *parent = Q_NULLPTR);
	~PRWaitBox();

	inline void Initialize(const QString &strTitle, const QString &strStatus) {
		setWindowTitle(strTitle);
		ui.statusLabel->setText(strStatus);
	};
	inline void SetStatus(const QString &strStatus) { ui.statusLabel->setText(strStatus); };
	void SetMinMaxVal(int min, int max);
	void SetValue(int nVal);

	/* This method is called by the timer checking
*  to see if the timer should be stopped
*/
	inline void SetDoneChecker(std::function<bool()> bStop) {
		updateCheck = bStop;
	}

protected:
	//void keyPressEvent(QKeyEvent *e) override;
	void showEvent(QShowEvent *event) override;


public slots:
	void timerUpdate();

private:
	Ui::PRWaitBox ui;
	QTimer *qTimer;

	std::function<bool()> updateCheck;

};
