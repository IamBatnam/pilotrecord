#include "PilotRecord.h"

#include "PilotRecordSetup.h"
#include "PRMsgBox.h"
#include "PilotRecordSplash.h"
#include "PRCalendarItem.h"
#include "PilotRecordFlightDuty.h"

#include <qtextstream.h>
#include <qtimer.h>
#include <QThread>
#include <QtConcurrent>
#include <QMenu>

#include <curl/curl.h>

#include <Bravo.h>


PilotRecord::PilotRecord(QWidget *parent)
	: QMainWindow(parent)
	, login(nullptr)
	, bAppRunning(true)
	, curPage(PilotRecord::HIDE_ALL)

	, lookBook(":/PilotRecord/Look.png")
	, pilotBook(":/PilotRecord/pilot.png")
	, loopTimer(nullptr)
{
	curl_global_init(CURL_GLOBAL_DEFAULT);

	ui.setupUi(this);
	ui.toolBar->setContextMenuPolicy(Qt::PreventContextMenu);

	login = new PilotRecordLogin(this);

	loopTimer = new QTimer(this);
	connect(loopTimer, SIGNAL(timeout()), this, SLOT(ErrorLoopUpdate()));

	RunErrorLoop();

	PilotRecordSplash splash;
	if (splash.exec() == QDialog::Rejected)
	{
		bAppRunning = false;
		QTimer::singleShot(0, this, SLOT(close()));
	}

	connect(ui.tableWidget, SIGNAL(activePageChanged(int, int)), this, SLOT(calendarChanged(int, int)));
	connect(ui.tableWidget, SIGNAL(itemDoubleClicked(QTableWidgetItem *)), this, SLOT(calendarDateDbleClicked(QTableWidgetItem *)));
	connect(ui.tableWidget, SIGNAL(itemSelectionChanged()), this, SLOT(calendarSelectChange()));
	connect(ui.detailsButton, SIGNAL(pressed()), this, SLOT(calendarDetails()));
	InitFlightDutyPage();

	// TOOLBAR
	connect(ui.actionLogbook, SIGNAL(triggered()), this, SLOT(togglePageToolbar()));
	connect(ui.actionLock_Logout, SIGNAL(triggered()), this, SLOT(lockLogoutToolbar()));
	connect(ui.actionAddLog, SIGNAL(triggered()), this, SLOT(addLogToolbar()));
	connect(ui.actionManage_User, SIGNAL(triggered()), this, SLOT(manageAccntsDlgToolbar()));
	connect(ui.actionFlight_Duty, SIGNAL(triggered()), this, SLOT(flightDutyToolbar()));

	connect(ui.reviewButton, SIGNAL(pressed()), this, SLOT(reviewItem()));
	connect(ui.entryList, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(reviewItem(QTreeWidgetItem*, int)));

	/*connect(ui.prevMonth, SIGNAL(pressed()), ui.tableWidget, SLOT(previousMonth()));
	connect(ui.nextMonth, SIGNAL(pressed()), ui.tableWidget, SLOT(nextMonth()));*/
	connect(ui.prevMonth, SIGNAL(pressed()), this, SLOT(calendarPreviousMonth()));
	connect(ui.nextMonth, SIGNAL(pressed()), this, SLOT(calendarNextMonth()));
}

PilotRecord::~PilotRecord()
{
	curl_global_cleanup();
	loopTimer->stop();
	bAppRunning = false;
}

/* UpdateHoursLog
*	This must be called to update the view of the list and
*	total hours from the database
*/
void PilotRecord::UpdateHoursLog()
{
	statusBar()->showMessage(tr("Updating hours and flight duty..."));
	ui.entryList->clear();

	// Call engine to update the hours
	auto entryController = BravoEngine::instance()->getEntry();
	entryController->GetInitialEntries<std::function<void(const char* , const char* , const char*, const char*, const char*)>>
		([=](
			const char* _date,
			const char* _PIC,
			const char* _ACtype,
			const char* _ACreg,
			const char* _remarks) {
		QString qstrRemarks(_remarks);
		qstrRemarks.replace("\n", " ");
		auto line = new QTreeWidgetItem(QStringList() << _date << _PIC <<_ACtype << _ACreg << qstrRemarks);
		ui.entryList->addTopLevelItem(line);
	});

	auto floatFunc = [](float ftoString) -> QString {
		return QString("%1").arg(ftoString, 0, 'f', 2);
	};

	ui.totalHours->setText(floatFunc(entryController->GetTotalHours()));
	ui.TotalSingleEngine->setText(floatFunc(entryController->GetTotalSEHours()));
	ui.TotalMultiEngine->setText(floatFunc(entryController->GetTotalMEHours()));
	ui.TotalXCun->setText(floatFunc(entryController->GetTotalXCunHours()));

	// Single Engine
	ui.SEDayDual->setText(floatFunc(entryController->GetTotalSEDayDual()));
	ui.SEDayPIC->setText(floatFunc(entryController->GetTotalSEDayPIC()));
	ui.SENightDual->setText(floatFunc(entryController->GetTotalSENightDual()));
	ui.SENightPIC->setText(floatFunc(entryController->GetTotalSENightPIC()));

	// Multi-Engine
	ui.MEDayDual->setText(floatFunc(entryController->GetTotalMEDayDual()));
	ui.MEDayPIC->setText(floatFunc(entryController->GetTotalMEDayPIC()));
	ui.MEDaySIC->setText(floatFunc(entryController->GetTotalMEDaySIC()));
	ui.MENightDual->setText(floatFunc(entryController->GetTotalMENightDual()));
	ui.MENightPIC->setText(floatFunc(entryController->GetTotalMENightPIC()));
	ui.MENightSIC->setText(floatFunc(entryController->GetTotalMENightSIC()));

	// Cross Country
	ui.XCunDayDual->setText(floatFunc(entryController->GetTotalXCountryDayDual()));
	ui.XCunDayPIC->setText(floatFunc(entryController->GetTotalXCountryDayPIC()));
	ui.XCunNightDual->setText(floatFunc(entryController->GetTotalXCountryNightDual()));
	ui.XCunNightPIC->setText(floatFunc(entryController->GetTotalXCountryNightPIC()));

	// IFR
	ui.simTime->setText(floatFunc(entryController->GetTotalSim()));
	ui.hoodTime->setText(floatFunc(entryController->GetTotalHood()));
	ui.actualIFR->setText(floatFunc(entryController->GetTotalIFR()));
	ui.numAppr->setText(QString("%1").arg(entryController->GetTotalInstrumentApproach()));

	UpdateCalendar(true);

	statusBar()->showMessage(tr("Hours and Flight Duty updated!"));
}

/** Update the calendar
*	@Brief: Initialize or updates the calendar; Initializing calendar sets the widget as well
*	@param bInit - Determines whether it's an initialize call - default: false
*/
void PilotRecord::UpdateCalendar(bool bInit)
{
	auto entryController = BravoEngine::instance()->getEntry();

	if(bInit)
		ui.tableWidget->Initialize(Qt::Sunday); // TODO: change to userSet

	/// Get The Flight Duty Hours
	auto startDate = ui.tableWidget->getDayWidgetStart();
	auto endDate = ui.tableWidget->getDayWidgetEnd();

	
	 
	entryController->GetDutiesMinInfo<std::function<void(void*, QDate, QDateTime, QDateTime, bool)>>(
		{ (unsigned int)startDate.year(), (unsigned int)startDate.month(), (unsigned int)startDate.day() },
		{ (unsigned int)endDate.year(), (unsigned int)endDate.month(), (unsigned int)endDate.day() },
		(void*)this,
		&PilotRecord::engineCallbackCalendarUpdate);

	statusBar()->showMessage(tr("Flight Duty updated!"));
}


/* Show
*	This method is called for when it's time to show the main
*	dialog
*/
void PilotRecord::Show()
{
	if (!bAppRunning)
		return;
	togglePage(HIDE_ALL);

	show();
	statusBar()->showMessage(tr("Login to continue"));

	this->setDisabled(true);
	login->setDisabled(false);

	// Show Login dialogue
	// Update the hours list
	//if(login->exec() == QDialog::Accepted)
	login->exec();
	if(BravoEngine::instance()->State() == BravoEngine::STATE_LOGGEDIN)
	{
		UpdateHoursLog();
		this->setDisabled(false);
	}
}

void PilotRecord::ErrorMsgShow(std::string strTitle, std::string strContent, int errorType)
{
	PRMsgBox alert;
	alert.SetMessage(strTitle.c_str(), strContent.c_str());
	alert.SetType(PRMsgBox::intToType(errorType));
	alert.doModal();
}

void PilotRecord::RunErrorLoop()
{
	loopTimer->start(100);
}

void PilotRecord::engineCallbackCalendarUpdate(void * pObj, QDate date, QDateTime startTime, QDateTime endTime, bool bDayOff)
{
	PilotRecord *pPRobj = (PilotRecord *)pObj;
	pPRobj->ui.tableWidget->addDataCalendarItem(date, { startTime.time(), endTime.time(), bDayOff });
}

void PilotRecord::ErrorLoopUpdate()
{
	std::string strContent, strTitle;
	int ntype, nAction;
	if (BravoEngine::instance()->fetchError(strTitle, strContent, ntype, nAction))
	{
		ErrorMsgShow(strTitle, strContent, ntype);

		// TODO: REQUIRED ACTION!
		switch (ntype)
		{
		case Bravo::ERROR_RECONNECT: // Reconnect
		{
			BravoEngine::instance()->ReconnectClient();
			break;
		}
		case Bravo::ERROR_FATAL: // Exit on error
		{	
			QTimer::singleShot(0, qApp, SLOT(quit()));
			break;
		}
		}
	}
}

void PilotRecord::refreshDutyCalendar()
{
	UpdateHoursLog();
}

void PilotRecord::closeEvent(QCloseEvent * event)
{
	if (!bAppRunning)
	{
		event->accept();
		QTimer::singleShot(0, qApp, SLOT(quit())); // for some reason, parentWidget->close does not work'
		return;
	}

	PRMsgBox alert;
	if (alert.doModal(
		tr("Close Confirmation"),
		tr("Do you wish to close this app?")) == PRMsgBox::RESULT::OKAY)
	{
		event->accept();
		QTimer::singleShot(0, qApp, SLOT(quit())); // for some reason, parentWidget->close does not work
		return;
	}
	event->ignore();
}

/* lockLogout() - toolbar method
*/
void PilotRecord::lockLogoutToolbar()
{
	// Change to locked state
	BravoEngine::instance()->LogOut();
	BravoEngine::instance()->Initialize();
	Show();
}

void PilotRecord::togglePageToolbar()
{

	togglePage();
}

void PilotRecord::addLogToolbar()
{
	togglePage(LOGBOOK_PAGE);
	PilotRecordLogBook addEntry(this);
	addEntry.exec();
}

void PilotRecord::manageAccntsDlgToolbar()
{
	PilotRecordAccounts manageAccounts;
	manageAccounts.exec();
}

void PilotRecord::flightDutyToolbar()
{
	togglePage(PilotRecord::FLIGHT_DUTY);
}

void PilotRecord::calendarChanged(int nYear, int nMonth)
{
	QDate month(nYear, nMonth, 1);
	ui.monthLabel->setText(month.toString("MMMM yyyy"));
}

void PilotRecord::calendarSelectChange()
{
	if (ui.tableWidget->selectedItems().size() == 1)
		ui.detailsButton->setEnabled(true);
	else
		ui.detailsButton->setEnabled(false);
}

void PilotRecord::calendarNextMonth()
{
	ui.tableWidget->nextMonth();
	UpdateCalendar();
}

void PilotRecord::calendarPreviousMonth()
{
	ui.tableWidget->previousMonth();
	UpdateCalendar();
}

void PilotRecord::calendarDetails()
{
	calendarDateDbleClicked(ui.tableWidget->currentItem());
}

void PilotRecord::calendarDateDbleClicked(QTableWidgetItem * pItem)
{
	auto date = qvariant_cast<PRCalendarItem>(pItem->data(0));

	PilotRecordFlightDuty fltDutyDlg(this, &date);
	connect(&fltDutyDlg, SIGNAL(refreshSignal()), this, SLOT(refreshDutyCalendar()));
	fltDutyDlg.exec();
}

void PilotRecord::reviewItem(QTreeWidgetItem *item, int nRow)
{	
	item = ui.entryList->currentItem();
	int nrow = ui.entryList->indexOfTopLevelItem(item);
	if (nrow < 0)
		return;

	PilotRecordLogBook reviewEntry(this, PilotRecordLogBook::SUMMARIZE_MODE);

	MOUSE_WAIT
	reviewEntry.SummarizeData(nrow);
	MOUSE_RST

	reviewEntry.exec();
}

void PilotRecord::togglePage(PilotRecord::CURRENT_STATE cpage)
{
	if (cpage == NEXT_PAGE)
	{
		auto curActive = ui.stackedWidget->currentIndex();
		if (curActive == 0 || curActive == 1)
		{
			if (curPage == LOGBOOK_PAGE)
				cpage = SUMMARY_PAGE;
			else
				cpage = LOGBOOK_PAGE;
		}
		else
			cpage = curPage;
	}
	
	switch (cpage)
	{
	case PilotRecord::CURRENT_STATE::LOGBOOK_PAGE:
	{
		ui.stackedWidget->setCurrentIndex(0);

		ui.actionLogbook->setIcon(pilotBook);

		curPage = CURRENT_STATE::LOGBOOK_PAGE;
		statusBar()->showMessage(tr("Entry details"));
		break;
	}
	case PilotRecord::CURRENT_STATE::SUMMARY_PAGE:
	{
		ui.stackedWidget->setCurrentIndex(1);

		ui.actionLogbook->setIcon(lookBook);

		curPage = CURRENT_STATE::SUMMARY_PAGE;
		statusBar()->showMessage(tr("Summary Page"));
		break;
	}
	case PilotRecord::FLIGHT_DUTY:
	{
		ui.stackedWidget->setCurrentIndex(2);
		break;
	}
	case PilotRecord::CURRENT_STATE::HIDE_ALL:
	default:
	{
		ui.stackedWidget->setCurrentIndex(3);

		//logbookGroup->hide();
		//summaryGroup->hide();
		curPage = CURRENT_STATE::HIDE_ALL;
		break;
	}
	
	}
}

void PilotRecord::InitFlightDutyPage()
{
	QMenu *pMenu = new QMenu;
	pMenu->addAction("Go to Date...");
	pMenu->addAction("Add Flight Duty...");


	ui.toolButton->setMenu(pMenu);
}
