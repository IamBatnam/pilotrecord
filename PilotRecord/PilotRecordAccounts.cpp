#include "PilotRecordAccounts.h"
#include "Bravo.h"

PilotRecordAccounts::PilotRecordAccounts(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

PilotRecordAccounts::~PilotRecordAccounts()
{
}

void PilotRecordAccounts::addAccount(std::string strUser, int nAccType)
{
	if (nAccType > 5 || nAccType < 0)
		nAccType = 5;

	// Add the account to the view
	auto item = new QTreeWidgetItem(QStringList() << strUser.c_str() << userType[nAccType]);
	ui.accountList->addTopLevelItem(item);
}

void PilotRecordAccounts::showEvent(QShowEvent * event)
{
	BravoEngine::instance()->RetrieveAccounts(std::bind(&PilotRecordAccounts::addAccount, this, std::placeholders::_1, std::placeholders::_2));
	/*BravoEngine::instance()->RetrieveAccounts([this]() {
	
	});*/
}
