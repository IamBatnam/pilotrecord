/********************************************************************************
** Form generated from reading UI file 'PilotRecordEditAccount.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDEDITACCOUNT_H
#define UI_PILOTRECORDEDITACCOUNT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordEditAccount
{
public:

    void setupUi(QWidget *PilotRecordEditAccount)
    {
        if (PilotRecordEditAccount->objectName().isEmpty())
            PilotRecordEditAccount->setObjectName(QStringLiteral("PilotRecordEditAccount"));
        PilotRecordEditAccount->resize(400, 300);

        retranslateUi(PilotRecordEditAccount);

        QMetaObject::connectSlotsByName(PilotRecordEditAccount);
    } // setupUi

    void retranslateUi(QWidget *PilotRecordEditAccount)
    {
        PilotRecordEditAccount->setWindowTitle(QApplication::translate("PilotRecordEditAccount", "PilotRecordEditAccount", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordEditAccount: public Ui_PilotRecordEditAccount {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDEDITACCOUNT_H
