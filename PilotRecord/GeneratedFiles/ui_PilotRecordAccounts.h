/********************************************************************************
** Form generated from reading UI file 'PilotRecordAccounts.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDACCOUNTS_H
#define UI_PILOTRECORDACCOUNTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordAccounts
{
public:
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QTreeWidget *accountList;
    QVBoxLayout *verticalLayout_2;
    QPushButton *addAccount;
    QPushButton *remAccount;
    QPushButton *editAccount;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *PilotRecordAccounts)
    {
        if (PilotRecordAccounts->objectName().isEmpty())
            PilotRecordAccounts->setObjectName(QStringLiteral("PilotRecordAccounts"));
        PilotRecordAccounts->resize(477, 261);
        PilotRecordAccounts->setModal(true);
        verticalLayout = new QVBoxLayout(PilotRecordAccounts);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(PilotRecordAccounts);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        accountList = new QTreeWidget(groupBox);
        accountList->setObjectName(QStringLiteral("accountList"));
        accountList->setRootIsDecorated(false);

        horizontalLayout->addWidget(accountList);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        addAccount = new QPushButton(groupBox);
        addAccount->setObjectName(QStringLiteral("addAccount"));

        verticalLayout_2->addWidget(addAccount);

        remAccount = new QPushButton(groupBox);
        remAccount->setObjectName(QStringLiteral("remAccount"));
        remAccount->setEnabled(false);

        verticalLayout_2->addWidget(remAccount);

        editAccount = new QPushButton(groupBox);
        editAccount->setObjectName(QStringLiteral("editAccount"));
        editAccount->setEnabled(false);

        verticalLayout_2->addWidget(editAccount);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_2);


        verticalLayout->addWidget(groupBox);


        retranslateUi(PilotRecordAccounts);

        QMetaObject::connectSlotsByName(PilotRecordAccounts);
    } // setupUi

    void retranslateUi(QDialog *PilotRecordAccounts)
    {
        PilotRecordAccounts->setWindowTitle(QApplication::translate("PilotRecordAccounts", "Manage Accounts", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("PilotRecordAccounts", "Accounts", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = accountList->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("PilotRecordAccounts", "Account Type", Q_NULLPTR));
        ___qtreewidgetitem->setText(0, QApplication::translate("PilotRecordAccounts", "User Name", Q_NULLPTR));
        addAccount->setText(QApplication::translate("PilotRecordAccounts", "Add Account", Q_NULLPTR));
        remAccount->setText(QApplication::translate("PilotRecordAccounts", "Remove Account", Q_NULLPTR));
        editAccount->setText(QApplication::translate("PilotRecordAccounts", "Edit Account", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordAccounts: public Ui_PilotRecordAccounts {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDACCOUNTS_H
