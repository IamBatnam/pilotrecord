/********************************************************************************
** Form generated from reading UI file 'PilotRecordFlightDuty.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDFLIGHTDUTY_H
#define UI_PILOTRECORDFLIGHTDUTY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTimeEdit>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordFlightDuty
{
public:
    QGridLayout *gridLayout;
    QGroupBox *manageDutiesBox;
    QGridLayout *gridLayout_2;
    QTimeEdit *middayDutyOut;
    QLabel *label_2;
    QTimeEdit *middayDutyStart;
    QLabel *middayDutyStartLabel;
    QLabel *middayDutyOutLabel;
    QLabel *label_4;
    QTimeEdit *EndTime;
    QTimeEdit *InTime;
    QCheckBox *splitCheckBox;
    QLabel *dutyDayLabel;
    QLabel *label_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QLabel *label_5;
    QLabel *label_6;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label;
    QLabel *totalFlightLabel;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *deleteButton;
    QPushButton *addLegButton;
    QGraphicsView *graphicsView;
    QCheckBox *offCheck;
    QLabel *linkDOFGen;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *saveButton;
    QPushButton *closeButton;
    QLabel *dateLabel;

    void setupUi(QDialog *PilotRecordFlightDuty)
    {
        if (PilotRecordFlightDuty->objectName().isEmpty())
            PilotRecordFlightDuty->setObjectName(QStringLiteral("PilotRecordFlightDuty"));
        PilotRecordFlightDuty->resize(623, 449);
        PilotRecordFlightDuty->setMinimumSize(QSize(623, 449));
        PilotRecordFlightDuty->setMaximumSize(QSize(623, 449));
        gridLayout = new QGridLayout(PilotRecordFlightDuty);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        manageDutiesBox = new QGroupBox(PilotRecordFlightDuty);
        manageDutiesBox->setObjectName(QStringLiteral("manageDutiesBox"));
        gridLayout_2 = new QGridLayout(manageDutiesBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        middayDutyOut = new QTimeEdit(manageDutiesBox);
        middayDutyOut->setObjectName(QStringLiteral("middayDutyOut"));

        gridLayout_2->addWidget(middayDutyOut, 2, 1, 1, 1);

        label_2 = new QLabel(manageDutiesBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        middayDutyStart = new QTimeEdit(manageDutiesBox);
        middayDutyStart->setObjectName(QStringLiteral("middayDutyStart"));

        gridLayout_2->addWidget(middayDutyStart, 2, 2, 1, 1);

        middayDutyStartLabel = new QLabel(manageDutiesBox);
        middayDutyStartLabel->setObjectName(QStringLiteral("middayDutyStartLabel"));
        middayDutyStartLabel->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(middayDutyStartLabel, 1, 2, 1, 1);

        middayDutyOutLabel = new QLabel(manageDutiesBox);
        middayDutyOutLabel->setObjectName(QStringLiteral("middayDutyOutLabel"));
        middayDutyOutLabel->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(middayDutyOutLabel, 1, 1, 1, 1);

        label_4 = new QLabel(manageDutiesBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_4, 1, 3, 1, 1);

        EndTime = new QTimeEdit(manageDutiesBox);
        EndTime->setObjectName(QStringLiteral("EndTime"));
        EndTime->setMinimumTime(QTime(0, 0, 0));
        EndTime->setCalendarPopup(false);
        EndTime->setTime(QTime(12, 0, 0));

        gridLayout_2->addWidget(EndTime, 2, 3, 1, 1);

        InTime = new QTimeEdit(manageDutiesBox);
        InTime->setObjectName(QStringLiteral("InTime"));

        gridLayout_2->addWidget(InTime, 2, 0, 1, 1);

        splitCheckBox = new QCheckBox(manageDutiesBox);
        splitCheckBox->setObjectName(QStringLiteral("splitCheckBox"));

        gridLayout_2->addWidget(splitCheckBox, 0, 0, 1, 1);

        dutyDayLabel = new QLabel(manageDutiesBox);
        dutyDayLabel->setObjectName(QStringLiteral("dutyDayLabel"));

        gridLayout_2->addWidget(dutyDayLabel, 3, 3, 1, 1);

        label_3 = new QLabel(manageDutiesBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 3, 0, 1, 1);


        gridLayout->addWidget(manageDutiesBox, 7, 0, 1, 1);

        groupBox_2 = new QGroupBox(PilotRecordFlightDuty);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 1, 0, 1, 1);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_3->addWidget(label_6, 2, 0, 1, 1);


        gridLayout->addWidget(groupBox_2, 7, 1, 1, 1);

        tableWidget = new QTableWidget(PilotRecordFlightDuty);
        if (tableWidget->columnCount() < 8)
            tableWidget->setColumnCount(8);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QFont font;
        font.setPointSize(8);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setFont(font);
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tableWidget->sizePolicy().hasHeightForWidth());
        tableWidget->setSizePolicy(sizePolicy);
        tableWidget->setMinimumSize(QSize(605, 142));
        tableWidget->horizontalHeader()->setDefaultSectionSize(70);

        gridLayout->addWidget(tableWidget, 3, 0, 1, 2);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label = new QLabel(PilotRecordFlightDuty);
        label->setObjectName(QStringLiteral("label"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label->setFont(font1);

        horizontalLayout_2->addWidget(label);

        totalFlightLabel = new QLabel(PilotRecordFlightDuty);
        totalFlightLabel->setObjectName(QStringLiteral("totalFlightLabel"));
        totalFlightLabel->setFont(font1);

        horizontalLayout_2->addWidget(totalFlightLabel);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        deleteButton = new QPushButton(PilotRecordFlightDuty);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));
        deleteButton->setEnabled(false);

        horizontalLayout_2->addWidget(deleteButton);

        addLegButton = new QPushButton(PilotRecordFlightDuty);
        addLegButton->setObjectName(QStringLiteral("addLegButton"));

        horizontalLayout_2->addWidget(addLegButton);


        gridLayout->addLayout(horizontalLayout_2, 5, 0, 1, 2);

        graphicsView = new QGraphicsView(PilotRecordFlightDuty);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));
        sizePolicy.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy);
        graphicsView->setMaximumSize(QSize(16777215, 50));

        gridLayout->addWidget(graphicsView, 2, 0, 1, 2);

        offCheck = new QCheckBox(PilotRecordFlightDuty);
        offCheck->setObjectName(QStringLiteral("offCheck"));

        gridLayout->addWidget(offCheck, 8, 0, 1, 1);

        linkDOFGen = new QLabel(PilotRecordFlightDuty);
        linkDOFGen->setObjectName(QStringLiteral("linkDOFGen"));
        linkDOFGen->setTextFormat(Qt::RichText);
        linkDOFGen->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        linkDOFGen->setTextInteractionFlags(Qt::TextBrowserInteraction);

        gridLayout->addWidget(linkDOFGen, 8, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        saveButton = new QPushButton(PilotRecordFlightDuty);
        saveButton->setObjectName(QStringLiteral("saveButton"));

        horizontalLayout->addWidget(saveButton);

        closeButton = new QPushButton(PilotRecordFlightDuty);
        closeButton->setObjectName(QStringLiteral("closeButton"));

        horizontalLayout->addWidget(closeButton);


        gridLayout->addLayout(horizontalLayout, 9, 0, 1, 2);

        dateLabel = new QLabel(PilotRecordFlightDuty);
        dateLabel->setObjectName(QStringLiteral("dateLabel"));
        QFont font2;
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        dateLabel->setFont(font2);
        dateLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(dateLabel, 0, 0, 1, 2);

        QWidget::setTabOrder(tableWidget, InTime);
        QWidget::setTabOrder(InTime, middayDutyOut);
        QWidget::setTabOrder(middayDutyOut, middayDutyStart);
        QWidget::setTabOrder(middayDutyStart, EndTime);
        QWidget::setTabOrder(EndTime, addLegButton);
        QWidget::setTabOrder(addLegButton, closeButton);
        QWidget::setTabOrder(closeButton, graphicsView);
        QWidget::setTabOrder(graphicsView, deleteButton);
        QWidget::setTabOrder(deleteButton, offCheck);
        QWidget::setTabOrder(offCheck, splitCheckBox);
        QWidget::setTabOrder(splitCheckBox, saveButton);

        retranslateUi(PilotRecordFlightDuty);

        QMetaObject::connectSlotsByName(PilotRecordFlightDuty);
    } // setupUi

    void retranslateUi(QDialog *PilotRecordFlightDuty)
    {
        PilotRecordFlightDuty->setWindowTitle(QApplication::translate("PilotRecordFlightDuty", "PilotRecordFlightDuty", Q_NULLPTR));
        manageDutiesBox->setTitle(QApplication::translate("PilotRecordFlightDuty", "Manage Duties", Q_NULLPTR));
        middayDutyOut->setDisplayFormat(QApplication::translate("PilotRecordFlightDuty", "hh:mm AP", Q_NULLPTR));
        label_2->setText(QApplication::translate("PilotRecordFlightDuty", "Duty Start", Q_NULLPTR));
        middayDutyStart->setDisplayFormat(QApplication::translate("PilotRecordFlightDuty", "hh:mm AP", Q_NULLPTR));
        middayDutyStartLabel->setText(QApplication::translate("PilotRecordFlightDuty", "Duty Midday Start", Q_NULLPTR));
        middayDutyOutLabel->setText(QApplication::translate("PilotRecordFlightDuty", "Duty Midday End", Q_NULLPTR));
        label_4->setText(QApplication::translate("PilotRecordFlightDuty", "Duty End", Q_NULLPTR));
        EndTime->setDisplayFormat(QApplication::translate("PilotRecordFlightDuty", "hh:mm AP", Q_NULLPTR));
        InTime->setDisplayFormat(QApplication::translate("PilotRecordFlightDuty", "hh:mm AP", Q_NULLPTR));
        splitCheckBox->setText(QApplication::translate("PilotRecordFlightDuty", "Split Duty", Q_NULLPTR));
        dutyDayLabel->setText(QApplication::translate("PilotRecordFlightDuty", "0.0", Q_NULLPTR));
        label_3->setText(QApplication::translate("PilotRecordFlightDuty", "Total Duty this Day: ", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("PilotRecordFlightDuty", "Flight Duty Limits", Q_NULLPTR));
        label_5->setText(QApplication::translate("PilotRecordFlightDuty", "30-Day Limit", Q_NULLPTR));
        label_6->setText(QApplication::translate("PilotRecordFlightDuty", "7-Day limit", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("PilotRecordFlightDuty", "In", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("PilotRecordFlightDuty", "Out", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("PilotRecordFlightDuty", "Flt Time", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("PilotRecordFlightDuty", "Crew", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("PilotRecordFlightDuty", "Aircraft", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QApplication::translate("PilotRecordFlightDuty", "Flight Route", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QApplication::translate("PilotRecordFlightDuty", "Flight Number", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QApplication::translate("PilotRecordFlightDuty", "Mileage", Q_NULLPTR));
        label->setText(QApplication::translate("PilotRecordFlightDuty", "Total Flight Time:", Q_NULLPTR));
        totalFlightLabel->setText(QApplication::translate("PilotRecordFlightDuty", "0.0", Q_NULLPTR));
        deleteButton->setText(QApplication::translate("PilotRecordFlightDuty", "Delete Leg", Q_NULLPTR));
        addLegButton->setText(QApplication::translate("PilotRecordFlightDuty", "Add Leg", Q_NULLPTR));
        offCheck->setText(QApplication::translate("PilotRecordFlightDuty", "Off Today (Ctrl + F)", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        offCheck->setShortcut(QApplication::translate("PilotRecordFlightDuty", "Ctrl+F", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        linkDOFGen->setText(QApplication::translate("PilotRecordFlightDuty", "<a href=\"#\">Generate DOF</a>", Q_NULLPTR));
        saveButton->setText(QApplication::translate("PilotRecordFlightDuty", "Save", Q_NULLPTR));
        closeButton->setText(QApplication::translate("PilotRecordFlightDuty", "Close", Q_NULLPTR));
        dateLabel->setText(QApplication::translate("PilotRecordFlightDuty", "January 10, 2019", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordFlightDuty: public Ui_PilotRecordFlightDuty {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDFLIGHTDUTY_H
