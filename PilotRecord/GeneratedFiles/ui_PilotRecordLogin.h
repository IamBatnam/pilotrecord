/********************************************************************************
** Form generated from reading UI file 'PilotRecordLogin.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDLOGIN_H
#define UI_PILOTRECORDLOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordLogin
{
public:
    QGroupBox *groupBox;
    QLineEdit *userEdit;
    QLineEdit *passEdit;
    QLabel *label;
    QLabel *label_2;
    QPushButton *loginButton;
    QPushButton *closeButton;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *logolabel;
    QLabel *statuslabel;

    void setupUi(QDialog *PilotRecordLogin)
    {
        if (PilotRecordLogin->objectName().isEmpty())
            PilotRecordLogin->setObjectName(QStringLiteral("PilotRecordLogin"));
        PilotRecordLogin->setEnabled(true);
        PilotRecordLogin->resize(361, 206);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PilotRecordLogin->sizePolicy().hasHeightForWidth());
        PilotRecordLogin->setSizePolicy(sizePolicy);
        PilotRecordLogin->setMinimumSize(QSize(361, 206));
        PilotRecordLogin->setMaximumSize(QSize(361, 206));
        PilotRecordLogin->setContextMenuPolicy(Qt::DefaultContextMenu);
        QIcon icon;
        icon.addFile(QStringLiteral(":/PilotRecord/Key-10.png"), QSize(), QIcon::Normal, QIcon::Off);
        PilotRecordLogin->setWindowIcon(icon);
        PilotRecordLogin->setSizeGripEnabled(false);
        PilotRecordLogin->setModal(true);
        groupBox = new QGroupBox(PilotRecordLogin);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(50, 80, 261, 111));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        userEdit = new QLineEdit(groupBox);
        userEdit->setObjectName(QStringLiteral("userEdit"));
        userEdit->setGeometry(QRect(80, 20, 131, 20));
        passEdit = new QLineEdit(groupBox);
        passEdit->setObjectName(QStringLiteral("passEdit"));
        passEdit->setGeometry(QRect(80, 50, 131, 20));
        passEdit->setEchoMode(QLineEdit::Password);
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 47, 13));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 50, 47, 13));
        loginButton = new QPushButton(groupBox);
        loginButton->setObjectName(QStringLiteral("loginButton"));
        loginButton->setGeometry(QRect(100, 80, 75, 23));
        loginButton->setStyleSheet(QStringLiteral(""));
        loginButton->setIcon(icon);
        closeButton = new QPushButton(groupBox);
        closeButton->setObjectName(QStringLiteral("closeButton"));
        closeButton->setGeometry(QRect(180, 80, 75, 23));
        horizontalLayoutWidget = new QWidget(PilotRecordLogin);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 0, 363, 61));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        logolabel = new QLabel(horizontalLayoutWidget);
        logolabel->setObjectName(QStringLiteral("logolabel"));
        logolabel->setEnabled(true);
        logolabel->setPixmap(QPixmap(QString::fromUtf8(":/PilotRecord/LoginLogo.png")));

        horizontalLayout->addWidget(logolabel);

        statuslabel = new QLabel(PilotRecordLogin);
        statuslabel->setObjectName(QStringLiteral("statuslabel"));
        statuslabel->setGeometry(QRect(140, 70, 81, 16));
        QFont font;
        font.setPointSize(13);
        font.setBold(true);
        font.setWeight(75);
        statuslabel->setFont(font);

        retranslateUi(PilotRecordLogin);

        QMetaObject::connectSlotsByName(PilotRecordLogin);
    } // setupUi

    void retranslateUi(QDialog *PilotRecordLogin)
    {
        PilotRecordLogin->setWindowTitle(QApplication::translate("PilotRecordLogin", "PilotRecord Login", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("PilotRecordLogin", "Login", Q_NULLPTR));
        passEdit->setInputMask(QString());
        label->setText(QApplication::translate("PilotRecordLogin", "Username", Q_NULLPTR));
        label_2->setText(QApplication::translate("PilotRecordLogin", "Password", Q_NULLPTR));
        loginButton->setText(QApplication::translate("PilotRecordLogin", "Login", Q_NULLPTR));
        closeButton->setText(QApplication::translate("PilotRecordLogin", "Close", Q_NULLPTR));
        logolabel->setText(QString());
        statuslabel->setText(QApplication::translate("PilotRecordLogin", "OFFLINE", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordLogin: public Ui_PilotRecordLogin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDLOGIN_H
