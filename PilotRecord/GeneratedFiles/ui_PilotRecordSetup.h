/********************************************************************************
** Form generated from reading UI file 'PilotRecordSetup.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDSETUP_H
#define UI_PILOTRECORDSETUP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordSetup
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_2;
    QLineEdit *UserName;
    QLineEdit *Password_2;
    QLineEdit *Password;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *createDatabase;
    QPushButton *closeButton;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *PilotRecordSetup)
    {
        if (PilotRecordSetup->objectName().isEmpty())
            PilotRecordSetup->setObjectName(QStringLiteral("PilotRecordSetup"));
        PilotRecordSetup->resize(363, 233);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PilotRecordSetup->sizePolicy().hasHeightForWidth());
        PilotRecordSetup->setSizePolicy(sizePolicy);
        PilotRecordSetup->setMinimumSize(QSize(363, 233));
        PilotRecordSetup->setMaximumSize(QSize(363, 233));
        QIcon icon;
        icon.addFile(QStringLiteral(":/PilotRecord/pilot.png"), QSize(), QIcon::Normal, QIcon::Off);
        PilotRecordSetup->setWindowIcon(icon);
        PilotRecordSetup->setModal(true);
        verticalLayout = new QVBoxLayout(PilotRecordSetup);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(PilotRecordSetup);
        label->setObjectName(QStringLiteral("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setMinimumSize(QSize(361, 59));
        label->setMaximumSize(QSize(361, 59));
        label->setPixmap(QPixmap(QString::fromUtf8(":/PilotRecord/LoginLogo.png")));
        label->setScaledContents(false);
        label->setWordWrap(false);

        horizontalLayout->addWidget(label);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(9, 9, 9, 9);
        groupBox = new QGroupBox(PilotRecordSetup);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 2, 0, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 1, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        UserName = new QLineEdit(groupBox);
        UserName->setObjectName(QStringLiteral("UserName"));

        gridLayout_2->addWidget(UserName, 0, 1, 1, 1);

        Password_2 = new QLineEdit(groupBox);
        Password_2->setObjectName(QStringLiteral("Password_2"));
        Password_2->setEchoMode(QLineEdit::Password);

        gridLayout_2->addWidget(Password_2, 2, 1, 1, 1);

        Password = new QLineEdit(groupBox);
        Password->setObjectName(QStringLiteral("Password"));
        Password->setEchoMode(QLineEdit::Password);

        gridLayout_2->addWidget(Password, 1, 1, 1, 1);


        gridLayout->addWidget(groupBox, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_7);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_6);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_5);

        createDatabase = new QPushButton(PilotRecordSetup);
        createDatabase->setObjectName(QStringLiteral("createDatabase"));
        createDatabase->setIconSize(QSize(16, 16));

        horizontalLayout_5->addWidget(createDatabase);

        closeButton = new QPushButton(PilotRecordSetup);
        closeButton->setObjectName(QStringLiteral("closeButton"));

        horizontalLayout_5->addWidget(closeButton);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);


        verticalLayout_2->addLayout(horizontalLayout_5);

        verticalSpacer = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        verticalLayout->addLayout(verticalLayout_2);

        QWidget::setTabOrder(UserName, Password);
        QWidget::setTabOrder(Password, Password_2);
        QWidget::setTabOrder(Password_2, createDatabase);
        QWidget::setTabOrder(createDatabase, closeButton);

        retranslateUi(PilotRecordSetup);

        QMetaObject::connectSlotsByName(PilotRecordSetup);
    } // setupUi

    void retranslateUi(QDialog *PilotRecordSetup)
    {
        PilotRecordSetup->setWindowTitle(QApplication::translate("PilotRecordSetup", "PilotRecord Setup", Q_NULLPTR));
        label->setText(QString());
        groupBox->setTitle(QApplication::translate("PilotRecordSetup", "Setup", Q_NULLPTR));
        label_4->setText(QApplication::translate("PilotRecordSetup", "Retype Password", Q_NULLPTR));
        label_3->setText(QApplication::translate("PilotRecordSetup", "Password", Q_NULLPTR));
        label_2->setText(QApplication::translate("PilotRecordSetup", "Username", Q_NULLPTR));
        createDatabase->setText(QApplication::translate("PilotRecordSetup", "Create Database", Q_NULLPTR));
        closeButton->setText(QApplication::translate("PilotRecordSetup", "Close", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordSetup: public Ui_PilotRecordSetup {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDSETUP_H
