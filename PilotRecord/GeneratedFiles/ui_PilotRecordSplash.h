/********************************************************************************
** Form generated from reading UI file 'PilotRecordSplash.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDSPLASH_H
#define UI_PILOTRECORDSPLASH_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordSplash
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *buildNumber;

    void setupUi(QDialog *PilotRecordSplash)
    {
        if (PilotRecordSplash->objectName().isEmpty())
            PilotRecordSplash->setObjectName(QStringLiteral("PilotRecordSplash"));
        PilotRecordSplash->resize(394, 240);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PilotRecordSplash->sizePolicy().hasHeightForWidth());
        PilotRecordSplash->setSizePolicy(sizePolicy);
        PilotRecordSplash->setMinimumSize(QSize(394, 240));
        PilotRecordSplash->setMaximumSize(QSize(394, 240));
        label = new QLabel(PilotRecordSplash);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 394, 240));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setMinimumSize(QSize(394, 240));
        label->setMaximumSize(QSize(394, 240));
        label->setPixmap(QPixmap(QString::fromUtf8(":/PilotRecord/SplashLogo.png")));
        label->setScaledContents(true);
        label_2 = new QLabel(PilotRecordSplash);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(0, 220, 171, 21));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_2->setFont(font);
        label_2->setLayoutDirection(Qt::LeftToRight);
        label_2->setAlignment(Qt::AlignCenter);
        label_2->setMargin(0);
        label_2->setIndent(1);
        buildNumber = new QLabel(PilotRecordSplash);
        buildNumber->setObjectName(QStringLiteral("buildNumber"));
        buildNumber->setGeometry(QRect(256, 220, 131, 20));
        buildNumber->setScaledContents(false);
        buildNumber->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        buildNumber->setWordWrap(true);

        retranslateUi(PilotRecordSplash);

        QMetaObject::connectSlotsByName(PilotRecordSplash);
    } // setupUi

    void retranslateUi(QDialog *PilotRecordSplash)
    {
        PilotRecordSplash->setWindowTitle(QApplication::translate("PilotRecordSplash", "PilotRecordSplash", Q_NULLPTR));
        label->setText(QString());
        label_2->setText(QApplication::translate("PilotRecordSplash", "Copyright (c) - AeWorks 2018", Q_NULLPTR));
        buildNumber->setText(QApplication::translate("PilotRecordSplash", "Build 0.0.0.0", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordSplash: public Ui_PilotRecordSplash {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDSPLASH_H
