/********************************************************************************
** Form generated from reading UI file 'PRWaitBox.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRWAITBOX_H
#define UI_PRWAITBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PRWaitBox
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QLabel *statusLabel;
    QProgressBar *progressBar;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *PRWaitBox)
    {
        if (PRWaitBox->objectName().isEmpty())
            PRWaitBox->setObjectName(QStringLiteral("PRWaitBox"));
        PRWaitBox->resize(400, 100);
        PRWaitBox->setMinimumSize(QSize(400, 100));
        PRWaitBox->setMaximumSize(QSize(400, 100));
        verticalLayout = new QVBoxLayout(PRWaitBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        statusLabel = new QLabel(PRWaitBox);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        statusLabel->setFont(font);
        statusLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(statusLabel);

        progressBar = new QProgressBar(PRWaitBox);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setMaximum(0);
        progressBar->setValue(-1);
        progressBar->setTextVisible(false);

        verticalLayout->addWidget(progressBar);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(PRWaitBox);

        QMetaObject::connectSlotsByName(PRWaitBox);
    } // setupUi

    void retranslateUi(QDialog *PRWaitBox)
    {
        PRWaitBox->setWindowTitle(QApplication::translate("PRWaitBox", "PRWaitBox", Q_NULLPTR));
        statusLabel->setText(QApplication::translate("PRWaitBox", "TextLabel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PRWaitBox: public Ui_PRWaitBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRWAITBOX_H
