/********************************************************************************
** Form generated from reading UI file 'PRMsgBox.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRMSGBOX_H
#define UI_PRMSGBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PRMsgBox
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout_4;
    QFrame *horizontalFrame;
    QHBoxLayout *horizontalLayout_2;
    QLabel *logoLabel;
    QVBoxLayout *verticalLayout;
    QLabel *titleText;
    QLabel *msgText;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *OkButton;
    QPushButton *CancelButton;

    void setupUi(QDialog *PRMsgBox)
    {
        if (PRMsgBox->objectName().isEmpty())
            PRMsgBox->setObjectName(QStringLiteral("PRMsgBox"));
        PRMsgBox->resize(407, 136);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PRMsgBox->sizePolicy().hasHeightForWidth());
        PRMsgBox->setSizePolicy(sizePolicy);
        PRMsgBox->setMinimumSize(QSize(407, 100));
        PRMsgBox->setModal(true);
        verticalLayout_2 = new QVBoxLayout(PRMsgBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, -1);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalFrame = new QFrame(PRMsgBox);
        horizontalFrame->setObjectName(QStringLiteral("horizontalFrame"));
        horizontalFrame->setStyleSheet(QStringLiteral("Background: #fff;"));
        horizontalLayout_2 = new QHBoxLayout(horizontalFrame);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(15, 9, 9, -1);
        logoLabel = new QLabel(horizontalFrame);
        logoLabel->setObjectName(QStringLiteral("logoLabel"));
        logoLabel->setMinimumSize(QSize(75, 75));
        logoLabel->setMaximumSize(QSize(75, 75));
        logoLabel->setStyleSheet(QStringLiteral(""));
        logoLabel->setPixmap(QPixmap(QString::fromUtf8(":/PilotRecord/Alert.png")));
        logoLabel->setScaledContents(true);

        horizontalLayout_2->addWidget(logoLabel);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        titleText = new QLabel(horizontalFrame);
        titleText->setObjectName(QStringLiteral("titleText"));
        QFont font;
        font.setPointSize(13);
        font.setBold(true);
        font.setWeight(75);
        titleText->setFont(font);

        verticalLayout->addWidget(titleText);

        msgText = new QLabel(horizontalFrame);
        msgText->setObjectName(QStringLiteral("msgText"));
        QFont font1;
        font1.setPointSize(9);
        msgText->setFont(font1);
        msgText->setWordWrap(true);

        verticalLayout->addWidget(msgText);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        horizontalLayout_2->addLayout(verticalLayout);


        verticalLayout_4->addWidget(horizontalFrame);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(10, -1, 10, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        OkButton = new QPushButton(PRMsgBox);
        OkButton->setObjectName(QStringLiteral("OkButton"));

        horizontalLayout_3->addWidget(OkButton);

        CancelButton = new QPushButton(PRMsgBox);
        CancelButton->setObjectName(QStringLiteral("CancelButton"));

        horizontalLayout_3->addWidget(CancelButton);


        verticalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_2->addLayout(verticalLayout_4);


        retranslateUi(PRMsgBox);
        QObject::connect(OkButton, SIGNAL(clicked()), PRMsgBox, SLOT(accept()));
        QObject::connect(CancelButton, SIGNAL(clicked()), PRMsgBox, SLOT(reject()));

        QMetaObject::connectSlotsByName(PRMsgBox);
    } // setupUi

    void retranslateUi(QDialog *PRMsgBox)
    {
        PRMsgBox->setWindowTitle(QApplication::translate("PRMsgBox", "PRMsgBox", Q_NULLPTR));
        logoLabel->setText(QString());
        titleText->setText(QApplication::translate("PRMsgBox", "TextLabel", Q_NULLPTR));
        msgText->setText(QApplication::translate("PRMsgBox", "Text", Q_NULLPTR));
        OkButton->setText(QApplication::translate("PRMsgBox", "Okay", Q_NULLPTR));
        CancelButton->setText(QApplication::translate("PRMsgBox", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PRMsgBox: public Ui_PRMsgBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRMSGBOX_H
