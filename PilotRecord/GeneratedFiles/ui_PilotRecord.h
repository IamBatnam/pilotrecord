/********************************************************************************
** Form generated from reading UI file 'PilotRecord.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORD_H
#define UI_PILOTRECORD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "PRCalendarWidget.h"

QT_BEGIN_NAMESPACE

class Ui_PilotRecordClass
{
public:
    QAction *actionLogbook;
    QAction *actionManage_User;
    QAction *actionSettings;
    QAction *actionLock_Logout;
    QAction *actionAddLog;
    QAction *actionFlight_Duty;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_3;
    QStackedWidget *stackedWidget;
    QWidget *LogBookWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_47;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *horizontalSpacer_6;
    QGridLayout *gridLayout_15;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *horizontalSpacer_9;
    QFormLayout *formLayout_2;
    QSpinBox *spinBox_3;
    QLabel *label_46;
    QPushButton *reviewButton;
    QTreeWidget *entryList;
    QWidget *SummaryWidget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_12;
    QLabel *totalHours;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_12;
    QLabel *label_6;
    QLabel *SENightDual;
    QLabel *label_7;
    QLabel *SENightPIC;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *SEDayDual;
    QLabel *SEDayPIC;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_44;
    QLabel *TotalSingleEngine;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_6;
    QLabel *MEDayPIC;
    QLabel *label_17;
    QLabel *label_11;
    QLabel *MEDayDual;
    QLabel *label_19;
    QLabel *MEDaySIC;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_7;
    QLabel *MENightPIC;
    QLabel *label_13;
    QLabel *MENightDual;
    QLabel *label_16;
    QLabel *label_21;
    QLabel *MENightSIC;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_42;
    QLabel *TotalMultiEngine;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_8;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_9;
    QLabel *label_24;
    QLabel *XCunDayPIC;
    QLabel *XCunDayDual;
    QLabel *label_23;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_40;
    QLabel *TotalXCun;
    QGroupBox *groupBox_10;
    QGridLayout *gridLayout_10;
    QLabel *XCunNightDual;
    QLabel *label_27;
    QLabel *label_28;
    QLabel *XCunNightPIC;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_11;
    QLabel *actualIFR;
    QLabel *label_31;
    QLabel *label_32;
    QLabel *label_33;
    QLabel *simTime;
    QLabel *label_34;
    QLabel *hoodTime;
    QLabel *numAppr;
    QWidget *FlightDutyPage;
    QGridLayout *gridLayout;
    PRCalendarWidget *tableWidget;
    QLabel *monthLabel;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *prevMonth;
    QPushButton *nextMonth;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *detailsButton;
    QToolButton *toolButton;
    QWidget *page_2;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *PilotRecordClass)
    {
        if (PilotRecordClass->objectName().isEmpty())
            PilotRecordClass->setObjectName(QStringLiteral("PilotRecordClass"));
        PilotRecordClass->resize(623, 607);
        PilotRecordClass->setMinimumSize(QSize(623, 607));
        PilotRecordClass->setContextMenuPolicy(Qt::DefaultContextMenu);
        QIcon icon;
        icon.addFile(QStringLiteral(":/PilotRecord/pilot.png"), QSize(), QIcon::Normal, QIcon::Off);
        PilotRecordClass->setWindowIcon(icon);
        PilotRecordClass->setStyleSheet(QLatin1String("QStatusBar {\n"
"border: rgb(167, 167, 167) solid 2px;\n"
"}\n"
""));
        PilotRecordClass->setDocumentMode(false);
        actionLogbook = new QAction(PilotRecordClass);
        actionLogbook->setObjectName(QStringLiteral("actionLogbook"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/PilotRecord/Look.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLogbook->setIcon(icon1);
        actionManage_User = new QAction(PilotRecordClass);
        actionManage_User->setObjectName(QStringLiteral("actionManage_User"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/PilotRecord/login-icon-png-27.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionManage_User->setIcon(icon2);
        actionSettings = new QAction(PilotRecordClass);
        actionSettings->setObjectName(QStringLiteral("actionSettings"));
        actionSettings->setEnabled(false);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/PilotRecord/Settings.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSettings->setIcon(icon3);
        actionLock_Logout = new QAction(PilotRecordClass);
        actionLock_Logout->setObjectName(QStringLiteral("actionLock_Logout"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/PilotRecord/Lock.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLock_Logout->setIcon(icon4);
        actionAddLog = new QAction(PilotRecordClass);
        actionAddLog->setObjectName(QStringLiteral("actionAddLog"));
        actionAddLog->setEnabled(true);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/PilotRecord/AddEntry.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAddLog->setIcon(icon5);
        actionFlight_Duty = new QAction(PilotRecordClass);
        actionFlight_Duty->setObjectName(QStringLiteral("actionFlight_Duty"));
        centralWidget = new QWidget(PilotRecordClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setEnabled(true);
        verticalLayout_3 = new QVBoxLayout(centralWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(1, 1, 1, 1);
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        LogBookWidget = new QWidget();
        LogBookWidget->setObjectName(QStringLiteral("LogBookWidget"));
        verticalLayout = new QVBoxLayout(LogBookWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_47 = new QLabel(LogBookWidget);
        label_47->setObjectName(QStringLiteral("label_47"));
        label_47->setMinimumSize(QSize(0, 0));
        label_47->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_2->addWidget(label_47);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);


        verticalLayout->addLayout(horizontalLayout_2);

        gridLayout_15 = new QGridLayout();
        gridLayout_15->setSpacing(6);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        horizontalSpacer_7 = new QSpacerItem(84, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_15->addItem(horizontalSpacer_7, 0, 2, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(82, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_15->addItem(horizontalSpacer_8, 0, 3, 1, 1);

        horizontalSpacer_9 = new QSpacerItem(79, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_15->addItem(horizontalSpacer_9, 0, 4, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        spinBox_3 = new QSpinBox(LogBookWidget);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setMinimum(1900);
        spinBox_3->setMaximum(2100);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, spinBox_3);

        label_46 = new QLabel(LogBookWidget);
        label_46->setObjectName(QStringLiteral("label_46"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_46);


        gridLayout_15->addLayout(formLayout_2, 0, 1, 1, 1);

        reviewButton = new QPushButton(LogBookWidget);
        reviewButton->setObjectName(QStringLiteral("reviewButton"));
        reviewButton->setMaximumSize(QSize(70, 20));

        gridLayout_15->addWidget(reviewButton, 0, 5, 1, 1);


        verticalLayout->addLayout(gridLayout_15);

        entryList = new QTreeWidget(LogBookWidget);
        entryList->setObjectName(QStringLiteral("entryList"));
        entryList->setMinimumSize(QSize(587, 418));
        entryList->setIndentation(0);
        entryList->setUniformRowHeights(true);
        entryList->setItemsExpandable(false);
        entryList->setWordWrap(false);
        entryList->setExpandsOnDoubleClick(false);
        entryList->header()->setVisible(true);
        entryList->header()->setDefaultSectionSize(100);
        entryList->header()->setHighlightSections(false);
        entryList->header()->setMinimumSectionSize(30);

        verticalLayout->addWidget(entryList);

        stackedWidget->addWidget(LogBookWidget);
        SummaryWidget = new QWidget();
        SummaryWidget->setObjectName(QStringLiteral("SummaryWidget"));
        verticalLayout_2 = new QVBoxLayout(SummaryWidget);
        verticalLayout_2->setSpacing(5);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(1, 1, 1, 1);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_12 = new QLabel(SummaryWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setMinimumSize(QSize(150, 0));
        QFont font;
        font.setPointSize(14);
        label_12->setFont(font);

        horizontalLayout->addWidget(label_12);

        totalHours = new QLabel(SummaryWidget);
        totalHours->setObjectName(QStringLiteral("totalHours"));
        QFont font1;
        font1.setPointSize(16);
        totalHours->setFont(font1);

        horizontalLayout->addWidget(totalHours);

        horizontalSpacer = new QSpacerItem(100, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout);

        groupBox = new QGroupBox(SummaryWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_12 = new QGridLayout(groupBox_3);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_12->addWidget(label_6, 0, 0, 1, 1);

        SENightDual = new QLabel(groupBox_3);
        SENightDual->setObjectName(QStringLiteral("SENightDual"));

        gridLayout_12->addWidget(SENightDual, 0, 1, 1, 1);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_12->addWidget(label_7, 1, 0, 1, 1);

        SENightPIC = new QLabel(groupBox_3);
        SENightPIC->setObjectName(QStringLiteral("SENightPIC"));

        gridLayout_12->addWidget(SENightPIC, 1, 1, 1, 1);


        gridLayout_2->addWidget(groupBox_3, 1, 1, 1, 1);

        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 1, 0, 1, 1);

        SEDayDual = new QLabel(groupBox_2);
        SEDayDual->setObjectName(QStringLiteral("SEDayDual"));

        gridLayout_3->addWidget(SEDayDual, 0, 1, 1, 1);

        SEDayPIC = new QLabel(groupBox_2);
        SEDayPIC->setObjectName(QStringLiteral("SEDayPIC"));

        gridLayout_3->addWidget(SEDayPIC, 1, 1, 1, 1);


        gridLayout_2->addWidget(groupBox_2, 1, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_44 = new QLabel(groupBox);
        label_44->setObjectName(QStringLiteral("label_44"));
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        label_44->setFont(font2);

        horizontalLayout_5->addWidget(label_44);

        TotalSingleEngine = new QLabel(groupBox);
        TotalSingleEngine->setObjectName(QStringLiteral("TotalSingleEngine"));
        TotalSingleEngine->setFont(font2);

        horizontalLayout_5->addWidget(TotalSingleEngine);


        gridLayout_2->addLayout(horizontalLayout_5, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox);

        groupBox_4 = new QGroupBox(SummaryWidget);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        groupBox_5 = new QGroupBox(groupBox_4);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        gridLayout_6 = new QGridLayout(groupBox_5);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        MEDayPIC = new QLabel(groupBox_5);
        MEDayPIC->setObjectName(QStringLiteral("MEDayPIC"));

        gridLayout_6->addWidget(MEDayPIC, 1, 1, 1, 1);

        label_17 = new QLabel(groupBox_5);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_6->addWidget(label_17, 0, 0, 1, 1);

        label_11 = new QLabel(groupBox_5);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_6->addWidget(label_11, 1, 0, 1, 1);

        MEDayDual = new QLabel(groupBox_5);
        MEDayDual->setObjectName(QStringLiteral("MEDayDual"));

        gridLayout_6->addWidget(MEDayDual, 0, 1, 1, 1);

        label_19 = new QLabel(groupBox_5);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_6->addWidget(label_19, 2, 0, 1, 1);

        MEDaySIC = new QLabel(groupBox_5);
        MEDaySIC->setObjectName(QStringLiteral("MEDaySIC"));

        gridLayout_6->addWidget(MEDaySIC, 2, 1, 1, 1);


        gridLayout_5->addWidget(groupBox_5, 1, 0, 1, 1);

        groupBox_6 = new QGroupBox(groupBox_4);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        gridLayout_7 = new QGridLayout(groupBox_6);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        MENightPIC = new QLabel(groupBox_6);
        MENightPIC->setObjectName(QStringLiteral("MENightPIC"));

        gridLayout_7->addWidget(MENightPIC, 1, 1, 1, 1);

        label_13 = new QLabel(groupBox_6);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_7->addWidget(label_13, 0, 0, 1, 1);

        MENightDual = new QLabel(groupBox_6);
        MENightDual->setObjectName(QStringLiteral("MENightDual"));

        gridLayout_7->addWidget(MENightDual, 0, 1, 1, 1);

        label_16 = new QLabel(groupBox_6);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_7->addWidget(label_16, 1, 0, 1, 1);

        label_21 = new QLabel(groupBox_6);
        label_21->setObjectName(QStringLiteral("label_21"));

        gridLayout_7->addWidget(label_21, 2, 0, 1, 1);

        MENightSIC = new QLabel(groupBox_6);
        MENightSIC->setObjectName(QStringLiteral("MENightSIC"));

        gridLayout_7->addWidget(MENightSIC, 2, 1, 1, 1);


        gridLayout_5->addWidget(groupBox_6, 1, 1, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_42 = new QLabel(groupBox_4);
        label_42->setObjectName(QStringLiteral("label_42"));
        label_42->setFont(font2);

        horizontalLayout_4->addWidget(label_42);

        TotalMultiEngine = new QLabel(groupBox_4);
        TotalMultiEngine->setObjectName(QStringLiteral("TotalMultiEngine"));
        TotalMultiEngine->setFont(font2);

        horizontalLayout_4->addWidget(TotalMultiEngine);


        gridLayout_5->addLayout(horizontalLayout_4, 0, 0, 1, 1);


        verticalLayout_2->addWidget(groupBox_4);

        groupBox_7 = new QGroupBox(SummaryWidget);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        gridLayout_8 = new QGridLayout(groupBox_7);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        groupBox_9 = new QGroupBox(groupBox_7);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        gridLayout_9 = new QGridLayout(groupBox_9);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        label_24 = new QLabel(groupBox_9);
        label_24->setObjectName(QStringLiteral("label_24"));

        gridLayout_9->addWidget(label_24, 1, 0, 1, 1);

        XCunDayPIC = new QLabel(groupBox_9);
        XCunDayPIC->setObjectName(QStringLiteral("XCunDayPIC"));

        gridLayout_9->addWidget(XCunDayPIC, 1, 1, 1, 1);

        XCunDayDual = new QLabel(groupBox_9);
        XCunDayDual->setObjectName(QStringLiteral("XCunDayDual"));

        gridLayout_9->addWidget(XCunDayDual, 0, 1, 1, 1);

        label_23 = new QLabel(groupBox_9);
        label_23->setObjectName(QStringLiteral("label_23"));

        gridLayout_9->addWidget(label_23, 0, 0, 1, 1);


        gridLayout_8->addWidget(groupBox_9, 2, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_40 = new QLabel(groupBox_7);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setFont(font2);

        horizontalLayout_3->addWidget(label_40);

        TotalXCun = new QLabel(groupBox_7);
        TotalXCun->setObjectName(QStringLiteral("TotalXCun"));
        TotalXCun->setFont(font2);

        horizontalLayout_3->addWidget(TotalXCun);


        gridLayout_8->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        groupBox_10 = new QGroupBox(groupBox_7);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        gridLayout_10 = new QGridLayout(groupBox_10);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        XCunNightDual = new QLabel(groupBox_10);
        XCunNightDual->setObjectName(QStringLiteral("XCunNightDual"));

        gridLayout_10->addWidget(XCunNightDual, 0, 1, 1, 1);

        label_27 = new QLabel(groupBox_10);
        label_27->setObjectName(QStringLiteral("label_27"));

        gridLayout_10->addWidget(label_27, 0, 0, 1, 1);

        label_28 = new QLabel(groupBox_10);
        label_28->setObjectName(QStringLiteral("label_28"));

        gridLayout_10->addWidget(label_28, 1, 0, 1, 1);

        XCunNightPIC = new QLabel(groupBox_10);
        XCunNightPIC->setObjectName(QStringLiteral("XCunNightPIC"));

        gridLayout_10->addWidget(XCunNightPIC, 1, 1, 1, 1);


        gridLayout_8->addWidget(groupBox_10, 2, 1, 1, 1);


        verticalLayout_2->addWidget(groupBox_7);

        groupBox_8 = new QGroupBox(SummaryWidget);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        gridLayout_11 = new QGridLayout(groupBox_8);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        actualIFR = new QLabel(groupBox_8);
        actualIFR->setObjectName(QStringLiteral("actualIFR"));

        gridLayout_11->addWidget(actualIFR, 1, 2, 1, 1);

        label_31 = new QLabel(groupBox_8);
        label_31->setObjectName(QStringLiteral("label_31"));

        gridLayout_11->addWidget(label_31, 0, 0, 1, 1);

        label_32 = new QLabel(groupBox_8);
        label_32->setObjectName(QStringLiteral("label_32"));

        gridLayout_11->addWidget(label_32, 0, 1, 1, 1);

        label_33 = new QLabel(groupBox_8);
        label_33->setObjectName(QStringLiteral("label_33"));

        gridLayout_11->addWidget(label_33, 0, 2, 1, 1);

        simTime = new QLabel(groupBox_8);
        simTime->setObjectName(QStringLiteral("simTime"));

        gridLayout_11->addWidget(simTime, 1, 0, 1, 1);

        label_34 = new QLabel(groupBox_8);
        label_34->setObjectName(QStringLiteral("label_34"));

        gridLayout_11->addWidget(label_34, 0, 3, 1, 1);

        hoodTime = new QLabel(groupBox_8);
        hoodTime->setObjectName(QStringLiteral("hoodTime"));

        gridLayout_11->addWidget(hoodTime, 1, 1, 1, 1);

        numAppr = new QLabel(groupBox_8);
        numAppr->setObjectName(QStringLiteral("numAppr"));

        gridLayout_11->addWidget(numAppr, 1, 3, 1, 1);


        verticalLayout_2->addWidget(groupBox_8);

        stackedWidget->addWidget(SummaryWidget);
        FlightDutyPage = new QWidget();
        FlightDutyPage->setObjectName(QStringLiteral("FlightDutyPage"));
        gridLayout = new QGridLayout(FlightDutyPage);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tableWidget = new PRCalendarWidget(FlightDutyPage);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));

        gridLayout->addWidget(tableWidget, 1, 1, 1, 5);

        monthLabel = new QLabel(FlightDutyPage);
        monthLabel->setObjectName(QStringLiteral("monthLabel"));
        QFont font3;
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setWeight(75);
        monthLabel->setFont(font3);
        monthLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(monthLabel, 0, 3, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 0, 4, 1, 1);

        prevMonth = new QPushButton(FlightDutyPage);
        prevMonth->setObjectName(QStringLiteral("prevMonth"));

        gridLayout->addWidget(prevMonth, 0, 1, 1, 1);

        nextMonth = new QPushButton(FlightDutyPage);
        nextMonth->setObjectName(QStringLiteral("nextMonth"));

        gridLayout->addWidget(nextMonth, 0, 5, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        detailsButton = new QPushButton(FlightDutyPage);
        detailsButton->setObjectName(QStringLiteral("detailsButton"));
        detailsButton->setEnabled(false);

        gridLayout->addWidget(detailsButton, 2, 5, 1, 1);

        toolButton = new QToolButton(FlightDutyPage);
        toolButton->setObjectName(QStringLiteral("toolButton"));
        toolButton->setPopupMode(QToolButton::InstantPopup);
        toolButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolButton->setArrowType(Qt::NoArrow);

        gridLayout->addWidget(toolButton, 2, 1, 1, 1);

        stackedWidget->addWidget(FlightDutyPage);
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        stackedWidget->addWidget(page_2);

        verticalLayout_3->addWidget(stackedWidget);

        PilotRecordClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(PilotRecordClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setStyleSheet(QStringLiteral(""));
        PilotRecordClass->setStatusBar(statusBar);
        toolBar = new QToolBar(PilotRecordClass);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        toolBar->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(toolBar->sizePolicy().hasHeightForWidth());
        toolBar->setSizePolicy(sizePolicy);
        toolBar->setMinimumSize(QSize(0, 0));
        toolBar->setMaximumSize(QSize(16777215, 16777215));
        toolBar->setMovable(false);
        toolBar->setOrientation(Qt::Horizontal);
        toolBar->setIconSize(QSize(54, 54));
        toolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
        toolBar->setFloatable(false);
        PilotRecordClass->addToolBar(Qt::TopToolBarArea, toolBar);

        toolBar->addAction(actionLogbook);
        toolBar->addAction(actionAddLog);
        toolBar->addSeparator();
        toolBar->addAction(actionFlight_Duty);
        toolBar->addSeparator();
        toolBar->addAction(actionManage_User);
        toolBar->addAction(actionSettings);
        toolBar->addSeparator();
        toolBar->addAction(actionLock_Logout);

        retranslateUi(PilotRecordClass);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PilotRecordClass);
    } // setupUi

    void retranslateUi(QMainWindow *PilotRecordClass)
    {
        PilotRecordClass->setWindowTitle(QApplication::translate("PilotRecordClass", "PilotRecord", Q_NULLPTR));
        actionLogbook->setText(QApplication::translate("PilotRecordClass", "Logbook", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionLogbook->setToolTip(QApplication::translate("PilotRecordClass", "View Logbook", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionManage_User->setText(QApplication::translate("PilotRecordClass", "Manage User", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionManage_User->setToolTip(QApplication::translate("PilotRecordClass", "Manage User(s)", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionSettings->setText(QApplication::translate("PilotRecordClass", "Settings", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionSettings->setToolTip(QApplication::translate("PilotRecordClass", "Settings", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionLock_Logout->setText(QApplication::translate("PilotRecordClass", "Lock/Logout", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionLock_Logout->setToolTip(QApplication::translate("PilotRecordClass", "Lock Current Session", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLock_Logout->setShortcut(QApplication::translate("PilotRecordClass", "Ctrl+L", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionAddLog->setText(QApplication::translate("PilotRecordClass", "AddLog", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionAddLog->setToolTip(QApplication::translate("PilotRecordClass", "Add Log Entry", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        actionFlight_Duty->setText(QApplication::translate("PilotRecordClass", "Flight Duty", Q_NULLPTR));
        label_47->setText(QApplication::translate("PilotRecordClass", "Select a flight to review or double click to open", Q_NULLPTR));
        label_46->setText(QApplication::translate("PilotRecordClass", "Year", Q_NULLPTR));
        reviewButton->setText(QApplication::translate("PilotRecordClass", "Review...", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = entryList->headerItem();
        ___qtreewidgetitem->setText(4, QApplication::translate("PilotRecordClass", "Remarks", Q_NULLPTR));
        ___qtreewidgetitem->setText(3, QApplication::translate("PilotRecordClass", "Aircraft Reg.", Q_NULLPTR));
        ___qtreewidgetitem->setText(2, QApplication::translate("PilotRecordClass", "Model", Q_NULLPTR));
        ___qtreewidgetitem->setText(1, QApplication::translate("PilotRecordClass", "PIC", Q_NULLPTR));
        ___qtreewidgetitem->setText(0, QApplication::translate("PilotRecordClass", "Date", Q_NULLPTR));
        label_12->setText(QApplication::translate("PilotRecordClass", "Total Hours:", Q_NULLPTR));
        totalHours->setText(QApplication::translate("PilotRecordClass", "0.0", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("PilotRecordClass", "Single Engine", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("PilotRecordClass", "Night", Q_NULLPTR));
        label_6->setText(QApplication::translate("PilotRecordClass", "Total Dual", Q_NULLPTR));
        SENightDual->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_7->setText(QApplication::translate("PilotRecordClass", "Total PIC", Q_NULLPTR));
        SENightPIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("PilotRecordClass", "Day", Q_NULLPTR));
        label_2->setText(QApplication::translate("PilotRecordClass", "Total Dual", Q_NULLPTR));
        label_3->setText(QApplication::translate("PilotRecordClass", "Total PIC", Q_NULLPTR));
        SEDayDual->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        SEDayPIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_44->setText(QApplication::translate("PilotRecordClass", "Total Single Engine", Q_NULLPTR));
        TotalSingleEngine->setText(QApplication::translate("PilotRecordClass", "0.0", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("PilotRecordClass", "Multi Engine", Q_NULLPTR));
        groupBox_5->setTitle(QApplication::translate("PilotRecordClass", "Day", Q_NULLPTR));
        MEDayPIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_17->setText(QApplication::translate("PilotRecordClass", "Total Dual", Q_NULLPTR));
        label_11->setText(QApplication::translate("PilotRecordClass", "Total PIC", Q_NULLPTR));
        MEDayDual->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_19->setText(QApplication::translate("PilotRecordClass", "Total Co-Pilot", Q_NULLPTR));
        MEDaySIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        groupBox_6->setTitle(QApplication::translate("PilotRecordClass", "Night", Q_NULLPTR));
        MENightPIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_13->setText(QApplication::translate("PilotRecordClass", "Total Dual", Q_NULLPTR));
        MENightDual->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_16->setText(QApplication::translate("PilotRecordClass", "Total PIC", Q_NULLPTR));
        label_21->setText(QApplication::translate("PilotRecordClass", "Total Co-Pilot", Q_NULLPTR));
        MENightSIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_42->setText(QApplication::translate("PilotRecordClass", "Total Multi Engine", Q_NULLPTR));
        TotalMultiEngine->setText(QApplication::translate("PilotRecordClass", "0.0", Q_NULLPTR));
        groupBox_7->setTitle(QApplication::translate("PilotRecordClass", "Cross Country", Q_NULLPTR));
        groupBox_9->setTitle(QApplication::translate("PilotRecordClass", "Day", Q_NULLPTR));
        label_24->setText(QApplication::translate("PilotRecordClass", "Total PIC", Q_NULLPTR));
        XCunDayPIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        XCunDayDual->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_23->setText(QApplication::translate("PilotRecordClass", "Total Dual", Q_NULLPTR));
        label_40->setText(QApplication::translate("PilotRecordClass", "Total Cross Country", Q_NULLPTR));
        TotalXCun->setText(QApplication::translate("PilotRecordClass", "0.0", Q_NULLPTR));
        groupBox_10->setTitle(QApplication::translate("PilotRecordClass", "Night", Q_NULLPTR));
        XCunNightDual->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_27->setText(QApplication::translate("PilotRecordClass", "Total Dual", Q_NULLPTR));
        label_28->setText(QApplication::translate("PilotRecordClass", "Total PIC", Q_NULLPTR));
        XCunNightPIC->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        groupBox_8->setTitle(QApplication::translate("PilotRecordClass", "Instrument", Q_NULLPTR));
        actualIFR->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_31->setText(QApplication::translate("PilotRecordClass", "SIM", Q_NULLPTR));
        label_32->setText(QApplication::translate("PilotRecordClass", "Hood", Q_NULLPTR));
        label_33->setText(QApplication::translate("PilotRecordClass", "Actual IFR", Q_NULLPTR));
        simTime->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        label_34->setText(QApplication::translate("PilotRecordClass", "# of Approaches", Q_NULLPTR));
        hoodTime->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        numAppr->setText(QApplication::translate("PilotRecordClass", "0", Q_NULLPTR));
        monthLabel->setText(QApplication::translate("PilotRecordClass", "Month Year", Q_NULLPTR));
        prevMonth->setText(QApplication::translate("PilotRecordClass", "<<", Q_NULLPTR));
        nextMonth->setText(QApplication::translate("PilotRecordClass", ">>", Q_NULLPTR));
        detailsButton->setText(QApplication::translate("PilotRecordClass", "Details", Q_NULLPTR));
        toolButton->setText(QApplication::translate("PilotRecordClass", "Menu...", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("PilotRecordClass", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordClass: public Ui_PilotRecordClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORD_H
