/********************************************************************************
** Form generated from reading UI file 'PilotRecordLogBook.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PILOTRECORDLOGBOOK_H
#define UI_PILOTRECORDLOGBOOK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PilotRecordLogBook
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_6;
    QDateEdit *dateEdit;
    QSpacerItem *horizontalSpacer;
    QLabel *dateModLabel;
    QDateEdit *dateModEdit;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_2;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *PICname;
    QLineEdit *PAXname;
    QGridLayout *gridLayout_3;
    QComboBox *ACtype;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label;
    QLabel *label_2;
    QComboBox *ACreg;
    QLabel *label_5;
    QPlainTextEdit *Remarks;
    QWidget *tab_2;
    QGridLayout *gridLayout_6;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_7;
    QGridLayout *gridLayout_9;
    QFormLayout *formLayout_10;
    QLabel *label_32;
    QLineEdit *XCUN_DAY_DUAL;
    QLabel *label_11;
    QLabel *label_12;
    QFormLayout *formLayout_9;
    QLineEdit *XCUN_DAY_PIC;
    QLabel *label_31;
    QFormLayout *formLayout_21;
    QLineEdit *XCUN_NIGHT_DUAL;
    QLabel *label_33;
    QFormLayout *formLayout_22;
    QLabel *label_34;
    QLineEdit *XCUN_NIGHT_PIC;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_5;
    QFormLayout *formLayout_11;
    QLabel *label_21;
    QLineEdit *ME_DAY_PIC;
    QLabel *label_9;
    QFormLayout *formLayout_12;
    QLabel *label_22;
    QLineEdit *ME_DAY_DUAL;
    QFormLayout *formLayout_18;
    QLabel *label_28;
    QLineEdit *ME_NIGHT_DUAL;
    QLabel *label_10;
    QFormLayout *formLayout_17;
    QLabel *label_27;
    QLineEdit *ME_NIGHT_PIC;
    QFormLayout *formLayout_13;
    QLabel *label_23;
    QLineEdit *ME_DAY_SIC;
    QFormLayout *formLayout_14;
    QLabel *label_24;
    QLineEdit *ME_NIGHT_SIC;
    QGroupBox *groupBox_5;
    QGridLayout *gridLayout_10;
    QLabel *label_39;
    QLabel *label_40;
    QLineEdit *TO_LAND_DAY;
    QLineEdit *TO_LAND_NIGHT;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_8;
    QLabel *label_35;
    QLabel *label_36;
    QLabel *label_38;
    QLabel *label_37;
    QLineEdit *IFR_APPR;
    QLineEdit *SIM_TIME;
    QLineEdit *HOOD_TIME;
    QLineEdit *ACTUAL_TIME;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *label_14;
    QLineEdit *SE_NIGHT_DUAL;
    QLabel *label_8;
    QFormLayout *formLayout_5;
    QLabel *label_16;
    QLineEdit *SE_NIGHT_PIC;
    QLabel *label_7;
    QFormLayout *formLayout_4;
    QLabel *label_15;
    QLineEdit *SE_DAY_PIC;
    QSpacerItem *verticalSpacer;
    QFormLayout *formLayout_2;
    QLabel *label_13;
    QLineEdit *SE_DAY_DUAL;
    QWidget *tab_4;
    QGridLayout *gridLayout_19;
    QGroupBox *groupBox_11;
    QGridLayout *gridLayout_20;
    QPlainTextEdit *T_Remarks;
    QGroupBox *groupBox_12;
    QGridLayout *gridLayout_21;
    QLabel *label_26;
    QLabel *label_29;
    QLabel *label_30;
    QLabel *T_ACreg;
    QLabel *T_ACtype;
    QLabel *label_25;
    QLabel *T_PICname;
    QLabel *T_PAXname;
    QWidget *tab_3;
    QGridLayout *gridLayout_12;
    QGroupBox *groupBox_6;
    QGridLayout *gridLayout_14;
    QLabel *label_18;
    QLabel *label_17;
    QLabel *label_19;
    QLabel *label_20;
    QLabel *T_SE_DAY_DUAL;
    QLabel *T_SE_NIGHT_DUAL;
    QLabel *T_SE_DAY_PIC;
    QLabel *T_SE_NIGHT_PIC;
    QGroupBox *groupBox_8;
    QGridLayout *gridLayout_13;
    QLabel *T_ME_NIGHT_DUAL;
    QLabel *label_52;
    QLabel *label_56;
    QLabel *T_ME_DAY_DUAL;
    QLabel *label_57;
    QLabel *T_ME_NIGHT_SIC;
    QLabel *label_53;
    QLabel *label_49;
    QLabel *T_ME_DAY_PIC;
    QLabel *T_ME_NIGHT_PIC;
    QLabel *T_ME_DAY_SIC;
    QGroupBox *groupBox_7;
    QGridLayout *gridLayout_15;
    QLabel *T_XCUN_DAY_DUAL;
    QLabel *label_48;
    QLabel *label_45;
    QLabel *label_43;
    QLabel *label_41;
    QLabel *T_XCUN_NIGHT_DUAL;
    QLabel *T_XCUN_DAY_PIC;
    QLabel *T_XCUN_NIGHT_PIC;
    QGroupBox *groupBox_9;
    QGridLayout *gridLayout_17;
    QLabel *label_63;
    QLabel *label_62;
    QLabel *T_IFR_APPR;
    QLabel *T_SIM_TIME;
    QLabel *T_HOOD_TIME;
    QLabel *T_ACTUAL_TIME;
    QLabel *label_61;
    QLabel *label_60;
    QGroupBox *groupBox_10;
    QGridLayout *gridLayout_16;
    QLabel *label_70;
    QLabel *T_TO_LAND_NIGHT;
    QLabel *label_68;
    QLabel *T_TO_LAND_DAY;
    QWidget *tab_5;
    QGridLayout *gridLayout_22;
    QLabel *label_42;
    QTreeWidget *previousList;
    QGridLayout *gridLayout_11;
    QPushButton *okayButton;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *cancelButton;
    QPushButton *editEntry;

    void setupUi(QDialog *PilotRecordLogBook)
    {
        if (PilotRecordLogBook->objectName().isEmpty())
            PilotRecordLogBook->setObjectName(QStringLiteral("PilotRecordLogBook"));
        PilotRecordLogBook->setWindowModality(Qt::ApplicationModal);
        PilotRecordLogBook->resize(525, 440);
        PilotRecordLogBook->setContextMenuPolicy(Qt::NoContextMenu);
        QIcon icon;
        icon.addFile(QStringLiteral(":/PilotRecord/pilot.png"), QSize(), QIcon::Normal, QIcon::Off);
        PilotRecordLogBook->setWindowIcon(icon);
        PilotRecordLogBook->setModal(true);
        verticalLayout = new QVBoxLayout(PilotRecordLogBook);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_6 = new QLabel(PilotRecordLogBook);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout->addWidget(label_6);

        dateEdit = new QDateEdit(PilotRecordLogBook);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));

        horizontalLayout->addWidget(dateEdit);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        dateModLabel = new QLabel(PilotRecordLogBook);
        dateModLabel->setObjectName(QStringLiteral("dateModLabel"));

        horizontalLayout->addWidget(dateModLabel);

        dateModEdit = new QDateEdit(PilotRecordLogBook);
        dateModEdit->setObjectName(QStringLiteral("dateModEdit"));
        dateModEdit->setEnabled(false);

        horizontalLayout->addWidget(dateModEdit);


        verticalLayout->addLayout(horizontalLayout);

        tabWidget = new QTabWidget(PilotRecordLogBook);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tab->sizePolicy().hasHeightForWidth());
        tab->setSizePolicy(sizePolicy);
        tab->setContextMenuPolicy(Qt::DefaultContextMenu);
        gridLayout_4 = new QGridLayout(tab);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label_3 = new QLabel(tab);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 0, 0, 1, 1);

        label_4 = new QLabel(tab);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_2->addWidget(label_4, 0, 1, 1, 1);

        PICname = new QLineEdit(tab);
        PICname->setObjectName(QStringLiteral("PICname"));

        gridLayout_2->addWidget(PICname, 1, 0, 1, 1);

        PAXname = new QLineEdit(tab);
        PAXname->setObjectName(QStringLiteral("PAXname"));

        gridLayout_2->addWidget(PAXname, 1, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_2, 1, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        ACtype = new QComboBox(tab);
        ACtype->setObjectName(QStringLiteral("ACtype"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(ACtype->sizePolicy().hasHeightForWidth());
        ACtype->setSizePolicy(sizePolicy1);
        ACtype->setEditable(true);

        gridLayout_3->addWidget(ACtype, 2, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 2, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_3, 2, 3, 1, 1);

        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 0, 1, 1, 1);

        ACreg = new QComboBox(tab);
        ACreg->setObjectName(QStringLiteral("ACreg"));
        sizePolicy1.setHeightForWidth(ACreg->sizePolicy().hasHeightForWidth());
        ACreg->setSizePolicy(sizePolicy1);
        ACreg->setEditable(true);

        gridLayout_3->addWidget(ACreg, 2, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);

        label_5 = new QLabel(tab);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_4->addWidget(label_5, 2, 0, 1, 1);

        Remarks = new QPlainTextEdit(tab);
        Remarks->setObjectName(QStringLiteral("Remarks"));

        gridLayout_4->addWidget(Remarks, 3, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_6 = new QGridLayout(tab_2);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        groupBox_3 = new QGroupBox(tab_2);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_7 = new QGridLayout(groupBox_3);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        formLayout_10 = new QFormLayout();
        formLayout_10->setSpacing(6);
        formLayout_10->setObjectName(QStringLiteral("formLayout_10"));
        label_32 = new QLabel(groupBox_3);
        label_32->setObjectName(QStringLiteral("label_32"));

        formLayout_10->setWidget(0, QFormLayout::LabelRole, label_32);

        XCUN_DAY_DUAL = new QLineEdit(groupBox_3);
        XCUN_DAY_DUAL->setObjectName(QStringLiteral("XCUN_DAY_DUAL"));

        formLayout_10->setWidget(0, QFormLayout::FieldRole, XCUN_DAY_DUAL);


        gridLayout_9->addLayout(formLayout_10, 1, 0, 1, 1);

        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_11, 0, 0, 1, 1);

        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_12, 0, 1, 1, 1);

        formLayout_9 = new QFormLayout();
        formLayout_9->setSpacing(6);
        formLayout_9->setObjectName(QStringLiteral("formLayout_9"));
        XCUN_DAY_PIC = new QLineEdit(groupBox_3);
        XCUN_DAY_PIC->setObjectName(QStringLiteral("XCUN_DAY_PIC"));

        formLayout_9->setWidget(0, QFormLayout::FieldRole, XCUN_DAY_PIC);

        label_31 = new QLabel(groupBox_3);
        label_31->setObjectName(QStringLiteral("label_31"));

        formLayout_9->setWidget(0, QFormLayout::LabelRole, label_31);


        gridLayout_9->addLayout(formLayout_9, 2, 0, 1, 1);

        formLayout_21 = new QFormLayout();
        formLayout_21->setSpacing(6);
        formLayout_21->setObjectName(QStringLiteral("formLayout_21"));
        XCUN_NIGHT_DUAL = new QLineEdit(groupBox_3);
        XCUN_NIGHT_DUAL->setObjectName(QStringLiteral("XCUN_NIGHT_DUAL"));

        formLayout_21->setWidget(0, QFormLayout::FieldRole, XCUN_NIGHT_DUAL);

        label_33 = new QLabel(groupBox_3);
        label_33->setObjectName(QStringLiteral("label_33"));

        formLayout_21->setWidget(0, QFormLayout::LabelRole, label_33);


        gridLayout_9->addLayout(formLayout_21, 1, 1, 1, 1);

        formLayout_22 = new QFormLayout();
        formLayout_22->setSpacing(6);
        formLayout_22->setObjectName(QStringLiteral("formLayout_22"));
        label_34 = new QLabel(groupBox_3);
        label_34->setObjectName(QStringLiteral("label_34"));

        formLayout_22->setWidget(0, QFormLayout::LabelRole, label_34);

        XCUN_NIGHT_PIC = new QLineEdit(groupBox_3);
        XCUN_NIGHT_PIC->setObjectName(QStringLiteral("XCUN_NIGHT_PIC"));

        formLayout_22->setWidget(0, QFormLayout::FieldRole, XCUN_NIGHT_PIC);


        gridLayout_9->addLayout(formLayout_22, 2, 1, 1, 1);


        gridLayout_7->addLayout(gridLayout_9, 4, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_3, 2, 0, 1, 1);

        groupBox_2 = new QGroupBox(tab_2);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_5 = new QGridLayout(groupBox_2);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        formLayout_11 = new QFormLayout();
        formLayout_11->setSpacing(6);
        formLayout_11->setObjectName(QStringLiteral("formLayout_11"));
        label_21 = new QLabel(groupBox_2);
        label_21->setObjectName(QStringLiteral("label_21"));

        formLayout_11->setWidget(0, QFormLayout::LabelRole, label_21);

        ME_DAY_PIC = new QLineEdit(groupBox_2);
        ME_DAY_PIC->setObjectName(QStringLiteral("ME_DAY_PIC"));

        formLayout_11->setWidget(0, QFormLayout::FieldRole, ME_DAY_PIC);


        gridLayout_5->addLayout(formLayout_11, 2, 0, 1, 1);

        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_9, 0, 0, 1, 1);

        formLayout_12 = new QFormLayout();
        formLayout_12->setSpacing(6);
        formLayout_12->setObjectName(QStringLiteral("formLayout_12"));
        label_22 = new QLabel(groupBox_2);
        label_22->setObjectName(QStringLiteral("label_22"));

        formLayout_12->setWidget(0, QFormLayout::LabelRole, label_22);

        ME_DAY_DUAL = new QLineEdit(groupBox_2);
        ME_DAY_DUAL->setObjectName(QStringLiteral("ME_DAY_DUAL"));

        formLayout_12->setWidget(0, QFormLayout::FieldRole, ME_DAY_DUAL);


        gridLayout_5->addLayout(formLayout_12, 1, 0, 1, 1);

        formLayout_18 = new QFormLayout();
        formLayout_18->setSpacing(6);
        formLayout_18->setObjectName(QStringLiteral("formLayout_18"));
        label_28 = new QLabel(groupBox_2);
        label_28->setObjectName(QStringLiteral("label_28"));

        formLayout_18->setWidget(0, QFormLayout::LabelRole, label_28);

        ME_NIGHT_DUAL = new QLineEdit(groupBox_2);
        ME_NIGHT_DUAL->setObjectName(QStringLiteral("ME_NIGHT_DUAL"));

        formLayout_18->setWidget(0, QFormLayout::FieldRole, ME_NIGHT_DUAL);


        gridLayout_5->addLayout(formLayout_18, 1, 1, 1, 1);

        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_10, 0, 1, 1, 1);

        formLayout_17 = new QFormLayout();
        formLayout_17->setSpacing(6);
        formLayout_17->setObjectName(QStringLiteral("formLayout_17"));
        label_27 = new QLabel(groupBox_2);
        label_27->setObjectName(QStringLiteral("label_27"));

        formLayout_17->setWidget(0, QFormLayout::LabelRole, label_27);

        ME_NIGHT_PIC = new QLineEdit(groupBox_2);
        ME_NIGHT_PIC->setObjectName(QStringLiteral("ME_NIGHT_PIC"));

        formLayout_17->setWidget(0, QFormLayout::FieldRole, ME_NIGHT_PIC);


        gridLayout_5->addLayout(formLayout_17, 2, 1, 1, 1);

        formLayout_13 = new QFormLayout();
        formLayout_13->setSpacing(6);
        formLayout_13->setObjectName(QStringLiteral("formLayout_13"));
        label_23 = new QLabel(groupBox_2);
        label_23->setObjectName(QStringLiteral("label_23"));

        formLayout_13->setWidget(0, QFormLayout::LabelRole, label_23);

        ME_DAY_SIC = new QLineEdit(groupBox_2);
        ME_DAY_SIC->setObjectName(QStringLiteral("ME_DAY_SIC"));

        formLayout_13->setWidget(0, QFormLayout::FieldRole, ME_DAY_SIC);


        gridLayout_5->addLayout(formLayout_13, 3, 0, 1, 1);

        formLayout_14 = new QFormLayout();
        formLayout_14->setSpacing(6);
        formLayout_14->setObjectName(QStringLiteral("formLayout_14"));
        label_24 = new QLabel(groupBox_2);
        label_24->setObjectName(QStringLiteral("label_24"));

        formLayout_14->setWidget(0, QFormLayout::LabelRole, label_24);

        ME_NIGHT_SIC = new QLineEdit(groupBox_2);
        ME_NIGHT_SIC->setObjectName(QStringLiteral("ME_NIGHT_SIC"));

        formLayout_14->setWidget(0, QFormLayout::FieldRole, ME_NIGHT_SIC);


        gridLayout_5->addLayout(formLayout_14, 3, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_2, 0, 1, 1, 1);

        groupBox_5 = new QGroupBox(tab_2);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        gridLayout_10 = new QGridLayout(groupBox_5);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        label_39 = new QLabel(groupBox_5);
        label_39->setObjectName(QStringLiteral("label_39"));
        label_39->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_39, 0, 0, 1, 1);

        label_40 = new QLabel(groupBox_5);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_40, 0, 1, 1, 1);

        TO_LAND_DAY = new QLineEdit(groupBox_5);
        TO_LAND_DAY->setObjectName(QStringLiteral("TO_LAND_DAY"));
        sizePolicy1.setHeightForWidth(TO_LAND_DAY->sizePolicy().hasHeightForWidth());
        TO_LAND_DAY->setSizePolicy(sizePolicy1);

        gridLayout_10->addWidget(TO_LAND_DAY, 1, 0, 1, 1);

        TO_LAND_NIGHT = new QLineEdit(groupBox_5);
        TO_LAND_NIGHT->setObjectName(QStringLiteral("TO_LAND_NIGHT"));

        gridLayout_10->addWidget(TO_LAND_NIGHT, 1, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_5, 3, 0, 1, 1);

        groupBox_4 = new QGroupBox(tab_2);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        gridLayout_8 = new QGridLayout(groupBox_4);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        label_35 = new QLabel(groupBox_4);
        label_35->setObjectName(QStringLiteral("label_35"));

        gridLayout_8->addWidget(label_35, 3, 0, 1, 1);

        label_36 = new QLabel(groupBox_4);
        label_36->setObjectName(QStringLiteral("label_36"));

        gridLayout_8->addWidget(label_36, 2, 0, 1, 1);

        label_38 = new QLabel(groupBox_4);
        label_38->setObjectName(QStringLiteral("label_38"));
        label_38->setWordWrap(false);

        gridLayout_8->addWidget(label_38, 0, 0, 1, 1);

        label_37 = new QLabel(groupBox_4);
        label_37->setObjectName(QStringLiteral("label_37"));

        gridLayout_8->addWidget(label_37, 1, 0, 1, 1);

        IFR_APPR = new QLineEdit(groupBox_4);
        IFR_APPR->setObjectName(QStringLiteral("IFR_APPR"));

        gridLayout_8->addWidget(IFR_APPR, 0, 1, 1, 1);

        SIM_TIME = new QLineEdit(groupBox_4);
        SIM_TIME->setObjectName(QStringLiteral("SIM_TIME"));

        gridLayout_8->addWidget(SIM_TIME, 1, 1, 1, 1);

        HOOD_TIME = new QLineEdit(groupBox_4);
        HOOD_TIME->setObjectName(QStringLiteral("HOOD_TIME"));

        gridLayout_8->addWidget(HOOD_TIME, 2, 1, 1, 1);

        ACTUAL_TIME = new QLineEdit(groupBox_4);
        ACTUAL_TIME->setObjectName(QStringLiteral("ACTUAL_TIME"));

        gridLayout_8->addWidget(ACTUAL_TIME, 3, 1, 1, 1);


        gridLayout_6->addWidget(groupBox_4, 2, 1, 2, 1);

        groupBox = new QGroupBox(tab_2);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_14);

        SE_NIGHT_DUAL = new QLineEdit(groupBox);
        SE_NIGHT_DUAL->setObjectName(QStringLiteral("SE_NIGHT_DUAL"));

        formLayout->setWidget(0, QFormLayout::FieldRole, SE_NIGHT_DUAL);


        gridLayout->addLayout(formLayout, 1, 1, 1, 1);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_8, 0, 1, 1, 1);

        formLayout_5 = new QFormLayout();
        formLayout_5->setSpacing(6);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        label_16 = new QLabel(groupBox);
        label_16->setObjectName(QStringLiteral("label_16"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_16);

        SE_NIGHT_PIC = new QLineEdit(groupBox);
        SE_NIGHT_PIC->setObjectName(QStringLiteral("SE_NIGHT_PIC"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, SE_NIGHT_PIC);


        gridLayout->addLayout(formLayout_5, 2, 1, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_7, 0, 0, 1, 1);

        formLayout_4 = new QFormLayout();
        formLayout_4->setSpacing(6);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        label_15 = new QLabel(groupBox);
        label_15->setObjectName(QStringLiteral("label_15"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_15);

        SE_DAY_PIC = new QLineEdit(groupBox);
        SE_DAY_PIC->setObjectName(QStringLiteral("SE_DAY_PIC"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, SE_DAY_PIC);


        gridLayout->addLayout(formLayout_4, 2, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 3, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_13);

        SE_DAY_DUAL = new QLineEdit(groupBox);
        SE_DAY_DUAL->setObjectName(QStringLiteral("SE_DAY_DUAL"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, SE_DAY_DUAL);


        gridLayout->addLayout(formLayout_2, 1, 0, 1, 1);


        gridLayout_6->addWidget(groupBox, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        gridLayout_19 = new QGridLayout(tab_4);
        gridLayout_19->setSpacing(6);
        gridLayout_19->setContentsMargins(11, 11, 11, 11);
        gridLayout_19->setObjectName(QStringLiteral("gridLayout_19"));
        groupBox_11 = new QGroupBox(tab_4);
        groupBox_11->setObjectName(QStringLiteral("groupBox_11"));
        gridLayout_20 = new QGridLayout(groupBox_11);
        gridLayout_20->setSpacing(6);
        gridLayout_20->setContentsMargins(11, 11, 11, 11);
        gridLayout_20->setObjectName(QStringLiteral("gridLayout_20"));
        T_Remarks = new QPlainTextEdit(groupBox_11);
        T_Remarks->setObjectName(QStringLiteral("T_Remarks"));
        T_Remarks->setEnabled(false);
        T_Remarks->setUndoRedoEnabled(false);

        gridLayout_20->addWidget(T_Remarks, 1, 0, 1, 1);


        gridLayout_19->addWidget(groupBox_11, 4, 0, 1, 2);

        groupBox_12 = new QGroupBox(tab_4);
        groupBox_12->setObjectName(QStringLiteral("groupBox_12"));
        gridLayout_21 = new QGridLayout(groupBox_12);
        gridLayout_21->setSpacing(6);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        label_26 = new QLabel(groupBox_12);
        label_26->setObjectName(QStringLiteral("label_26"));

        gridLayout_21->addWidget(label_26, 1, 0, 1, 1);

        label_29 = new QLabel(groupBox_12);
        label_29->setObjectName(QStringLiteral("label_29"));

        gridLayout_21->addWidget(label_29, 0, 0, 1, 1);

        label_30 = new QLabel(groupBox_12);
        label_30->setObjectName(QStringLiteral("label_30"));

        gridLayout_21->addWidget(label_30, 0, 2, 1, 1);

        T_ACreg = new QLabel(groupBox_12);
        T_ACreg->setObjectName(QStringLiteral("T_ACreg"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        T_ACreg->setFont(font);
        T_ACreg->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_21->addWidget(T_ACreg, 0, 3, 1, 1);

        T_ACtype = new QLabel(groupBox_12);
        T_ACtype->setObjectName(QStringLiteral("T_ACtype"));
        T_ACtype->setFont(font);
        T_ACtype->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        T_ACtype->setIndent(-1);

        gridLayout_21->addWidget(T_ACtype, 0, 1, 1, 1);

        label_25 = new QLabel(groupBox_12);
        label_25->setObjectName(QStringLiteral("label_25"));

        gridLayout_21->addWidget(label_25, 1, 2, 1, 1);

        T_PICname = new QLabel(groupBox_12);
        T_PICname->setObjectName(QStringLiteral("T_PICname"));
        T_PICname->setFont(font);

        gridLayout_21->addWidget(T_PICname, 1, 1, 1, 1);

        T_PAXname = new QLabel(groupBox_12);
        T_PAXname->setObjectName(QStringLiteral("T_PAXname"));
        T_PAXname->setFont(font);

        gridLayout_21->addWidget(T_PAXname, 1, 3, 1, 1);


        gridLayout_19->addWidget(groupBox_12, 0, 0, 2, 2);

        tabWidget->addTab(tab_4, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        tab_3->setVisible(false);
        gridLayout_12 = new QGridLayout(tab_3);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        groupBox_6 = new QGroupBox(tab_3);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        groupBox_6->setStyleSheet(QStringLiteral(""));
        gridLayout_14 = new QGridLayout(groupBox_6);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        label_18 = new QLabel(groupBox_6);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_18, 0, 2, 1, 1);

        label_17 = new QLabel(groupBox_6);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_17, 0, 1, 1, 1);

        label_19 = new QLabel(groupBox_6);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_14->addWidget(label_19, 1, 0, 1, 1);

        label_20 = new QLabel(groupBox_6);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_14->addWidget(label_20, 2, 0, 1, 1);

        T_SE_DAY_DUAL = new QLabel(groupBox_6);
        T_SE_DAY_DUAL->setObjectName(QStringLiteral("T_SE_DAY_DUAL"));
        T_SE_DAY_DUAL->setFont(font);
        T_SE_DAY_DUAL->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(T_SE_DAY_DUAL, 1, 1, 1, 1);

        T_SE_NIGHT_DUAL = new QLabel(groupBox_6);
        T_SE_NIGHT_DUAL->setObjectName(QStringLiteral("T_SE_NIGHT_DUAL"));
        T_SE_NIGHT_DUAL->setFont(font);
        T_SE_NIGHT_DUAL->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(T_SE_NIGHT_DUAL, 1, 2, 1, 1);

        T_SE_DAY_PIC = new QLabel(groupBox_6);
        T_SE_DAY_PIC->setObjectName(QStringLiteral("T_SE_DAY_PIC"));
        T_SE_DAY_PIC->setFont(font);
        T_SE_DAY_PIC->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(T_SE_DAY_PIC, 2, 1, 1, 1);

        T_SE_NIGHT_PIC = new QLabel(groupBox_6);
        T_SE_NIGHT_PIC->setObjectName(QStringLiteral("T_SE_NIGHT_PIC"));
        T_SE_NIGHT_PIC->setFont(font);
        T_SE_NIGHT_PIC->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(T_SE_NIGHT_PIC, 2, 2, 1, 1);


        gridLayout_12->addWidget(groupBox_6, 2, 0, 1, 1);

        groupBox_8 = new QGroupBox(tab_3);
        groupBox_8->setObjectName(QStringLiteral("groupBox_8"));
        gridLayout_13 = new QGridLayout(groupBox_8);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        T_ME_NIGHT_DUAL = new QLabel(groupBox_8);
        T_ME_NIGHT_DUAL->setObjectName(QStringLiteral("T_ME_NIGHT_DUAL"));
        T_ME_NIGHT_DUAL->setFont(font);
        T_ME_NIGHT_DUAL->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(T_ME_NIGHT_DUAL, 1, 2, 1, 1);

        label_52 = new QLabel(groupBox_8);
        label_52->setObjectName(QStringLiteral("label_52"));
        label_52->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_52, 0, 1, 1, 1);

        label_56 = new QLabel(groupBox_8);
        label_56->setObjectName(QStringLiteral("label_56"));

        gridLayout_13->addWidget(label_56, 1, 0, 1, 1);

        T_ME_DAY_DUAL = new QLabel(groupBox_8);
        T_ME_DAY_DUAL->setObjectName(QStringLiteral("T_ME_DAY_DUAL"));
        T_ME_DAY_DUAL->setFont(font);
        T_ME_DAY_DUAL->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(T_ME_DAY_DUAL, 1, 1, 1, 1);

        label_57 = new QLabel(groupBox_8);
        label_57->setObjectName(QStringLiteral("label_57"));

        gridLayout_13->addWidget(label_57, 4, 0, 1, 1);

        T_ME_NIGHT_SIC = new QLabel(groupBox_8);
        T_ME_NIGHT_SIC->setObjectName(QStringLiteral("T_ME_NIGHT_SIC"));
        T_ME_NIGHT_SIC->setFont(font);
        T_ME_NIGHT_SIC->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(T_ME_NIGHT_SIC, 4, 2, 1, 1);

        label_53 = new QLabel(groupBox_8);
        label_53->setObjectName(QStringLiteral("label_53"));
        label_53->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_53, 0, 2, 1, 1);

        label_49 = new QLabel(groupBox_8);
        label_49->setObjectName(QStringLiteral("label_49"));

        gridLayout_13->addWidget(label_49, 2, 0, 1, 1);

        T_ME_DAY_PIC = new QLabel(groupBox_8);
        T_ME_DAY_PIC->setObjectName(QStringLiteral("T_ME_DAY_PIC"));
        T_ME_DAY_PIC->setFont(font);
        T_ME_DAY_PIC->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(T_ME_DAY_PIC, 2, 1, 1, 1);

        T_ME_NIGHT_PIC = new QLabel(groupBox_8);
        T_ME_NIGHT_PIC->setObjectName(QStringLiteral("T_ME_NIGHT_PIC"));
        T_ME_NIGHT_PIC->setFont(font);
        T_ME_NIGHT_PIC->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(T_ME_NIGHT_PIC, 2, 2, 1, 1);

        T_ME_DAY_SIC = new QLabel(groupBox_8);
        T_ME_DAY_SIC->setObjectName(QStringLiteral("T_ME_DAY_SIC"));
        T_ME_DAY_SIC->setFont(font);
        T_ME_DAY_SIC->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(T_ME_DAY_SIC, 4, 1, 1, 1);


        gridLayout_12->addWidget(groupBox_8, 2, 1, 1, 1);

        groupBox_7 = new QGroupBox(tab_3);
        groupBox_7->setObjectName(QStringLiteral("groupBox_7"));
        gridLayout_15 = new QGridLayout(groupBox_7);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        T_XCUN_DAY_DUAL = new QLabel(groupBox_7);
        T_XCUN_DAY_DUAL->setObjectName(QStringLiteral("T_XCUN_DAY_DUAL"));
        T_XCUN_DAY_DUAL->setFont(font);
        T_XCUN_DAY_DUAL->setAlignment(Qt::AlignCenter);

        gridLayout_15->addWidget(T_XCUN_DAY_DUAL, 1, 1, 1, 1);

        label_48 = new QLabel(groupBox_7);
        label_48->setObjectName(QStringLiteral("label_48"));

        gridLayout_15->addWidget(label_48, 1, 0, 1, 1);

        label_45 = new QLabel(groupBox_7);
        label_45->setObjectName(QStringLiteral("label_45"));
        label_45->setAlignment(Qt::AlignCenter);

        gridLayout_15->addWidget(label_45, 0, 1, 1, 1);

        label_43 = new QLabel(groupBox_7);
        label_43->setObjectName(QStringLiteral("label_43"));

        gridLayout_15->addWidget(label_43, 2, 0, 1, 1);

        label_41 = new QLabel(groupBox_7);
        label_41->setObjectName(QStringLiteral("label_41"));
        label_41->setAlignment(Qt::AlignCenter);

        gridLayout_15->addWidget(label_41, 0, 2, 1, 1);

        T_XCUN_NIGHT_DUAL = new QLabel(groupBox_7);
        T_XCUN_NIGHT_DUAL->setObjectName(QStringLiteral("T_XCUN_NIGHT_DUAL"));
        T_XCUN_NIGHT_DUAL->setFont(font);
        T_XCUN_NIGHT_DUAL->setAlignment(Qt::AlignCenter);

        gridLayout_15->addWidget(T_XCUN_NIGHT_DUAL, 1, 2, 1, 1);

        T_XCUN_DAY_PIC = new QLabel(groupBox_7);
        T_XCUN_DAY_PIC->setObjectName(QStringLiteral("T_XCUN_DAY_PIC"));
        T_XCUN_DAY_PIC->setFont(font);
        T_XCUN_DAY_PIC->setAlignment(Qt::AlignCenter);

        gridLayout_15->addWidget(T_XCUN_DAY_PIC, 2, 1, 1, 1);

        T_XCUN_NIGHT_PIC = new QLabel(groupBox_7);
        T_XCUN_NIGHT_PIC->setObjectName(QStringLiteral("T_XCUN_NIGHT_PIC"));
        T_XCUN_NIGHT_PIC->setFont(font);
        T_XCUN_NIGHT_PIC->setAlignment(Qt::AlignCenter);

        gridLayout_15->addWidget(T_XCUN_NIGHT_PIC, 2, 2, 1, 1);


        gridLayout_12->addWidget(groupBox_7, 3, 0, 1, 1);

        groupBox_9 = new QGroupBox(tab_3);
        groupBox_9->setObjectName(QStringLiteral("groupBox_9"));
        gridLayout_17 = new QGridLayout(groupBox_9);
        gridLayout_17->setSpacing(6);
        gridLayout_17->setContentsMargins(11, 11, 11, 11);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        label_63 = new QLabel(groupBox_9);
        label_63->setObjectName(QStringLiteral("label_63"));

        gridLayout_17->addWidget(label_63, 3, 0, 1, 1);

        label_62 = new QLabel(groupBox_9);
        label_62->setObjectName(QStringLiteral("label_62"));

        gridLayout_17->addWidget(label_62, 2, 0, 1, 1);

        T_IFR_APPR = new QLabel(groupBox_9);
        T_IFR_APPR->setObjectName(QStringLiteral("T_IFR_APPR"));
        T_IFR_APPR->setFont(font);
        T_IFR_APPR->setAlignment(Qt::AlignCenter);

        gridLayout_17->addWidget(T_IFR_APPR, 0, 1, 1, 1);

        T_SIM_TIME = new QLabel(groupBox_9);
        T_SIM_TIME->setObjectName(QStringLiteral("T_SIM_TIME"));
        T_SIM_TIME->setFont(font);
        T_SIM_TIME->setAlignment(Qt::AlignCenter);

        gridLayout_17->addWidget(T_SIM_TIME, 1, 1, 1, 1);

        T_HOOD_TIME = new QLabel(groupBox_9);
        T_HOOD_TIME->setObjectName(QStringLiteral("T_HOOD_TIME"));
        T_HOOD_TIME->setFont(font);
        T_HOOD_TIME->setAlignment(Qt::AlignCenter);

        gridLayout_17->addWidget(T_HOOD_TIME, 2, 1, 1, 1);

        T_ACTUAL_TIME = new QLabel(groupBox_9);
        T_ACTUAL_TIME->setObjectName(QStringLiteral("T_ACTUAL_TIME"));
        T_ACTUAL_TIME->setFont(font);
        T_ACTUAL_TIME->setAlignment(Qt::AlignCenter);

        gridLayout_17->addWidget(T_ACTUAL_TIME, 3, 1, 1, 1);

        label_61 = new QLabel(groupBox_9);
        label_61->setObjectName(QStringLiteral("label_61"));

        gridLayout_17->addWidget(label_61, 1, 0, 1, 1);

        label_60 = new QLabel(groupBox_9);
        label_60->setObjectName(QStringLiteral("label_60"));

        gridLayout_17->addWidget(label_60, 0, 0, 1, 1);


        gridLayout_12->addWidget(groupBox_9, 3, 1, 3, 1);

        groupBox_10 = new QGroupBox(tab_3);
        groupBox_10->setObjectName(QStringLiteral("groupBox_10"));
        gridLayout_16 = new QGridLayout(groupBox_10);
        gridLayout_16->setSpacing(6);
        gridLayout_16->setContentsMargins(11, 11, 11, 11);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        label_70 = new QLabel(groupBox_10);
        label_70->setObjectName(QStringLiteral("label_70"));
        label_70->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(label_70, 0, 0, 1, 1);

        T_TO_LAND_NIGHT = new QLabel(groupBox_10);
        T_TO_LAND_NIGHT->setObjectName(QStringLiteral("T_TO_LAND_NIGHT"));
        T_TO_LAND_NIGHT->setFont(font);
        T_TO_LAND_NIGHT->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(T_TO_LAND_NIGHT, 1, 1, 1, 1);

        label_68 = new QLabel(groupBox_10);
        label_68->setObjectName(QStringLiteral("label_68"));
        label_68->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(label_68, 0, 1, 1, 1);

        T_TO_LAND_DAY = new QLabel(groupBox_10);
        T_TO_LAND_DAY->setObjectName(QStringLiteral("T_TO_LAND_DAY"));
        T_TO_LAND_DAY->setFont(font);
        T_TO_LAND_DAY->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(T_TO_LAND_DAY, 1, 0, 1, 1);


        gridLayout_12->addWidget(groupBox_10, 4, 0, 2, 1);

        tabWidget->addTab(tab_3, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        gridLayout_22 = new QGridLayout(tab_5);
        gridLayout_22->setSpacing(6);
        gridLayout_22->setContentsMargins(11, 11, 11, 11);
        gridLayout_22->setObjectName(QStringLiteral("gridLayout_22"));
        label_42 = new QLabel(tab_5);
        label_42->setObjectName(QStringLiteral("label_42"));

        gridLayout_22->addWidget(label_42, 0, 0, 1, 1);

        previousList = new QTreeWidget(tab_5);
        previousList->setObjectName(QStringLiteral("previousList"));
        previousList->setIndentation(0);
        previousList->setItemsExpandable(true);

        gridLayout_22->addWidget(previousList, 1, 0, 1, 1);

        tabWidget->addTab(tab_5, QString());

        verticalLayout->addWidget(tabWidget);

        gridLayout_11 = new QGridLayout();
        gridLayout_11->setSpacing(6);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        okayButton = new QPushButton(PilotRecordLogBook);
        okayButton->setObjectName(QStringLiteral("okayButton"));

        gridLayout_11->addWidget(okayButton, 0, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_11->addItem(horizontalSpacer_4, 0, 1, 1, 1);

        cancelButton = new QPushButton(PilotRecordLogBook);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        gridLayout_11->addWidget(cancelButton, 0, 3, 1, 1);

        editEntry = new QPushButton(PilotRecordLogBook);
        editEntry->setObjectName(QStringLiteral("editEntry"));

        gridLayout_11->addWidget(editEntry, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout_11);

        QWidget::setTabOrder(dateEdit, ACtype);
        QWidget::setTabOrder(ACtype, ACreg);
        QWidget::setTabOrder(ACreg, PICname);
        QWidget::setTabOrder(PICname, PAXname);
        QWidget::setTabOrder(PAXname, Remarks);
        QWidget::setTabOrder(Remarks, tabWidget);
        QWidget::setTabOrder(tabWidget, SE_DAY_DUAL);
        QWidget::setTabOrder(SE_DAY_DUAL, SE_DAY_PIC);
        QWidget::setTabOrder(SE_DAY_PIC, SE_NIGHT_DUAL);
        QWidget::setTabOrder(SE_NIGHT_DUAL, SE_NIGHT_PIC);
        QWidget::setTabOrder(SE_NIGHT_PIC, ME_DAY_DUAL);
        QWidget::setTabOrder(ME_DAY_DUAL, ME_DAY_PIC);
        QWidget::setTabOrder(ME_DAY_PIC, ME_NIGHT_DUAL);
        QWidget::setTabOrder(ME_NIGHT_DUAL, ME_NIGHT_PIC);
        QWidget::setTabOrder(ME_NIGHT_PIC, XCUN_DAY_DUAL);
        QWidget::setTabOrder(XCUN_DAY_DUAL, XCUN_DAY_PIC);
        QWidget::setTabOrder(XCUN_DAY_PIC, XCUN_NIGHT_DUAL);
        QWidget::setTabOrder(XCUN_NIGHT_DUAL, XCUN_NIGHT_PIC);
        QWidget::setTabOrder(XCUN_NIGHT_PIC, TO_LAND_DAY);
        QWidget::setTabOrder(TO_LAND_DAY, TO_LAND_NIGHT);
        QWidget::setTabOrder(TO_LAND_NIGHT, IFR_APPR);
        QWidget::setTabOrder(IFR_APPR, SIM_TIME);
        QWidget::setTabOrder(SIM_TIME, HOOD_TIME);
        QWidget::setTabOrder(HOOD_TIME, ACTUAL_TIME);
        QWidget::setTabOrder(ACTUAL_TIME, okayButton);
        QWidget::setTabOrder(okayButton, cancelButton);

        retranslateUi(PilotRecordLogBook);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PilotRecordLogBook);
    } // setupUi

    void retranslateUi(QDialog *PilotRecordLogBook)
    {
        PilotRecordLogBook->setWindowTitle(QApplication::translate("PilotRecordLogBook", "Manage Log Book", Q_NULLPTR));
        label_6->setText(QApplication::translate("PilotRecordLogBook", "Flight Date", Q_NULLPTR));
        dateEdit->setDisplayFormat(QApplication::translate("PilotRecordLogBook", "dd-MMM-yyyy", Q_NULLPTR));
        dateModLabel->setText(QApplication::translate("PilotRecordLogBook", "Date Modified", Q_NULLPTR));
        dateModEdit->setDisplayFormat(QApplication::translate("PilotRecordLogBook", "dd-MMM-yyyy", Q_NULLPTR));
        label_3->setText(QApplication::translate("PilotRecordLogBook", "Pilot's Name", Q_NULLPTR));
        label_4->setText(QApplication::translate("PilotRecordLogBook", "Student or Passenger", Q_NULLPTR));
        label->setText(QApplication::translate("PilotRecordLogBook", "Aircraft Model", Q_NULLPTR));
        label_2->setText(QApplication::translate("PilotRecordLogBook", "Registration", Q_NULLPTR));
        label_5->setText(QApplication::translate("PilotRecordLogBook", "Remarks", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("PilotRecordLogBook", "Flight Information", Q_NULLPTR));
        groupBox_3->setTitle(QApplication::translate("PilotRecordLogBook", "Cross Country", Q_NULLPTR));
        label_32->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_11->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_12->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        label_31->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        label_33->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_34->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("PilotRecordLogBook", "Multi-engine", Q_NULLPTR));
        label_21->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        label_9->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_22->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_28->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_10->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        label_27->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        label_23->setText(QApplication::translate("PilotRecordLogBook", "SIC", Q_NULLPTR));
        label_24->setText(QApplication::translate("PilotRecordLogBook", "SIC", Q_NULLPTR));
        groupBox_5->setTitle(QApplication::translate("PilotRecordLogBook", "Takeoffs and Landings", Q_NULLPTR));
        label_39->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_40->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        groupBox_4->setTitle(QApplication::translate("PilotRecordLogBook", "Instrument", Q_NULLPTR));
        label_35->setText(QApplication::translate("PilotRecordLogBook", "Actual", Q_NULLPTR));
        label_36->setText(QApplication::translate("PilotRecordLogBook", "Hood", Q_NULLPTR));
        label_38->setText(QApplication::translate("PilotRecordLogBook", "# IFR approaches", Q_NULLPTR));
        label_37->setText(QApplication::translate("PilotRecordLogBook", "SIM.", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("PilotRecordLogBook", "Single Engine", Q_NULLPTR));
        label_14->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_8->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        label_16->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        label_7->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_15->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        label_13->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("PilotRecordLogBook", "Hours Information", Q_NULLPTR));
        groupBox_11->setTitle(QApplication::translate("PilotRecordLogBook", "Remarks", Q_NULLPTR));
        T_Remarks->setPlainText(QString());
        groupBox_12->setTitle(QApplication::translate("PilotRecordLogBook", "Flight Info", Q_NULLPTR));
        label_26->setText(QApplication::translate("PilotRecordLogBook", "Pilot's Name", Q_NULLPTR));
        label_29->setText(QApplication::translate("PilotRecordLogBook", "Aircraft Type", Q_NULLPTR));
        label_30->setText(QApplication::translate("PilotRecordLogBook", "Aircraft Registration", Q_NULLPTR));
        T_ACreg->setText(QApplication::translate("PilotRecordLogBook", "TextLabel", Q_NULLPTR));
        T_ACtype->setText(QApplication::translate("PilotRecordLogBook", "TextLabel", Q_NULLPTR));
        label_25->setText(QApplication::translate("PilotRecordLogBook", "Passenger", Q_NULLPTR));
        T_PICname->setText(QApplication::translate("PilotRecordLogBook", "TextLabel", Q_NULLPTR));
        T_PAXname->setText(QApplication::translate("PilotRecordLogBook", "TextLabel", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("PilotRecordLogBook", "Flight Information", Q_NULLPTR));
        groupBox_6->setTitle(QApplication::translate("PilotRecordLogBook", "Single Engine", Q_NULLPTR));
        label_18->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        label_17->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_19->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_20->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        T_SE_DAY_DUAL->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_SE_NIGHT_DUAL->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_SE_DAY_PIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_SE_NIGHT_PIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        groupBox_8->setTitle(QApplication::translate("PilotRecordLogBook", "Multi Engine", Q_NULLPTR));
        T_ME_NIGHT_DUAL->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        label_52->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_56->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        T_ME_DAY_DUAL->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        label_57->setText(QApplication::translate("PilotRecordLogBook", "SIC", Q_NULLPTR));
        T_ME_NIGHT_SIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        label_53->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        label_49->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        T_ME_DAY_PIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_ME_NIGHT_PIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_ME_DAY_SIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        groupBox_7->setTitle(QApplication::translate("PilotRecordLogBook", "Cross Country", Q_NULLPTR));
        T_XCUN_DAY_DUAL->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        label_48->setText(QApplication::translate("PilotRecordLogBook", "Dual", Q_NULLPTR));
        label_45->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        label_43->setText(QApplication::translate("PilotRecordLogBook", "PIC", Q_NULLPTR));
        label_41->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        T_XCUN_NIGHT_DUAL->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_XCUN_DAY_PIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_XCUN_NIGHT_PIC->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        groupBox_9->setTitle(QApplication::translate("PilotRecordLogBook", "Instrument", Q_NULLPTR));
        label_63->setText(QApplication::translate("PilotRecordLogBook", "Actual", Q_NULLPTR));
        label_62->setText(QApplication::translate("PilotRecordLogBook", "Hood", Q_NULLPTR));
        T_IFR_APPR->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_SIM_TIME->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_HOOD_TIME->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        T_ACTUAL_TIME->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        label_61->setText(QApplication::translate("PilotRecordLogBook", "Sim", Q_NULLPTR));
        label_60->setText(QApplication::translate("PilotRecordLogBook", "# IFR Approaches", Q_NULLPTR));
        groupBox_10->setTitle(QApplication::translate("PilotRecordLogBook", "Takeoffs and Landings", Q_NULLPTR));
        label_70->setText(QApplication::translate("PilotRecordLogBook", "Day", Q_NULLPTR));
        T_TO_LAND_NIGHT->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        label_68->setText(QApplication::translate("PilotRecordLogBook", "Night", Q_NULLPTR));
        T_TO_LAND_DAY->setText(QApplication::translate("PilotRecordLogBook", "0", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("PilotRecordLogBook", "Summary", Q_NULLPTR));
        label_42->setText(QApplication::translate("PilotRecordLogBook", "Double click to compare current version.", Q_NULLPTR));
        QTreeWidgetItem *___qtreewidgetitem = previousList->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("PilotRecordLogBook", "Remarks", Q_NULLPTR));
        ___qtreewidgetitem->setText(0, QApplication::translate("PilotRecordLogBook", "Modified Date", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("PilotRecordLogBook", "Previous Versions", Q_NULLPTR));
        okayButton->setText(QApplication::translate("PilotRecordLogBook", "Okay", Q_NULLPTR));
        cancelButton->setText(QApplication::translate("PilotRecordLogBook", "Cancel", Q_NULLPTR));
        editEntry->setText(QApplication::translate("PilotRecordLogBook", "Edit Entry", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PilotRecordLogBook: public Ui_PilotRecordLogBook {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PILOTRECORDLOGBOOK_H
