/********************************************************************************
** Form generated from reading UI file 'DOFRiskAssesment.ui'
**
** Created by: Qt User Interface Compiler version 5.9.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOFRISKASSESMENT_H
#define UI_DOFRISKASSESMENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_DOFRiskAssesment
{
public:
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_7;
    QPushButton *pushButton;

    void setupUi(QDialog *DOFRiskAssesment)
    {
        if (DOFRiskAssesment->objectName().isEmpty())
            DOFRiskAssesment->setObjectName(QStringLiteral("DOFRiskAssesment"));
        DOFRiskAssesment->resize(682, 363);
        label = new QLabel(DOFRiskAssesment);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 681, 371));
        label->setMinimumSize(QSize(681, 371));
        label->setMaximumSize(QSize(681, 371));
        label->setPixmap(QPixmap(QString::fromUtf8(":/PilotRecord/RiskAssesment.png")));
        label->setScaledContents(true);
        lineEdit = new QLineEdit(DOFRiskAssesment);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(20, 190, 51, 20));
        lineEdit_2 = new QLineEdit(DOFRiskAssesment);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(20, 210, 51, 20));
        lineEdit_3 = new QLineEdit(DOFRiskAssesment);
        lineEdit_3->setObjectName(QStringLiteral("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(20, 230, 51, 20));
        lineEdit_4 = new QLineEdit(DOFRiskAssesment);
        lineEdit_4->setObjectName(QStringLiteral("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(20, 250, 51, 20));
        lineEdit_5 = new QLineEdit(DOFRiskAssesment);
        lineEdit_5->setObjectName(QStringLiteral("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(20, 270, 51, 20));
        lineEdit_6 = new QLineEdit(DOFRiskAssesment);
        lineEdit_6->setObjectName(QStringLiteral("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(20, 290, 51, 20));
        lineEdit_7 = new QLineEdit(DOFRiskAssesment);
        lineEdit_7->setObjectName(QStringLiteral("lineEdit_7"));
        lineEdit_7->setGeometry(QRect(20, 310, 51, 20));
        pushButton = new QPushButton(DOFRiskAssesment);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(100, 240, 75, 23));

        retranslateUi(DOFRiskAssesment);

        QMetaObject::connectSlotsByName(DOFRiskAssesment);
    } // setupUi

    void retranslateUi(QDialog *DOFRiskAssesment)
    {
        DOFRiskAssesment->setWindowTitle(QApplication::translate("DOFRiskAssesment", "DOFRiskAssesment", Q_NULLPTR));
        label->setText(QString());
        pushButton->setText(QApplication::translate("DOFRiskAssesment", "OK", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DOFRiskAssesment: public Ui_DOFRiskAssesment {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOFRISKASSESMENT_H
